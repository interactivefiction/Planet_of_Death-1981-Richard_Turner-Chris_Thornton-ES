# The PLanet of Death
© 1981 by Richard Turner and Chris Thornton

2009 Adaptación libre por Mastodon

http://www.caad.es/fichas/aventura-planet-death.html

Genre: Science Fiction

YUID: 4qfi5hjkux9o409f

https://ifdb.org/viewgame?id=4qfi5hjkux9o409f

## Info

Tools: The Inker, ngPAWS
- N Game fully tested?
- Y Missing images?
- N/A Errors rendering images?
- Y Modern code for ngPAWS?
- 1 Edit txp (1) or sce (2)

## Notes

- txp to sce conversion messes up with accented characters, use sce file

## Test ngPAWS port

Testing: https://interactivefiction.gitlab.io/Planet_of_Death-1981-Richard_Turner-Chris_Thornton-ES/
Report issues: https://gitlab.com/interactivefiction/Planet_of_Death-1981-Richard_Turner-Chris_Thornton-ES/-/issues
