// This file is (C) Carlos Sanchez 2014, released under the MIT license


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES AND CONSTANTS ///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// CONSTANTS 
var VOCABULARY_ID = 0;
var VOCABULARY_WORD = 1;
var VOCABULARY_TYPE = 2;

var WORDTYPE_VERB = 0;
var WORDTYPE_NOUN = 1
var WORDTYPE_ADJECT = 2;
var WORDTYPE_ADVERB = 3;
var WORDTYPE_PRONOUN = 4;
var WORDTYPE_CONJUNCTION = 5;
var WORDTYPE_PREPOSITION = 6;

var TIMER_MILLISECONDS  = 40;

var RESOURCE_TYPE_IMG = 1;
var RESOURCE_TYPE_SND = 2;

var PROCESS_RESPONSE = 0;
var PROCESS_DESCRIPTION = 1;
var PROCESS_TURN = 2;

var DIV_TEXT_SCROLL_STEP = 40;


// Aux
var SET_VALUE = 255; // Value assigned by SET condact
var EMPTY_WORD = 255; // Value for word types when no match is found (as for  sentences without adjective or name)
var MAX_WORD_LENGHT = 10;  // Number of characters considered per word
var FLAG_COUNT = 256;  // Number of flags
var NUM_CONNECTION_VERBS = 14; // Number of verbs used as connection, from 0 to N - 1
var NUM_CONVERTIBLE_NOUNS = 20;
var NUM_PROPER_NOUNS = 50; // Number of proper nouns, can't be used as pronoun reference
var EMPTY_OBJECT = 255; // To remark there is no object when the action requires a objno parameter
var NO_EXIT = 255;  // If an exit does not exist, its value is this value
var MAX_CHANNELS = 17; // Number of SFX channels
var RESOURCES_DIR='dat/';


//Attributes
var ATTR_LIGHT=0;			// Object produces light
var ATTR_WEARABLE=1;		// Object is wearable
var ATTR_CONTAINER=2;       // Object is a container
var ATTR_NPC=3;             // Object is actually an NPC
var ATTR_CONCEALED = 4; /// Present but not visible
var ATTR_EDIBLE = 5;   /// Can be eaten
var ATTR_DRINKABLE=6;
var ATTR_ENTERABLE = 7;
var ATTR_FEMALE = 8;
var ATTR_LOCKABLE = 9;
var ATTR_LOCKED = 10;
var ATTR_MALE = 11;
var ATTR_NEUTER=12;
var ATTR_OPENABLE =13;
var ATTR_OPEN=14;
var ATTR_PLURALNAME = 15;
var ATTR_TRANSPARENT=16;
var ATTR_SCENERY=17;
var ATTR_SUPPORTER = 18;
var ATTR_SWITCHABLE=19;
var ATTR_ON  =20;
var ATTR_STATIC  =21;



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// INTERNAL STRINGS ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General messages & strings
var STR_NEWLINE = '<br />';
var STR_PROMPT_START = '<span class="feedback">&gt; ';
var STR_PROMPT_END = '</span>';
var STR_RAMSAVE_FILENAME = 'RAMSAVE_SAVEGAME';



// Runtime error messages
var STR_WRONG_SYSMESS = 'WARNING: System message requested does not exist.'; 
var STR_WRONG_LOCATION = 'WARNING: Location requested does not exist.'; 
var STR_WRONG_MESSAGE = 'WARNING: Message requested does not exist.'; 
var STR_WRONG_PROCESS = 'WARNING: Process requested does not exist.' 
var STR_RAMLOAD_ERROR= 'WARNING: You can\'t restore game as it has not yet been saved.'; 
var STR_RUNTIME_VERSION  = 'ngPAWS runtime (C) 2014 Carlos Sanchez.  Released under {URL|http://www.opensource.org/licenses/MIT| MIT license}.\nBuzz sound libray (C) Jay Salvat. Released under the {URL|http://www.opensource.org/licenses/MIT| MIT license} \n jQuery (C) jQuery Foundation. Released under the {URL|https://jquery.org/license/| MIT license}.';
var STR_TRANSCRIPT = 'To copy the transcript to your clipboard, press Ctrl+C, then press Enter';

var STR_INVALID_TAG_SEQUENCE = 'Invalid tag sequence: ';
var STR_INVALID_TAG_SEQUENCE_EMPTY = 'Invalid tag sequence.';
var STR_INVALID_TAG_SEQUENCE_BADPARAMS = 'Invalid tag sequence: bad parameters.';
var STR_INVALID_TAG_SEQUENCE_BADTAG = 'Invalid tag sequence: unknown tag.';
var STR_BADIE = 'You are using a very old version of Internet Explorer. Some features of this product won\'t be avaliable, and other may not work properly. For a better experience please upgrade your browser or install some other one like Firefox, Chrome or Opera.\n\nIt\'s up to you to continue but be warned your experience may be affected.';
var STR_INVALID_OBJECT = 'WARNING: Trying to access object that does not exist'


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////     FLAGS     ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


var FLAG_LIGHT = 0;
var FLAG_OBJECTS_CARRIED_COUNT = 1;
var FLAG_AUTODEC2 = 2; 
var FLAG_AUTODEC3 = 3;
var FLAG_AUTODEC4 = 4;
var FLAG_AUTODEC5 = 5;
var FLAG_AUTODEC6 = 6;
var FLAG_AUTODEC7 = 7;
var FLAG_AUTODEC8 = 8;
var FLAG_AUTODEC9 = 9;
var FLAG_AUTODEC10 = 10;
var FLAG_ESCAPE = 11;
var FLAG_PARSER_SETTINGS = 12;
var FLAG_PICTURE_SETTINGS = 29
var FLAG_SCORE = 30;
var FLAG_TURNS_LOW = 31;
var FLAG_TURNS_HIGH = 32;
var FLAG_VERB = 33;
var FLAG_NOUN1 =34;
var FLAG_ADJECT1 = 35;
var FLAG_ADVERB = 36;
var FLAG_MAXOBJECTS_CARRIED = 37;
var FLAG_LOCATION = 38;
var FLAG_TOPLINE = 39;   // deprecated
var FLAG_MODE = 40;  // deprecated
var FLAG_PROTECT = 41;   // deprecated
var FLAG_PROMPT = 42; 
var FLAG_PREP = 43;
var FLAG_NOUN2 = 44;
var FLAG_ADJECT2 = 45;
var FLAG_PRONOUN = 46;
var FLAG_PRONOUN_ADJECT = 47;
var FLAG_TIMEOUT_LENGTH = 48;
var FLAG_TIMEOUT_SETTINGS = 49; 
var FLAG_DOALL_LOC = 50;
var FLAG_REFERRED_OBJECT = 51;
var FLAG_MAXWEIGHT_CARRIED = 52;
var FLAG_OBJECT_LIST_FORMAT = 53;
var FLAG_REFERRED_OBJECT_LOCATION = 54;
var FLAG_REFERRED_OBJECT_WEIGHT = 55;
var FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES = 56;
var FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES = 57;
var FLAG_EXPANSION1 = 58;
var FLAG_EXPANSION2 = 59;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// SPECIAL LOCATIONS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var LOCATION_WORN = 253;
var LOCATION_CARRIED = 254;
var LOCATION_NONCREATED = 252;
var LOCATION_HERE = 255;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////  SYSTEM MESSAGES  ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



var SYSMESS_ISDARK = 0;
var SYSMESS_YOUCANSEE = 1;
var SYSMESS_PROMPT0 = 2;
var SYSMESS_PROMPT1 = 3;
var SYSMESS_PROMPT2 = 4
var SYSMESS_PROMPT3= 5;
var SYSMESS_IDONTUNDERSTAND = 6;
var SYSMESS_WRONGDIRECTION = 7
var SYSMESS_CANTDOTHAT = 8;
var SYSMESS_YOUARECARRYING = 9;
var SYSMESS_WORN = 10;
var SYSMESS_CARRYING_NOTHING = 11;
var SYSMESS_AREYOUSURE = 12;
var SYSMESS_PLAYAGAIN = 13;
var SYSMESS_FAREWELL = 14;
var SYSMESS_OK = 15;
var SYSMESS_PRESSANYKEY = 16;
var SYSMESS_TURNS_START = 17;
var SYSMESS_TURNS_CONTINUE = 18;
var SYSMESS_TURNS_PLURAL = 19;
var SYSMESS_TURNS_END = 20;
var SYSMESS_SCORE_START= 21;
var SYSMESS_SCORE_END =22;
var SYSMESS_YOURENOTWEARINGTHAT = 23;
var SYSMESS_YOUAREALREADYWEARINGTHAT = 24;
var SYSMESS_YOUALREADYHAVEOBJECT = 25;
var SYSMESS_CANTSEETHAT = 26;
var SYSMESS_CANTCARRYANYMORE = 27;
var SYSMESS_YOUDONTHAVETHAT = 28;
var SYSMESS_YOUAREALREADYWAERINGOBJECT = 29;
var SYSMESS_YES = 30;
var SYSMESS_NO = 31;
var SYSMESS_MORE = 32;
var SYSMESS_CARET = 33;
var SYSMESS_TIMEOUT=35;
var SYSMESS_YOUTAKEOBJECT = 36;
var SYSMESS_YOUWEAROBJECT = 37;
var SYSMESS_YOUREMOVEOBJECT = 38;
var SYSMESS_YOUDROPOBJECT = 39;
var SYSMESS_YOUCANTWEAROBJECT = 40;
var SYSMESS_YOUCANTREMOVEOBJECT = 41;
var SYSMESS_CANTREMOVE_TOOMANYOBJECTS = 42;
var SYSMESS_WEIGHSTOOMUCH = 43;
var SYSMESS_YOUPUTOBJECTIN = 44;
var SYSMESS_YOUCANTTAKEOBJECTOUTOF = 45;
var SYSMESS_LISTSEPARATOR = 46;
var SYSMESS_LISTLASTSEPARATOR = 47;
var SYSMESS_LISTEND = 48;
var SYSMESS_YOUDONTHAVEOBJECT = 49;
var SYSMESS_YOUARENOTWEARINGOBJECT = 50;
var SYSMESS_PUTINTAKEOUTTERMINATION = 51;
var SYSMESS_THATISNOTIN = 52;
var SYSMESS_EMPTYOBJECTLIST = 53;
var SYSMESS_FILENOTFOUND = 54;
var SYSMESS_CORRUPTFILE = 55;
var SYSMESS_IOFAILURE = 56;
var SYSMESS_DIRECTORYFULL = 57;
var SYSMESS_LOADFILE = 58;
var SYSMESS_FILENOTFOUND = 59;
var SYSMESS_SAVEFILE = 60;
var SYSMESS_SORRY = 61;
var SYSMESS_NONSENSE_SENTENCE = 62;
var SYSMESS_NPCLISTSTART = 63;
var SYSMESS_NPCLISTCONTINUE = 64;
var SYSMESS_NPCLISTCONTINUE_PLURAL = 65;
var SYSMESS_INSIDE_YOUCANSEE = 66;
var SYSMESS_OVER_YOUCANSEE = 67;
var SYSMESS_YOUPUTOBJECTON = 68;
var SYSMESS_YOUCANTTAKEOBJECTFROM = 69;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// GLOBAL VARS //////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Parser vars
var last_player_orders = [];   // Store last player orders, to be able to restore it when pressing arrow up
var last_player_orders_pointer = 0;
var parser_word_found;
var player_order_buffer = '';
var player_order = ''; // Current player order
var previous_verb = EMPTY_WORD;
var previous_noun = EMPTY_WORD;
var previous_adject = EMPTY_WORD;
var pronoun_suffixes = [];


//Settings
var graphicsON = true; 
var soundsON = true; 
var interruptDisabled = false;
var showWarnings = true;

// waitkey commands callback function
var waitkey_callback_function = [];

//PAUSE
var inPause=false;
var pauseRemainingTime = 0;



// Transcript
var inTranscript = false;
var transcript = '';


// Block
var inBlock = false;
var unblock_process = null;


// END
var inEND = false;

//QUIT
var inQUIT = false;

//ANYKEY
var inAnykey = false;

//GETKEY
var inGetkey = false;
var getkey_return_flag = null;

// Status flags
var done_flag;
var describe_location_flag;
var in_response;
var success;

// doall control
var doall_flag;
var process_in_doall;
var entry_for_doall	= '';
var current_process;


var timeout_progress = 0;
var ramsave_value = null;
var num_objects;


// The flags
var flags = new Array();


// The sound channels
var soundChannels = [];
var soundLoopCount = [];

//The last free object attribute
var nextFreeAttr = 22;

//Autocomplete array
var autocomplete = new Array();
var autocompleteStep = 0;
var autocompleteBaseWord = '';
// PROCESSES

interruptProcessExists = false;

function pro000()
{
process_restart=true;
pro000_restart: while(process_restart)
{
	process_restart=false;
	// _ TODO
	p000e0000:
	{
 		if (skipdoall('p000e0000')) break p000e0000;
 		if (in_response)
		{
			if (!CNDnoun1(51)) break p000e0000;
 		}
		if (!CNDnoteq(33,50)) break p000e0000;
		if (!CNDnoteq(33,44)) break p000e0000;
 		ACCsysmess(105);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ GUSANO
	p000e0001:
	{
 		if (skipdoall('p000e0001')) break p000e0001;
 		if (in_response)
		{
			if (!CNDnoun1(69)) break p000e0001;
 		}
		if (!CNDpresent(22)) break p000e0001;
 		ACCwhato();
		if (!CNDeq(54,23)) break p000e0001;
 		ACCwrite(0);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ CRIATURA
	p000e0002:
	{
 		if (skipdoall('p000e0002')) break p000e0002;
 		if (in_response)
		{
			if (!CNDnoun1(73)) break p000e0002;
 		}
		if (!CNDpresent(14)) break p000e0002;
		if (!CNDat(25)) break p000e0002;
 		ACCwrite(1);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ CRIATURA
	p000e0003:
	{
 		if (skipdoall('p000e0003')) break p000e0003;
 		if (in_response)
		{
			if (!CNDnoun1(73)) break p000e0003;
 		}
		if (!CNDpresent(22)) break p000e0003;
 		ACCwhato();
		if (!CNDeq(54,23)) break p000e0003;
 		ACCwrite(2);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ CONTROLES
	p000e0004:
	{
 		if (skipdoall('p000e0004')) break p000e0004;
 		if (in_response)
		{
			if (!CNDnoun1(80)) break p000e0004;
 		}
		if (!CNDpresent(21)) break p000e0004;
 		ACClet(34,81);
		{}

	}

	// _ BOTON
	p000e0005:
	{
 		if (skipdoall('p000e0005')) break p000e0005;
 		if (in_response)
		{
			if (!CNDnoun1(92)) break p000e0005;
 		}
		if (!CNDpresent(21)) break p000e0005;
		if (!CNDnoteq(44,9)) break p000e0005;
		if (!CNDnoteq(44,10)) break p000e0005;
		if (!CNDnoteq(44,13)) break p000e0005;
		if (!CNDnoteq(44,2)) break p000e0005;
		if (!CNDnoteq(44,3)) break p000e0005;
		if (!CNDnoteq(44,4)) break p000e0005;
 		ACCwrite(3);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ BOTON
	p000e0006:
	{
 		if (skipdoall('p000e0006')) break p000e0006;
 		if (in_response)
		{
			if (!CNDnoun1(92)) break p000e0006;
 		}
		if (!CNDpresent(21)) break p000e0006;
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
		{}

	}

	// _ BOTON
	p000e0007:
	{
 		if (skipdoall('p000e0007')) break p000e0007;
 		if (in_response)
		{
			if (!CNDnoun1(92)) break p000e0007;
 		}
		if (!CNDat(29)) break p000e0007;
		if (!CNDnoteq(44,93)) break p000e0007;
		if (!CNDnoteq(44,94)) break p000e0007;
 		ACCwrite(4);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ BOTON
	p000e0008:
	{
 		if (skipdoall('p000e0008')) break p000e0008;
 		if (in_response)
		{
			if (!CNDnoun1(92)) break p000e0008;
 		}
		if (!CNDat(29)) break p000e0008;
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
		{}

	}

	//  _
	p000e0009:
	{
 		if (skipdoall('p000e0009')) break p000e0009;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0009;
 		}
		if (!CNDat(14)) break p000e0009;
		if (!CNDnotworn(6)) break p000e0009;
 		ACCwrite(5);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	//  _
	p000e0010:
	{
 		if (skipdoall('p000e0010')) break p000e0010;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0010;
 		}
		if (!CNDat(14)) break p000e0010;
 		ACCwrite(6);
 		ACCnewline();
 		ACCgoto(15);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	//  _
	p000e0011:
	{
 		if (skipdoall('p000e0011')) break p000e0011;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0011;
 		}
		if (!CNDat(19)) break p000e0011;
 		ACCwrite(7);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	//  _
	p000e0012:
	{
 		if (skipdoall('p000e0012')) break p000e0012;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0012;
 		}
		if (!CNDat(27)) break p000e0012;
		if (!CNDzero(26)) break p000e0012;
 		ACCwrite(8);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	//  _
	p000e0013:
	{
 		if (skipdoall('p000e0013')) break p000e0013;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0013;
 		}
		if (!CNDat(27)) break p000e0013;
 		ACCgoto(28);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	//  _
	p000e0014:
	{
 		if (skipdoall('p000e0014')) break p000e0014;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0014;
 		}
		if (!CNDat(19)) break p000e0014;
		if (!CNDcarried(5)) break p000e0014;
 		ACCwrite(9);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	//  _
	p000e0015:
	{
 		if (skipdoall('p000e0015')) break p000e0015;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0015;
 		}
		if (!CNDat(19)) break p000e0015;
 		ACCwrite(10);
 		ACCwrite(11);
 		ACCnewline();
 		ACCgoto(18);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	//  _
	p000e0016:
	{
 		if (skipdoall('p000e0016')) break p000e0016;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0016;
 		}
		if (!CNDat(11)) break p000e0016;
 		ACCwrite(12);
 		ACCwrite(13);
 		ACCnewline();
 		ACCgoto(8);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	//  _
	p000e0017:
	{
 		if (skipdoall('p000e0017')) break p000e0017;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0017;
 		}
		if (!CNDat(14)) break p000e0017;
		if (!CNDzero(102)) break p000e0017;
 		ACCwrite(14);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	//  _
	p000e0018:
	{
 		if (skipdoall('p000e0018')) break p000e0018;
 		if (in_response)
		{
			if (!CNDverb(4)) break p000e0018;
 		}
		if (!CNDat(14)) break p000e0018;
 		ACCgoto(17);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	//  _
	p000e0019:
	{
 		if (skipdoall('p000e0019')) break p000e0019;
 		if (in_response)
		{
			if (!CNDverb(8)) break p000e0019;
 		}
		if (!CNDat(8)) break p000e0019;
 		ACCwrite(15);
 		ACCwrite(16);
 		ACCnewline();
 		ACCgoto(11);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SUBE ARBOL
	p000e0020:
	{
 		if (skipdoall('p000e0020')) break p000e0020;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0020;
			if (!CNDnoun1(53)) break p000e0020;
 		}
		if (!CNDat(11)) break p000e0020;
 		ACCwrite(17);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SUBE CUERDA
	p000e0021:
	{
 		if (skipdoall('p000e0021')) break p000e0021;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0021;
			if (!CNDnoun1(54)) break p000e0021;
 		}
		if (!CNDat(11)) break p000e0021;
 		ACCwrite(18);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SUBE BLOQUE
	p000e0022:
	{
 		if (skipdoall('p000e0022')) break p000e0022;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0022;
			if (!CNDnoun1(65)) break p000e0022;
 		}
		if (!CNDat(19)) break p000e0022;
		if (!CNDpresent(8)) break p000e0022;
 		ACCprocess(57);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SUBE _
	p000e0023:
	{
 		if (skipdoall('p000e0023')) break p000e0023;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0023;
 		}
		if (!CNDat(11)) break p000e0023;
 		ACCwrite(19);
 		ACCwrite(20);
 		ACCnewline();
 		ACCgoto(8);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SUBE _
	p000e0024:
	{
 		if (skipdoall('p000e0024')) break p000e0024;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0024;
 		}
		if (!CNDpresent(27)) break p000e0024;
 		ACCgoto(29);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// BAJA FOSA
	p000e0025:
	{
 		if (skipdoall('p000e0025')) break p000e0025;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0025;
			if (!CNDnoun1(56)) break p000e0025;
 		}
		if (!CNDat(12)) break p000e0025;
		if (!CNDnoun2(54)) break p000e0025;
		if (!CNDcarried(3)) break p000e0025;
		if (!CNDzero(85)) break p000e0025;
 		ACCwrite(21);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BAJA _
	p000e0026:
	{
 		if (skipdoall('p000e0026')) break p000e0026;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0026;
 		}
		if (!CNDat(8)) break p000e0026;
 		ACCwrite(22);
 		ACCwrite(23);
 		ACCnewline();
 		ACCgoto(11);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// BAJA _
	p000e0027:
	{
 		if (skipdoall('p000e0027')) break p000e0027;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0027;
 		}
		if (!CNDat(12)) break p000e0027;
		if (!CNDzero(85)) break p000e0027;
 		ACCwrite(24);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BAJA _
	p000e0028:
	{
 		if (skipdoall('p000e0028')) break p000e0028;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0028;
 		}
		if (!CNDat(12)) break p000e0028;
		if (!CNDcarried(5)) break p000e0028;
 		ACCwrite(25);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BAJA _
	p000e0029:
	{
 		if (skipdoall('p000e0029')) break p000e0029;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0029;
 		}
		if (!CNDat(12)) break p000e0029;
 		ACCclear(65);
 		ACCclear(61);
 		ACCwrite(26);
 		ACCnewline();
 		ACCgoto(13);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// BAJA _
	p000e0030:
	{
 		if (skipdoall('p000e0030')) break p000e0030;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0030;
 		}
		if (!CNDat(13)) break p000e0030;
 		ACCwrite(27);
 		ACCprocess(86);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BAJA _
	p000e0031:
	{
 		if (skipdoall('p000e0031')) break p000e0031;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0031;
 		}
		if (!CNDat(26)) break p000e0031;
 		ACCwrite(28);
 		ACCnewline();
 		ACCgoto(12);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// BAJA _
	p000e0032:
	{
 		if (skipdoall('p000e0032')) break p000e0032;
 		if (in_response)
		{
			if (!CNDverb(10)) break p000e0032;
 		}
		if (!CNDnotzero(84)) break p000e0032;
 		ACCwrite(29);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTRA FOSA
	p000e0033:
	{
 		if (skipdoall('p000e0033')) break p000e0033;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0033;
			if (!CNDnoun1(56)) break p000e0033;
 		}
		if (!CNDat(12)) break p000e0033;
		if (!CNDnoun2(54)) break p000e0033;
		if (!CNDcarried(3)) break p000e0033;
		if (!CNDzero(85)) break p000e0033;
 		ACCwrite(30);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTRA LAGO
	p000e0034:
	{
 		if (skipdoall('p000e0034')) break p000e0034;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0034;
			if (!CNDnoun1(57)) break p000e0034;
 		}
		if (!CNDat(14)) break p000e0034;
		if (!CNDnotworn(6)) break p000e0034;
 		ACCwrite(31);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTRA LAGO
	p000e0035:
	{
 		if (skipdoall('p000e0035')) break p000e0035;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0035;
			if (!CNDnoun1(57)) break p000e0035;
 		}
		if (!CNDat(14)) break p000e0035;
 		ACCwrite(32);
 		ACCnewline();
 		ACCgoto(15);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTRA BLOQUE
	p000e0036:
	{
 		if (skipdoall('p000e0036')) break p000e0036;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0036;
			if (!CNDnoun1(65)) break p000e0036;
 		}
		if (!CNDat(19)) break p000e0036;
		if (!CNDpresent(8)) break p000e0036;
 		ACCprocess(57);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTRA SUCCIONADO
	p000e0037:
	{
 		if (skipdoall('p000e0037')) break p000e0037;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0037;
			if (!CNDnoun1(85)) break p000e0037;
 		}
		if (!CNDpresent(25)) break p000e0037;
		if (!CNDisnotat(11,25)) break p000e0037;
 		ACCwrite(33);
 		ACCnewline();
 		ACCgoto(25);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTRA SUCCIONADO
	p000e0038:
	{
 		if (skipdoall('p000e0038')) break p000e0038;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0038;
			if (!CNDnoun1(85)) break p000e0038;
 		}
		if (!CNDpresent(25)) break p000e0038;
 		ACCwrite(34);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTRA NAVE
	p000e0039:
	{
 		if (skipdoall('p000e0039')) break p000e0039;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0039;
			if (!CNDnoun1(87)) break p000e0039;
 		}
		if (!CNDpresent(27)) break p000e0039;
 		ACCgoto(29);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ENTRA _
	p000e0040:
	{
 		if (skipdoall('p000e0040')) break p000e0040;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0040;
 		}
		if (!CNDat(12)) break p000e0040;
		if (!CNDzero(85)) break p000e0040;
 		ACCwrite(35);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTRA _
	p000e0041:
	{
 		if (skipdoall('p000e0041')) break p000e0041;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0041;
 		}
		if (!CNDat(12)) break p000e0041;
		if (!CNDcarried(5)) break p000e0041;
 		ACCwrite(36);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENTRA _
	p000e0042:
	{
 		if (skipdoall('p000e0042')) break p000e0042;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0042;
 		}
		if (!CNDat(12)) break p000e0042;
 		ACCclear(65);
 		ACCclear(61);
 		ACCwrite(37);
 		ACCnewline();
 		ACCgoto(13);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SAL _
	p000e0043:
	{
 		if (skipdoall('p000e0043')) break p000e0043;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0043;
 		}
		if (!CNDat(26)) break p000e0043;
 		ACCwrite(38);
 		ACCnewline();
 		ACCgoto(12);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SAL _
	p000e0044:
	{
 		if (skipdoall('p000e0044')) break p000e0044;
 		if (in_response)
		{
			if (!CNDverb(12)) break p000e0044;
 		}
		if (!CNDnotzero(84)) break p000e0044;
 		ACCwrite(39);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	//  _
	p000e0045:
	{
 		if (skipdoall('p000e0045')) break p000e0045;
 		if (in_response)
		{
			if (!CNDverb(13)) break p000e0045;
 		}
		if (!CNDat(11)) break p000e0045;
 		ACCwrite(40);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	//  _
	p000e0046:
	{
 		if (skipdoall('p000e0046')) break p000e0046;
 		if (in_response)
		{
			if (!CNDverb(13)) break p000e0046;
 		}
		if (!CNDat(18)) break p000e0046;
		if (!CNDcarried(5)) break p000e0046;
 		ACCwrite(41);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	//  _
	p000e0047:
	{
 		if (skipdoall('p000e0047')) break p000e0047;
 		if (in_response)
		{
			if (!CNDverb(13)) break p000e0047;
 		}
		if (!CNDat(18)) break p000e0047;
 		ACCwrite(42);
 		ACCwrite(43);
 		ACCnewline();
 		ACCgoto(19);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	//  _
	p000e0048:
	{
 		if (skipdoall('p000e0048')) break p000e0048;
 		if (in_response)
		{
			if (!CNDverb(14)) break p000e0048;
 		}
 		ACCprocess(46);
 		ACCdone();
		break pro000_restart;
		{}

	}

	//  _
	p000e0049:
	{
 		if (skipdoall('p000e0049')) break p000e0049;
 		if (in_response)
		{
			if (!CNDverb(14)) break p000e0049;
 		}
 		ACCsysmess(9);
 		ACClistat(254);
 		ACCsysmess(10);
 		ACClistat(253);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA TABLON
	p000e0050:
	{
 		if (skipdoall('p000e0050')) break p000e0050;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0050;
			if (!CNDnoun1(60)) break p000e0050;
 		}
		if (!CNDat(16)) break p000e0050;
		if (!CNDzero(101)) break p000e0050;
 		ACCset(101);
 		ACCplace(5,254);
 		ACCplus(30,1);
 		ACCwrite(44);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA TABLON
	p000e0051:
	{
 		if (skipdoall('p000e0051')) break p000e0051;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0051;
			if (!CNDnoun1(60)) break p000e0051;
 		}
		if (!CNDat(16)) break p000e0051;
		if (!CNDabsent(5)) break p000e0051;
 		ACCwrite(45);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA LIQUIDO
	p000e0052:
	{
 		if (skipdoall('p000e0052')) break p000e0052;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0052;
			if (!CNDnoun1(70)) break p000e0052;
 		}
		if (!CNDcarried(12)) break p000e0052;
 		ACClet(33,250);
		{}

	}

	// ARRANCA BABOSA
	p000e0053:
	{
 		if (skipdoall('p000e0053')) break p000e0053;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0053;
			if (!CNDnoun1(75)) break p000e0053;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0053;
 		ACCwrite(46);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// ARRANCA BARROTE
	p000e0054:
	{
 		if (skipdoall('p000e0054')) break p000e0054;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0054;
			if (!CNDnoun1(84)) break p000e0054;
 		}
		if (!CNDat(24)) break p000e0054;
		if (!CNDzero(100)) break p000e0054;
 		ACCwrite(47);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA BARROTE
	p000e0055:
	{
 		if (skipdoall('p000e0055')) break p000e0055;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0055;
			if (!CNDnoun1(84)) break p000e0055;
 		}
		if (!CNDabsent(24)) break p000e0055;
		if (!CNDat(24)) break p000e0055;
 		ACCwrite(48);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA TECNICO
	p000e0056:
	{
 		if (skipdoall('p000e0056')) break p000e0056;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0056;
			if (!CNDnoun1(89)) break p000e0056;
 		}
		if (!CNDpresent(28)) break p000e0056;
		if (!CNDzero(66)) break p000e0056;
 		ACCwrite(49);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// ARRANCA TECNICO
	p000e0057:
	{
 		if (skipdoall('p000e0057')) break p000e0057;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0057;
			if (!CNDnoun1(89)) break p000e0057;
 		}
		if (!CNDpresent(28)) break p000e0057;
 		ACCwrite(50);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA PALANCA
	p000e0058:
	{
 		if (skipdoall('p000e0058')) break p000e0058;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0058;
			if (!CNDnoun1(95)) break p000e0058;
 		}
		if (!CNDpresent(33)) break p000e0058;
		if (!CNDisnotat(24,9)) break p000e0058;
 		ACCwrite(51);
 		ACCwrite(52);
 		ACCwrite(53);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA PALANCA
	p000e0059:
	{
 		if (skipdoall('p000e0059')) break p000e0059;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0059;
			if (!CNDnoun1(95)) break p000e0059;
 		}
		if (!CNDpresent(33)) break p000e0059;
		if (!CNDnotzero(103)) break p000e0059;
 		ACCwrite(54);
 		ACCwrite(55);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA PALANCA
	p000e0060:
	{
 		if (skipdoall('p000e0060')) break p000e0060;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0060;
			if (!CNDnoun1(95)) break p000e0060;
 		}
		if (!CNDpresent(33)) break p000e0060;
 		ACCset(103);
 		ACCplus(30,3);
 		ACCwrite(56);
 		ACCwrite(57);
 		ACCwrite(58);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA _
	p000e0061:
	{
 		if (skipdoall('p000e0061')) break p000e0061;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0061;
 		}
		if (!CNDat(8)) break p000e0061;
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0061;
		if (!CNDnoun2(50)) break p000e0061;
 		ACCwrite(59);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA _
	p000e0062:
	{
 		if (skipdoall('p000e0062')) break p000e0062;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0062;
 		}
		if (!CNDat(12)) break p000e0062;
		if (!CNDnoun2(56)) break p000e0062;
 		ACCprocess(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA _
	p000e0063:
	{
 		if (skipdoall('p000e0063')) break p000e0063;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0063;
 		}
		if (!CNDat(14)) break p000e0063;
		if (!CNDnoun2(57)) break p000e0063;
 		ACCwrite(60);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA _
	p000e0064:
	{
 		if (skipdoall('p000e0064')) break p000e0064;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0064;
 		}
		if (!CNDnoun2(75)) break p000e0064;
 		ACCwhato();
		if (!CNDeq(54,254)) break p000e0064;
 		ACClet(44,255);
 		ACCcopyff(38,67);
 		ACCprocess(42);
 		ACClet(44,75);
		if (!CNDnotzero(69)) break p000e0064;
 		ACCwrite(61);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA _
	p000e0065:
	{
 		if (skipdoall('p000e0065')) break p000e0065;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0065;
 		}
		if (!CNDnoun2(85)) break p000e0065;
 		ACCprocess(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA _
	p000e0066:
	{
 		if (skipdoall('p000e0066')) break p000e0066;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0066;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0066;
		if (!CNDprep(3)) break p000e0066;
 		ACCprocess(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA _
	p000e0067:
	{
 		if (skipdoall('p000e0067')) break p000e0067;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0067;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0067;
		if (!CNDprep(5)) break p000e0067;
 		ACCprocess(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA _
	p000e0068:
	{
 		if (skipdoall('p000e0068')) break p000e0068;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0068;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0068;
 		ACCwrite(62);
 		ACCprocess(67);
 		ACCwrite(63);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARRANCA _
	p000e0069:
	{
 		if (skipdoall('p000e0069')) break p000e0069;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0069;
 		}
 		ACCwrite(64);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA LIQUIDO
	p000e0070:
	{
 		if (skipdoall('p000e0070')) break p000e0070;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0070;
			if (!CNDnoun1(70)) break p000e0070;
 		}
		if (!CNDcarried(12)) break p000e0070;
 		ACClet(33,250);
		{}

	}

	// ARROJA _
	p000e0071:
	{
 		if (skipdoall('p000e0071')) break p000e0071;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0071;
 		}
		if (!CNDat(8)) break p000e0071;
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0071;
		if (!CNDnoun2(50)) break p000e0071;
 		ACCwrite(65);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0072:
	{
 		if (skipdoall('p000e0072')) break p000e0072;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0072;
 		}
		if (!CNDat(14)) break p000e0072;
		if (!CNDnoun2(57)) break p000e0072;
 		ACCwrite(66);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0073:
	{
 		if (skipdoall('p000e0073')) break p000e0073;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0073;
 		}
		if (!CNDnoun2(75)) break p000e0073;
 		ACCwhato();
		if (!CNDeq(54,254)) break p000e0073;
 		ACClet(44,255);
 		ACCcopyff(38,67);
 		ACCprocess(42);
 		ACClet(44,75);
		if (!CNDnotzero(69)) break p000e0073;
 		ACCwrite(67);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ARROJA _
	p000e0074:
	{
 		if (skipdoall('p000e0074')) break p000e0074;
 		if (in_response)
		{
			if (!CNDverb(21)) break p000e0074;
 		}
 		ACCprocess(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// NADA RIACHUELO
	p000e0075:
	{
 		if (skipdoall('p000e0075')) break p000e0075;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0075;
			if (!CNDnoun1(58)) break p000e0075;
 		}
		if (!CNDat(14)) break p000e0075;
 		ACCwrite(68);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// NADA _
	p000e0076:
	{
 		if (skipdoall('p000e0076')) break p000e0076;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0076;
 		}
 		ACCwrite(69);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CRUZA RIACHUELO
	p000e0077:
	{
 		if (skipdoall('p000e0077')) break p000e0077;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0077;
			if (!CNDnoun1(58)) break p000e0077;
 		}
		if (!CNDat(14)) break p000e0077;
		if (!CNDzero(102)) break p000e0077;
 		ACCwrite(70);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CRUZA RIACHUELO
	p000e0078:
	{
 		if (skipdoall('p000e0078')) break p000e0078;
 		if (in_response)
		{
			if (!CNDverb(23)) break p000e0078;
			if (!CNDnoun1(58)) break p000e0078;
 		}
		if (!CNDat(14)) break p000e0078;
 		ACCgoto(17);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// SALTA RIACHUELO
	p000e0079:
	{
 		if (skipdoall('p000e0079')) break p000e0079;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0079;
			if (!CNDnoun1(58)) break p000e0079;
 		}
		if (!CNDat(14)) break p000e0079;
 		ACCwrite(71);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SALTA _
	p000e0080:
	{
 		if (skipdoall('p000e0080')) break p000e0080;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0080;
 		}
		if (!CNDat(13)) break p000e0080;
 		ACCwrite(72);
 		ACCprocess(86);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SALTA _
	p000e0081:
	{
 		if (skipdoall('p000e0081')) break p000e0081;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0081;
 		}
		if (!CNDat(24)) break p000e0081;
 		ACCprocess(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SALTA _
	p000e0082:
	{
 		if (skipdoall('p000e0082')) break p000e0082;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0082;
 		}
 		ACCwrite(73);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PUNTOS _
	p000e0083:
	{
 		if (skipdoall('p000e0083')) break p000e0083;
 		if (in_response)
		{
			if (!CNDverb(25)) break p000e0083;
 		}
 		ACCsysmess(21);
 		ACCprint(30);
 		ACCsysmess(22);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PATEA _
	p000e0084:
	{
 		if (skipdoall('p000e0084')) break p000e0084;
 		if (in_response)
		{
			if (!CNDverb(26)) break p000e0084;
 		}
		if (!CNDworn(6)) break p000e0084;
 		ACClet(33,27);
 		ACClet(43,2);
 		ACClet(44,61);
		{}

	}

	// PATEA _
	p000e0085:
	{
 		if (skipdoall('p000e0085')) break p000e0085;
 		if (in_response)
		{
			if (!CNDverb(26)) break p000e0085;
 		}
		if (!CNDnotworn(6)) break p000e0085;
 		ACClet(33,27);
 		ACClet(43,2);
 		ACClet(44,66);
		{}

	}

	// PATEA _
	p000e0086:
	{
 		if (skipdoall('p000e0086')) break p000e0086;
 		if (in_response)
		{
			if (!CNDverb(26)) break p000e0086;
 		}
 		ACCwrite(74);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA CUERDA
	p000e0087:
	{
 		if (skipdoall('p000e0087')) break p000e0087;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0087;
			if (!CNDnoun1(54)) break p000e0087;
 		}
		if (!CNDat(11)) break p000e0087;
 		ACClet(33,28);
		{}

	}

	// GOLPEA TABLON
	p000e0088:
	{
 		if (skipdoall('p000e0088')) break p000e0088;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0088;
			if (!CNDnoun1(60)) break p000e0088;
 		}
		if (!CNDat(16)) break p000e0088;
		if (!CNDzero(101)) break p000e0088;
 		ACCset(101);
 		ACCplace(5,254);
 		ACCplus(30,1);
 		ACCwrite(75);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA TABLON
	p000e0089:
	{
 		if (skipdoall('p000e0089')) break p000e0089;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0089;
			if (!CNDnoun1(60)) break p000e0089;
 		}
		if (!CNDat(16)) break p000e0089;
		if (!CNDabsent(5)) break p000e0089;
 		ACCwrite(76);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA BLOQUE
	p000e0090:
	{
 		if (skipdoall('p000e0090')) break p000e0090;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0090;
			if (!CNDnoun1(65)) break p000e0090;
 		}
		if (!CNDat(19)) break p000e0090;
		if (!CNDprep(2)) break p000e0090;
 		ACCprocess(82);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA BLOQUE
	p000e0091:
	{
 		if (skipdoall('p000e0091')) break p000e0091;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0091;
			if (!CNDnoun1(65)) break p000e0091;
 		}
		if (!CNDat(19)) break p000e0091;
 		ACCwrite(77);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA GUSANO
	p000e0092:
	{
 		if (skipdoall('p000e0092')) break p000e0092;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0092;
			if (!CNDnoun1(69)) break p000e0092;
 		}
		if (!CNDpresent(11)) break p000e0092;
 		ACCwrite(78);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA CRIATURA
	p000e0093:
	{
 		if (skipdoall('p000e0093')) break p000e0093;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0093;
			if (!CNDnoun1(73)) break p000e0093;
 		}
		if (!CNDpresent(14)) break p000e0093;
		if (!CNDzero(64)) break p000e0093;
 		ACCwrite(79);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA PUERTA
	p000e0094:
	{
 		if (skipdoall('p000e0094')) break p000e0094;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0094;
			if (!CNDnoun1(76)) break p000e0094;
 		}
		if (!CNDpresent(30)) break p000e0094;
 		ACCwrite(80);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA REJILLA
	p000e0095:
	{
 		if (skipdoall('p000e0095')) break p000e0095;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0095;
			if (!CNDnoun1(83)) break p000e0095;
 		}
		if (!CNDat(24)) break p000e0095;
		if (!CNDnoun2(66)) break p000e0095;
 		ACCprocess(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA REJILLA
	p000e0096:
	{
 		if (skipdoall('p000e0096')) break p000e0096;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0096;
			if (!CNDnoun1(83)) break p000e0096;
 		}
		if (!CNDat(24)) break p000e0096;
		if (!CNDworn(6)) break p000e0096;
		if (!CNDnoun2(61)) break p000e0096;
 		ACCprocess(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA BARROTE
	p000e0097:
	{
 		if (skipdoall('p000e0097')) break p000e0097;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0097;
			if (!CNDnoun1(84)) break p000e0097;
 		}
		if (!CNDat(24)) break p000e0097;
		if (!CNDnoun2(66)) break p000e0097;
 		ACCprocess(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA BARROTE
	p000e0098:
	{
 		if (skipdoall('p000e0098')) break p000e0098;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0098;
			if (!CNDnoun1(84)) break p000e0098;
 		}
		if (!CNDat(24)) break p000e0098;
		if (!CNDworn(6)) break p000e0098;
		if (!CNDnoun2(61)) break p000e0098;
 		ACCprocess(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA BARROTE
	p000e0099:
	{
 		if (skipdoall('p000e0099')) break p000e0099;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0099;
			if (!CNDnoun1(84)) break p000e0099;
 		}
		if (!CNDat(24)) break p000e0099;
		if (!CNDzero(100)) break p000e0099;
 		ACCwrite(81);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA BARROTE
	p000e0100:
	{
 		if (skipdoall('p000e0100')) break p000e0100;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0100;
			if (!CNDnoun1(84)) break p000e0100;
 		}
		if (!CNDabsent(24)) break p000e0100;
		if (!CNDat(24)) break p000e0100;
 		ACCwrite(82);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA TECNICO
	p000e0101:
	{
 		if (skipdoall('p000e0101')) break p000e0101;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0101;
			if (!CNDnoun1(89)) break p000e0101;
 		}
		if (!CNDpresent(28)) break p000e0101;
 		ACCprocess(40);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GOLPEA _
	p000e0102:
	{
 		if (skipdoall('p000e0102')) break p000e0102;
 		if (in_response)
		{
			if (!CNDverb(27)) break p000e0102;
 		}
 		ACCwrite(83);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA CUERDA
	p000e0103:
	{
 		if (skipdoall('p000e0103')) break p000e0103;
 		if (in_response)
		{
			if (!CNDverb(28)) break p000e0103;
			if (!CNDnoun1(54)) break p000e0103;
 		}
		if (!CNDat(11)) break p000e0103;
		if (!CNDcarried(0)) break p000e0103;
		if (!CNDzero(86)) break p000e0103;
 		ACCset(86);
 		ACCplus(30,1);
 		ACCwrite(84);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA CUERDA
	p000e0104:
	{
 		if (skipdoall('p000e0104')) break p000e0104;
 		if (in_response)
		{
			if (!CNDverb(28)) break p000e0104;
			if (!CNDnoun1(54)) break p000e0104;
 		}
		if (!CNDat(11)) break p000e0104;
		if (!CNDcarried(0)) break p000e0104;
 		ACCwrite(85);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA CUERDA
	p000e0105:
	{
 		if (skipdoall('p000e0105')) break p000e0105;
 		if (in_response)
		{
			if (!CNDverb(28)) break p000e0105;
			if (!CNDnoun1(54)) break p000e0105;
 		}
		if (!CNDat(11)) break p000e0105;
 		ACCwrite(86);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0106:
	{
 		if (skipdoall('p000e0106')) break p000e0106;
 		if (in_response)
		{
			if (!CNDverb(28)) break p000e0106;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0106;
 		ACCwrite(87);
 		ACCprocess(64);
 		ACCwrite(88);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CORTA _
	p000e0107:
	{
 		if (skipdoall('p000e0107')) break p000e0107;
 		if (in_response)
		{
			if (!CNDverb(28)) break p000e0107;
 		}
 		ACCwrite(89);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA S
	p000e0108:
	{
 		if (skipdoall('p000e0108')) break p000e0108;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0108;
			if (!CNDnoun1(2)) break p000e0108;
 		}
		if (!CNDpresent(21)) break p000e0108;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA E
	p000e0109:
	{
 		if (skipdoall('p000e0109')) break p000e0109;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0109;
			if (!CNDnoun1(3)) break p000e0109;
 		}
		if (!CNDpresent(21)) break p000e0109;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA O
	p000e0110:
	{
 		if (skipdoall('p000e0110')) break p000e0110;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0110;
			if (!CNDnoun1(4)) break p000e0110;
 		}
		if (!CNDpresent(21)) break p000e0110;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA AR
	p000e0111:
	{
 		if (skipdoall('p000e0111')) break p000e0111;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0111;
			if (!CNDnoun1(9)) break p000e0111;
 		}
		if (!CNDpresent(21)) break p000e0111;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA AB
	p000e0112:
	{
 		if (skipdoall('p000e0112')) break p000e0112;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0112;
			if (!CNDnoun1(10)) break p000e0112;
 		}
		if (!CNDpresent(21)) break p000e0112;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA N
	p000e0113:
	{
 		if (skipdoall('p000e0113')) break p000e0113;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0113;
			if (!CNDnoun1(13)) break p000e0113;
 		}
		if (!CNDpresent(21)) break p000e0113;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA BLOQUE
	p000e0114:
	{
 		if (skipdoall('p000e0114')) break p000e0114;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0114;
			if (!CNDnoun1(65)) break p000e0114;
 		}
		if (!CNDat(19)) break p000e0114;
		if (!CNDpresent(8)) break p000e0114;
 		ACCdestroy(8);
 		ACCwrite(90);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA BLOQUE
	p000e0115:
	{
 		if (skipdoall('p000e0115')) break p000e0115;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0115;
			if (!CNDnoun1(65)) break p000e0115;
 		}
		if (!CNDat(19)) break p000e0115;
 		ACCwrite(91);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA GUSANO
	p000e0116:
	{
 		if (skipdoall('p000e0116')) break p000e0116;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0116;
			if (!CNDnoun1(69)) break p000e0116;
 		}
		if (!CNDpresent(25)) break p000e0116;
		if (!CNDisat(11,25)) break p000e0116;
 		ACCwrite(92);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA CRIATURA
	p000e0117:
	{
 		if (skipdoall('p000e0117')) break p000e0117;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0117;
			if (!CNDnoun1(73)) break p000e0117;
 		}
		if (!CNDpresent(14)) break p000e0117;
		if (!CNDnotat(25)) break p000e0117;
 		ACCwrite(93);
 		ACCwrite(94);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// EMPUJA BABOSA
	p000e0118:
	{
 		if (skipdoall('p000e0118')) break p000e0118;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0118;
			if (!CNDnoun1(75)) break p000e0118;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0118;
 		ACCwrite(95);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// EMPUJA PUERTA
	p000e0119:
	{
 		if (skipdoall('p000e0119')) break p000e0119;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0119;
			if (!CNDnoun1(76)) break p000e0119;
 		}
		if (!CNDpresent(30)) break p000e0119;
 		ACCwrite(96);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA BARROTE
	p000e0120:
	{
 		if (skipdoall('p000e0120')) break p000e0120;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0120;
			if (!CNDnoun1(84)) break p000e0120;
 		}
		if (!CNDat(24)) break p000e0120;
		if (!CNDzero(100)) break p000e0120;
 		ACCwrite(97);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA BARROTE
	p000e0121:
	{
 		if (skipdoall('p000e0121')) break p000e0121;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0121;
			if (!CNDnoun1(84)) break p000e0121;
 		}
		if (!CNDabsent(24)) break p000e0121;
		if (!CNDat(24)) break p000e0121;
 		ACCwrite(98);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA TECNICO
	p000e0122:
	{
 		if (skipdoall('p000e0122')) break p000e0122;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0122;
			if (!CNDnoun1(89)) break p000e0122;
 		}
		if (!CNDpresent(28)) break p000e0122;
		if (!CNDzero(66)) break p000e0122;
 		ACCwrite(99);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// EMPUJA TECNICO
	p000e0123:
	{
 		if (skipdoall('p000e0123')) break p000e0123;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0123;
			if (!CNDnoun1(89)) break p000e0123;
 		}
		if (!CNDpresent(28)) break p000e0123;
 		ACCwrite(100);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA INICIAR
	p000e0124:
	{
 		if (skipdoall('p000e0124')) break p000e0124;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0124;
			if (!CNDnoun1(93)) break p000e0124;
 		}
		if (!CNDat(29)) break p000e0124;
 		ACClet(33,30);
		{}

	}

	// EMPUJA IGNICION
	p000e0125:
	{
 		if (skipdoall('p000e0125')) break p000e0125;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0125;
			if (!CNDnoun1(94)) break p000e0125;
 		}
		if (!CNDat(29)) break p000e0125;
 		ACClet(33,30);
		{}

	}

	// EMPUJA PALANCA
	p000e0126:
	{
 		if (skipdoall('p000e0126')) break p000e0126;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0126;
			if (!CNDnoun1(95)) break p000e0126;
 		}
		if (!CNDpresent(33)) break p000e0126;
 		ACCwrite(101);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0127:
	{
 		if (skipdoall('p000e0127')) break p000e0127;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0127;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0127;
 		ACCwrite(102);
 		ACCprocess(64);
 		ACCwrite(103);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EMPUJA _
	p000e0128:
	{
 		if (skipdoall('p000e0128')) break p000e0128;
 		if (in_response)
		{
			if (!CNDverb(29)) break p000e0128;
 		}
 		ACCwrite(104);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR S
	p000e0129:
	{
 		if (skipdoall('p000e0129')) break p000e0129;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0129;
			if (!CNDnoun1(2)) break p000e0129;
 		}
		if (!CNDpresent(21)) break p000e0129;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR E
	p000e0130:
	{
 		if (skipdoall('p000e0130')) break p000e0130;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0130;
			if (!CNDnoun1(3)) break p000e0130;
 		}
		if (!CNDpresent(21)) break p000e0130;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR O
	p000e0131:
	{
 		if (skipdoall('p000e0131')) break p000e0131;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0131;
			if (!CNDnoun1(4)) break p000e0131;
 		}
		if (!CNDpresent(21)) break p000e0131;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR AR
	p000e0132:
	{
 		if (skipdoall('p000e0132')) break p000e0132;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0132;
			if (!CNDnoun1(9)) break p000e0132;
 		}
		if (!CNDpresent(21)) break p000e0132;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR AB
	p000e0133:
	{
 		if (skipdoall('p000e0133')) break p000e0133;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0133;
			if (!CNDnoun1(10)) break p000e0133;
 		}
		if (!CNDpresent(21)) break p000e0133;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR N
	p000e0134:
	{
 		if (skipdoall('p000e0134')) break p000e0134;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0134;
			if (!CNDnoun1(13)) break p000e0134;
 		}
		if (!CNDpresent(21)) break p000e0134;
 		ACCprocess(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR INICIAR
	p000e0135:
	{
 		if (skipdoall('p000e0135')) break p000e0135;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0135;
			if (!CNDnoun1(93)) break p000e0135;
 		}
		if (!CNDat(29)) break p000e0135;
 		ACCprocess(44);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR IGNICION
	p000e0136:
	{
 		if (skipdoall('p000e0136')) break p000e0136;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0136;
			if (!CNDnoun1(94)) break p000e0136;
 		}
		if (!CNDat(29)) break p000e0136;
		if (!CNDnotzero(84)) break p000e0136;
 		ACCcls();
 		ACCwrite(105);
 		ACCnewline();
 		ACCanykey();
 		function anykey00000() 
		{
 		ACCcls();
 		ACCwrite(106);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		return;
		}
 		waitKey(anykey00000);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// APRETAR IGNICION
	p000e0137:
	{
 		if (skipdoall('p000e0137')) break p000e0137;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0137;
			if (!CNDnoun1(94)) break p000e0137;
 		}
		if (!CNDat(29)) break p000e0137;
 		ACCwrite(107);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR _
	p000e0138:
	{
 		if (skipdoall('p000e0138')) break p000e0138;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0138;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0138;
 		ACCwrite(108);
 		ACCprocess(64);
 		ACCwrite(109);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// APRETAR _
	p000e0139:
	{
 		if (skipdoall('p000e0139')) break p000e0139;
 		if (in_response)
		{
			if (!CNDverb(30)) break p000e0139;
 		}
 		ACCwrite(110);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MONTA BLOQUE
	p000e0140:
	{
 		if (skipdoall('p000e0140')) break p000e0140;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0140;
			if (!CNDnoun1(65)) break p000e0140;
 		}
		if (!CNDat(19)) break p000e0140;
		if (!CNDpresent(8)) break p000e0140;
 		ACCprocess(57);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MONTA _
	p000e0141:
	{
 		if (skipdoall('p000e0141')) break p000e0141;
 		if (in_response)
		{
			if (!CNDverb(31)) break p000e0141;
 		}
 		ACCwrite(111);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCALA ARBOL
	p000e0142:
	{
 		if (skipdoall('p000e0142')) break p000e0142;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0142;
			if (!CNDnoun1(53)) break p000e0142;
 		}
		if (!CNDat(11)) break p000e0142;
 		ACCwrite(112);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCALA CUERDA
	p000e0143:
	{
 		if (skipdoall('p000e0143')) break p000e0143;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0143;
			if (!CNDnoun1(54)) break p000e0143;
 		}
		if (!CNDat(11)) break p000e0143;
 		ACCwrite(113);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCALA CUERDA
	p000e0144:
	{
 		if (skipdoall('p000e0144')) break p000e0144;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0144;
			if (!CNDnoun1(54)) break p000e0144;
 		}
		if (!CNDat(12)) break p000e0144;
		if (!CNDnotzero(85)) break p000e0144;
 		ACCclear(65);
 		ACCclear(61);
 		ACCwrite(114);
 		ACCnewline();
 		ACCgoto(13);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ESCALA CUERDA
	p000e0145:
	{
 		if (skipdoall('p000e0145')) break p000e0145;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0145;
			if (!CNDnoun1(54)) break p000e0145;
 		}
		if (!CNDat(12)) break p000e0145;
		if (!CNDcarried(3)) break p000e0145;
 		ACCwrite(115);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCALA FOSA
	p000e0146:
	{
 		if (skipdoall('p000e0146')) break p000e0146;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0146;
			if (!CNDnoun1(56)) break p000e0146;
 		}
		if (!CNDat(12)) break p000e0146;
		if (!CNDnotzero(85)) break p000e0146;
 		ACCclear(65);
 		ACCclear(61);
 		ACCwrite(116);
 		ACCnewline();
 		ACCgoto(13);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ESCALA FOSA
	p000e0147:
	{
 		if (skipdoall('p000e0147')) break p000e0147;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0147;
			if (!CNDnoun1(56)) break p000e0147;
 		}
		if (!CNDat(12)) break p000e0147;
		if (!CNDnoun2(54)) break p000e0147;
		if (!CNDcarried(3)) break p000e0147;
		if (!CNDzero(85)) break p000e0147;
 		ACCwrite(117);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCALA FOSA
	p000e0148:
	{
 		if (skipdoall('p000e0148')) break p000e0148;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0148;
			if (!CNDnoun1(56)) break p000e0148;
 		}
		if (!CNDat(12)) break p000e0148;
		if (!CNDzero(85)) break p000e0148;
 		ACCwrite(118);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCALA FOSA
	p000e0149:
	{
 		if (skipdoall('p000e0149')) break p000e0149;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0149;
			if (!CNDnoun1(56)) break p000e0149;
 		}
		if (!CNDat(12)) break p000e0149;
		if (!CNDcarried(5)) break p000e0149;
 		ACCwrite(119);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCALA FOSA
	p000e0150:
	{
 		if (skipdoall('p000e0150')) break p000e0150;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0150;
			if (!CNDnoun1(56)) break p000e0150;
 		}
		if (!CNDat(12)) break p000e0150;
 		ACCclear(65);
 		ACCclear(61);
 		ACCwrite(120);
 		ACCnewline();
 		ACCgoto(13);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ESCALA _
	p000e0151:
	{
 		if (skipdoall('p000e0151')) break p000e0151;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0151;
 		}
		if (!CNDat(13)) break p000e0151;
 		ACCgoto(12);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// ESCALA _
	p000e0152:
	{
 		if (skipdoall('p000e0152')) break p000e0152;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0152;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0152;
 		ACCwrite(121);
 		ACCprocess(64);
 		ACCwrite(122);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESCALA _
	p000e0153:
	{
 		if (skipdoall('p000e0153')) break p000e0153;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0153;
 		}
 		ACCwrite(123);
 		ACCwrite(124);
 		ACCwrite(125);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA CUERDA
	p000e0154:
	{
 		if (skipdoall('p000e0154')) break p000e0154;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0154;
			if (!CNDnoun1(54)) break p000e0154;
 		}
		if (!CNDat(12)) break p000e0154;
 		ACCprocess(9);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA _
	p000e0155:
	{
 		if (skipdoall('p000e0155')) break p000e0155;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0155;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0155;
 		ACCwrite(126);
 		ACCprocess(64);
 		ACCwrite(127);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ANUDA _
	p000e0156:
	{
 		if (skipdoall('p000e0156')) break p000e0156;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0156;
 		}
 		ACCwrite(128);
 		ACCwrite(129);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESANUDA CUERDA
	p000e0157:
	{
 		if (skipdoall('p000e0157')) break p000e0157;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0157;
			if (!CNDnoun1(54)) break p000e0157;
 		}
		if (!CNDat(12)) break p000e0157;
		if (!CNDnotzero(85)) break p000e0157;
 		ACCclear(85);
 		ACCplace(3,254);
 		ACCwrite(130);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESANUDA _
	p000e0158:
	{
 		if (skipdoall('p000e0158')) break p000e0158;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0158;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0158;
 		ACCwrite(131);
 		ACCprocess(64);
 		ACCwrite(132);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESANUDA _
	p000e0159:
	{
 		if (skipdoall('p000e0159')) break p000e0159;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0159;
 		}
 		ACCwrite(133);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA CUERDA
	p000e0160:
	{
 		if (skipdoall('p000e0160')) break p000e0160;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0160;
			if (!CNDnoun1(54)) break p000e0160;
 		}
		if (!CNDat(13)) break p000e0160;
 		ACCprocess(15);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0161:
	{
 		if (skipdoall('p000e0161')) break p000e0161;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0161;
 		}
		if (!CNDat(13)) break p000e0161;
		if (!CNDeq(34,255)) break p000e0161;
 		ACCprocess(15);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0162:
	{
 		if (skipdoall('p000e0162')) break p000e0162;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0162;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0162;
 		ACCwrite(134);
 		ACCprocess(64);
 		ACCwrite(135);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BALANCEA _
	p000e0163:
	{
 		if (skipdoall('p000e0163')) break p000e0163;
 		if (in_response)
		{
			if (!CNDverb(35)) break p000e0163;
 		}
 		ACCwrite(136);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPERTAR CRIATURA
	p000e0164:
	{
 		if (skipdoall('p000e0164')) break p000e0164;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0164;
			if (!CNDnoun1(73)) break p000e0164;
 		}
		if (!CNDpresent(14)) break p000e0164;
		if (!CNDnotat(25)) break p000e0164;
 		ACCwrite(137);
 		ACCwrite(138);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// DESPERTAR TECNICO
	p000e0165:
	{
 		if (skipdoall('p000e0165')) break p000e0165;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0165;
			if (!CNDnoun1(89)) break p000e0165;
 		}
		if (!CNDpresent(28)) break p000e0165;
		if (!CNDzero(66)) break p000e0165;
 		ACCwrite(139);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// DESPERTAR TECNICO
	p000e0166:
	{
 		if (skipdoall('p000e0166')) break p000e0166;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0166;
			if (!CNDnoun1(89)) break p000e0166;
 		}
		if (!CNDpresent(28)) break p000e0166;
 		ACCwrite(140);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPERTAR _
	p000e0167:
	{
 		if (skipdoall('p000e0167')) break p000e0167;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0167;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0167;
 		ACCwrite(141);
 		ACCprocess(64);
 		ACCwrite(142);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DESPERTAR _
	p000e0168:
	{
 		if (skipdoall('p000e0168')) break p000e0168;
 		if (in_response)
		{
			if (!CNDverb(36)) break p000e0168;
 		}
 		ACCwrite(143);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DISPARA PISTOLA
	p000e0169:
	{
 		if (skipdoall('p000e0169')) break p000e0169;
 		if (in_response)
		{
			if (!CNDverb(37)) break p000e0169;
			if (!CNDnoun1(63)) break p000e0169;
 		}
 		ACCcopyff(44,88);
 		ACCcopyff(45,24);
 		ACCcopyff(34,44);
 		ACCcopyff(35,45);
 		ACCcopyff(88,34);
 		ACCcopyff(24,35);
		{}

	}

	// DISPARA BABOSA
	p000e0170:
	{
 		if (skipdoall('p000e0170')) break p000e0170;
 		if (in_response)
		{
			if (!CNDverb(37)) break p000e0170;
			if (!CNDnoun1(75)) break p000e0170;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0170;
		if (!CNDcarried(7)) break p000e0170;
 		ACCwrite(144);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// DISPARA PUERTA
	p000e0171:
	{
 		if (skipdoall('p000e0171')) break p000e0171;
 		if (in_response)
		{
			if (!CNDverb(37)) break p000e0171;
			if (!CNDnoun1(76)) break p000e0171;
 		}
		if (!CNDpresent(30)) break p000e0171;
 		ACCwrite(145);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DISPARA TECNICO
	p000e0172:
	{
 		if (skipdoall('p000e0172')) break p000e0172;
 		if (in_response)
		{
			if (!CNDverb(37)) break p000e0172;
			if (!CNDnoun1(89)) break p000e0172;
 		}
		if (!CNDpresent(28)) break p000e0172;
		if (!CNDcarried(7)) break p000e0172;
		if (!CNDeq(66,0)) break p000e0172;
 		ACClet(66,1);
 		ACCwrite(146);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DISPARA TECNICO
	p000e0173:
	{
 		if (skipdoall('p000e0173')) break p000e0173;
 		if (in_response)
		{
			if (!CNDverb(37)) break p000e0173;
			if (!CNDnoun1(89)) break p000e0173;
 		}
		if (!CNDpresent(28)) break p000e0173;
		if (!CNDeq(66,1)) break p000e0173;
 		ACCwrite(147);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DISPARA _
	p000e0174:
	{
 		if (skipdoall('p000e0174')) break p000e0174;
 		if (in_response)
		{
			if (!CNDverb(37)) break p000e0174;
 		}
 		ACCprocess(32);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MATA GUSANO
	p000e0175:
	{
 		if (skipdoall('p000e0175')) break p000e0175;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0175;
			if (!CNDnoun1(69)) break p000e0175;
 		}
		if (!CNDpresent(11)) break p000e0175;
 		ACCwrite(148);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MATA CRIATURA
	p000e0176:
	{
 		if (skipdoall('p000e0176')) break p000e0176;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0176;
			if (!CNDnoun1(73)) break p000e0176;
 		}
		if (!CNDpresent(14)) break p000e0176;
		if (!CNDzero(64)) break p000e0176;
 		ACCwrite(149);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MATA _
	p000e0177:
	{
 		if (skipdoall('p000e0177')) break p000e0177;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0177;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0177;
 		ACCwrite(150);
 		ACCprocess(63);
 		ACCwrite(151);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MATA _
	p000e0178:
	{
 		if (skipdoall('p000e0178')) break p000e0178;
 		if (in_response)
		{
			if (!CNDverb(38)) break p000e0178;
 		}
 		ACCwrite(152);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLOCA TARJETA
	p000e0179:
	{
 		if (skipdoall('p000e0179')) break p000e0179;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0179;
			if (!CNDnoun1(79)) break p000e0179;
 		}
		if (!CNDpresent(30)) break p000e0179;
		if (!CNDcarried(20)) break p000e0179;
		if (!CNDnoun2(91)) break p000e0179;
 		ACCprocess(10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLOCA _
	p000e0180:
	{
 		if (skipdoall('p000e0180')) break p000e0180;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0180;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0180;
		if (!CNDpresent(30)) break p000e0180;
		if (!CNDnoun2(91)) break p000e0180;
 		ACCwrite(153);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// COLOCA _
	p000e0181:
	{
 		if (skipdoall('p000e0181')) break p000e0181;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0181;
 		}
 		ACCprocess(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MOVER PALANCA
	p000e0182:
	{
 		if (skipdoall('p000e0182')) break p000e0182;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0182;
			if (!CNDnoun1(95)) break p000e0182;
 		}
		if (!CNDpresent(33)) break p000e0182;
		if (!CNDisnotat(24,9)) break p000e0182;
 		ACCwrite(154);
 		ACCwrite(155);
 		ACCwrite(156);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MOVER PALANCA
	p000e0183:
	{
 		if (skipdoall('p000e0183')) break p000e0183;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0183;
			if (!CNDnoun1(95)) break p000e0183;
 		}
		if (!CNDpresent(33)) break p000e0183;
		if (!CNDnotzero(103)) break p000e0183;
 		ACCwrite(157);
 		ACCwrite(158);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MOVER PALANCA
	p000e0184:
	{
 		if (skipdoall('p000e0184')) break p000e0184;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0184;
			if (!CNDnoun1(95)) break p000e0184;
 		}
		if (!CNDpresent(33)) break p000e0184;
 		ACCset(103);
 		ACCplus(30,3);
 		ACCwrite(159);
 		ACCwrite(160);
 		ACCwrite(161);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// MOVER _
	p000e0185:
	{
 		if (skipdoall('p000e0185')) break p000e0185;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0185;
 		}
 		ACCwrite(162);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ESPERA _
	p000e0186:
	{
 		if (skipdoall('p000e0186')) break p000e0186;
 		if (in_response)
		{
			if (!CNDverb(41)) break p000e0186;
 		}
 		ACCwrite(163);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE PUERTA
	p000e0187:
	{
 		if (skipdoall('p000e0187')) break p000e0187;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0187;
			if (!CNDnoun1(76)) break p000e0187;
 		}
		if (!CNDat(20)) break p000e0187;
 		ACCwrite(164);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE PUERTA
	p000e0188:
	{
 		if (skipdoall('p000e0188')) break p000e0188;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0188;
			if (!CNDnoun1(76)) break p000e0188;
 		}
		if (!CNDat(21)) break p000e0188;
 		ACCwrite(165);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE PUERTA
	p000e0189:
	{
 		if (skipdoall('p000e0189')) break p000e0189;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0189;
			if (!CNDnoun1(76)) break p000e0189;
 		}
		if (!CNDat(31)) break p000e0189;
 		ACCwrite(166);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE PUERTA
	p000e0190:
	{
 		if (skipdoall('p000e0190')) break p000e0190;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0190;
			if (!CNDnoun1(76)) break p000e0190;
 		}
		if (!CNDpresent(30)) break p000e0190;
 		ACCwrite(167);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE COMPARTIME
	p000e0191:
	{
 		if (skipdoall('p000e0191')) break p000e0191;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0191;
			if (!CNDnoun1(88)) break p000e0191;
 		}
		if (!CNDpresent(10)) break p000e0191;
		if (!CNDzero(27)) break p000e0191;
 		ACCset(27);
 		ACCprocess(4);
 		ACCprocess(51);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE CUADRO
	p000e0192:
	{
 		if (skipdoall('p000e0192')) break p000e0192;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0192;
			if (!CNDnoun1(96)) break p000e0192;
 		}
		if (!CNDpresent(9)) break p000e0192;
		if (!CNDzero(62)) break p000e0192;
 		ACCset(62);
 		ACCprocess(4);
 		ACCprocess(51);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABRE _
	p000e0193:
	{
 		if (skipdoall('p000e0193')) break p000e0193;
 		if (in_response)
		{
			if (!CNDverb(42)) break p000e0193;
 		}
 		ACCprocess(3);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR PUERTA
	p000e0194:
	{
 		if (skipdoall('p000e0194')) break p000e0194;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0194;
			if (!CNDnoun1(76)) break p000e0194;
 		}
		if (!CNDpresent(30)) break p000e0194;
 		ACCwrite(168);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR COMPARTIME
	p000e0195:
	{
 		if (skipdoall('p000e0195')) break p000e0195;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0195;
			if (!CNDnoun1(88)) break p000e0195;
 		}
		if (!CNDpresent(10)) break p000e0195;
		if (!CNDnotzero(27)) break p000e0195;
 		ACCclear(27);
 		ACCprocess(19);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR CUADRO
	p000e0196:
	{
 		if (skipdoall('p000e0196')) break p000e0196;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0196;
			if (!CNDnoun1(96)) break p000e0196;
 		}
		if (!CNDpresent(9)) break p000e0196;
		if (!CNDnotzero(62)) break p000e0196;
 		ACCclear(62);
 		ACCprocess(19);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CERRAR _
	p000e0197:
	{
 		if (skipdoall('p000e0197')) break p000e0197;
 		if (in_response)
		{
			if (!CNDverb(43)) break p000e0197;
 		}
 		ACCprocess(18);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0198:
	{
 		if (skipdoall('p000e0198')) break p000e0198;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0198;
 		}
 		ACCclear(50);
		{}

	}

	// DEJA _
	p000e0199:
	{
 		if (skipdoall('p000e0199')) break p000e0199;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0199;
 		}
		if (!CNDeq(34,51)) break p000e0199;
		entry_for_doall = 'p000e0200';
		process_in_doall = 0;
 		ACCdoall(254);
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0200:
	{
 		if (skipdoall('p000e0200')) break p000e0200;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0200;
 		}
		if (!CNDnotzero(50)) break p000e0200;
 		ACCwrite(169);
		{}

	}

	// DEJA CUERDA
	p000e0201:
	{
 		if (skipdoall('p000e0201')) break p000e0201;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0201;
			if (!CNDnoun1(54)) break p000e0201;
 		}
		if (!CNDat(13)) break p000e0201;
 		ACCwrite(170);
 		ACCprocess(86);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA LIQUIDO
	p000e0202:
	{
 		if (skipdoall('p000e0202')) break p000e0202;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0202;
			if (!CNDnoun1(70)) break p000e0202;
 		}
		if (!CNDcarried(12)) break p000e0202;
		if (!CNDnotat(7)) break p000e0202;
 		ACCwrite(171);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0203:
	{
 		if (skipdoall('p000e0203')) break p000e0203;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0203;
 		}
		if (!CNDat(15)) break p000e0203;
 		ACCwhato();
		if (!CNDeq(54,254)) break p000e0203;
 		ACCputo(255);
 		ACCwrite(172);
 		ACCprocess(64);
 		ACCwrite(173);
 		ACCprocess(72);
 		ACCwrite(174);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0204:
	{
 		if (skipdoall('p000e0204')) break p000e0204;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0204;
 		}
		if (!CNDnoun2(255)) break p000e0204;
 		ACCprocess(29);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DEJA _
	p000e0205:
	{
 		if (skipdoall('p000e0205')) break p000e0205;
 		if (in_response)
		{
			if (!CNDverb(44)) break p000e0205;
 		}
 		ACCprocess(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GRABA _
	p000e0206:
	{
 		if (skipdoall('p000e0206')) break p000e0206;
 		if (in_response)
		{
			if (!CNDverb(47)) break p000e0206;
 		}
 		ACCsave();
		break pro000_restart;
		{}

	}

	// CARGA _
	p000e0207:
	{
 		if (skipdoall('p000e0207')) break p000e0207;
 		if (in_response)
		{
			if (!CNDverb(48)) break p000e0207;
 		}
 		ACCload();
		break pro000_restart;
		{}

	}

	// RAMSAVE _
	p000e0208:
	{
 		if (skipdoall('p000e0208')) break p000e0208;
 		if (in_response)
		{
			if (!CNDverb(49)) break p000e0208;
 		}
 		ACCramsave();
 		ACCwrite(175);
		{}

	}

	// AGARRA _
	p000e0209:
	{
 		if (skipdoall('p000e0209')) break p000e0209;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0209;
 		}
 		ACCclear(50);
		{}

	}

	// AGARRA _
	p000e0210:
	{
 		if (skipdoall('p000e0210')) break p000e0210;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0210;
 		}
		if (!CNDeq(34,51)) break p000e0210;
		entry_for_doall = 'p000e0211';
		process_in_doall = 0;
 		ACCdoall(255);
		break pro000_restart;
		{}

	}

	// AGARRA _
	p000e0211:
	{
 		if (skipdoall('p000e0211')) break p000e0211;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0211;
 		}
		if (!CNDnotzero(50)) break p000e0211;
 		ACCwrite(176);
		{}

	}

	// AGARRA CUERDA
	p000e0212:
	{
 		if (skipdoall('p000e0212')) break p000e0212;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0212;
			if (!CNDnoun1(54)) break p000e0212;
 		}
		if (!CNDat(11)) break p000e0212;
		if (!CNDzero(86)) break p000e0212;
 		ACCwrite(177);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA CUERDA
	p000e0213:
	{
 		if (skipdoall('p000e0213')) break p000e0213;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0213;
			if (!CNDnoun1(54)) break p000e0213;
 		}
		if (!CNDat(11)) break p000e0213;
		if (!CNDabsent(3)) break p000e0213;
 		ACCwrite(178);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA CUERDA
	p000e0214:
	{
 		if (skipdoall('p000e0214')) break p000e0214;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0214;
			if (!CNDnoun1(54)) break p000e0214;
 		}
		if (!CNDat(12)) break p000e0214;
		if (!CNDnotzero(85)) break p000e0214;
 		ACCclear(85);
 		ACCplace(3,254);
 		ACCwrite(179);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA TABLON
	p000e0215:
	{
 		if (skipdoall('p000e0215')) break p000e0215;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0215;
			if (!CNDnoun1(60)) break p000e0215;
 		}
		if (!CNDat(16)) break p000e0215;
		if (!CNDzero(101)) break p000e0215;
 		ACCset(101);
 		ACCplace(5,254);
 		ACCplus(30,1);
 		ACCwrite(180);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA TABLON
	p000e0216:
	{
 		if (skipdoall('p000e0216')) break p000e0216;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0216;
			if (!CNDnoun1(60)) break p000e0216;
 		}
		if (!CNDat(16)) break p000e0216;
		if (!CNDabsent(5)) break p000e0216;
 		ACCwrite(181);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA UTENSILIO
	p000e0217:
	{
 		if (skipdoall('p000e0217')) break p000e0217;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0217;
			if (!CNDnoun1(62)) break p000e0217;
 		}
		if (!CNDat(17)) break p000e0217;
 		ACCwrite(182);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA BLOQUE
	p000e0218:
	{
 		if (skipdoall('p000e0218')) break p000e0218;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0218;
			if (!CNDnoun1(65)) break p000e0218;
 		}
		if (!CNDat(19)) break p000e0218;
		if (!CNDisat(8,19)) break p000e0218;
 		ACCwrite(183);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA BLOQUE
	p000e0219:
	{
 		if (skipdoall('p000e0219')) break p000e0219;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0219;
			if (!CNDnoun1(65)) break p000e0219;
 		}
		if (!CNDat(19)) break p000e0219;
 		ACCwrite(184);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA GUSANO
	p000e0220:
	{
 		if (skipdoall('p000e0220')) break p000e0220;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0220;
			if (!CNDnoun1(69)) break p000e0220;
 		}
		if (!CNDpresent(11)) break p000e0220;
 		ACCwrite(185);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA GUSANO
	p000e0221:
	{
 		if (skipdoall('p000e0221')) break p000e0221;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0221;
			if (!CNDnoun1(69)) break p000e0221;
 		}
		if (!CNDpresent(25)) break p000e0221;
		if (!CNDisat(11,25)) break p000e0221;
 		ACCwrite(186);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA LIQUIDO
	p000e0222:
	{
 		if (skipdoall('p000e0222')) break p000e0222;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0222;
			if (!CNDnoun1(70)) break p000e0222;
 		}
		if (!CNDnotcarr(12)) break p000e0222;
		if (!CNDpresent(12)) break p000e0222;
 		ACCprocess(21);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA ESPEJO
	p000e0223:
	{
 		if (skipdoall('p000e0223')) break p000e0223;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0223;
			if (!CNDnoun1(71)) break p000e0223;
 		}
		if (!CNDisat(16,255)) break p000e0223;
 		ACClet(35,5);
		{}

	}

	// AGARRA ESPEJO
	p000e0224:
	{
 		if (skipdoall('p000e0224')) break p000e0224;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0224;
			if (!CNDnoun1(71)) break p000e0224;
 		}
		if (!CNDat(5)) break p000e0224;
		if (!CNDzero(64)) break p000e0224;
 		ACCwrite(187);
 		ACCwrite(188);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// AGARRA ESPEJO
	p000e0225:
	{
 		if (skipdoall('p000e0225')) break p000e0225;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0225;
			if (!CNDnoun1(71)) break p000e0225;
 		}
		if (!CNDzero(95)) break p000e0225;
		if (!CNDpresent(16)) break p000e0225;
 		ACCset(95);
 		ACCplus(30,2);
		{}

	}

	// AGARRA CRIATURA
	p000e0226:
	{
 		if (skipdoall('p000e0226')) break p000e0226;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0226;
			if (!CNDnoun1(73)) break p000e0226;
 		}
		if (!CNDpresent(14)) break p000e0226;
		if (!CNDnotat(25)) break p000e0226;
 		ACCwrite(189);
 		ACCwrite(190);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// AGARRA ESQUELETO
	p000e0227:
	{
 		if (skipdoall('p000e0227')) break p000e0227;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0227;
			if (!CNDnoun1(74)) break p000e0227;
 		}
		if (!CNDpresent(15)) break p000e0227;
 		ACCwrite(191);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA BABOSA
	p000e0228:
	{
 		if (skipdoall('p000e0228')) break p000e0228;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0228;
			if (!CNDnoun1(75)) break p000e0228;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0228;
 		ACCwrite(192);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// AGARRA BARROTE
	p000e0229:
	{
 		if (skipdoall('p000e0229')) break p000e0229;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0229;
			if (!CNDnoun1(84)) break p000e0229;
 		}
		if (!CNDat(24)) break p000e0229;
		if (!CNDzero(100)) break p000e0229;
 		ACCwrite(193);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA BARROTE
	p000e0230:
	{
 		if (skipdoall('p000e0230')) break p000e0230;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0230;
			if (!CNDnoun1(84)) break p000e0230;
 		}
		if (!CNDabsent(24)) break p000e0230;
		if (!CNDat(24)) break p000e0230;
 		ACCwrite(194);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA TECNICO
	p000e0231:
	{
 		if (skipdoall('p000e0231')) break p000e0231;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0231;
			if (!CNDnoun1(89)) break p000e0231;
 		}
		if (!CNDpresent(28)) break p000e0231;
		if (!CNDzero(66)) break p000e0231;
 		ACCwrite(195);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// AGARRA TECNICO
	p000e0232:
	{
 		if (skipdoall('p000e0232')) break p000e0232;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0232;
			if (!CNDnoun1(89)) break p000e0232;
 		}
		if (!CNDpresent(28)) break p000e0232;
 		ACCwrite(196);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA UNIFORME
	p000e0233:
	{
 		if (skipdoall('p000e0233')) break p000e0233;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0233;
			if (!CNDnoun1(90)) break p000e0233;
 		}
		if (!CNDpresent(28)) break p000e0233;
		if (!CNDeq(66,1)) break p000e0233;
 		ACCplus(30,2);
 		ACCdestroy(28);
 		ACClet(66,2);
 		ACCplace(29,254);
 		ACCwrite(197);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA UNIFORME
	p000e0234:
	{
 		if (skipdoall('p000e0234')) break p000e0234;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0234;
			if (!CNDnoun1(90)) break p000e0234;
 		}
		if (!CNDpresent(28)) break p000e0234;
 		ACCwrite(198);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// AGARRA _
	p000e0235:
	{
 		if (skipdoall('p000e0235')) break p000e0235;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0235;
 		}
		if (!CNDnoun2(255)) break p000e0235;
 		ACCprocess(22);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// AGARRA _
	p000e0236:
	{
 		if (skipdoall('p000e0236')) break p000e0236;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0236;
 		}
 		ACCprocess(83);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// QUITA BOTA
	p000e0237:
	{
 		if (skipdoall('p000e0237')) break p000e0237;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0237;
			if (!CNDnoun1(61)) break p000e0237;
 		}
		if (!CNDat(15)) break p000e0237;
 		ACCwrite(199);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// QUITA GUSANO
	p000e0238:
	{
 		if (skipdoall('p000e0238')) break p000e0238;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0238;
			if (!CNDnoun1(69)) break p000e0238;
 		}
		if (!CNDnoun2(85)) break p000e0238;
		if (!CNDpresent(25)) break p000e0238;
		if (!CNDisat(11,25)) break p000e0238;
 		ACCwrite(200);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// QUITA GUANTES
	p000e0239:
	{
 		if (skipdoall('p000e0239')) break p000e0239;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0239;
			if (!CNDnoun1(77)) break p000e0239;
 		}
		if (!CNDcarried(12)) break p000e0239;
 		ACCwrite(201);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// QUITA UNIFORME
	p000e0240:
	{
 		if (skipdoall('p000e0240')) break p000e0240;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0240;
			if (!CNDnoun1(90)) break p000e0240;
 		}
		if (!CNDpresent(28)) break p000e0240;
		if (!CNDeq(66,1)) break p000e0240;
 		ACCplus(30,2);
 		ACCdestroy(28);
 		ACClet(66,2);
 		ACCplace(29,254);
 		ACCwrite(202);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// QUITA UNIFORME
	p000e0241:
	{
 		if (skipdoall('p000e0241')) break p000e0241;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0241;
			if (!CNDnoun1(90)) break p000e0241;
 		}
		if (!CNDpresent(28)) break p000e0241;
 		ACCwrite(203);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro000_restart;
		{}

	}

	// QUITA _
	p000e0242:
	{
 		if (skipdoall('p000e0242')) break p000e0242;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0242;
 		}
		if (!CNDat(15)) break p000e0242;
		if (!CNDnoun2(57)) break p000e0242;
 		ACCprocess(22);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// QUITA _
	p000e0243:
	{
 		if (skipdoall('p000e0243')) break p000e0243;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0243;
 		}
		if (!CNDnoun2(255)) break p000e0243;
 		ACCprocess(76);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// QUITA _
	p000e0244:
	{
 		if (skipdoall('p000e0244')) break p000e0244;
 		if (in_response)
		{
			if (!CNDverb(51)) break p000e0244;
 		}
 		ACCprocess(83);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// RAMLOAD _
	p000e0245:
	{
 		if (skipdoall('p000e0245')) break p000e0245;
 		if (in_response)
		{
			if (!CNDverb(52)) break p000e0245;
 		}
 		ACCramload(255);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// M _
	p000e0246:
	{
 		if (skipdoall('p000e0246')) break p000e0246;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0246;
 		}
		if (!CNDprep(5)) break p000e0246;
 		ACCprocess(56);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// M _
	p000e0247:
	{
 		if (skipdoall('p000e0247')) break p000e0247;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0247;
 		}
		if (!CNDnoteq(34,255)) break p000e0247;
 		ACClet(33,55);
		{}

	}

	// M _
	p000e0248:
	{
 		if (skipdoall('p000e0248')) break p000e0248;
 		if (in_response)
		{
			if (!CNDverb(54)) break p000e0248;
 		}
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EX LAGO
	p000e0249:
	{
 		if (skipdoall('p000e0249')) break p000e0249;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0249;
			if (!CNDnoun1(57)) break p000e0249;
 		}
		if (!CNDat(14)) break p000e0249;
 		ACCwrite(204);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX SUELO
	p000e0250:
	{
 		if (skipdoall('p000e0250')) break p000e0250;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0250;
			if (!CNDnoun1(59)) break p000e0250;
 		}
		if (!CNDat(16)) break p000e0250;
 		ACCwrite(205);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX TABLON
	p000e0251:
	{
 		if (skipdoall('p000e0251')) break p000e0251;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0251;
			if (!CNDnoun1(60)) break p000e0251;
 		}
		if (!CNDat(16)) break p000e0251;
		if (!CNDabsent(5)) break p000e0251;
 		ACCwrite(206);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX PINTURA
	p000e0252:
	{
 		if (skipdoall('p000e0252')) break p000e0252;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0252;
			if (!CNDnoun1(64)) break p000e0252;
 		}
		if (!CNDat(18)) break p000e0252;
 		ACCwrite(207);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX BLOQUE
	p000e0253:
	{
 		if (skipdoall('p000e0253')) break p000e0253;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0253;
			if (!CNDnoun1(65)) break p000e0253;
 		}
		if (!CNDat(19)) break p000e0253;
		if (!CNDisnotat(8,19)) break p000e0253;
 		ACCwrite(208);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX GUSANO
	p000e0254:
	{
 		if (skipdoall('p000e0254')) break p000e0254;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0254;
			if (!CNDnoun1(69)) break p000e0254;
 		}
		if (!CNDpresent(25)) break p000e0254;
		if (!CNDisat(11,25)) break p000e0254;
 		ACCwrite(209);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX SIMBOLO
	p000e0255:
	{
 		if (skipdoall('p000e0255')) break p000e0255;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0255;
			if (!CNDnoun1(72)) break p000e0255;
 		}
		if (!CNDpresent(13)) break p000e0255;
 		ACCwrite(210);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX MONITOR
	p000e0256:
	{
 		if (skipdoall('p000e0256')) break p000e0256;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0256;
			if (!CNDnoun1(78)) break p000e0256;
 		}
		if (!CNDat(21)) break p000e0256;
 		ACCwrite(211);
		{}

	}

	// EX MONITOR
	p000e0257:
	{
 		if (skipdoall('p000e0257')) break p000e0257;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0257;
			if (!CNDnoun1(78)) break p000e0257;
 		}
		if (!CNDat(21)) break p000e0257;
		if (!CNDzero(103)) break p000e0257;
 		ACCwrite(212);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX SUCCIONADO
	p000e0258:
	{
 		if (skipdoall('p000e0258')) break p000e0258;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0258;
			if (!CNDnoun1(85)) break p000e0258;
 		}
		if (!CNDpresent(25)) break p000e0258;
 		ACCwrite(213);
		{}

	}

	// EX SUCCIONADO
	p000e0259:
	{
 		if (skipdoall('p000e0259')) break p000e0259;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0259;
			if (!CNDnoun1(85)) break p000e0259;
 		}
		if (!CNDpresent(25)) break p000e0259;
		if (!CNDisat(11,25)) break p000e0259;
 		ACCwrite(214);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX SUCCIONADO
	p000e0260:
	{
 		if (skipdoall('p000e0260')) break p000e0260;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0260;
			if (!CNDnoun1(85)) break p000e0260;
 		}
		if (!CNDpresent(25)) break p000e0260;
 		ACCwrite(215);
 		ACCwrite(216);
 		ACCnewline();
 		ACCgoto(25);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0261:
	{
 		if (skipdoall('p000e0261')) break p000e0261;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0261;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0261;
 		ACCprocess(35);
		if (!CNDzero(69)) break p000e0261;
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0262:
	{
 		if (skipdoall('p000e0262')) break p000e0262;
 		if (in_response)
		{
			if (!CNDverb(55)) break p000e0262;
 		}
 		ACCprocess(36);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PON LIQUIDO
	p000e0263:
	{
 		if (skipdoall('p000e0263')) break p000e0263;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0263;
			if (!CNDnoun1(70)) break p000e0263;
 		}
		if (!CNDcarried(12)) break p000e0263;
 		ACClet(33,250);
		{}

	}

	// PON TARJETA
	p000e0264:
	{
 		if (skipdoall('p000e0264')) break p000e0264;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0264;
			if (!CNDnoun1(79)) break p000e0264;
 		}
		if (!CNDpresent(30)) break p000e0264;
		if (!CNDcarried(20)) break p000e0264;
		if (!CNDnoun2(91)) break p000e0264;
 		ACCprocess(10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PON _
	p000e0265:
	{
 		if (skipdoall('p000e0265')) break p000e0265;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0265;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0265;
		if (!CNDpresent(30)) break p000e0265;
		if (!CNDnoun2(91)) break p000e0265;
 		ACCwrite(217);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PON _
	p000e0266:
	{
 		if (skipdoall('p000e0266')) break p000e0266;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0266;
 		}
		if (!CNDnoun2(255)) break p000e0266;
 		ACCprocess(60);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PON _
	p000e0267:
	{
 		if (skipdoall('p000e0267')) break p000e0267;
 		if (in_response)
		{
			if (!CNDverb(56)) break p000e0267;
 		}
 		ACCprocess(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// METE LIQUIDO
	p000e0268:
	{
 		if (skipdoall('p000e0268')) break p000e0268;
 		if (in_response)
		{
			if (!CNDverb(57)) break p000e0268;
			if (!CNDnoun1(70)) break p000e0268;
 		}
		if (!CNDcarried(12)) break p000e0268;
 		ACClet(33,250);
		{}

	}

	// METE TARJETA
	p000e0269:
	{
 		if (skipdoall('p000e0269')) break p000e0269;
 		if (in_response)
		{
			if (!CNDverb(57)) break p000e0269;
			if (!CNDnoun1(79)) break p000e0269;
 		}
		if (!CNDpresent(30)) break p000e0269;
		if (!CNDcarried(20)) break p000e0269;
		if (!CNDnoun2(91)) break p000e0269;
 		ACCprocess(10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// METE _
	p000e0270:
	{
 		if (skipdoall('p000e0270')) break p000e0270;
 		if (in_response)
		{
			if (!CNDverb(57)) break p000e0270;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0270;
		if (!CNDpresent(30)) break p000e0270;
		if (!CNDnoun2(91)) break p000e0270;
 		ACCwrite(218);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// METE _
	p000e0271:
	{
 		if (skipdoall('p000e0271')) break p000e0271;
 		if (in_response)
		{
			if (!CNDverb(57)) break p000e0271;
 		}
 		ACCprocess(55);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PONTE _
	p000e0272:
	{
 		if (skipdoall('p000e0272')) break p000e0272;
 		if (in_response)
		{
			if (!CNDverb(58)) break p000e0272;
 		}
 		ACCprocess(60);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SACATE BOTA
	p000e0273:
	{
 		if (skipdoall('p000e0273')) break p000e0273;
 		if (in_response)
		{
			if (!CNDverb(59)) break p000e0273;
			if (!CNDnoun1(61)) break p000e0273;
 		}
		if (!CNDat(15)) break p000e0273;
 		ACCwrite(219);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SACATE GUANTES
	p000e0274:
	{
 		if (skipdoall('p000e0274')) break p000e0274;
 		if (in_response)
		{
			if (!CNDverb(59)) break p000e0274;
			if (!CNDnoun1(77)) break p000e0274;
 		}
		if (!CNDcarried(12)) break p000e0274;
 		ACCwrite(220);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SACATE _
	p000e0275:
	{
 		if (skipdoall('p000e0275')) break p000e0275;
 		if (in_response)
		{
			if (!CNDverb(59)) break p000e0275;
 		}
 		ACCprocess(76);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ABANDONA _
	p000e0276:
	{
 		if (skipdoall('p000e0276')) break p000e0276;
 		if (in_response)
		{
			if (!CNDverb(60)) break p000e0276;
 		}
 		ACCquit();
 		function anykey00001() 
		{
 		ACCturns();
 		ACCscore();
 		ACCend();
		return;
		}
 		waitKey(anykey00001);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// DA PATADA
	p000e0277:
	{
 		if (skipdoall('p000e0277')) break p000e0277;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0277;
			if (!CNDnoun1(67)) break p000e0277;
 		}
		if (!CNDat(19)) break p000e0277;
		if (!CNDprep(3)) break p000e0277;
		if (!CNDnoun2(65)) break p000e0277;
 		ACClet(33,27);
 		ACClet(34,65);
 		ACClet(35,255);
 		ACClet(44,66);
 		ACClet(45,255);
 		ACCprocess(82);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA PATADA
	p000e0278:
	{
 		if (skipdoall('p000e0278')) break p000e0278;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0278;
			if (!CNDnoun1(67)) break p000e0278;
 		}
		if (!CNDat(24)) break p000e0278;
		if (!CNDnoun2(84)) break p000e0278;
 		ACCprocess(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA PATADA
	p000e0279:
	{
 		if (skipdoall('p000e0279')) break p000e0279;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0279;
			if (!CNDnoun1(67)) break p000e0279;
 		}
		if (!CNDat(24)) break p000e0279;
		if (!CNDnoun2(83)) break p000e0279;
 		ACCprocess(85);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA PATADA
	p000e0280:
	{
 		if (skipdoall('p000e0280')) break p000e0280;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0280;
			if (!CNDnoun1(67)) break p000e0280;
 		}
 		ACCwrite(221);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA PUNETAZO
	p000e0281:
	{
 		if (skipdoall('p000e0281')) break p000e0281;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0281;
			if (!CNDnoun1(68)) break p000e0281;
 		}
		if (!CNDat(19)) break p000e0281;
		if (!CNDprep(3)) break p000e0281;
		if (!CNDnoun2(65)) break p000e0281;
 		ACCwrite(222);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA PUNETAZO
	p000e0282:
	{
 		if (skipdoall('p000e0282')) break p000e0282;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0282;
			if (!CNDnoun1(68)) break p000e0282;
 		}
 		ACCwrite(223);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0283:
	{
 		if (skipdoall('p000e0283')) break p000e0283;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0283;
 		}
		if (!CNDnoun2(73)) break p000e0283;
		if (!CNDnotat(25)) break p000e0283;
		if (!CNDpresent(14)) break p000e0283;
 		ACCwrite(224);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0284:
	{
 		if (skipdoall('p000e0284')) break p000e0284;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0284;
 		}
		if (!CNDnoun2(75)) break p000e0284;
 		ACCwhato();
		if (!CNDeq(54,254)) break p000e0284;
		if (!CNDeq(51,16)) break p000e0284;
 		ACClet(44,255);
 		ACCcopyff(38,67);
 		ACCprocess(42);
 		ACClet(44,75);
		if (!CNDnotzero(69)) break p000e0284;
 		ACCwrite(225);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0285:
	{
 		if (skipdoall('p000e0285')) break p000e0285;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0285;
 		}
		if (!CNDnoun2(75)) break p000e0285;
 		ACCwhato();
		if (!CNDeq(54,254)) break p000e0285;
 		ACClet(44,255);
 		ACCcopyff(38,67);
 		ACCprocess(42);
 		ACClet(44,75);
		if (!CNDnotzero(69)) break p000e0285;
 		ACCwrite(226);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DA _
	p000e0286:
	{
 		if (skipdoall('p000e0286')) break p000e0286;
 		if (in_response)
		{
			if (!CNDverb(61)) break p000e0286;
 		}
 		ACCprocess(28);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSENA _
	p000e0287:
	{
 		if (skipdoall('p000e0287')) break p000e0287;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0287;
 		}
		if (!CNDnoun2(73)) break p000e0287;
		if (!CNDnotat(25)) break p000e0287;
		if (!CNDpresent(14)) break p000e0287;
 		ACCwrite(227);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSENA _
	p000e0288:
	{
 		if (skipdoall('p000e0288')) break p000e0288;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0288;
 		}
		if (!CNDnoun2(75)) break p000e0288;
 		ACCwhato();
		if (!CNDeq(54,254)) break p000e0288;
		if (!CNDeq(51,16)) break p000e0288;
 		ACClet(44,255);
 		ACCcopyff(38,67);
 		ACCprocess(42);
 		ACClet(44,75);
		if (!CNDnotzero(69)) break p000e0288;
 		ACCwrite(228);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSENA _
	p000e0289:
	{
 		if (skipdoall('p000e0289')) break p000e0289;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0289;
 		}
		if (!CNDnoun2(75)) break p000e0289;
 		ACCwhato();
		if (!CNDeq(54,254)) break p000e0289;
 		ACClet(44,255);
 		ACCcopyff(38,67);
 		ACCprocess(42);
 		ACClet(44,75);
		if (!CNDnotzero(69)) break p000e0289;
 		ACCwrite(229);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ENSENA _
	p000e0290:
	{
 		if (skipdoall('p000e0290')) break p000e0290;
 		if (in_response)
		{
			if (!CNDverb(62)) break p000e0290;
 		}
 		ACCprocess(58);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SALIDAS _
	p000e0291:
	{
 		if (skipdoall('p000e0291')) break p000e0291;
 		if (in_response)
		{
			if (!CNDverb(63)) break p000e0291;
 		}
 		ACCprocess(84);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DERRAMA LIQUIDO
	p000e0292:
	{
 		if (skipdoall('p000e0292')) break p000e0292;
 		if (in_response)
		{
			if (!CNDverb(250)) break p000e0292;
			if (!CNDnoun1(70)) break p000e0292;
 		}
		if (!CNDcarried(12)) break p000e0292;
		if (!CNDat(27)) break p000e0292;
		if (!CNDnoun2(86)) break p000e0292;
		if (!CNDzero(26)) break p000e0292;
 		ACCset(26);
 		ACCplus(30,2);
 		ACCwrite(230);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DERRAMA LIQUIDO
	p000e0293:
	{
 		if (skipdoall('p000e0293')) break p000e0293;
 		if (in_response)
		{
			if (!CNDverb(250)) break p000e0293;
			if (!CNDnoun1(70)) break p000e0293;
 		}
		if (!CNDcarried(12)) break p000e0293;
		if (!CNDat(27)) break p000e0293;
		if (!CNDnoun2(86)) break p000e0293;
 		ACCwrite(231);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DERRAMA LIQUIDO
	p000e0294:
	{
 		if (skipdoall('p000e0294')) break p000e0294;
 		if (in_response)
		{
			if (!CNDverb(250)) break p000e0294;
			if (!CNDnoun1(70)) break p000e0294;
 		}
		if (!CNDcarried(12)) break p000e0294;
		if (!CNDat(24)) break p000e0294;
		if (!CNDnoun2(84)) break p000e0294;
		if (!CNDzero(100)) break p000e0294;
 		ACCwrite(232);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DERRAMA LIQUIDO
	p000e0295:
	{
 		if (skipdoall('p000e0295')) break p000e0295;
 		if (in_response)
		{
			if (!CNDverb(250)) break p000e0295;
			if (!CNDnoun1(70)) break p000e0295;
 		}
		if (!CNDcarried(12)) break p000e0295;
		if (!CNDat(24)) break p000e0295;
		if (!CNDnoun2(83)) break p000e0295;
		if (!CNDzero(100)) break p000e0295;
 		ACCwrite(233);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DERRAMA LIQUIDO
	p000e0296:
	{
 		if (skipdoall('p000e0296')) break p000e0296;
 		if (in_response)
		{
			if (!CNDverb(250)) break p000e0296;
			if (!CNDnoun1(70)) break p000e0296;
 		}
		if (!CNDcarried(12)) break p000e0296;
 		ACCwrite(234);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DERRAMA _
	p000e0297:
	{
 		if (skipdoall('p000e0297')) break p000e0297;
 		if (in_response)
		{
			if (!CNDverb(250)) break p000e0297;
 		}
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p000e0297;
 		ACCwrite(235);
 		ACCprocess(64);
 		ACCwrite(236);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DERRAMA _
	p000e0298:
	{
 		if (skipdoall('p000e0298')) break p000e0298;
 		if (in_response)
		{
			if (!CNDverb(250)) break p000e0298;
 		}
 		ACCwrite(237);
 		ACCnewline();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ INICIAR
	p000e0299:
	{
 		if (skipdoall('p000e0299')) break p000e0299;
 		if (in_response)
		{
			if (!CNDnoun1(93)) break p000e0299;
 		}
		if (!CNDat(29)) break p000e0299;
 		ACCnotdone();
		break pro000_restart;
		{}

	}

	// _ IGNICION
	p000e0300:
	{
 		if (skipdoall('p000e0300')) break p000e0300;
 		if (in_response)
		{
			if (!CNDnoun1(94)) break p000e0300;
 		}
		if (!CNDat(29)) break p000e0300;
 		ACCnotdone();
		break pro000_restart;
		{}

	}


}
}

function pro001()
{
process_restart=true;
pro001_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p001e0000:
	{
 		if (skipdoall('p001e0000')) break p001e0000;
		if (!CNDat(0)) break p001e0000;
 		ACCmode(1);
 		ACClet(53,64);
 		ACCbset(12,5);
 		ACCprompt(2);
 		ACCability(255,255);
 		ACCprocess(45);
 		ACClet(92,22);
 		ACCanykey();
 		function anykey00002() 
		{
 		ACCgoto(8);
 		ACCcls();
 		ACCwrite(238);
 		ACCdesc();
		return;
		}
 		waitKey(anykey00002);
		done_flag=true;
		break pro001_restart;
		{}

	}

	// _ _
	p001e0001:
	{
 		if (skipdoall('p001e0001')) break p001e0001;
		if (!CNDgt(38,2)) break p001e0001;
		if (!CNDlt(38,7)) break p001e0001;
 		ACCwrite(239);
		{}

	}

	// _ _
	p001e0002:
	{
 		if (skipdoall('p001e0002')) break p001e0002;
		if (!CNDat(3)) break p001e0002;
 		ACCplace(13,3);
 		ACCwrite(240);
 		ACCwrite(241);
 		ACCwrite(242);
		{}

	}

	// _ _
	p001e0003:
	{
 		if (skipdoall('p001e0003')) break p001e0003;
		if (!CNDat(6)) break p001e0003;
 		ACCplace(13,6);
 		ACCwrite(243);
 		ACCwrite(244);
 		ACCwrite(245);
		{}

	}

	// _ _
	p001e0004:
	{
 		if (skipdoall('p001e0004')) break p001e0004;
		if (!CNDat(4)) break p001e0004;
 		ACCplace(13,4);
 		ACCwrite(246);
 		ACCwrite(247);
 		ACCwrite(248);
		{}

	}

	// _ _
	p001e0005:
	{
 		if (skipdoall('p001e0005')) break p001e0005;
		if (!CNDat(3)) break p001e0005;
 		ACCwrite(249);
		{}

	}

	// _ _
	p001e0006:
	{
 		if (skipdoall('p001e0006')) break p001e0006;
		if (!CNDat(25)) break p001e0006;
		if (!CNDpresent(11)) break p001e0006;
 		ACCwrite(250);
		{}

	}

	// _ _
	p001e0007:
	{
 		if (skipdoall('p001e0007')) break p001e0007;
		if (!CNDat(25)) break p001e0007;
 		ACCwrite(251);
		{}

	}

	// _ _
	p001e0008:
	{
 		if (skipdoall('p001e0008')) break p001e0008;
		if (!CNDat(7)) break p001e0008;
		if (!CNDisat(11,7)) break p001e0008;
 		ACCwrite(252);
 		ACCwrite(253);
		{}

	}

	// _ _
	p001e0009:
	{
 		if (skipdoall('p001e0009')) break p001e0009;
		if (!CNDat(7)) break p001e0009;
		if (!CNDisat(11,7)) break p001e0009;
		if (!CNDisat(12,7)) break p001e0009;
 		ACCwrite(254);
		{}

	}

	// _ _
	p001e0010:
	{
 		if (skipdoall('p001e0010')) break p001e0010;
		if (!CNDat(7)) break p001e0010;
		if (!CNDisnotat(11,7)) break p001e0010;
		if (!CNDisat(12,7)) break p001e0010;
 		ACCwrite(255);
 		ACCwrite(256);
		{}

	}

	// _ _
	p001e0011:
	{
 		if (skipdoall('p001e0011')) break p001e0011;
		if (!CNDat(7)) break p001e0011;
 		ACCwrite(257);
		{}

	}

	// _ _
	p001e0012:
	{
 		if (skipdoall('p001e0012')) break p001e0012;
 		ACCnewline();
		if (!CNDzero(0)) break p001e0012;
		if (!CNDabsent(0)) break p001e0012;
 		ACCprocess(53);
		{}

	}

	// _ _
	p001e0013:
	{
 		if (skipdoall('p001e0013')) break p001e0013;
		if (!CNDpresent(0)) break p001e0013;
 		ACCprocess(53);
		{}

	}

	// _ _
	p001e0014:
	{
 		if (skipdoall('p001e0014')) break p001e0014;
		if (!CNDpresent(14)) break p001e0014;
		if (!CNDnotat(25)) break p001e0014;
		if (!CNDnotat(26)) break p001e0014;
 		ACCwrite(258);
		if (!CNDzero(64)) break p001e0014;
 		ACCwrite(259);
		{}

	}

	// _ _
	p001e0015:
	{
 		if (skipdoall('p001e0015')) break p001e0015;
		if (!CNDpresent(14)) break p001e0015;
		if (!CNDnotat(25)) break p001e0015;
		if (!CNDnotat(26)) break p001e0015;
		if (!CNDnotzero(64)) break p001e0015;
 		ACCwrite(260);
		{}

	}

	// _ _
	p001e0016:
	{
 		if (skipdoall('p001e0016')) break p001e0016;
		if (!CNDpresent(14)) break p001e0016;
		if (!CNDat(25)) break p001e0016;
 		ACCwrite(261);
		{}

	}

	// _ _
	p001e0017:
	{
 		if (skipdoall('p001e0017')) break p001e0017;
		if (!CNDat(12)) break p001e0017;
		if (!CNDnotzero(85)) break p001e0017;
 		ACCwrite(262);
 		ACCnewline();
		{}

	}

	// _ _
	p001e0018:
	{
 		if (skipdoall('p001e0018')) break p001e0018;
		if (!CNDat(13)) break p001e0018;
 		ACCprocess(31);
		{}

	}

	// _ _
	p001e0019:
	{
 		if (skipdoall('p001e0019')) break p001e0019;
		if (!CNDat(17)) break p001e0019;
		if (!CNDzero(106)) break p001e0019;
 		ACCset(106);
 		ACCplus(30,2);
		{}

	}

	// _ _
	p001e0020:
	{
 		if (skipdoall('p001e0020')) break p001e0020;
		if (!CNDgt(38,2)) break p001e0020;
		if (!CNDlt(38,8)) break p001e0020;
 		ACCwrite(263);
 		ACCprocess(84);
		{}

	}

	// _ _
	p001e0021:
	{
 		if (skipdoall('p001e0021')) break p001e0021;
		if (!CNDnotsame(25,38)) break p001e0021;
 		ACCclear(25);
		{}

	}

	// _ _
	p001e0022:
	{
 		if (skipdoall('p001e0022')) break p001e0022;
		if (!CNDpresent(22)) break p001e0022;
 		ACCprocess(80);
		{}

	}

	// _ _
	p001e0023:
	{
 		if (skipdoall('p001e0023')) break p001e0023;
		if (!CNDat(24)) break p001e0023;
		if (!CNDisat(11,25)) break p001e0023;
 		ACCwrite(264);
		{}

	}

	// _ _
	p001e0024:
	{
 		if (skipdoall('p001e0024')) break p001e0024;
		if (!CNDat(27)) break p001e0024;
		if (!CNDnotzero(26)) break p001e0024;
 		ACCwrite(265);
		{}

	}

	// _ _
	p001e0025:
	{
 		if (skipdoall('p001e0025')) break p001e0025;
		if (!CNDat(28)) break p001e0025;
 		ACCwrite(266);
		{}

	}

	// _ _
	p001e0026:
	{
 		if (skipdoall('p001e0026')) break p001e0026;
		if (!CNDat(28)) break p001e0026;
		if (!CNDzero(66)) break p001e0026;
 		ACCwrite(267);
 		ACCwrite(268);
		{}

	}

	// _ _
	p001e0027:
	{
 		if (skipdoall('p001e0027')) break p001e0027;
		if (!CNDat(28)) break p001e0027;
		if (!CNDeq(66,1)) break p001e0027;
 		ACCwrite(269);
 		ACCwrite(270);
		{}

	}

	// _ _
	p001e0028:
	{
 		if (skipdoall('p001e0028')) break p001e0028;
		if (!CNDnotzero(6)) break p001e0028;
		if (!CNDnotat(28)) break p001e0028;
 		ACCclear(6);
		{}

	}


}
}

function pro002()
{
process_restart=true;
pro002_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p002e0000:
	{
 		if (skipdoall('p002e0000')) break p002e0000;
		if (!CNDisat(14,26)) break p002e0000;
 		ACCdestroy(14);
		if (!CNDat(26)) break p002e0000;
 		ACCwrite(271);
		{}

	}

	// _ _
	p002e0001:
	{
 		if (skipdoall('p002e0001')) break p002e0001;
		if (!CNDat(12)) break p002e0001;
 		ACClet(67,2);
 		ACCprocess(23);
		if (!CNDnotzero(69)) break p002e0001;
 		ACCprocess(47);
		{}

	}

	// _ _
	p002e0002:
	{
 		if (skipdoall('p002e0002')) break p002e0002;
		if (!CNDat(13)) break p002e0002;
 		ACClet(67,13);
 		ACCprocess(23);
		if (!CNDnotzero(69)) break p002e0002;
 		ACCprocess(49);
		{}

	}

	// _ _
	p002e0003:
	{
 		if (skipdoall('p002e0003')) break p002e0003;
		if (!CNDat(13)) break p002e0003;
		if (!CNDnotzero(65)) break p002e0003;
 		ACCprocess(14);
		if (!CNDgt(65,1)) break p002e0003;
 		ACCprocess(31);
		{}

	}

	// _ _
	p002e0004:
	{
 		if (skipdoall('p002e0004')) break p002e0004;
		if (!CNDat(14)) break p002e0004;
		if (!CNDnotzero(102)) break p002e0004;
		if (!CNDisnotat(5,1)) break p002e0004;
 		ACCclear(102);
		{}

	}

	// _ _
	p002e0005:
	{
 		if (skipdoall('p002e0005')) break p002e0005;
		if (!CNDat(14)) break p002e0005;
		if (!CNDzero(102)) break p002e0005;
		if (!CNDisat(5,1)) break p002e0005;
 		ACCset(102);
 		ACCwrite(272);
 		ACCnewline();
		{}

	}

	// _ _
	p002e0006:
	{
 		if (skipdoall('p002e0006')) break p002e0006;
		if (!CNDat(14)) break p002e0006;
 		ACClet(67,1);
 		ACCprocess(23);
		if (!CNDnotzero(69)) break p002e0006;
 		ACCprocess(48);
		{}

	}

	// _ _
	p002e0007:
	{
 		if (skipdoall('p002e0007')) break p002e0007;
		if (!CNDat(19)) break p002e0007;
		if (!CNDzero(11)) break p002e0007;
 		ACCset(11);
 		ACCplus(30,2);
		{}

	}

	// _ _
	p002e0008:
	{
 		if (skipdoall('p002e0008')) break p002e0008;
		if (!CNDnotzero(25)) break p002e0008;
		if (!CNDnotcarr(16)) break p002e0008;
 		ACCwrite(273);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0009:
	{
 		if (skipdoall('p002e0009')) break p002e0009;
		if (!CNDnotzero(25)) break p002e0009;
 		ACCclear(25);
 		ACCwrite(274);
 		ACCnewline();
		{}

	}

	// _ _
	p002e0010:
	{
 		if (skipdoall('p002e0010')) break p002e0010;
 		ACClet(67,1);
 		ACCprocess(12);
 		ACClet(67,2);
 		ACCprocess(12);
		{}

	}

	// _ _
	p002e0011:
	{
 		if (skipdoall('p002e0011')) break p002e0011;
		if (!CNDzero(5)) break p002e0011;
 		ACClet(67,25);
 		ACCprocess(26);
		if (!CNDnotzero(69)) break p002e0011;
 		ACClet(5,3);
		{}

	}

	// _ _
	p002e0012:
	{
 		if (skipdoall('p002e0012')) break p002e0012;
		if (!CNDzero(5)) break p002e0012;
		if (!CNDat(25)) break p002e0012;
 		ACClet(5,3);
		{}

	}

	// _ _
	p002e0013:
	{
 		if (skipdoall('p002e0013')) break p002e0013;
		if (!CNDeq(5,1)) break p002e0013;
 		ACCprocess(90);
		{}

	}

	// _ _
	p002e0014:
	{
 		if (skipdoall('p002e0014')) break p002e0014;
		if (!CNDat(28)) break p002e0014;
		if (!CNDzero(6)) break p002e0014;
		if (!CNDnotworn(29)) break p002e0014;
 		ACClet(6,5);
		{}

	}

	// _ _
	p002e0015:
	{
 		if (skipdoall('p002e0015')) break p002e0015;
		if (!CNDat(28)) break p002e0015;
		if (!CNDnotworn(29)) break p002e0015;
		if (!CNDeq(6,3)) break p002e0015;
 		ACCwrite(275);
 		ACCnewline();
		{}

	}

	// _ _
	p002e0016:
	{
 		if (skipdoall('p002e0016')) break p002e0016;
		if (!CNDat(28)) break p002e0016;
		if (!CNDnotworn(29)) break p002e0016;
		if (!CNDeq(6,1)) break p002e0016;
 		ACCwrite(276);
 		ACCnewline();
		if (!CNDisnotat(11,25)) break p002e0016;
 		ACCgoto(25);
 		ACCdesc();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0017:
	{
 		if (skipdoall('p002e0017')) break p002e0017;
		if (!CNDat(28)) break p002e0017;
		if (!CNDnotworn(29)) break p002e0017;
		if (!CNDeq(6,1)) break p002e0017;
 		ACCgoto(26);
 		ACCdesc();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0018:
	{
 		if (skipdoall('p002e0018')) break p002e0018;
		if (!CNDat(32)) break p002e0018;
		if (!CNDnotworn(29)) break p002e0018;
 		ACCwrite(277);
 		ACCnewline();
		if (!CNDisnotat(11,25)) break p002e0018;
 		ACCgoto(25);
 		ACCdesc();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0019:
	{
 		if (skipdoall('p002e0019')) break p002e0019;
		if (!CNDat(32)) break p002e0019;
		if (!CNDnotworn(29)) break p002e0019;
 		ACCgoto(26);
 		ACCdesc();
		break pro002_restart;
		{}

	}


}
}

function pro003()
{
process_restart=true;
pro003_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p003e0000:
	{
 		if (skipdoall('p003e0000')) break p003e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p003e0001:
	{
 		if (skipdoall('p003e0001')) break p003e0001;
		if (!CNDsame(38,54)) break p003e0001;
		if (!CNDlt(34,50)) break p003e0001;
 		ACCsysmess(142);
 		ACCnewtext();
 		ACCdone();
		break pro003_restart;
		{}

	}

	// _ _
	p003e0002:
	{
 		if (skipdoall('p003e0002')) break p003e0002;
		if (!CNDnotsame(54,38)) break p003e0002;
		if (!CNDnoteq(54,254)) break p003e0002;
		if (!CNDnoteq(54,253)) break p003e0002;
 		ACCsysmess(100);
 		ACCsysmess(110);
 		ACCsysmess(104);
 		ACCnewtext();
 		ACCdone();
		break pro003_restart;
		{}

	}

	// _ _
	p003e0003:
	{
 		if (skipdoall('p003e0003')) break p003e0003;
 		ACCprocess(38);
		if (!CNDnotzero(16)) break p003e0003;
 		ACCsysmess(114);
 		ACCprocess(88);
 		ACCsysmess(115);
 		ACCprocess(87);
 		ACCwrite(278);
 		ACCdone();
		break pro003_restart;
		{}

	}

	// _ _
	p003e0004:
	{
 		if (skipdoall('p003e0004')) break p003e0004;
 		ACCsysmess(99);
 		ACCsysmess(110);
 		ACCprocess(6);
 		ACCwrite(279);
 		ACCnewtext();
 		ACCdone();
		break pro003_restart;
		{}

	}


}
}

function pro004()
{
process_restart=true;
pro004_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p004e0000:
	{
 		if (skipdoall('p004e0000')) break p004e0000;
 		ACCsysmess(111);
 		ACCprocess(64);
 		ACCwrite(280);
 		ACCdone();
		break pro004_restart;
		{}

	}


}
}

function pro005()
{
process_restart=true;
pro005_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p005e0000:
	{
 		if (skipdoall('p005e0000')) break p005e0000;
		if (!CNDeq(34,9)) break p005e0000;
 		ACCprocess(78);
 		ACCdone();
		break pro005_restart;
		{}

	}

	// _ _
	p005e0001:
	{
 		if (skipdoall('p005e0001')) break p005e0001;
		if (!CNDeq(34,10)) break p005e0001;
 		ACCprocess(79);
 		ACCdone();
		break pro005_restart;
		{}

	}

	// _ _
	p005e0002:
	{
 		if (skipdoall('p005e0002')) break p005e0002;
 		ACCcopyff(34,33);
 		ACCprocess(81);
 		ACCdone();
		break pro005_restart;
		{}

	}


}
}

function pro006()
{
process_restart=true;
pro006_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p006e0000:
	{
 		if (skipdoall('p006e0000')) break p006e0000;
 		ACCprocess(38);
		if (!CNDeq(21,1)) break p006e0000;
 		ACCwrite(281);
 		ACCdone();
		break pro006_restart;
		{}

	}

	// _ _
	p006e0001:
	{
 		if (skipdoall('p006e0001')) break p006e0001;
 		ACCwrite(282);
 		ACCprocess(87);
 		ACCdone();
		break pro006_restart;
		{}

	}


}
}

function pro007()
{
process_restart=true;
pro007_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p007e0000:
	{
 		if (skipdoall('p007e0000')) break p007e0000;
 		ACCprocess(38);
		if (!CNDeq(21,1)) break p007e0000;
 		ACCwrite(283);
 		ACCdone();
		break pro007_restart;
		{}

	}

	// _ _
	p007e0001:
	{
 		if (skipdoall('p007e0001')) break p007e0001;
 		ACCwrite(284);
 		ACCprocess(87);
 		ACCdone();
		break pro007_restart;
		{}

	}


}
}

function pro008()
{
process_restart=true;
pro008_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p008e0000:
	{
 		if (skipdoall('p008e0000')) break p008e0000;
 		ACCprocess(38);
		if (!CNDnotzero(22)) break p008e0000;
 		ACCdone();
		break pro008_restart;
		{}

	}

	// _ _
	p008e0001:
	{
 		if (skipdoall('p008e0001')) break p008e0001;
 		ACCwrite(285);
		{}

	}

	// _ _
	p008e0002:
	{
 		if (skipdoall('p008e0002')) break p008e0002;
		if (!CNDnoteq(21,1)) break p008e0002;
 		ACCprocess(87);
		{}

	}

	// _ _
	p008e0003:
	{
 		if (skipdoall('p008e0003')) break p008e0003;
 		ACCdone();
		break pro008_restart;
		{}

	}


}
}

function pro009()
{
process_restart=true;
pro009_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p009e0000:
	{
 		if (skipdoall('p009e0000')) break p009e0000;
		if (!CNDnoun2(55)) break p009e0000;
		if (!CNDcarried(3)) break p009e0000;
		if (!CNDzero(85)) break p009e0000;
 		ACCset(85);
 		ACCdestroy(3);
 		ACCwrite(286);
 		ACCnewline();
 		ACCdone();
		break pro009_restart;
		{}

	}

	// _ _
	p009e0001:
	{
 		if (skipdoall('p009e0001')) break p009e0001;
		if (!CNDnoun2(55)) break p009e0001;
		if (!CNDnotzero(85)) break p009e0001;
 		ACCwrite(287);
 		ACCnewline();
 		ACCdone();
		break pro009_restart;
		{}

	}

	// _ _
	p009e0002:
	{
 		if (skipdoall('p009e0002')) break p009e0002;
		if (!CNDnoun2(56)) break p009e0002;
		if (!CNDcarried(3)) break p009e0002;
 		ACCwrite(288);
 		ACCnewline();
 		ACCdone();
		break pro009_restart;
		{}

	}

	// _ _
	p009e0003:
	{
 		if (skipdoall('p009e0003')) break p009e0003;
		if (!CNDnotcarr(3)) break p009e0003;
 		ACCwrite(289);
 		ACCnewline();
 		ACCdone();
		break pro009_restart;
		{}

	}

	// _ _
	p009e0004:
	{
 		if (skipdoall('p009e0004')) break p009e0004;
		if (!CNDprep(3)) break p009e0004;
 		ACCwrite(290);
 		ACCnewline();
 		ACCdone();
		break pro009_restart;
		{}

	}

	// _ _
	p009e0005:
	{
 		if (skipdoall('p009e0005')) break p009e0005;
 		ACCwrite(291);
 		ACCnewline();
 		ACCdone();
		break pro009_restart;
		{}

	}


}
}

function pro010()
{
process_restart=true;
pro010_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p010e0000:
	{
 		if (skipdoall('p010e0000')) break p010e0000;
		if (!CNDzero(98)) break p010e0000;
 		ACCset(98);
 		ACCplus(30,1);
		{}

	}

	// _ _
	p010e0001:
	{
 		if (skipdoall('p010e0001')) break p010e0001;
 		ACCwrite(292);
 		ACCnewline();
		if (!CNDat(28)) break p010e0001;
 		ACCplace(30,30);
 		ACCgoto(30);
 		ACCdesc();
		break pro010_restart;
		{}

	}

	// _ _
	p010e0002:
	{
 		if (skipdoall('p010e0002')) break p010e0002;
		if (!CNDat(30)) break p010e0002;
 		ACCplace(30,28);
 		ACCgoto(28);
 		ACCdesc();
		break pro010_restart;
		{}

	}


}
}

function pro011()
{
process_restart=true;
pro011_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p011e0000:
	{
 		if (skipdoall('p011e0000')) break p011e0000;
		if (!CNDsame(38,90)) break p011e0000;
 		ACCwrite(293);
		{}

	}

	// _ _
	p011e0001:
	{
 		if (skipdoall('p011e0001')) break p011e0001;
		if (!CNDzero(64)) break p011e0001;
		if (!CNDsame(38,90)) break p011e0001;
 		ACCwrite(294);
 		ACCnewline();
 		ACCdone();
		break pro011_restart;
		{}

	}

	// _ _
	p011e0002:
	{
 		if (skipdoall('p011e0002')) break p011e0002;
		if (!CNDnotzero(64)) break p011e0002;
 		ACCswap(14,15);
		if (!CNDsame(38,90)) break p011e0002;
 		ACCwrite(295);
 		ACCnewline();
 		ACCdone();
		break pro011_restart;
		{}

	}


}
}

function pro012()
{
process_restart=true;
pro012_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p012e0000:
	{
 		if (skipdoall('p012e0000')) break p012e0000;
 		ACCclear(28);
 		ACCcopyff(67,89);
		{}

	}

	// _ _
	p012e0001:
	{
 		if (skipdoall('p012e0001')) break p012e0001;
		if (!CNDeq(89,1)) break p012e0001;
 		ACClet(51,17);
		{}

	}

	// _ _
	p012e0002:
	{
 		if (skipdoall('p012e0002')) break p012e0002;
		if (!CNDeq(89,2)) break p012e0002;
 		ACClet(51,18);
		{}

	}

	// _ _
	p012e0003:
	{
 		if (skipdoall('p012e0003')) break p012e0003;
 		ACCprocess(37);
 		ACCcopyff(12,90);
 		ACCcopyff(90,67);
 		ACCprocess(59);
		{}

	}

	// _ _
	p012e0004:
	{
 		if (skipdoall('p012e0004')) break p012e0004;
		if (!CNDlt(69,3)) break p012e0004;
 		ACCcopyff(90,63);
		{}

	}

	// _ _
	p012e0005:
	{
 		if (skipdoall('p012e0005')) break p012e0005;
		if (!CNDgt(69,2)) break p012e0005;
 		ACCcopyff(69,63);
		{}

	}

	// _ _
	p012e0006:
	{
 		if (skipdoall('p012e0006')) break p012e0006;
		if (!CNDzero(69)) break p012e0006;
		if (!CNDsame(38,90)) break p012e0006;
 		ACCwrite(296);
 		ACCnewline();
		{}

	}

	// _ _
	p012e0007:
	{
 		if (skipdoall('p012e0007')) break p012e0007;
		if (!CNDeq(69,1)) break p012e0007;
 		ACCprocess(11);
		{}

	}

	// _ _
	p012e0008:
	{
 		if (skipdoall('p012e0008')) break p012e0008;
		if (!CNDeq(69,2)) break p012e0008;
		if (!CNDsame(38,90)) break p012e0008;
 		ACCcopyff(38,25);
 		ACCwrite(297);
 		ACCnewline();
		{}

	}

	// _ _
	p012e0009:
	{
 		if (skipdoall('p012e0009')) break p012e0009;
		if (!CNDsame(38,90)) break p012e0009;
		if (!CNDnotsame(90,63)) break p012e0009;
 		ACCwrite(298);
 		ACCcopyff(63,67);
 		ACCprocess(13);
 		ACCset(28);
		{}

	}

	// _ _
	p012e0010:
	{
 		if (skipdoall('p012e0010')) break p012e0010;
		if (!CNDsame(38,63)) break p012e0010;
		if (!CNDnotsame(90,63)) break p012e0010;
 		ACCwrite(299);
 		ACCcopyff(90,67);
 		ACCprocess(13);
 		ACCset(28);
		{}

	}

	// _ _
	p012e0011:
	{
 		if (skipdoall('p012e0011')) break p012e0011;
		if (!CNDnotsame(90,63)) break p012e0011;
 		ACCcopyff(63,67);
 		ACCprocess(42);
		if (!CNDnotzero(69)) break p012e0011;
 		ACCcopyff(90,63);
		if (!CNDnotzero(28)) break p012e0011;
 		ACCwrite(300);
 		ACCnewline();
		{}

	}

	// _ _
	p012e0012:
	{
 		if (skipdoall('p012e0012')) break p012e0012;
		if (!CNDnotsame(90,63)) break p012e0012;
		if (!CNDzero(69)) break p012e0012;
		if (!CNDnotzero(28)) break p012e0012;
 		ACCwrite(301);
		{}

	}

	// _ _
	p012e0013:
	{
 		if (skipdoall('p012e0013')) break p012e0013;
		if (!CNDeq(89,1)) break p012e0013;
 		ACCcopyff(63,13);
 		ACCcopyfo(63,17);
		{}

	}

	// _ _
	p012e0014:
	{
 		if (skipdoall('p012e0014')) break p012e0014;
		if (!CNDeq(89,2)) break p012e0014;
 		ACCcopyff(63,14);
 		ACCcopyfo(63,18);
		{}

	}


}
}

function pro013()
{
process_restart=true;
pro013_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p013e0000:
	{
 		if (skipdoall('p013e0000')) break p013e0000;
		if (!CNDat(6)) break p013e0000;
		if (!CNDeq(67,3)) break p013e0000;
 		ACCwrite(302);
		{}

	}

	// _ _
	p013e0001:
	{
 		if (skipdoall('p013e0001')) break p013e0001;
		if (!CNDat(6)) break p013e0001;
		if (!CNDeq(67,7)) break p013e0001;
 		ACCwrite(303);
		{}

	}

	// _ _
	p013e0002:
	{
 		if (skipdoall('p013e0002')) break p013e0002;
		if (!CNDat(7)) break p013e0002;
		if (!CNDeq(67,6)) break p013e0002;
 		ACCwrite(304);
		{}

	}

	// _ _
	p013e0003:
	{
 		if (skipdoall('p013e0003')) break p013e0003;
		if (!CNDat(7)) break p013e0003;
		if (!CNDeq(67,3)) break p013e0003;
 		ACCwrite(305);
		{}

	}

	// _ _
	p013e0004:
	{
 		if (skipdoall('p013e0004')) break p013e0004;
		if (!CNDat(3)) break p013e0004;
		if (!CNDeq(67,7)) break p013e0004;
 		ACCwrite(306);
		{}

	}

	// _ _
	p013e0005:
	{
 		if (skipdoall('p013e0005')) break p013e0005;
		if (!CNDat(3)) break p013e0005;
		if (!CNDeq(67,6)) break p013e0005;
 		ACCwrite(307);
		{}

	}

	// _ _
	p013e0006:
	{
 		if (skipdoall('p013e0006')) break p013e0006;
		if (!CNDat(3)) break p013e0006;
		if (!CNDeq(67,4)) break p013e0006;
 		ACCwrite(308);
		{}

	}

	// _ _
	p013e0007:
	{
 		if (skipdoall('p013e0007')) break p013e0007;
		if (!CNDat(4)) break p013e0007;
		if (!CNDeq(67,3)) break p013e0007;
 		ACCwrite(309);
		{}

	}

	// _ _
	p013e0008:
	{
 		if (skipdoall('p013e0008')) break p013e0008;
		if (!CNDat(4)) break p013e0008;
		if (!CNDeq(67,5)) break p013e0008;
 		ACCwrite(310);
		{}

	}

	// _ _
	p013e0009:
	{
 		if (skipdoall('p013e0009')) break p013e0009;
		if (!CNDat(5)) break p013e0009;
		if (!CNDeq(67,4)) break p013e0009;
 		ACCwrite(311);
		{}

	}


}
}

function pro014()
{
process_restart=true;
pro014_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p014e0000:
	{
 		if (skipdoall('p014e0000')) break p014e0000;
 		ACCminus(61,1);
		{}

	}

	// _ _
	p014e0001:
	{
 		if (skipdoall('p014e0001')) break p014e0001;
		if (!CNDzero(61)) break p014e0001;
 		ACCclear(65);
 		ACCwrite(312);
 		ACCnewline();
 		ACCdone();
		break pro014_restart;
		{}

	}

	// _ _
	p014e0002:
	{
 		if (skipdoall('p014e0002')) break p014e0002;
		if (!CNDeq(61,2)) break p014e0002;
 		ACClet(65,1);
 		ACCwrite(313);
 		ACCnewline();
 		ACCdone();
		break pro014_restart;
		{}

	}

	// _ _
	p014e0003:
	{
 		if (skipdoall('p014e0003')) break p014e0003;
		if (!CNDeq(65,2)) break p014e0003;
 		ACClet(65,4);
 		ACCdone();
		break pro014_restart;
		{}

	}

	// _ _
	p014e0004:
	{
 		if (skipdoall('p014e0004')) break p014e0004;
		if (!CNDeq(65,4)) break p014e0004;
 		ACClet(65,3);
 		ACCdone();
		break pro014_restart;
		{}

	}

	// _ _
	p014e0005:
	{
 		if (skipdoall('p014e0005')) break p014e0005;
		if (!CNDeq(65,3)) break p014e0005;
 		ACClet(65,5);
 		ACCdone();
		break pro014_restart;
		{}

	}

	// _ _
	p014e0006:
	{
 		if (skipdoall('p014e0006')) break p014e0006;
		if (!CNDeq(65,5)) break p014e0006;
 		ACClet(65,2);
		{}

	}


}
}

function pro015()
{
process_restart=true;
pro015_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p015e0000:
	{
 		if (skipdoall('p015e0000')) break p015e0000;
		if (!CNDzero(65)) break p015e0000;
 		ACClet(65,1);
 		ACClet(61,2);
 		ACCwrite(314);
 		ACCnewline();
 		ACCdone();
		break pro015_restart;
		{}

	}

	// _ _
	p015e0001:
	{
 		if (skipdoall('p015e0001')) break p015e0001;
		if (!CNDeq(65,1)) break p015e0001;
 		ACClet(61,10);
 		ACClet(65,4);
 		ACCwrite(315);
 		ACCnewline();
 		ACCdone();
		break pro015_restart;
		{}

	}

	// _ _
	p015e0002:
	{
 		if (skipdoall('p015e0002')) break p015e0002;
		if (!CNDgt(65,1)) break p015e0002;
 		ACClet(61,10);
 		ACCwrite(316);
 		ACCnewline();
 		ACCdone();
		break pro015_restart;
		{}

	}


}
}

function pro016()
{
process_restart=true;
pro016_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p016e0000:
	{
 		if (skipdoall('p016e0000')) break p016e0000;
 		ACCprocess(26);
		if (!CNDzero(69)) break p016e0000;
 		ACCdone();
		break pro016_restart;
		{}

	}

	// _ _
	p016e0001:
	{
 		if (skipdoall('p016e0001')) break p016e0001;
 		ACCprocess(41);
 		ACCcopyff(38,104);
		if (!CNDnoteq(67,253)) break p016e0001;
		if (!CNDnoteq(67,255)) break p016e0001;
		if (!CNDnoteq(67,252)) break p016e0001;
		if (!CNDnoteq(67,254)) break p016e0001;
 		ACCcopyff(67,38);
		{}

	}

	// _ _
	p016e0002:
	{
 		if (skipdoall('p016e0002')) break p016e0002;
 		ACCclear(69);
 		ACCprocess(17);
 		ACCcopyff(104,38);
 		ACCprocess(77);
 		ACCdone();
		break pro016_restart;
		{}

	}


}
}

function pro017()
{
process_restart=true;
pro017_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p017e0000:
	{
 		if (skipdoall('p017e0000')) break p017e0000;
		if (!CNDnoteq(67,253)) break p017e0000;
		if (!CNDnoteq(67,252)) break p017e0000;
		if (!CNDnoteq(67,254)) break p017e0000;
		entry_for_doall = 'p017e0001';
		process_in_doall = 17;
 		ACCdoall(255);
		break pro017_restart;
		{}

	}

	// _ _
	p017e0001:
	{
 		if (skipdoall('p017e0001')) break p017e0001;
		if (!CNDeq(67,253)) break p017e0001;
		entry_for_doall = 'p017e0002';
		process_in_doall = 17;
 		ACCdoall(253);
		break pro017_restart;
		{}

	}

	// _ _
	p017e0002:
	{
 		if (skipdoall('p017e0002')) break p017e0002;
		if (!CNDeq(67,252)) break p017e0002;
		entry_for_doall = 'p017e0003';
		process_in_doall = 17;
 		ACCdoall(252);
		break pro017_restart;
		{}

	}

	// _ _
	p017e0003:
	{
 		if (skipdoall('p017e0003')) break p017e0003;
		if (!CNDeq(67,254)) break p017e0003;
		entry_for_doall = 'p017e0004';
		process_in_doall = 17;
 		ACCdoall(254);
		break pro017_restart;
		{}

	}

	// _ _
	p017e0004:
	{
 		if (skipdoall('p017e0004')) break p017e0004;
		if (!CNDzero(69)) break p017e0004;
 		ACCprocess(38);
		if (!CNDnotzero(19)) break p017e0004;
		if (!CNDnotzero(16)) break p017e0004;
		if (!CNDsame(68,51)) break p017e0004;
 		ACCcopyff(68,69);
 		ACCcopyff(34,70);
 		ACCcopyff(35,71);
 		ACCdone();
		break pro017_restart;
		{}

	}


}
}

function pro018()
{
process_restart=true;
pro018_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p018e0000:
	{
 		if (skipdoall('p018e0000')) break p018e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p018e0001:
	{
 		if (skipdoall('p018e0001')) break p018e0001;
		if (!CNDsame(38,54)) break p018e0001;
		if (!CNDlt(34,50)) break p018e0001;
 		ACCsysmess(142);
 		ACCnewtext();
 		ACCdone();
		break pro018_restart;
		{}

	}

	// _ _
	p018e0002:
	{
 		if (skipdoall('p018e0002')) break p018e0002;
		if (!CNDnotsame(54,38)) break p018e0002;
		if (!CNDnoteq(54,254)) break p018e0002;
		if (!CNDnoteq(54,253)) break p018e0002;
 		ACCsysmess(100);
 		ACCsysmess(112);
 		ACCsysmess(104);
 		ACCnewtext();
 		ACCdone();
		break pro018_restart;
		{}

	}

	// _ _
	p018e0003:
	{
 		if (skipdoall('p018e0003')) break p018e0003;
 		ACCprocess(38);
		if (!CNDnotzero(17)) break p018e0003;
		if (!CNDzero(16)) break p018e0003;
 		ACCsysmess(114);
 		ACCprocess(88);
 		ACCsysmess(116);
 		ACCprocess(87);
 		ACCwrite(317);
 		ACCdone();
		break pro018_restart;
		{}

	}

	// _ _
	p018e0004:
	{
 		if (skipdoall('p018e0004')) break p018e0004;
 		ACCsysmess(99);
 		ACCsysmess(112);
 		ACCprocess(6);
 		ACCwrite(318);
 		ACCnewtext();
 		ACCdone();
		break pro018_restart;
		{}

	}


}
}

function pro019()
{
process_restart=true;
pro019_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p019e0000:
	{
 		if (skipdoall('p019e0000')) break p019e0000;
 		ACCsysmess(113);
 		ACCprocess(64);
 		ACCwrite(319);
 		ACCdone();
		break pro019_restart;
		{}

	}


}
}

function pro020()
{
process_restart=true;
pro020_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p020e0000:
	{
 		if (skipdoall('p020e0000')) break p020e0000;
		if (!CNDeq(44,255)) break p020e0000;
 		ACClet(69,255);
 		ACCdone();
		break pro020_restart;
		{}

	}

	// _ _
	p020e0001:
	{
 		if (skipdoall('p020e0001')) break p020e0001;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCcopyff(51,69);
 		ACCprocess(77);
 		ACCdone();
		break pro020_restart;
		{}

	}


}
}

function pro021()
{
process_restart=true;
pro021_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p021e0000:
	{
 		if (skipdoall('p021e0000')) break p021e0000;
		if (!CNDworn(19)) break p021e0000;
		if (!CNDpresent(11)) break p021e0000;
		if (!CNDcarried(16)) break p021e0000;
 		ACCwrite(320);
 		ACCnewline();
 		ACCplace(12,254);
 		ACCprocess(75);
 		ACCdone();
		break pro021_restart;
		{}

	}

	// _ _
	p021e0001:
	{
 		if (skipdoall('p021e0001')) break p021e0001;
		if (!CNDpresent(11)) break p021e0001;
		if (!CNDnotcarr(16)) break p021e0001;
 		ACCwrite(321);
 		ACCnewline();
 		ACCdone();
		break pro021_restart;
		{}

	}

	// _ _
	p021e0002:
	{
 		if (skipdoall('p021e0002')) break p021e0002;
		if (!CNDnotworn(19)) break p021e0002;
 		ACCwrite(322);
 		ACCnewline();
 		ACCdone();
		break pro021_restart;
		{}

	}

	// _ _
	p021e0003:
	{
 		if (skipdoall('p021e0003')) break p021e0003;
 		ACCwrite(323);
 		ACCnewline();
 		ACCplace(12,254);
 		ACCprocess(75);
 		ACCdone();
		break pro021_restart;
		{}

	}


}
}

function pro022()
{
process_restart=true;
pro022_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p022e0000:
	{
 		if (skipdoall('p022e0000')) break p022e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p022e0001:
	{
 		if (skipdoall('p022e0001')) break p022e0001;
		if (!CNDnoteq(51,12)) break p022e0001;
		if (!CNDcarried(12)) break p022e0001;
 		ACCwrite(324);
 		ACCnewline();
 		ACCdone();
		break pro022_restart;
		{}

	}

	// _ _
	p022e0002:
	{
 		if (skipdoall('p022e0002')) break p022e0002;
		if (!CNDsame(38,54)) break p022e0002;
		if (!CNDlt(34,50)) break p022e0002;
 		ACCsysmess(142);
 		ACCdone();
		break pro022_restart;
		{}

	}

	// _ _
	p022e0003:
	{
 		if (skipdoall('p022e0003')) break p022e0003;
		if (!CNDeq(54,254)) break p022e0003;
 		ACCsysmess(25);
 		ACCprocess(64);
 		ACCwrite(325);
 		ACCnewtext();
 		ACCdone();
		break pro022_restart;
		{}

	}

	// _ _
	p022e0004:
	{
 		if (skipdoall('p022e0004')) break p022e0004;
		if (!CNDeq(54,253)) break p022e0004;
 		ACCsysmess(25);
 		ACCprocess(64);
 		ACCwrite(326);
 		ACCnewtext();
 		ACCdone();
		break pro022_restart;
		{}

	}

	// _ _
	p022e0005:
	{
 		if (skipdoall('p022e0005')) break p022e0005;
		if (!CNDsame(54,38)) break p022e0005;
 		ACCprocess(38);
		if (!CNDnotzero(20)) break p022e0005;
 		ACCsysmess(99);
 		ACCsysmess(107);
 		ACCprocess(64);
 		ACCsysmess(101);
 		ACCprocess(88);
 		ACCsysmess(102);
 		ACCprocess(87);
 		ACCsysmess(103);
 		ACCnewtext();
 		ACCdone();
		break pro022_restart;
		{}

	}

	// _ _
	p022e0006:
	{
 		if (skipdoall('p022e0006')) break p022e0006;
		if (!CNDsame(54,38)) break p022e0006;
 		ACCsysmess(36);
 		ACCprocess(64);
 		ACCwrite(327);
 		ACCputo(254);
 		ACCdone();
		break pro022_restart;
		{}

	}

	// _ _
	p022e0007:
	{
 		if (skipdoall('p022e0007')) break p022e0007;
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p022e0007;
 		ACCcopyff(70,44);
 		ACCcopyff(71,45);
 		ACCprocess(83);
 		ACCdone();
		break pro022_restart;
		{}

	}

	// _ _
	p022e0008:
	{
 		if (skipdoall('p022e0008')) break p022e0008;
 		ACCsysmess(100);
 		ACCsysmess(26);
 		ACCsysmess(104);
 		ACCnewtext();
 		ACCdone();
		break pro022_restart;
		{}

	}


}
}

function pro023()
{
process_restart=true;
pro023_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p023e0000:
	{
 		if (skipdoall('p023e0000')) break p023e0000;
 		ACCclear(70);
 		ACCprocess(41);
		if (!CNDeq(67,254)) break p023e0000;
 		ACClet(70,1);
 		ACCcopyff(1,69);
 		ACCdone();
		break pro023_restart;
		{}

	}

	// _ _
	p023e0001:
	{
 		if (skipdoall('p023e0001')) break p023e0001;
 		ACCcopyff(38,104);
 		ACCclear(69);
		if (!CNDnoteq(67,253)) break p023e0001;
		if (!CNDnoteq(67,255)) break p023e0001;
		if (!CNDnoteq(67,252)) break p023e0001;
 		ACCcopyff(67,38);
		{}

	}

	// _ _
	p023e0002:
	{
 		if (skipdoall('p023e0002')) break p023e0002;
 		ACCprocess(24);
 		ACCcopyff(104,38);
 		ACCprocess(77);
 		ACCdone();
		break pro023_restart;
		{}

	}


}
}

function pro024()
{
process_restart=true;
pro024_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p024e0000:
	{
 		if (skipdoall('p024e0000')) break p024e0000;
		if (!CNDnoteq(67,253)) break p024e0000;
		if (!CNDnoteq(67,252)) break p024e0000;
		entry_for_doall = 'p024e0001';
		process_in_doall = 24;
 		ACCdoall(255);
		break pro024_restart;
		{}

	}

	// _ _
	p024e0001:
	{
 		if (skipdoall('p024e0001')) break p024e0001;
		if (!CNDeq(67,253)) break p024e0001;
		entry_for_doall = 'p024e0002';
		process_in_doall = 24;
 		ACCdoall(253);
		break pro024_restart;
		{}

	}

	// _ _
	p024e0002:
	{
 		if (skipdoall('p024e0002')) break p024e0002;
		if (!CNDeq(67,252)) break p024e0002;
		entry_for_doall = 'p024e0003';
		process_in_doall = 24;
 		ACCdoall(252);
		break pro024_restart;
		{}

	}

	// _ _
	p024e0003:
	{
 		if (skipdoall('p024e0003')) break p024e0003;
 		ACCprocess(38);
		if (!CNDzero(23)) break p024e0003;
 		ACCplus(69,1);
		{}

	}

	// _ _
	p024e0004:
	{
 		if (skipdoall('p024e0004')) break p024e0004;
		if (!CNDnotzero(23)) break p024e0004;
		if (!CNDeq(50,254)) break p024e0004;
 		ACCplus(69,1);
		{}

	}

	// _ _
	p024e0005:
	{
 		if (skipdoall('p024e0005')) break p024e0005;
		if (!CNDnotzero(23)) break p024e0005;
		if (!CNDeq(50,253)) break p024e0005;
 		ACCplus(69,1);
		{}

	}

	// _ _
	p024e0006:
	{
 		if (skipdoall('p024e0006')) break p024e0006;
		if (!CNDzero(70)) break p024e0006;
 		ACCcopyff(21,70);
		{}

	}


}
}

function pro025()
{
process_restart=true;
pro025_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p025e0000:
	{
 		if (skipdoall('p025e0000')) break p025e0000;
		if (!CNDnoteq(67,253)) break p025e0000;
		if (!CNDnoteq(67,252)) break p025e0000;
		entry_for_doall = 'p025e0001';
		process_in_doall = 25;
 		ACCdoall(255);
		break pro025_restart;
		{}

	}

	// _ _
	p025e0001:
	{
 		if (skipdoall('p025e0001')) break p025e0001;
		if (!CNDeq(67,253)) break p025e0001;
		entry_for_doall = 'p025e0002';
		process_in_doall = 25;
 		ACCdoall(253);
		break pro025_restart;
		{}

	}

	// _ _
	p025e0002:
	{
 		if (skipdoall('p025e0002')) break p025e0002;
		if (!CNDeq(67,252)) break p025e0002;
		entry_for_doall = 'p025e0003';
		process_in_doall = 25;
 		ACCdoall(252);
		break pro025_restart;
		{}

	}

	// _ _
	p025e0003:
	{
 		if (skipdoall('p025e0003')) break p025e0003;
 		ACCplus(69,1);
		if (!CNDzero(70)) break p025e0003;
 		ACCcopyff(21,70);
		{}

	}


}
}

function pro026()
{
process_restart=true;
pro026_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p026e0000:
	{
 		if (skipdoall('p026e0000')) break p026e0000;
 		ACCclear(70);
 		ACCprocess(41);
		if (!CNDeq(67,254)) break p026e0000;
 		ACClet(70,1);
 		ACCcopyff(1,69);
 		ACCdone();
		break pro026_restart;
		{}

	}

	// _ _
	p026e0001:
	{
 		if (skipdoall('p026e0001')) break p026e0001;
 		ACCcopyff(38,104);
 		ACCclear(69);
		if (!CNDnoteq(67,253)) break p026e0001;
		if (!CNDnoteq(67,255)) break p026e0001;
		if (!CNDnoteq(67,252)) break p026e0001;
 		ACCcopyff(67,38);
		{}

	}

	// _ _
	p026e0002:
	{
 		if (skipdoall('p026e0002')) break p026e0002;
 		ACCprocess(25);
 		ACCcopyff(104,38);
 		ACCprocess(77);
 		ACCdone();
		break pro026_restart;
		{}

	}


}
}

function pro027()
{
process_restart=true;
pro027_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p027e0000:
	{
 		if (skipdoall('p027e0000')) break p027e0000;
 		ACCprocess(41);
 		ACClet(34,73);
 		ACClet(35,4);
 		ACCwhato();
 		ACCcopyff(54,87);
 		ACCprocess(77);
		{}

	}


}
}

function pro028()
{
process_restart=true;
pro028_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p028e0000:
	{
 		if (skipdoall('p028e0000')) break p028e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p028e0001:
	{
 		if (skipdoall('p028e0001')) break p028e0001;
 		ACCprocess(61);
		if (!CNDzero(69)) break p028e0001;
 		ACCsysmess(28);
 		ACCnewtext();
 		ACCdone();
		break pro028_restart;
		{}

	}

	// _ _
	p028e0002:
	{
 		if (skipdoall('p028e0002')) break p028e0002;
 		ACCprocess(20);
		if (!CNDeq(69,255)) break p028e0002;
 		ACCsysmess(127);
 		ACCprocess(64);
 		ACCwrite(328);
 		ACCnewtext();
 		ACCdone();
		break pro028_restart;
		{}

	}

	// _ _
	p028e0003:
	{
 		if (skipdoall('p028e0003')) break p028e0003;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCcopyff(54,104);
 		ACCprocess(77);
		if (!CNDnotsame(104,38)) break p028e0003;
		if (!CNDnoteq(104,254)) break p028e0003;
		if (!CNDnoteq(104,253)) break p028e0003;
 		ACCsysmess(127);
 		ACCprocess(64);
 		ACCwrite(329);
 		ACCnewtext();
 		ACCdone();
		break pro028_restart;
		{}

	}

	// _ _
	p028e0004:
	{
 		if (skipdoall('p028e0004')) break p028e0004;
		if (!CNDgt(44,49)) break p028e0004;
 		ACCsysmess(99);
 		ACCsysmess(123);
 		ACCprocess(64);
 		ACCsysmess(124);
 		ACCprocess(70);
 		ACCwrite(330);
 		ACCnewtext();
 		ACCdone();
		break pro028_restart;
		{}

	}

	// _ _
	p028e0005:
	{
 		if (skipdoall('p028e0005')) break p028e0005;
		if (!CNDeq(54,253)) break p028e0005;
 		ACCsysmess(99);
 		ACCsysmess(123);
 		ACCprocess(64);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCwrite(331);
 		ACCprocess(77);
 		ACCprocess(72);
 		ACCsysmess(106);
 		ACCprocess(87);
 		ACCwrite(332);
 		ACCnewtext();
 		ACCdone();
		break pro028_restart;
		{}

	}

	// _ _
	p028e0006:
	{
 		if (skipdoall('p028e0006')) break p028e0006;
 		ACCprocess(61);
		if (!CNDnoteq(69,254)) break p028e0006;
		if (!CNDgt(70,49)) break p028e0006;
 		ACCsysmess(99);
 		ACCsysmess(123);
 		ACCprocess(64);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCwrite(333);
 		ACCprocess(77);
 		ACCsysmess(146);
 		ACCprocess(88);
 		ACCsysmess(136);
 		ACCprocess(41);
 		ACCcopyff(70,34);
 		ACCcopyff(71,35);
 		ACCprocess(64);
 		ACCprocess(77);
 		ACCwrite(334);
 		ACCnewtext();
 		ACCdone();
		break pro028_restart;
		{}

	}

	// _ _
	p028e0007:
	{
 		if (skipdoall('p028e0007')) break p028e0007;
 		ACCprocess(61);
		if (!CNDnoteq(69,254)) break p028e0007;
 		ACCsysmess(99);
 		ACCsysmess(123);
 		ACCprocess(64);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCwrite(335);
 		ACCprocess(77);
 		ACCsysmess(125);
 		ACCprocess(88);
 		ACCprocess(41);
 		ACCcopyff(70,34);
 		ACCcopyff(71,35);
 		ACCwhato();
 		ACCwrite(336);
 		ACCprocess(77);
 		ACCnewtext();
 		ACCdone();
		break pro028_restart;
		{}

	}

	// _ _
	p028e0008:
	{
 		if (skipdoall('p028e0008')) break p028e0008;
 		ACCprocess(65);
 		ACCsysmess(126);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCwrite(337);
 		ACCprocess(77);
 		ACCnewtext();
 		ACCdone();
		break pro028_restart;
		{}

	}


}
}

function pro029()
{
process_restart=true;
pro029_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p029e0000:
	{
 		if (skipdoall('p029e0000')) break p029e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p029e0001:
	{
 		if (skipdoall('p029e0001')) break p029e0001;
		if (!CNDsame(38,54)) break p029e0001;
		if (!CNDlt(34,50)) break p029e0001;
 		ACCsysmess(145);
 		ACCprocess(87);
 		ACCwrite(338);
 		ACCdone();
		break pro029_restart;
		{}

	}

	// _ _
	p029e0002:
	{
 		if (skipdoall('p029e0002')) break p029e0002;
		if (!CNDeq(54,254)) break p029e0002;
 		ACCsysmess(98);
 		ACCprocess(64);
 		ACCwrite(339);
 		ACCputo(255);
 		ACCdone();
		break pro029_restart;
		{}

	}

	// _ _
	p029e0003:
	{
 		if (skipdoall('p029e0003')) break p029e0003;
		if (!CNDeq(54,253)) break p029e0003;
 		ACCsysmess(24);
 		ACCprocess(64);
 		ACCwrite(340);
 		ACCprocess(72);
 		ACCsysmess(106);
 		ACCprocess(87);
 		ACCwrite(341);
 		ACCnewtext();
 		ACCdone();
		break pro029_restart;
		{}

	}

	// _ _
	p029e0004:
	{
 		if (skipdoall('p029e0004')) break p029e0004;
		if (!CNDsame(54,38)) break p029e0004;
 		ACCsysmess(49);
 		ACCprocess(64);
 		ACCwrite(342);
 		ACCnewtext();
 		ACCdone();
		break pro029_restart;
		{}

	}

	// _ _
	p029e0005:
	{
 		if (skipdoall('p029e0005')) break p029e0005;
 		ACCsysmess(28);
 		ACCnewtext();
 		ACCdone();
		break pro029_restart;
		{}

	}


}
}

function pro030()
{
process_restart=true;
pro030_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p030e0000:
	{
 		if (skipdoall('p030e0000')) break p030e0000;
		if (!CNDeq(51,10)) break p030e0000;
		if (!CNDnotzero(27)) break p030e0000;
		if (!CNDisnotat(36,10)) break p030e0000;
 		ACCwrite(343);
 		ACCwrite(344);
 		ACCdone();
		break pro030_restart;
		{}

	}

	// _ _
	p030e0001:
	{
 		if (skipdoall('p030e0001')) break p030e0001;
 		ACCprocess(38);
		{}

	}

	// _ _
	p030e0002:
	{
 		if (skipdoall('p030e0002')) break p030e0002;
		if (!CNDnotzero(17)) break p030e0002;
		if (!CNDnotzero(16)) break p030e0002;
 		ACCprocess(65);
 		ACCsysmess(146);
 		ACCprocess(88);
 		ACCsysmess(115);
 		ACCprocess(87);
		if (!CNDnotzero(19)) break p030e0002;
 		ACCcopyff(51,67);
 		ACCprocess(23);
		if (!CNDzero(69)) break p030e0002;
 		ACCsysmess(148);
 		ACCdone();
		break pro030_restart;
		{}

	}

	// _ _
	p030e0003:
	{
 		if (skipdoall('p030e0003')) break p030e0003;
 		ACCprocess(38);
		if (!CNDnotzero(17)) break p030e0003;
		if (!CNDzero(16)) break p030e0003;
 		ACCprocess(65);
 		ACCsysmess(146);
 		ACCprocess(88);
 		ACCsysmess(116);
 		ACCprocess(87);
		{}

	}

	// _ _
	p030e0004:
	{
 		if (skipdoall('p030e0004')) break p030e0004;
 		ACCprocess(38);
		if (!CNDnotzero(17)) break p030e0004;
		if (!CNDnotzero(16)) break p030e0004;
		if (!CNDnotzero(19)) break p030e0004;
 		ACCwrite(345);
 		ACCprocess(51);
 		ACCnewline();
 		ACCdone();
		break pro030_restart;
		{}

	}

	// _ _
	p030e0005:
	{
 		if (skipdoall('p030e0005')) break p030e0005;
		if (!CNDzero(17)) break p030e0005;
		if (!CNDnotzero(19)) break p030e0005;
 		ACCprocess(51);
 		ACCdone();
		break pro030_restart;
		{}

	}

	// _ _
	p030e0006:
	{
 		if (skipdoall('p030e0006')) break p030e0006;
 		ACCwrite(346);
		{}

	}


}
}

function pro031()
{
process_restart=true;
pro031_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p031e0000:
	{
 		if (skipdoall('p031e0000')) break p031e0000;
		if (!CNDlt(65,2)) break p031e0000;
 		ACCwrite(347);
		{}

	}

	// _ _
	p031e0001:
	{
 		if (skipdoall('p031e0001')) break p031e0001;
		if (!CNDgt(65,1)) break p031e0001;
 		ACCwrite(348);
		{}

	}

	// _ _
	p031e0002:
	{
 		if (skipdoall('p031e0002')) break p031e0002;
 		ACCwrite(349);
		{}

	}

	// _ _
	p031e0003:
	{
 		if (skipdoall('p031e0003')) break p031e0003;
		if (!CNDeq(65,1)) break p031e0003;
 		ACCwrite(350);
		{}

	}

	// _ _
	p031e0004:
	{
 		if (skipdoall('p031e0004')) break p031e0004;
		if (!CNDlt(65,2)) break p031e0004;
 		ACCwrite(351);
 		ACCnewline();
		{}

	}

	// _ _
	p031e0005:
	{
 		if (skipdoall('p031e0005')) break p031e0005;
		if (!CNDeq(65,2)) break p031e0005;
 		ACCwrite(352);
 		ACCnewline();
		{}

	}

	// _ _
	p031e0006:
	{
 		if (skipdoall('p031e0006')) break p031e0006;
		if (!CNDeq(65,3)) break p031e0006;
 		ACCwrite(353);
 		ACCnewline();
		{}

	}

	// _ _
	p031e0007:
	{
 		if (skipdoall('p031e0007')) break p031e0007;
		if (!CNDgt(65,3)) break p031e0007;
 		ACCwrite(354);
 		ACCnewline();
		{}

	}


}
}

function pro032()
{
process_restart=true;
pro032_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p032e0000:
	{
 		if (skipdoall('p032e0000')) break p032e0000;
		if (!CNDeq(34,73)) break p032e0000;
		if (!CNDpresent(14)) break p032e0000;
		if (!CNDcarried(7)) break p032e0000;
 		ACCwrite(355);
 		ACCnewline();
 		ACCdone();
		break pro032_restart;
		{}

	}

	// _ _
	p032e0001:
	{
 		if (skipdoall('p032e0001')) break p032e0001;
		if (!CNDeq(34,69)) break p032e0001;
		if (!CNDpresent(11)) break p032e0001;
		if (!CNDcarried(7)) break p032e0001;
 		ACCwrite(356);
 		ACCnewline();
 		ACCdone();
		break pro032_restart;
		{}

	}

	// _ _
	p032e0002:
	{
 		if (skipdoall('p032e0002')) break p032e0002;
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p032e0002;
		if (!CNDcarried(7)) break p032e0002;
 		ACCwrite(357);
 		ACCwrite(358);
 		ACCprocess(63);
 		ACCwrite(359);
 		ACCnewline();
 		ACCdone();
		break pro032_restart;
		{}

	}

	// _ _
	p032e0003:
	{
 		if (skipdoall('p032e0003')) break p032e0003;
 		ACCprocess(61);
		if (!CNDnotzero(69)) break p032e0003;
 		ACCwrite(360);
 		ACCprocess(63);
 		ACCwrite(361);
 		ACCnewline();
 		ACCdone();
		break pro032_restart;
		{}

	}

	// _ _
	p032e0004:
	{
 		if (skipdoall('p032e0004')) break p032e0004;
		if (!CNDcarried(7)) break p032e0004;
		if (!CNDeq(34,255)) break p032e0004;
 		ACCwrite(362);
 		ACCwrite(363);
 		ACCnewline();
 		ACCdone();
		break pro032_restart;
		{}

	}

	// _ _
	p032e0005:
	{
 		if (skipdoall('p032e0005')) break p032e0005;
		if (!CNDcarried(7)) break p032e0005;
 		ACCwrite(364);
 		ACCwrite(365);
 		ACCwrite(366);
 		ACCnewline();
 		ACCdone();
		break pro032_restart;
		{}

	}

	// _ _
	p032e0006:
	{
 		if (skipdoall('p032e0006')) break p032e0006;
 		ACCwrite(367);
 		ACCnewline();
 		ACCdone();
		break pro032_restart;
		{}

	}


}
}

function pro033()
{
process_restart=true;
pro033_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p033e0000:
	{
 		if (skipdoall('p033e0000')) break p033e0000;
 		ACCclear(69);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
		if (!CNDbnotzero(56,2)) break p033e0000;
 		ACCset(69);
		{}

	}

	// _ _
	p033e0001:
	{
 		if (skipdoall('p033e0001')) break p033e0001;
 		ACCprocess(77);
 		ACCdone();
		break pro033_restart;
		{}

	}


}
}

function pro034()
{
process_restart=true;
pro034_restart: while(process_restart)
{
	process_restart=false;

}
}

function pro035()
{
process_restart=true;
pro035_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p035e0000:
	{
 		if (skipdoall('p035e0000')) break p035e0000;
 		ACCclear(69);
 		ACCcopyff(34,78);
 		ACCcopyff(35,73);
		if (!CNDparse()) break p035e0000;
 		ACCcopyff(78,34);
 		ACCcopyff(73,35);
		{}

	}

	// _ FOSA
	p035e0001:
	{
 		if (skipdoall('p035e0001')) break p035e0001;
 		if (in_response)
		{
			if (!CNDnoun1(56)) break p035e0001;
 		}
 		ACCwrite(368);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ TABLON
	p035e0002:
	{
 		if (skipdoall('p035e0002')) break p035e0002;
 		if (in_response)
		{
			if (!CNDnoun1(60)) break p035e0002;
 		}
 		ACCwrite(369);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ BOTA
	p035e0003:
	{
 		if (skipdoall('p035e0003')) break p035e0003;
 		if (in_response)
		{
			if (!CNDnoun1(61)) break p035e0003;
 		}
 		ACCwrite(370);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ BOTA
	p035e0004:
	{
 		if (skipdoall('p035e0004')) break p035e0004;
 		if (in_response)
		{
			if (!CNDnoun1(61)) break p035e0004;
 		}
 		ACCwrite(371);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ PISTOLA
	p035e0005:
	{
 		if (skipdoall('p035e0005')) break p035e0005;
 		if (in_response)
		{
			if (!CNDnoun1(63)) break p035e0005;
 		}
 		ACCwrite(372);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ BLOQUE
	p035e0006:
	{
 		if (skipdoall('p035e0006')) break p035e0006;
 		if (in_response)
		{
			if (!CNDnoun1(65)) break p035e0006;
 		}
 		ACCwrite(373);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ GUSANO
	p035e0007:
	{
 		if (skipdoall('p035e0007')) break p035e0007;
 		if (in_response)
		{
			if (!CNDnoun1(69)) break p035e0007;
 		}
 		ACCwrite(374);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ LIQUIDO
	p035e0008:
	{
 		if (skipdoall('p035e0008')) break p035e0008;
 		if (in_response)
		{
			if (!CNDnoun1(70)) break p035e0008;
 		}
 		ACCwrite(375);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ ESPEJO
	p035e0009:
	{
 		if (skipdoall('p035e0009')) break p035e0009;
 		if (in_response)
		{
			if (!CNDnoun1(71)) break p035e0009;
 		}
		if (!CNDabsent(16)) break p035e0009;
 		ACCwrite(376);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ ESPEJO
	p035e0010:
	{
 		if (skipdoall('p035e0010')) break p035e0010;
 		if (in_response)
		{
			if (!CNDnoun1(71)) break p035e0010;
 		}
 		ACCwrite(377);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ CRIATURA
	p035e0011:
	{
 		if (skipdoall('p035e0011')) break p035e0011;
 		if (in_response)
		{
			if (!CNDnoun1(73)) break p035e0011;
 		}
 		ACCwrite(378);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ BABOSA
	p035e0012:
	{
 		if (skipdoall('p035e0012')) break p035e0012;
 		if (in_response)
		{
			if (!CNDnoun1(75)) break p035e0012;
 		}
 		ACCwrite(379);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ PUERTA
	p035e0013:
	{
 		if (skipdoall('p035e0013')) break p035e0013;
 		if (in_response)
		{
			if (!CNDnoun1(76)) break p035e0013;
 		}
		if (!CNDpresent(30)) break p035e0013;
 		ACCwrite(380);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ PANEL
	p035e0014:
	{
 		if (skipdoall('p035e0014')) break p035e0014;
 		if (in_response)
		{
			if (!CNDnoun1(81)) break p035e0014;
 		}
 		ACCwrite(381);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ ROBOT
	p035e0015:
	{
 		if (skipdoall('p035e0015')) break p035e0015;
 		if (in_response)
		{
			if (!CNDnoun1(82)) break p035e0015;
 		}
 		ACCwrite(382);
 		ACCprocess(80);
 		ACCclear(69);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ REJILLA
	p035e0016:
	{
 		if (skipdoall('p035e0016')) break p035e0016;
 		if (in_response)
		{
			if (!CNDnoun1(83)) break p035e0016;
 		}
 		ACCwrite(383);
		if (!CNDzero(100)) break p035e0016;
 		ACCwrite(384);
 		ACCnewline();
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ REJILLA
	p035e0017:
	{
 		if (skipdoall('p035e0017')) break p035e0017;
 		if (in_response)
		{
			if (!CNDnoun1(83)) break p035e0017;
 		}
 		ACCwrite(385);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ DISPOSITIV
	p035e0018:
	{
 		if (skipdoall('p035e0018')) break p035e0018;
 		if (in_response)
		{
			if (!CNDnoun1(86)) break p035e0018;
 		}
		if (!CNDzero(26)) break p035e0018;
 		ACCwrite(386);
 		ACCnewline();
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ DISPOSITIV
	p035e0019:
	{
 		if (skipdoall('p035e0019')) break p035e0019;
 		if (in_response)
		{
			if (!CNDnoun1(86)) break p035e0019;
 		}
 		ACCwrite(387);
 		ACCnewline();
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ NAVE
	p035e0020:
	{
 		if (skipdoall('p035e0020')) break p035e0020;
 		if (in_response)
		{
			if (!CNDnoun1(87)) break p035e0020;
 		}
 		ACCwrite(388);
 		ACClet(34,88);
 		ACClet(35,255);
		{}

	}

	// _ TECNICO
	p035e0021:
	{
 		if (skipdoall('p035e0021')) break p035e0021;
 		if (in_response)
		{
			if (!CNDnoun1(89)) break p035e0021;
 		}
		if (!CNDzero(66)) break p035e0021;
 		ACCwrite(389);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ TECNICO
	p035e0022:
	{
 		if (skipdoall('p035e0022')) break p035e0022;
 		if (in_response)
		{
			if (!CNDnoun1(89)) break p035e0022;
 		}
		if (!CNDeq(66,1)) break p035e0022;
 		ACCwrite(390);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ FUSIBLE
	p035e0023:
	{
 		if (skipdoall('p035e0023')) break p035e0023;
 		if (in_response)
		{
			if (!CNDnoun1(97)) break p035e0023;
 		}
 		ACCwrite(391);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ VENTANA
	p035e0024:
	{
 		if (skipdoall('p035e0024')) break p035e0024;
 		if (in_response)
		{
			if (!CNDnoun1(98)) break p035e0024;
 		}
 		ACCwrite(392);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ CANALIZADO
	p035e0025:
	{
 		if (skipdoall('p035e0025')) break p035e0025;
 		if (in_response)
		{
			if (!CNDnoun1(99)) break p035e0025;
 		}
 		ACCwrite(393);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ PEDERNAL
	p035e0026:
	{
 		if (skipdoall('p035e0026')) break p035e0026;
 		if (in_response)
		{
			if (!CNDnoun1(52)) break p035e0026;
 		}
 		ACCwriteln(394);
 		ACCdone();
		break pro035_restart;
		{}

	}

	// _ _
	p035e0027:
	{
 		if (skipdoall('p035e0027')) break p035e0027;
 		ACCwriteln(395);
		{}

	}

	// _ _
	p035e0028:
	{
 		if (skipdoall('p035e0028')) break p035e0028;
 		ACCset(69);
		{}

	}


}
}

function pro036()
{
process_restart=true;
pro036_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p036e0000:
	{
 		if (skipdoall('p036e0000')) break p036e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p036e0001:
	{
 		if (skipdoall('p036e0001')) break p036e0001;
 		ACCprocess(61);
		if (!CNDzero(69)) break p036e0001;
 		ACCsysmess(108);
 		ACCwrite(396);
 		ACCdone();
		break pro036_restart;
		{}

	}

	// _ _
	p036e0002:
	{
 		if (skipdoall('p036e0002')) break p036e0002;
 		ACCprocess(38);
		if (!CNDzero(17)) break p036e0002;
		if (!CNDzero(19)) break p036e0002;
		if (!CNDgt(34,49)) break p036e0002;
 		ACCsysmess(108);
 		ACCwrite(397);
 		ACCprocess(64);
 		ACCwrite(398);
 		ACCdone();
		break pro036_restart;
		{}

	}

	// _ _
	p036e0003:
	{
 		if (skipdoall('p036e0003')) break p036e0003;
 		ACCprocess(38);
		if (!CNDlt(34,50)) break p036e0003;
 		ACCsysmess(109);
 		ACCwrite(399);
 		ACCdone();
		break pro036_restart;
		{}

	}

	// _ _
	p036e0004:
	{
 		if (skipdoall('p036e0004')) break p036e0004;
 		ACCprocess(30);
 		ACCdone();
		break pro036_restart;
		{}

	}


}
}

function pro037()
{
process_restart=true;
pro037_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p037e0000:
	{
 		if (skipdoall('p037e0000')) break p037e0000;
 		ACCclear(18);
 		ACCclear(12);
		{}

	}

	// _ _
	p037e0001:
	{
 		if (skipdoall('p037e0001')) break p037e0001;
		if (!CNDeq(51,17)) break p037e0001;
 		ACCset(18);
 		ACCcopyff(13,12);
		{}

	}

	// _ _
	p037e0002:
	{
 		if (skipdoall('p037e0002')) break p037e0002;
		if (!CNDeq(51,18)) break p037e0002;
 		ACCset(18);
 		ACCcopyff(14,12);
		{}

	}


}
}

function pro038()
{
process_restart=true;
pro038_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p038e0000:
	{
 		if (skipdoall('p038e0000')) break p038e0000;
 		ACCclear(23);
 		ACCclear(20);
 		ACCclear(17);
 		ACCclear(16);
 		ACCclear(19);
 		ACCclear(22);
 		ACClet(21,5);
 		ACCwhato();
		{}

	}

	// _ _
	p038e0001:
	{
 		if (skipdoall('p038e0001')) break p038e0001;
		if (!CNDeq(51,3)) break p038e0001;
		if (!CNDzero(86)) break p038e0001;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0002:
	{
 		if (skipdoall('p038e0002')) break p038e0002;
		if (!CNDeq(51,4)) break p038e0002;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0003:
	{
 		if (skipdoall('p038e0003')) break p038e0003;
		if (!CNDeq(51,2)) break p038e0003;
 		ACCset(20);
 		ACClet(21,2);
		{}

	}

	// _ _
	p038e0004:
	{
 		if (skipdoall('p038e0004')) break p038e0004;
		if (!CNDeq(51,1)) break p038e0004;
 		ACCset(20);
 		ACCset(23);
 		ACClet(21,1);
		{}

	}

	// _ _
	p038e0005:
	{
 		if (skipdoall('p038e0005')) break p038e0005;
		if (!CNDeq(51,11)) break p038e0005;
 		ACCset(20);
		{}

	}

	// _ _
	p038e0006:
	{
 		if (skipdoall('p038e0006')) break p038e0006;
		if (!CNDeq(51,11)) break p038e0006;
		if (!CNDisat(11,7)) break p038e0006;
 		ACCset(23);
		{}

	}

	// _ _
	p038e0007:
	{
 		if (skipdoall('p038e0007')) break p038e0007;
		if (!CNDeq(51,11)) break p038e0007;
		if (!CNDisat(11,25)) break p038e0007;
 		ACCset(23);
		{}

	}

	// _ _
	p038e0008:
	{
 		if (skipdoall('p038e0008')) break p038e0008;
		if (!CNDeq(51,12)) break p038e0008;
 		ACCset(23);
 		ACCset(20);
		{}

	}

	// _ _
	p038e0009:
	{
 		if (skipdoall('p038e0009')) break p038e0009;
		if (!CNDeq(51,13)) break p038e0009;
 		ACCset(23);
 		ACCset(20);
		{}

	}

	// _ _
	p038e0010:
	{
 		if (skipdoall('p038e0010')) break p038e0010;
		if (!CNDeq(51,21)) break p038e0010;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0011:
	{
 		if (skipdoall('p038e0011')) break p038e0011;
		if (!CNDeq(51,22)) break p038e0011;
 		ACCset(20);
		{}

	}

	// _ _
	p038e0012:
	{
 		if (skipdoall('p038e0012')) break p038e0012;
		if (!CNDeq(51,23)) break p038e0012;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0013:
	{
 		if (skipdoall('p038e0013')) break p038e0013;
		if (!CNDeq(51,25)) break p038e0013;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0014:
	{
 		if (skipdoall('p038e0014')) break p038e0014;
		if (!CNDeq(51,26)) break p038e0014;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0015:
	{
 		if (skipdoall('p038e0015')) break p038e0015;
		if (!CNDeq(51,27)) break p038e0015;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0016:
	{
 		if (skipdoall('p038e0016')) break p038e0016;
		if (!CNDeq(51,10)) break p038e0016;
 		ACCset(20);
 		ACCset(23);
 		ACCset(17);
 		ACClet(21,1);
		if (!CNDnotzero(27)) break p038e0016;
 		ACCset(16);
		{}

	}

	// _ _
	p038e0017:
	{
 		if (skipdoall('p038e0017')) break p038e0017;
		if (!CNDeq(51,28)) break p038e0017;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0018:
	{
 		if (skipdoall('p038e0018')) break p038e0018;
		if (!CNDeq(51,30)) break p038e0018;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0019:
	{
 		if (skipdoall('p038e0019')) break p038e0019;
		if (!CNDeq(51,31)) break p038e0019;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0020:
	{
 		if (skipdoall('p038e0020')) break p038e0020;
		if (!CNDeq(51,32)) break p038e0020;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0021:
	{
 		if (skipdoall('p038e0021')) break p038e0021;
		if (!CNDeq(51,33)) break p038e0021;
 		ACCset(20);
		{}

	}

	// _ _
	p038e0022:
	{
 		if (skipdoall('p038e0022')) break p038e0022;
		if (!CNDeq(51,9)) break p038e0022;
 		ACCset(20);
 		ACCset(17);
 		ACClet(21,1);
		if (!CNDnotzero(62)) break p038e0022;
 		ACCset(16);
		{}

	}

	// _ _
	p038e0023:
	{
 		if (skipdoall('p038e0023')) break p038e0023;
		if (!CNDeq(51,35)) break p038e0023;
 		ACCset(20);
 		ACCset(23);
		{}

	}

	// _ _
	p038e0024:
	{
 		if (skipdoall('p038e0024')) break p038e0024;
		if (!CNDeq(21,5)) break p038e0024;
 		ACCcopyff(55,21);
		{}

	}

	// _ _
	p038e0025:
	{
 		if (skipdoall('p038e0025')) break p038e0025;
		if (!CNDbnotzero(56,2)) break p038e0025;
 		ACCset(19);
		if (!CNDzero(17)) break p038e0025;
 		ACCset(16);
		{}

	}

	// _ _
	p038e0026:
	{
 		if (skipdoall('p038e0026')) break p038e0026;
 		ACCdone();
		break pro038_restart;
		{}

	}


}
}

function pro039()
{
process_restart=true;
pro039_restart: while(process_restart)
{
	process_restart=false;

}
}

function pro040()
{
process_restart=true;
pro040_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p040e0000:
	{
 		if (skipdoall('p040e0000')) break p040e0000;
		if (!CNDeq(66,1)) break p040e0000;
 		ACCwrite(400);
 		ACCnewline();
 		ACCdone();
		break pro040_restart;
		{}

	}

	// _ _
	p040e0001:
	{
 		if (skipdoall('p040e0001')) break p040e0001;
		if (!CNDprep(2)) break p040e0001;
 		ACCprocess(62);
		if (!CNDnoteq(69,254)) break p040e0001;
		if (!CNDnoteq(69,253)) break p040e0001;
 		ACCsysmess(28);
 		ACCdone();
		break pro040_restart;
		{}

	}

	// _ _
	p040e0002:
	{
 		if (skipdoall('p040e0002')) break p040e0002;
		if (!CNDnoun2(52)) break p040e0002;
 		ACClet(66,1);
 		ACCwrite(401);
 		ACCprocess(69);
 		ACCwrite(402);
 		ACCnewline();
 		ACCdone();
		break pro040_restart;
		{}

	}

	// _ _
	p040e0003:
	{
 		if (skipdoall('p040e0003')) break p040e0003;
		if (!CNDnoun2(61)) break p040e0003;
 		ACClet(66,1);
 		ACCwrite(403);
 		ACCprocess(69);
 		ACCwrite(404);
 		ACCnewline();
 		ACCdone();
		break pro040_restart;
		{}

	}

	// _ _
	p040e0004:
	{
 		if (skipdoall('p040e0004')) break p040e0004;
		if (!CNDnoun2(84)) break p040e0004;
 		ACClet(66,1);
 		ACCwrite(405);
 		ACCprocess(69);
 		ACCwrite(406);
 		ACCnewline();
 		ACCdone();
		break pro040_restart;
		{}

	}

	// _ _
	p040e0005:
	{
 		if (skipdoall('p040e0005')) break p040e0005;
		if (!CNDeq(69,254)) break p040e0005;
 		ACCwrite(407);
 		ACCnewline();
 		ACCdone();
		break pro040_restart;
		{}

	}

	// _ _
	p040e0006:
	{
 		if (skipdoall('p040e0006')) break p040e0006;
		if (!CNDeq(69,253)) break p040e0006;
 		ACCwrite(408);
 		ACCnewline();
 		ACCdone();
		break pro040_restart;
		{}

	}

	// _ _
	p040e0007:
	{
 		if (skipdoall('p040e0007')) break p040e0007;
 		ACCwrite(409);
 		ACCnewline();
 		ACCdone();
		break pro040_restart;
		{}

	}


}
}

function pro041()
{
process_restart=true;
pro041_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p041e0000:
	{
 		if (skipdoall('p041e0000')) break p041e0000;
 		ACCcopyff(34,75);
 		ACCcopyff(35,74);
 		ACCcopyff(51,81);
 		ACCcopyff(54,80);
 		ACCcopyff(55,83);
 		ACCcopyff(56,79);
 		ACCcopyff(57,82);
 		ACCdone();
		break pro041_restart;
		{}

	}


}
}

function pro042()
{
process_restart=true;
pro042_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p042e0000:
	{
 		if (skipdoall('p042e0000')) break p042e0000;
 		ACCprocess(41);
 		ACCclear(69);
 		ACCcopyff(38,76);
 		ACCcopyff(67,38);
		{}

	}

	// _ _
	p042e0001:
	{
 		if (skipdoall('p042e0001')) break p042e0001;
 		ACCprocess(43);
		{}

	}

	// _ _
	p042e0002:
	{
 		if (skipdoall('p042e0002')) break p042e0002;
 		ACCcopyff(76,38);
 		ACCprocess(77);
		{}

	}


}
}

function pro043()
{
process_restart=true;
pro043_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p043e0000:
	{
 		if (skipdoall('p043e0000')) break p043e0000;
		entry_for_doall = 'p043e0001';
		process_in_doall = 43;
 		ACCdoall(255);
		break pro043_restart;
		{}

	}

	// _ _
	p043e0001:
	{
 		if (skipdoall('p043e0001')) break p043e0001;
 		ACCwhato();
 		ACCprocess(37);
		if (!CNDnotzero(18)) break p043e0001;
 		ACCset(69);
		{}

	}


}
}

function pro044()
{
process_restart=true;
pro044_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p044e0000:
	{
 		if (skipdoall('p044e0000')) break p044e0000;
		if (!CNDzero(84)) break p044e0000;
 		ACCwrite(410);
 		ACCpause(40);
 		function anykey00003() 
		{
		}
 		waitKey(anykey00003);
		done_flag=true;
		break pro044_restart;
		{}

	}

	// _ _
	p044e0001:
	{
 		if (skipdoall('p044e0001')) break p044e0001;
		if (!CNDisat(36,10)) break p044e0001;
		if (!CNDzero(27)) break p044e0001;
		if (!CNDnotzero(103)) break p044e0001;
		if (!CNDzero(84)) break p044e0001;
		if (!CNDzero(96)) break p044e0001;
 		ACCset(96);
 		ACCplus(30,5);
		{}

	}

	// _ _
	p044e0002:
	{
 		if (skipdoall('p044e0002')) break p044e0002;
		if (!CNDisat(36,10)) break p044e0002;
		if (!CNDzero(27)) break p044e0002;
		if (!CNDnotzero(103)) break p044e0002;
		if (!CNDzero(84)) break p044e0002;
 		ACCset(84);
 		ACCwrite(411);
 		ACCnewline();
 		ACCdone();
		break pro044_restart;
		{}

	}

	// _ _
	p044e0003:
	{
 		if (skipdoall('p044e0003')) break p044e0003;
		if (!CNDnotzero(84)) break p044e0003;
 		ACCclear(84);
 		ACCwrite(412);
 		ACCnewline();
 		ACCdone();
		break pro044_restart;
		{}

	}

	// _ _
	p044e0004:
	{
 		if (skipdoall('p044e0004')) break p044e0004;
		if (!CNDnotzero(27)) break p044e0004;
 		ACCwrite(413);
 		ACCnewline();
 		ACCdone();
		break pro044_restart;
		{}

	}

	// _ _
	p044e0005:
	{
 		if (skipdoall('p044e0005')) break p044e0005;
		if (!CNDisnotat(36,10)) break p044e0005;
 		ACCwrite(414);
 		ACCnewline();
 		ACCdone();
		break pro044_restart;
		{}

	}

	// _ _
	p044e0006:
	{
 		if (skipdoall('p044e0006')) break p044e0006;
		if (!CNDzero(103)) break p044e0006;
 		ACCwrite(415);
 		ACCnewline();
 		ACCdone();
		break pro044_restart;
		{}

	}


}
}

function pro045()
{
process_restart=true;
pro045_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p045e0000:
	{
 		if (skipdoall('p045e0000')) break p045e0000;
 		ACClet(13,6);
 		ACCplace(17,6);
 		ACClet(14,5);
 		ACCplace(18,5);
		{}

	}


}
}

function pro046()
{
process_restart=true;
pro046_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p046e0000:
	{
 		if (skipdoall('p046e0000')) break p046e0000;
		if (!CNDnotzero(1)) break p046e0000;
 		ACCsysmess(9);
 		ACClet(67,254);
 		ACCprocess(50);
 		ACClet(67,253);
 		ACCprocess(23);
		if (!CNDnotzero(69)) break p046e0000;
 		ACCsysmess(96);
 		ACCsysmess(10);
		{}

	}

	// _ _
	p046e0001:
	{
 		if (skipdoall('p046e0001')) break p046e0001;
		if (!CNDnotzero(69)) break p046e0001;
		if (!CNDeq(70,1)) break p046e0001;
 		ACCwrite(416);
		{}

	}

	// _ _
	p046e0002:
	{
 		if (skipdoall('p046e0002')) break p046e0002;
		if (!CNDnotzero(69)) break p046e0002;
		if (!CNDeq(70,2)) break p046e0002;
 		ACCwrite(417);
		{}

	}

	// _ _
	p046e0003:
	{
 		if (skipdoall('p046e0003')) break p046e0003;
		if (!CNDnotzero(69)) break p046e0003;
		if (!CNDeq(70,3)) break p046e0003;
 		ACCwrite(418);
		{}

	}

	// _ _
	p046e0004:
	{
 		if (skipdoall('p046e0004')) break p046e0004;
		if (!CNDnotzero(69)) break p046e0004;
		if (!CNDeq(70,4)) break p046e0004;
 		ACCwrite(419);
		{}

	}

	// _ _
	p046e0005:
	{
 		if (skipdoall('p046e0005')) break p046e0005;
		if (!CNDnotzero(69)) break p046e0005;
 		ACCprocess(50);
 		ACCnewline();
 		ACCdone();
		break pro046_restart;
		{}

	}

	// _ _
	p046e0006:
	{
 		if (skipdoall('p046e0006')) break p046e0006;
 		ACClet(67,253);
 		ACCprocess(23);
		if (!CNDnotzero(69)) break p046e0006;
 		ACCwrite(420);
 		ACCsysmess(10);
		{}

	}

	// _ _
	p046e0007:
	{
 		if (skipdoall('p046e0007')) break p046e0007;
		if (!CNDnotzero(69)) break p046e0007;
		if (!CNDeq(70,1)) break p046e0007;
 		ACCwrite(421);
		{}

	}

	// _ _
	p046e0008:
	{
 		if (skipdoall('p046e0008')) break p046e0008;
		if (!CNDnotzero(69)) break p046e0008;
		if (!CNDeq(70,2)) break p046e0008;
 		ACCwrite(422);
		{}

	}

	// _ _
	p046e0009:
	{
 		if (skipdoall('p046e0009')) break p046e0009;
		if (!CNDnotzero(69)) break p046e0009;
		if (!CNDeq(70,3)) break p046e0009;
 		ACCwrite(423);
		{}

	}

	// _ _
	p046e0010:
	{
 		if (skipdoall('p046e0010')) break p046e0010;
		if (!CNDnotzero(69)) break p046e0010;
		if (!CNDeq(70,4)) break p046e0010;
 		ACCwrite(424);
		{}

	}

	// _ _
	p046e0011:
	{
 		if (skipdoall('p046e0011')) break p046e0011;
		if (!CNDnotzero(69)) break p046e0011;
 		ACCprocess(50);
 		ACCnewline();
 		ACCdone();
		break pro046_restart;
		{}

	}

	// _ _
	p046e0012:
	{
 		if (skipdoall('p046e0012')) break p046e0012;
		if (!CNDzero(1)) break p046e0012;
 		ACClet(67,253);
 		ACCprocess(23);
		if (!CNDzero(69)) break p046e0012;
 		ACCsysmess(97);
 		ACCnewline();
 		ACCdone();
		break pro046_restart;
		{}

	}

	// _ _
	p046e0013:
	{
 		if (skipdoall('p046e0013')) break p046e0013;
		if (!CNDzero(69)) break p046e0013;
 		ACCnewline();
		{}

	}


}
}

function pro047()
{
process_restart=true;
pro047_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p047e0000:
	{
 		if (skipdoall('p047e0000')) break p047e0000;
		entry_for_doall = 'p047e0001';
		process_in_doall = 47;
 		ACCdoall(2);
		break pro047_restart;
		{}

	}

	// _ _
	p047e0001:
	{
 		if (skipdoall('p047e0001')) break p047e0001;
 		ACCwhato();
		{}

	}

	// _ _
	p047e0002:
	{
 		if (skipdoall('p047e0002')) break p047e0002;
		if (!CNDeq(34,60)) break p047e0002;
 		ACCputo(19);
		{}

	}

	// _ _
	p047e0003:
	{
 		if (skipdoall('p047e0003')) break p047e0003;
		if (!CNDnoteq(34,71)) break p047e0003;
		if (!CNDnoteq(34,60)) break p047e0003;
 		ACCputo(3);
		{}

	}

	// _ _
	p047e0004:
	{
 		if (skipdoall('p047e0004')) break p047e0004;
 		ACCprocess(65);
 		ACCwrite(425);
 		ACCprocess(88);
 		ACCwrite(426);
 		ACCnewline();
		{}

	}

	// _ _
	p047e0005:
	{
 		if (skipdoall('p047e0005')) break p047e0005;
		if (!CNDeq(34,71)) break p047e0005;
 		ACCdestroy(16);
 		ACCwrite(427);
 		ACCnewline();
		{}

	}

	// _ _
	p047e0006:
	{
 		if (skipdoall('p047e0006')) break p047e0006;
 		ACCdone();
		break pro047_restart;
		{}

	}


}
}

function pro048()
{
process_restart=true;
pro048_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p048e0000:
	{
 		if (skipdoall('p048e0000')) break p048e0000;
		entry_for_doall = 'p048e0001';
		process_in_doall = 48;
 		ACCdoall(1);
		break pro048_restart;
		{}

	}

	// _ _
	p048e0001:
	{
 		if (skipdoall('p048e0001')) break p048e0001;
		if (!CNDeq(34,61)) break p048e0001;
 		ACCwrite(428);
 		ACCnewline();
 		ACCputo(14);
 		ACCdone();
		break pro048_restart;
		{}

	}

	// _ _
	p048e0002:
	{
 		if (skipdoall('p048e0002')) break p048e0002;
		if (!CNDnoteq(34,60)) break p048e0002;
 		ACCwhato();
 		ACCputo(15);
 		ACCwrite(429);
 		ACCprocess(64);
 		ACCwrite(430);
 		ACCprocess(87);
 		ACCwrite(431);
 		ACCnewline();
 		ACCdone();
		break pro048_restart;
		{}

	}


}
}

function pro049()
{
process_restart=true;
pro049_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p049e0000:
	{
 		if (skipdoall('p049e0000')) break p049e0000;
		entry_for_doall = 'p049e0001';
		process_in_doall = 49;
 		ACCdoall(13);
		break pro049_restart;
		{}

	}

	// _ _
	p049e0001:
	{
 		if (skipdoall('p049e0001')) break p049e0001;
 		ACCwhato();
		{}

	}

	// _ _
	p049e0002:
	{
 		if (skipdoall('p049e0002')) break p049e0002;
		if (!CNDnoteq(65,3)) break p049e0002;
 		ACCputo(3);
		{}

	}

	// _ _
	p049e0003:
	{
 		if (skipdoall('p049e0003')) break p049e0003;
		if (!CNDeq(65,3)) break p049e0003;
 		ACCputo(19);
		{}

	}

	// _ _
	p049e0004:
	{
 		if (skipdoall('p049e0004')) break p049e0004;
 		ACCprocess(65);
 		ACCwrite(432);
 		ACCprocess(88);
 		ACCwrite(433);
 		ACCnewline();
 		ACCdone();
		break pro049_restart;
		{}

	}


}
}

function pro050()
{
process_restart=true;
pro050_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p050e0000:
	{
 		if (skipdoall('p050e0000')) break p050e0000;
 		ACCprocess(23);
		if (!CNDzero(69)) break p050e0000;
 		ACCdone();
		break pro050_restart;
		{}

	}

	// _ _
	p050e0001:
	{
 		if (skipdoall('p050e0001')) break p050e0001;
 		ACCprocess(41);
 		ACCcopyff(38,104);
		if (!CNDnoteq(67,253)) break p050e0001;
		if (!CNDnoteq(67,255)) break p050e0001;
		if (!CNDnoteq(67,252)) break p050e0001;
		if (!CNDnoteq(67,254)) break p050e0001;
 		ACCcopyff(67,38);
		{}

	}

	// _ _
	p050e0002:
	{
 		if (skipdoall('p050e0002')) break p050e0002;
 		ACCprocess(52);
 		ACCcopyff(104,38);
 		ACCprocess(77);
 		ACCdone();
		break pro050_restart;
		{}

	}


}
}

function pro051()
{
process_restart=true;
pro051_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p051e0000:
	{
 		if (skipdoall('p051e0000')) break p051e0000;
		if (!CNDeq(51,10)) break p051e0000;
 		ACCwrite(434);
		if (!CNDisat(36,10)) break p051e0000;
 		ACCwrite(435);
 		ACCdone();
		break pro051_restart;
		{}

	}

	// _ _
	p051e0001:
	{
 		if (skipdoall('p051e0001')) break p051e0001;
		if (!CNDeq(51,10)) break p051e0001;
 		ACCwrite(436);
 		ACCdone();
		break pro051_restart;
		{}

	}

	// _ _
	p051e0002:
	{
 		if (skipdoall('p051e0002')) break p051e0002;
 		ACCwhato();
 		ACCsysmess(138);
 		ACCprocess(64);
 		ACCcopyff(51,67);
 		ACCprocess(23);
		if (!CNDnotzero(69)) break p051e0002;
 		ACCsysmess(139);
 		ACCprocess(50);
 		ACCdone();
		break pro051_restart;
		{}

	}

	// _ _
	p051e0003:
	{
 		if (skipdoall('p051e0003')) break p051e0003;
 		ACCsysmess(140);
 		ACCdone();
		break pro051_restart;
		{}

	}


}
}

function pro052()
{
process_restart=true;
pro052_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p052e0000:
	{
 		if (skipdoall('p052e0000')) break p052e0000;
 		ACClet(105,1);
		{}

	}

	// _ _
	p052e0001:
	{
 		if (skipdoall('p052e0001')) break p052e0001;
		if (!CNDnoteq(67,253)) break p052e0001;
		if (!CNDnoteq(67,252)) break p052e0001;
		if (!CNDnoteq(67,254)) break p052e0001;
		entry_for_doall = 'p052e0002';
		process_in_doall = 52;
 		ACCdoall(255);
		break pro052_restart;
		{}

	}

	// _ _
	p052e0002:
	{
 		if (skipdoall('p052e0002')) break p052e0002;
		if (!CNDeq(67,253)) break p052e0002;
		entry_for_doall = 'p052e0003';
		process_in_doall = 52;
 		ACCdoall(253);
		break pro052_restart;
		{}

	}

	// _ _
	p052e0003:
	{
 		if (skipdoall('p052e0003')) break p052e0003;
		if (!CNDeq(67,252)) break p052e0003;
		entry_for_doall = 'p052e0004';
		process_in_doall = 52;
 		ACCdoall(252);
		break pro052_restart;
		{}

	}

	// _ _
	p052e0004:
	{
 		if (skipdoall('p052e0004')) break p052e0004;
		if (!CNDeq(67,254)) break p052e0004;
		entry_for_doall = 'p052e0005';
		process_in_doall = 52;
 		ACCdoall(254);
		break pro052_restart;
		{}

	}

	// _ _
	p052e0005:
	{
 		if (skipdoall('p052e0005')) break p052e0005;
 		ACCprocess(38);
		if (!CNDnotzero(23)) break p052e0005;
		if (!CNDnoteq(50,254)) break p052e0005;
		if (!CNDnoteq(50,253)) break p052e0005;
 		ACCdone();
		break pro052_restart;
		{}

	}

	// _ _
	p052e0006:
	{
 		if (skipdoall('p052e0006')) break p052e0006;
		if (!CNDgt(105,1)) break p052e0006;
		if (!CNDnotsame(105,69)) break p052e0006;
 		ACCsysmess(46);
		{}

	}

	// _ _
	p052e0007:
	{
 		if (skipdoall('p052e0007')) break p052e0007;
		if (!CNDgt(105,1)) break p052e0007;
		if (!CNDsame(105,69)) break p052e0007;
 		ACCsysmess(47);
		{}

	}

	// _ _
	p052e0008:
	{
 		if (skipdoall('p052e0008')) break p052e0008;
 		ACCprocess(66);
		if (!CNDsame(105,69)) break p052e0008;
 		ACCwrite(437);
		{}

	}

	// _ _
	p052e0009:
	{
 		if (skipdoall('p052e0009')) break p052e0009;
 		ACCplus(105,1);
 		ACCdone();
		break pro052_restart;
		{}

	}


}
}

function pro053()
{
process_restart=true;
pro053_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p053e0000:
	{
 		if (skipdoall('p053e0000')) break p053e0000;
 		ACClet(67,255);
 		ACCprocess(23);
		if (!CNDzero(69)) break p053e0000;
 		ACCdone();
		break pro053_restart;
		{}

	}

	// _ _
	p053e0001:
	{
 		if (skipdoall('p053e0001')) break p053e0001;
 		ACCsysmess(1);
 		ACClet(67,255);
 		ACCprocess(50);
 		ACCnewline();
 		ACCdone();
		break pro053_restart;
		{}

	}


}
}

function pro054()
{
process_restart=true;
pro054_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p054e0000:
	{
 		if (skipdoall('p054e0000')) break p054e0000;
		if (!CNDeq(44,255)) break p054e0000;
 		ACClet(69,252);
 		ACCdone();
		break pro054_restart;
		{}

	}

	// _ _
	p054e0001:
	{
 		if (skipdoall('p054e0001')) break p054e0001;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCcopyff(54,69);
 		ACCprocess(77);
 		ACCdone();
		break pro054_restart;
		{}

	}


}
}

function pro055()
{
process_restart=true;
pro055_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p055e0000:
	{
 		if (skipdoall('p055e0000')) break p055e0000;
		if (!CNDnoun2(88)) break p055e0000;
		if (!CNDnotzero(27)) break p055e0000;
		if (!CNDpresent(10)) break p055e0000;
		if (!CNDnoteq(34,99)) break p055e0000;
 		ACCwrite(438);
 		ACCnewline();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0001:
	{
 		if (skipdoall('p055e0001')) break p055e0001;
		if (!CNDnoun2(88)) break p055e0001;
		if (!CNDnotzero(27)) break p055e0001;
		if (!CNDpresent(10)) break p055e0001;
		if (!CNDzero(94)) break p055e0001;
 		ACCset(94);
 		ACCplus(30,1);
		{}

	}

	// _ _
	p055e0002:
	{
 		if (skipdoall('p055e0002')) break p055e0002;
 		ACCwhato();
		{}

	}

	// _ _
	p055e0003:
	{
 		if (skipdoall('p055e0003')) break p055e0003;
		if (!CNDeq(54,254)) break p055e0003;
		if (!CNDnoun2(85)) break p055e0003;
		if (!CNDpresent(25)) break p055e0003;
		if (!CNDisat(11,25)) break p055e0003;
 		ACCwrite(439);
 		ACCnewline();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0004:
	{
 		if (skipdoall('p055e0004')) break p055e0004;
		if (!CNDeq(54,254)) break p055e0004;
		if (!CNDnoun2(85)) break p055e0004;
		if (!CNDpresent(25)) break p055e0004;
 		ACCputo(25);
 		ACCprocess(65);
 		ACCwrite(440);
 		ACCprocess(88);
 		ACCwrite(441);
 		ACCprocess(87);
 		ACCwrite(442);
 		ACCnewline();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0005:
	{
 		if (skipdoall('p055e0005')) break p055e0005;
		if (!CNDnoun2(96)) break p055e0005;
		if (!CNDpresent(9)) break p055e0005;
		if (!CNDeq(54,254)) break p055e0005;
		if (!CNDnotzero(62)) break p055e0005;
 		ACClet(67,9);
 		ACCprocess(23);
		if (!CNDnotzero(69)) break p055e0005;
 		ACCwrite(443);
 		ACCprocess(50);
 		ACCnewline();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0006:
	{
 		if (skipdoall('p055e0006')) break p055e0006;
		if (!CNDnoun2(96)) break p055e0006;
		if (!CNDpresent(9)) break p055e0006;
		if (!CNDeq(54,254)) break p055e0006;
		if (!CNDnotzero(62)) break p055e0006;
		if (!CNDnoteq(51,34)) break p055e0006;
		if (!CNDnoteq(51,24)) break p055e0006;
 		ACCwrite(444);
 		ACCnewline();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0007:
	{
 		if (skipdoall('p055e0007')) break p055e0007;
		if (!CNDnotsame(38,54)) break p055e0007;
		if (!CNDnoteq(54,254)) break p055e0007;
		if (!CNDnoteq(54,253)) break p055e0007;
 		ACCsysmess(100);
 		ACCsysmess(117);
 		ACCsysmess(104);
 		ACCnewtext();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0008:
	{
 		if (skipdoall('p055e0008')) break p055e0008;
		if (!CNDeq(54,253)) break p055e0008;
 		ACCsysmess(99);
 		ACCsysmess(117);
 		ACCprocess(64);
 		ACCwrite(445);
 		ACCprocess(72);
 		ACCsysmess(106);
 		ACCprocess(87);
 		ACCwrite(446);
 		ACCnewtext();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0009:
	{
 		if (skipdoall('p055e0009')) break p055e0009;
		if (!CNDsame(54,38)) break p055e0009;
		if (!CNDlt(34,50)) break p055e0009;
 		ACCsysmess(142);
 		ACCnewtext();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0010:
	{
 		if (skipdoall('p055e0010')) break p055e0010;
		if (!CNDsame(54,38)) break p055e0010;
 		ACCsysmess(49);
 		ACCprocess(64);
 		ACCwrite(447);
 		ACCnewtext();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0011:
	{
 		if (skipdoall('p055e0011')) break p055e0011;
		if (!CNDnoun2(255)) break p055e0011;
		if (!CNDeq(54,254)) break p055e0011;
 		ACCsysmess(132);
 		ACCprocess(64);
 		ACCwrite(448);
 		ACCnewtext();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0012:
	{
 		if (skipdoall('p055e0012')) break p055e0012;
 		ACCprocess(54);
		if (!CNDnotsame(38,69)) break p055e0012;
		if (!CNDnoteq(69,254)) break p055e0012;
		if (!CNDnoteq(69,253)) break p055e0012;
 		ACCsysmess(132);
 		ACCprocess(64);
 		ACCwrite(449);
 		ACCnewtext();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0013:
	{
 		if (skipdoall('p055e0013')) break p055e0013;
 		ACCprocess(33);
		if (!CNDlt(44,50)) break p055e0013;
 		ACCsysmess(142);
 		ACCnewtext();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0014:
	{
 		if (skipdoall('p055e0014')) break p055e0014;
 		ACCprocess(33);
		if (!CNDzero(69)) break p055e0014;
 		ACCsysmess(134);
 		ACCprocess(70);
 		ACCwrite(450);
 		ACCnewtext();
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0015:
	{
 		if (skipdoall('p055e0015')) break p055e0015;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCprocess(38);
 		ACCprocess(77);
		if (!CNDnotzero(17)) break p055e0015;
		if (!CNDzero(16)) break p055e0015;
 		ACCsysmess(99);
 		ACCsysmess(117);
 		ACCsysmess(119);
 		ACCprocess(69);
 		ACCsysmess(120);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCprocess(88);
 		ACCsysmess(116);
 		ACCprocess(87);
 		ACCprocess(77);
 		ACCwrite(451);
 		ACCdone();
		break pro055_restart;
		{}

	}

	// _ _
	p055e0016:
	{
 		if (skipdoall('p055e0016')) break p055e0016;
 		ACCprocess(20);
 		ACCcopyff(38,104);
 		ACCcopyff(69,38);
 		ACCautod();
		if (!success) break pro055_restart;
 		ACCcopyff(104,38);
 		ACCsysmess(135);
 		ACCprocess(64);
 		ACCsysmess(136);
 		ACCprocess(69);
 		ACCwrite(452);
 		ACCdone();
		break pro055_restart;
		{}

	}


}
}

function pro056()
{
process_restart=true;
pro056_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p056e0000:
	{
 		if (skipdoall('p056e0000')) break p056e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p056e0001:
	{
 		if (skipdoall('p056e0001')) break p056e0001;
		if (!CNDat(24)) break p056e0001;
		if (!CNDeq(34,85)) break p056e0001;
		if (!CNDisat(11,25)) break p056e0001;
 		ACCwrite(453);
 		ACCnewline();
 		ACCdone();
		break pro056_restart;
		{}

	}

	// _ _
	p056e0002:
	{
 		if (skipdoall('p056e0002')) break p056e0002;
		if (!CNDat(24)) break p056e0002;
		if (!CNDeq(34,83)) break p056e0002;
 		ACCwrite(454);
 		ACCnewline();
 		ACCdone();
		break pro056_restart;
		{}

	}

	// _ _
	p056e0003:
	{
 		if (skipdoall('p056e0003')) break p056e0003;
		if (!CNDbnotzero(56,2)) break p056e0003;
 		ACCprocess(30);
 		ACCdone();
		break pro056_restart;
		{}

	}

	// _ _
	p056e0004:
	{
 		if (skipdoall('p056e0004')) break p056e0004;
		if (!CNDnoteq(51,255)) break p056e0004;
 		ACCsysmess(138);
 		ACCprocess(64);
 		ACCsysmess(140);
 		ACCdone();
		break pro056_restart;
		{}

	}

	// _ _
	p056e0005:
	{
 		if (skipdoall('p056e0005')) break p056e0005;
 		ACCsysmess(141);
 		ACCdone();
		break pro056_restart;
		{}

	}


}
}

function pro057()
{
process_restart=true;
pro057_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p057e0000:
	{
 		if (skipdoall('p057e0000')) break p057e0000;
		if (!CNDcarried(5)) break p057e0000;
 		ACCwrite(455);
 		ACCnewline();
 		ACCdone();
		break pro057_restart;
		{}

	}

	// _ _
	p057e0001:
	{
 		if (skipdoall('p057e0001')) break p057e0001;
 		ACCdestroy(8);
 		ACCprocess(74);
 		ACCwrite(456);
 		ACCnewline();
 		ACCgoto(3);
 		ACCdesc();
		break pro057_restart;
		{}

	}


}
}

function pro058()
{
process_restart=true;
pro058_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p058e0000:
	{
 		if (skipdoall('p058e0000')) break p058e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p058e0001:
	{
 		if (skipdoall('p058e0001')) break p058e0001;
 		ACCprocess(61);
		if (!CNDzero(69)) break p058e0001;
 		ACCsysmess(28);
 		ACCnewtext();
 		ACCdone();
		break pro058_restart;
		{}

	}

	// _ _
	p058e0002:
	{
 		if (skipdoall('p058e0002')) break p058e0002;
 		ACCprocess(20);
		if (!CNDeq(69,255)) break p058e0002;
 		ACCsysmess(129);
 		ACCprocess(64);
 		ACCwrite(457);
 		ACCnewtext();
 		ACCdone();
		break pro058_restart;
		{}

	}

	// _ _
	p058e0003:
	{
 		if (skipdoall('p058e0003')) break p058e0003;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCcopyff(54,104);
 		ACCprocess(77);
		if (!CNDnotsame(104,38)) break p058e0003;
		if (!CNDnoteq(104,254)) break p058e0003;
		if (!CNDnoteq(104,253)) break p058e0003;
 		ACCsysmess(129);
 		ACCprocess(64);
 		ACCwrite(458);
 		ACCnewtext();
 		ACCdone();
		break pro058_restart;
		{}

	}

	// _ _
	p058e0004:
	{
 		if (skipdoall('p058e0004')) break p058e0004;
		if (!CNDgt(44,49)) break p058e0004;
 		ACCsysmess(99);
 		ACCsysmess(128);
 		ACCprocess(64);
 		ACCsysmess(124);
 		ACCprocess(70);
 		ACCwrite(459);
 		ACCnewtext();
 		ACCdone();
		break pro058_restart;
		{}

	}

	// _ _
	p058e0005:
	{
 		if (skipdoall('p058e0005')) break p058e0005;
 		ACCprocess(61);
		if (!CNDnoteq(69,254)) break p058e0005;
		if (!CNDnoteq(69,253)) break p058e0005;
		if (!CNDgt(70,49)) break p058e0005;
 		ACCsysmess(99);
 		ACCsysmess(128);
 		ACCprocess(64);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCwrite(460);
 		ACCprocess(77);
 		ACCsysmess(146);
 		ACCprocess(88);
 		ACCsysmess(136);
 		ACCprocess(41);
 		ACCcopyff(70,34);
 		ACCcopyff(71,35);
 		ACCprocess(64);
 		ACCprocess(77);
 		ACCwrite(461);
 		ACCnewtext();
 		ACCdone();
		break pro058_restart;
		{}

	}

	// _ _
	p058e0006:
	{
 		if (skipdoall('p058e0006')) break p058e0006;
 		ACCprocess(61);
		if (!CNDnoteq(69,254)) break p058e0006;
		if (!CNDnoteq(69,253)) break p058e0006;
 		ACCsysmess(99);
 		ACCsysmess(128);
 		ACCprocess(64);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCwrite(462);
 		ACCprocess(77);
 		ACCsysmess(125);
 		ACCprocess(88);
 		ACCprocess(41);
 		ACCcopyff(70,34);
 		ACCcopyff(71,35);
 		ACCwhato();
 		ACCwrite(463);
 		ACCprocess(77);
 		ACCnewtext();
 		ACCdone();
		break pro058_restart;
		{}

	}

	// _ _
	p058e0007:
	{
 		if (skipdoall('p058e0007')) break p058e0007;
 		ACCprocess(65);
 		ACCsysmess(126);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCwrite(464);
 		ACCprocess(77);
 		ACCnewtext();
 		ACCdone();
		break pro058_restart;
		{}

	}


}
}

function pro059()
{
process_restart=true;
pro059_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p059e0000:
	{
 		if (skipdoall('p059e0000')) break p059e0000;
 		ACCcopyff(67,69);
		{}

	}

	// _ _
	p059e0001:
	{
 		if (skipdoall('p059e0001')) break p059e0001;
		if (!CNDchance(30)) break p059e0001;
 		ACCdone();
		break pro059_restart;
		{}

	}

	// _ _
	p059e0002:
	{
 		if (skipdoall('p059e0002')) break p059e0002;
 		ACCprocess(27);
		if (!CNDsame(67,87)) break p059e0002;
		if (!CNDchance(50)) break p059e0002;
 		ACClet(69,1);
 		ACCdone();
		break pro059_restart;
		{}

	}

	// _ _
	p059e0003:
	{
 		if (skipdoall('p059e0003')) break p059e0003;
		if (!CNDsame(67,38)) break p059e0003;
		if (!CNDchance(50)) break p059e0003;
 		ACClet(69,2);
 		ACCdone();
		break pro059_restart;
		{}

	}

	// _ _
	p059e0004:
	{
 		if (skipdoall('p059e0004')) break p059e0004;
 		ACCrandom(99);
		{}

	}

	// _ _
	p059e0005:
	{
 		if (skipdoall('p059e0005')) break p059e0005;
		if (!CNDnotzero(99)) break p059e0005;
		if (!CNDeq(67,3)) break p059e0005;
		if (!CNDlt(99,26)) break p059e0005;
 		ACCclear(99);
 		ACClet(69,7);
		{}

	}

	// _ _
	p059e0006:
	{
 		if (skipdoall('p059e0006')) break p059e0006;
		if (!CNDnotzero(99)) break p059e0006;
		if (!CNDeq(67,3)) break p059e0006;
		if (!CNDgt(99,25)) break p059e0006;
		if (!CNDlt(99,51)) break p059e0006;
 		ACCclear(99);
 		ACClet(69,6);
		{}

	}

	// _ _
	p059e0007:
	{
 		if (skipdoall('p059e0007')) break p059e0007;
		if (!CNDnotzero(99)) break p059e0007;
		if (!CNDeq(67,3)) break p059e0007;
		if (!CNDgt(99,50)) break p059e0007;
		if (!CNDlt(99,76)) break p059e0007;
 		ACCclear(99);
 		ACClet(69,4);
		{}

	}

	// _ _
	p059e0008:
	{
 		if (skipdoall('p059e0008')) break p059e0008;
		if (!CNDnotzero(99)) break p059e0008;
		if (!CNDeq(67,3)) break p059e0008;
 		ACCclear(99);
 		ACCclear(69);
		{}

	}

	// _ _
	p059e0009:
	{
 		if (skipdoall('p059e0009')) break p059e0009;
		if (!CNDnotzero(99)) break p059e0009;
		if (!CNDeq(67,7)) break p059e0009;
		if (!CNDlt(99,51)) break p059e0009;
 		ACCclear(99);
 		ACClet(69,3);
		{}

	}

	// _ _
	p059e0010:
	{
 		if (skipdoall('p059e0010')) break p059e0010;
		if (!CNDnotzero(99)) break p059e0010;
		if (!CNDeq(67,7)) break p059e0010;
 		ACCclear(99);
 		ACClet(69,6);
		{}

	}

	// _ _
	p059e0011:
	{
 		if (skipdoall('p059e0011')) break p059e0011;
		if (!CNDnotzero(99)) break p059e0011;
		if (!CNDeq(67,6)) break p059e0011;
		if (!CNDlt(99,34)) break p059e0011;
 		ACCclear(99);
 		ACClet(69,7);
		{}

	}

	// _ _
	p059e0012:
	{
 		if (skipdoall('p059e0012')) break p059e0012;
		if (!CNDnotzero(99)) break p059e0012;
		if (!CNDeq(67,6)) break p059e0012;
		if (!CNDgt(99,66)) break p059e0012;
 		ACCclear(99);
 		ACClet(69,3);
		{}

	}

	// _ _
	p059e0013:
	{
 		if (skipdoall('p059e0013')) break p059e0013;
		if (!CNDnotzero(99)) break p059e0013;
		if (!CNDeq(67,6)) break p059e0013;
 		ACCclear(99);
 		ACCclear(69);
		{}

	}

	// _ _
	p059e0014:
	{
 		if (skipdoall('p059e0014')) break p059e0014;
		if (!CNDnotzero(99)) break p059e0014;
		if (!CNDeq(67,4)) break p059e0014;
		if (!CNDlt(99,34)) break p059e0014;
 		ACCclear(99);
 		ACClet(69,3);
		{}

	}

	// _ _
	p059e0015:
	{
 		if (skipdoall('p059e0015')) break p059e0015;
		if (!CNDnotzero(99)) break p059e0015;
		if (!CNDeq(67,4)) break p059e0015;
		if (!CNDgt(99,66)) break p059e0015;
 		ACCclear(99);
 		ACClet(69,5);
		{}

	}

	// _ _
	p059e0016:
	{
 		if (skipdoall('p059e0016')) break p059e0016;
		if (!CNDnotzero(99)) break p059e0016;
		if (!CNDeq(67,4)) break p059e0016;
 		ACCclear(99);
 		ACCclear(69);
		{}

	}

	// _ _
	p059e0017:
	{
 		if (skipdoall('p059e0017')) break p059e0017;
		if (!CNDnotzero(99)) break p059e0017;
 		ACCclear(99);
 		ACClet(69,4);
		{}

	}


}
}

function pro060()
{
process_restart=true;
pro060_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p060e0000:
	{
 		if (skipdoall('p060e0000')) break p060e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p060e0001:
	{
 		if (skipdoall('p060e0001')) break p060e0001;
		if (!CNDeq(51,29)) break p060e0001;
		if (!CNDcarried(29)) break p060e0001;
 		ACClet(67,253);
 		ACCprocess(23);
		if (!CNDnotzero(69)) break p060e0001;
 		ACCwrite(465);
 		ACCnewline();
 		ACCdone();
		break pro060_restart;
		{}

	}

	// _ _
	p060e0002:
	{
 		if (skipdoall('p060e0002')) break p060e0002;
		if (!CNDsame(38,54)) break p060e0002;
		if (!CNDlt(34,50)) break p060e0002;
 		ACCsysmess(142);
 		ACCdone();
		break pro060_restart;
		{}

	}

	// _ _
	p060e0003:
	{
 		if (skipdoall('p060e0003')) break p060e0003;
		if (!CNDeq(54,254)) break p060e0003;
		if (!CNDbnotzero(56,1)) break p060e0003;
 		ACCsysmess(37);
 		ACCprocess(64);
 		ACCwrite(466);
 		ACCputo(253);
 		ACCdone();
		break pro060_restart;
		{}

	}

	// _ _
	p060e0004:
	{
 		if (skipdoall('p060e0004')) break p060e0004;
		if (!CNDeq(54,253)) break p060e0004;
 		ACCsysmess(29);
 		ACCprocess(87);
 		ACCwrite(467);
 		ACCprocess(64);
 		ACCwrite(468);
 		ACCnewtext();
 		ACCdone();
		break pro060_restart;
		{}

	}

	// _ _
	p060e0005:
	{
 		if (skipdoall('p060e0005')) break p060e0005;
		if (!CNDeq(54,254)) break p060e0005;
 		ACCsysmess(40);
 		ACCprocess(8);
 		ACCwrite(469);
 		ACCnewtext();
 		ACCdone();
		break pro060_restart;
		{}

	}

	// _ _
	p060e0006:
	{
 		if (skipdoall('p060e0006')) break p060e0006;
		if (!CNDsame(54,38)) break p060e0006;
 		ACCsysmess(49);
 		ACCprocess(64);
 		ACCwrite(470);
 		ACCnewtext();
 		ACCdone();
		break pro060_restart;
		{}

	}

	// _ _
	p060e0007:
	{
 		if (skipdoall('p060e0007')) break p060e0007;
 		ACCsysmess(28);
 		ACCnewtext();
 		ACCdone();
		break pro060_restart;
		{}

	}


}
}

function pro061()
{
process_restart=true;
pro061_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p061e0000:
	{
 		if (skipdoall('p061e0000')) break p061e0000;
 		ACCclear(69);
 		ACCwhato();
		if (!CNDsame(54,38)) break p061e0000;
 		ACCcopyff(38,69);
 		ACCdone();
		break pro061_restart;
		{}

	}

	// _ _
	p061e0001:
	{
 		if (skipdoall('p061e0001')) break p061e0001;
		if (!CNDeq(54,254)) break p061e0001;
 		ACCcopyff(54,69);
 		ACCdone();
		break pro061_restart;
		{}

	}

	// _ _
	p061e0002:
	{
 		if (skipdoall('p061e0002')) break p061e0002;
		if (!CNDeq(54,253)) break p061e0002;
 		ACCcopyff(54,69);
 		ACCdone();
		break pro061_restart;
		{}

	}

	// _ _
	p061e0003:
	{
 		if (skipdoall('p061e0003')) break p061e0003;
 		ACCcopyff(54,68);
 		ACClet(67,255);
 		ACCprocess(16);
		if (!CNDzero(69)) break p061e0003;
 		ACClet(67,254);
 		ACCprocess(16);
		if (!CNDzero(69)) break p061e0003;
 		ACClet(67,253);
 		ACCprocess(16);
		{}

	}


}
}

function pro062()
{
process_restart=true;
pro062_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p062e0000:
	{
 		if (skipdoall('p062e0000')) break p062e0000;
 		ACCcopyff(34,77);
 		ACCcopyff(35,72);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCprocess(61);
 		ACCcopyff(77,34);
 		ACCcopyff(72,35);
 		ACCwhato();
		{}

	}


}
}

function pro063()
{
process_restart=true;
pro063_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p063e0000:
	{
 		if (skipdoall('p063e0000')) break p063e0000;
 		ACCwhato();
 		ACCwrite(471);
 		ACCprocess(38);
		if (!CNDeq(21,1)) break p063e0000;
 		ACCwrite(472);
		{}

	}

	// _ _
	p063e0001:
	{
 		if (skipdoall('p063e0001')) break p063e0001;
		if (!CNDnoteq(21,1)) break p063e0001;
 		ACCwrite(473);
 		ACCprocess(6);
		{}

	}

	// _ _
	p063e0002:
	{
 		if (skipdoall('p063e0002')) break p063e0002;
 		ACCwrite(474);
 		ACCdone();
		break pro063_restart;
		{}

	}


}
}

function pro064()
{
process_restart=true;
pro064_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p064e0000:
	{
 		if (skipdoall('p064e0000')) break p064e0000;
		if (!CNDgt(34,49)) break p064e0000;
 		ACCprocess(6);
 		ACCwrite(475);
		{}

	}

	// _ _
	p064e0001:
	{
 		if (skipdoall('p064e0001')) break p064e0001;
 		ACCwrite(476);
 		ACCdone();
		break pro064_restart;
		{}

	}


}
}

function pro065()
{
process_restart=true;
pro065_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p065e0000:
	{
 		if (skipdoall('p065e0000')) break p065e0000;
		if (!CNDgt(34,49)) break p065e0000;
 		ACCprocess(7);
 		ACCwrite(477);
		{}

	}

	// _ _
	p065e0001:
	{
 		if (skipdoall('p065e0001')) break p065e0001;
 		ACCwrite(478);
 		ACCdone();
		break pro065_restart;
		{}

	}


}
}

function pro066()
{
process_restart=true;
pro066_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p066e0000:
	{
 		if (skipdoall('p066e0000')) break p066e0000;
		if (!CNDgt(34,49)) break p066e0000;
 		ACCprocess(8);
 		ACCprocess(38);
		if (!CNDzero(22)) break p066e0000;
 		ACCwrite(479);
		{}

	}

	// _ _
	p066e0001:
	{
 		if (skipdoall('p066e0001')) break p066e0001;
 		ACCwrite(480);
 		ACCdone();
		break pro066_restart;
		{}

	}


}
}

function pro067()
{
process_restart=true;
pro067_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p067e0000:
	{
 		if (skipdoall('p067e0000')) break p067e0000;
 		ACCwhato();
 		ACCwrite(481);
 		ACCprocess(38);
		if (!CNDnoteq(21,1)) break p067e0000;
 		ACCwrite(482);
		{}

	}

	// _ _
	p067e0001:
	{
 		if (skipdoall('p067e0001')) break p067e0001;
 		ACCprocess(6);
 		ACCwrite(483);
 		ACCdone();
		break pro067_restart;
		{}

	}


}
}

function pro068()
{
process_restart=true;
pro068_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p068e0000:
	{
 		if (skipdoall('p068e0000')) break p068e0000;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCwrite(484);
 		ACCprocess(77);
 		ACCdone();
		break pro068_restart;
		{}

	}


}
}

function pro069()
{
process_restart=true;
pro069_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p069e0000:
	{
 		if (skipdoall('p069e0000')) break p069e0000;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCprocess(6);
 		ACCwrite(485);
 		ACCprocess(77);
 		ACCdone();
		break pro069_restart;
		{}

	}


}
}

function pro070()
{
process_restart=true;
pro070_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p070e0000:
	{
 		if (skipdoall('p070e0000')) break p070e0000;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCprocess(8);
 		ACCwrite(486);
 		ACCprocess(77);
 		ACCdone();
		break pro070_restart;
		{}

	}


}
}

function pro071()
{
process_restart=true;
pro071_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p071e0000:
	{
 		if (skipdoall('p071e0000')) break p071e0000;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCprocess(67);
		{}

	}

	// _ _
	p071e0001:
	{
 		if (skipdoall('p071e0001')) break p071e0001;
 		ACCprocess(77);
		{}

	}


}
}

function pro072()
{
process_restart=true;
pro072_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p072e0000:
	{
 		if (skipdoall('p072e0000')) break p072e0000;
 		ACCwrite(487);
 		ACCprocess(87);
 		ACCdone();
		break pro072_restart;
		{}

	}


}
}

function pro073()
{
process_restart=true;
pro073_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p073e0000:
	{
 		if (skipdoall('p073e0000')) break p073e0000;
 		ACCwrite(488);
 		ACCprocess(38);
		if (!CNDgt(21,2)) break p073e0000;
 		ACCwrite(489);
 		ACCdone();
		break pro073_restart;
		{}

	}

	// _ _
	p073e0001:
	{
 		if (skipdoall('p073e0001')) break p073e0001;
 		ACCdone();
		break pro073_restart;
		{}

	}


}
}

function pro074()
{
process_restart=true;
pro074_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p074e0000:
	{
 		if (skipdoall('p074e0000')) break p074e0000;
		if (!CNDzero(15)) break p074e0000;
 		ACCset(15);
 		ACCplus(30,1);
		{}

	}


}
}

function pro075()
{
process_restart=true;
pro075_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p075e0000:
	{
 		if (skipdoall('p075e0000')) break p075e0000;
		if (!CNDzero(97)) break p075e0000;
 		ACCset(97);
 		ACCplus(30,2);
		{}

	}


}
}

function pro076()
{
process_restart=true;
pro076_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p076e0000:
	{
 		if (skipdoall('p076e0000')) break p076e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p076e0001:
	{
 		if (skipdoall('p076e0001')) break p076e0001;
		if (!CNDeq(51,29)) break p076e0001;
		if (!CNDworn(29)) break p076e0001;
 		ACClet(67,253);
 		ACCprocess(23);
		if (!CNDgt(69,1)) break p076e0001;
 		ACCwrite(490);
 		ACCnewline();
 		ACCdone();
		break pro076_restart;
		{}

	}

	// _ _
	p076e0002:
	{
 		if (skipdoall('p076e0002')) break p076e0002;
		if (!CNDsame(38,54)) break p076e0002;
		if (!CNDlt(34,50)) break p076e0002;
 		ACCsysmess(145);
 		ACCprocess(87);
 		ACCwrite(491);
 		ACCdone();
		break pro076_restart;
		{}

	}

	// _ _
	p076e0003:
	{
 		if (skipdoall('p076e0003')) break p076e0003;
		if (!CNDeq(54,253)) break p076e0003;
 		ACCsysmess(38);
 		ACCprocess(64);
 		ACCwrite(492);
 		ACCputo(254);
 		ACCdone();
		break pro076_restart;
		{}

	}

	// _ _
	p076e0004:
	{
 		if (skipdoall('p076e0004')) break p076e0004;
		if (!CNDeq(54,254)) break p076e0004;
 		ACCsysmess(50);
 		ACCprocess(72);
 		ACCsysmess(106);
 		ACCprocess(87);
 		ACCwrite(493);
 		ACCnewtext();
 		ACCdone();
		break pro076_restart;
		{}

	}

	// _ _
	p076e0005:
	{
 		if (skipdoall('p076e0005')) break p076e0005;
		if (!CNDsame(54,38)) break p076e0005;
 		ACCsysmess(49);
 		ACCprocess(64);
 		ACCwrite(494);
 		ACCnewtext();
 		ACCdone();
		break pro076_restart;
		{}

	}

	// _ _
	p076e0006:
	{
 		if (skipdoall('p076e0006')) break p076e0006;
 		ACCsysmess(28);
 		ACCnewtext();
 		ACCdone();
		break pro076_restart;
		{}

	}


}
}

function pro077()
{
process_restart=true;
pro077_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p077e0000:
	{
 		if (skipdoall('p077e0000')) break p077e0000;
 		ACCcopyff(75,34);
 		ACCcopyff(74,35);
 		ACCcopyff(81,51);
 		ACCcopyff(80,54);
 		ACCcopyff(83,55);
 		ACCcopyff(79,56);
 		ACCcopyff(82,57);
 		ACCdone();
		break pro077_restart;
		{}

	}


}
}

function pro078()
{
process_restart=true;
pro078_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p078e0000:
	{
 		if (skipdoall('p078e0000')) break p078e0000;
 		ACClet(67,23);
 		ACCprocess(26);
		if (!CNDnotzero(69)) break p078e0000;
 		ACCwrite(495);
 		ACCnewline();
 		ACCdone();
		break pro078_restart;
		{}

	}

	// _ _
	p078e0001:
	{
 		if (skipdoall('p078e0001')) break p078e0001;
		if (!CNDpresent(22)) break p078e0001;
		if (!CNDpresent(11)) break p078e0001;
 		ACCwrite(496);
 		ACCwrite(497);
 		ACCnewline();
		{}

	}

	// _ _
	p078e0002:
	{
 		if (skipdoall('p078e0002')) break p078e0002;
 		ACClet(34,69);
 		ACClet(35,255);
 		ACCwhato();
		if (!CNDsame(92,54)) break p078e0002;
 		ACCplace(11,23);
 		ACCwrite(498);
 		ACCnewline();
 		ACCdone();
		break pro078_restart;
		{}

	}

	// _ _
	p078e0003:
	{
 		if (skipdoall('p078e0003')) break p078e0003;
		if (!CNDsame(92,38)) break p078e0003;
 		ACCwrite(499);
 		ACCnewline();
 		ACCdone();
		break pro078_restart;
		{}

	}

	// _ _
	p078e0004:
	{
 		if (skipdoall('p078e0004')) break p078e0004;
 		ACClet(34,73);
 		ACClet(35,255);
 		ACCwhato();
		if (!CNDsame(92,54)) break p078e0004;
 		ACCplace(14,23);
 		ACCwrite(500);
 		ACCnewline();
 		ACCset(64);
 		ACCdone();
		break pro078_restart;
		{}

	}

	// _ _
	p078e0005:
	{
 		if (skipdoall('p078e0005')) break p078e0005;
 		ACCwrite(501);
 		ACCnewline();
		{}

	}


}
}

function pro079()
{
process_restart=true;
pro079_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p079e0000:
	{
 		if (skipdoall('p079e0000')) break p079e0000;
 		ACClet(67,23);
 		ACCprocess(26);
		if (!CNDzero(69)) break p079e0000;
 		ACCwrite(502);
 		ACCnewline();
 		ACCdone();
		break pro079_restart;
		{}

	}

	// _ _
	p079e0001:
	{
 		if (skipdoall('p079e0001')) break p079e0001;
		if (!CNDpresent(22)) break p079e0001;
		if (!CNDisat(11,23)) break p079e0001;
 		ACCwrite(503);
 		ACCwrite(504);
 		ACCnewline();
		{}

	}

	// _ _
	p079e0002:
	{
 		if (skipdoall('p079e0002')) break p079e0002;
		if (!CNDpresent(22)) break p079e0002;
		if (!CNDisat(14,23)) break p079e0002;
 		ACCwrite(505);
 		ACCwrite(506);
 		ACCnewline();
		{}

	}

	// _ _
	p079e0003:
	{
 		if (skipdoall('p079e0003')) break p079e0003;
		if (!CNDisat(11,23)) break p079e0003;
		if (!CNDeq(92,24)) break p079e0003;
 		ACCplace(11,25);
 		ACCwrite(507);
 		ACCwrite(508);
 		ACCnewline();
 		ACCdone();
		break pro079_restart;
		{}

	}

	// _ _
	p079e0004:
	{
 		if (skipdoall('p079e0004')) break p079e0004;
		if (!CNDisat(14,23)) break p079e0004;
		if (!CNDeq(92,24)) break p079e0004;
		if (!CNDisnotat(11,25)) break p079e0004;
 		ACCplace(14,25);
 		ACCwrite(509);
 		ACCwrite(510);
 		ACCnewline();
 		ACCdone();
		break pro079_restart;
		{}

	}

	// _ _
	p079e0005:
	{
 		if (skipdoall('p079e0005')) break p079e0005;
		if (!CNDisat(11,23)) break p079e0005;
 		ACCcopyfo(92,11);
 		ACCwrite(511);
 		ACCnewline();
 		ACCdone();
		break pro079_restart;
		{}

	}

	// _ _
	p079e0006:
	{
 		if (skipdoall('p079e0006')) break p079e0006;
		if (!CNDisat(14,23)) break p079e0006;
 		ACCcopyfo(92,14);
 		ACCwrite(512);
 		ACCnewline();
 		ACCdone();
		break pro079_restart;
		{}

	}


}
}

function pro080()
{
process_restart=true;
pro080_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p080e0000:
	{
 		if (skipdoall('p080e0000')) break p080e0000;
 		ACClet(67,23);
 		ACCprocess(26);
		if (!CNDzero(69)) break p080e0000;
 		ACCdone();
		break pro080_restart;
		{}

	}

	// _ _
	p080e0001:
	{
 		if (skipdoall('p080e0001')) break p080e0001;
 		ACCwrite(513);
		{}

	}

	// _ _
	p080e0002:
	{
 		if (skipdoall('p080e0002')) break p080e0002;
		if (!CNDisat(11,23)) break p080e0002;
 		ACCwrite(514);
		{}

	}

	// _ _
	p080e0003:
	{
 		if (skipdoall('p080e0003')) break p080e0003;
		if (!CNDisat(14,23)) break p080e0003;
 		ACCwrite(515);
		{}

	}

	// _ _
	p080e0004:
	{
 		if (skipdoall('p080e0004')) break p080e0004;
 		ACCwrite(516);
 		ACCdone();
		break pro080_restart;
		{}

	}


}
}

function pro081()
{
process_restart=true;
pro081_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p081e0000:
	{
 		if (skipdoall('p081e0000')) break p081e0000;
 		ACCcopyff(92,91);
		if (!CNDmove(92)) break p081e0000;
		{}

	}

	// _ _
	p081e0001:
	{
 		if (skipdoall('p081e0001')) break p081e0001;
		if (!CNDeq(92,20)) break p081e0001;
 		ACCcopyff(91,92);
		{}

	}

	// _ _
	p081e0002:
	{
 		if (skipdoall('p081e0002')) break p081e0002;
		if (!CNDeq(92,27)) break p081e0002;
 		ACCcopyff(91,92);
		{}

	}

	// _ _
	p081e0003:
	{
 		if (skipdoall('p081e0003')) break p081e0003;
		if (!CNDnotsame(91,92)) break p081e0003;
 		ACCcopyfo(92,22);
 		ACCwrite(517);
 		ACCnewline();
		{}

	}

	// _ _
	p081e0004:
	{
 		if (skipdoall('p081e0004')) break p081e0004;
		if (!CNDnotsame(91,92)) break p081e0004;
		if (!CNDpresent(22)) break p081e0004;
 		ACCwrite(518);
 		ACCnewline();
		{}

	}

	// _ _
	p081e0005:
	{
 		if (skipdoall('p081e0005')) break p081e0005;
		if (!CNDnotsame(91,92)) break p081e0005;
		if (!CNDsame(91,38)) break p081e0005;
 		ACCwrite(519);
 		ACCnewline();
		{}

	}

	// _ _
	p081e0006:
	{
 		if (skipdoall('p081e0006')) break p081e0006;
		if (!CNDsame(91,92)) break p081e0006;
 		ACCwrite(520);
 		ACCnewline();
		{}

	}


}
}

function pro082()
{
process_restart=true;
pro082_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p082e0000:
	{
 		if (skipdoall('p082e0000')) break p082e0000;
		if (!CNDpresent(8)) break p082e0000;
 		ACCwrite(521);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0001:
	{
 		if (skipdoall('p082e0001')) break p082e0001;
		if (!CNDnoun2(61)) break p082e0001;
		if (!CNDpresent(6)) break p082e0001;
		if (!CNDnotworn(6)) break p082e0001;
 		ACCwrite(522);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0002:
	{
 		if (skipdoall('p082e0002')) break p082e0002;
		if (!CNDnoun2(66)) break p082e0002;
		if (!CNDpresent(6)) break p082e0002;
		if (!CNDnotworn(6)) break p082e0002;
 		ACCwrite(523);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0003:
	{
 		if (skipdoall('p082e0003')) break p082e0003;
		if (!CNDnoun2(61)) break p082e0003;
		if (!CNDworn(6)) break p082e0003;
 		ACCplace(8,19);
 		ACCwrite(524);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0004:
	{
 		if (skipdoall('p082e0004')) break p082e0004;
		if (!CNDnoun2(66)) break p082e0004;
		if (!CNDworn(6)) break p082e0004;
 		ACCplace(8,19);
 		ACCwrite(525);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0005:
	{
 		if (skipdoall('p082e0005')) break p082e0005;
		if (!CNDnoun2(66)) break p082e0005;
 		ACCwrite(526);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0006:
	{
 		if (skipdoall('p082e0006')) break p082e0006;
		if (!CNDnoun2(60)) break p082e0006;
		if (!CNDcarried(5)) break p082e0006;
 		ACCplace(8,19);
 		ACCwrite(527);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0007:
	{
 		if (skipdoall('p082e0007')) break p082e0007;
		if (!CNDnoun2(60)) break p082e0007;
 		ACCwrite(528);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0008:
	{
 		if (skipdoall('p082e0008')) break p082e0008;
		if (!CNDnoun2(52)) break p082e0008;
		if (!CNDcarried(0)) break p082e0008;
 		ACCwrite(529);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0009:
	{
 		if (skipdoall('p082e0009')) break p082e0009;
		if (!CNDnoun2(52)) break p082e0009;
 		ACCwrite(530);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}

	// _ _
	p082e0010:
	{
 		if (skipdoall('p082e0010')) break p082e0010;
 		ACCwrite(531);
 		ACCnewline();
 		ACCdone();
		break pro082_restart;
		{}

	}


}
}

function pro083()
{
process_restart=true;
pro083_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p083e0000:
	{
 		if (skipdoall('p083e0000')) break p083e0000;
 		ACCwhato();
		{}

	}

	// _ _
	p083e0001:
	{
 		if (skipdoall('p083e0001')) break p083e0001;
 		ACCprocess(61);
		if (!CNDzero(69)) break p083e0001;
 		ACCprocess(62);
		if (!CNDzero(69)) break p083e0001;
 		ACCsysmess(122);
 		ACCnewtext();
 		ACCdone();
		break pro083_restart;
		{}

	}

	// _ _
	p083e0002:
	{
 		if (skipdoall('p083e0002')) break p083e0002;
 		ACCprocess(54);
		if (!CNDnotsame(38,69)) break p083e0002;
		if (!CNDnoteq(69,254)) break p083e0002;
		if (!CNDnoteq(69,253)) break p083e0002;
 		ACCsysmess(133);
 		ACCprocess(64);
 		ACCwrite(532);
 		ACCnewtext();
 		ACCdone();
		break pro083_restart;
		{}

	}

	// _ _
	p083e0003:
	{
 		if (skipdoall('p083e0003')) break p083e0003;
 		ACCprocess(33);
		if (!CNDzero(69)) break p083e0003;
		if (!CNDlt(44,50)) break p083e0003;
 		ACCprocess(68);
 		ACCsysmess(143);
 		ACCnewtext();
 		ACCdone();
		break pro083_restart;
		{}

	}

	// _ _
	p083e0004:
	{
 		if (skipdoall('p083e0004')) break p083e0004;
		if (!CNDzero(69)) break p083e0004;
 		ACCsysmess(99);
 		ACCsysmess(118);
 		ACCsysmess(121);
 		ACCwrite(533);
 		ACCprocess(70);
 		ACCwrite(534);
 		ACCnewtext();
 		ACCdone();
		break pro083_restart;
		{}

	}

	// _ _
	p083e0005:
	{
 		if (skipdoall('p083e0005')) break p083e0005;
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCprocess(38);
 		ACCprocess(77);
		if (!CNDnotzero(17)) break p083e0005;
		if (!CNDzero(16)) break p083e0005;
 		ACCsysmess(99);
 		ACCsysmess(118);
 		ACCsysmess(121);
 		ACCprocess(71);
 		ACCsysmess(120);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCprocess(88);
 		ACCsysmess(116);
 		ACCprocess(87);
 		ACCprocess(77);
 		ACCwrite(535);
 		ACCdone();
		break pro083_restart;
		{}

	}

	// _ _
	p083e0006:
	{
 		if (skipdoall('p083e0006')) break p083e0006;
 		ACCprocess(20);
		if (!CNDnotsame(54,69)) break p083e0006;
		if (!CNDlt(44,50)) break p083e0006;
 		ACCprocess(68);
 		ACCsysmess(143);
 		ACCnewtext();
 		ACCdone();
		break pro083_restart;
		{}

	}

	// _ _
	p083e0007:
	{
 		if (skipdoall('p083e0007')) break p083e0007;
		if (!CNDnotsame(54,69)) break p083e0007;
 		ACCsysmess(52);
 		ACCprocess(69);
 		ACCwrite(536);
 		ACCnewtext();
 		ACCdone();
		break pro083_restart;
		{}

	}

	// _ _
	p083e0008:
	{
 		if (skipdoall('p083e0008')) break p083e0008;
		if (!CNDlt(44,50)) break p083e0008;
 		ACCsysmess(99);
 		ACCsysmess(107);
 		ACCprocess(64);
 		ACCsysmess(125);
 		ACCprocess(88);
 		ACCprocess(41);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
 		ACCwrite(537);
 		ACCprocess(77);
 		ACCnewtext();
 		ACCdone();
		break pro083_restart;
		{}

	}

	// _ _
	p083e0009:
	{
 		if (skipdoall('p083e0009')) break p083e0009;
 		ACCprocess(38);
		if (!CNDnotzero(20)) break p083e0009;
 		ACCsysmess(99);
 		ACCsysmess(118);
 		ACCprocess(64);
 		ACCsysmess(130);
 		ACCprocess(88);
 		ACCsysmess(131);
 		ACCprocess(71);
 		ACCwrite(538);
 		ACCnewtext();
 		ACCdone();
		break pro083_restart;
		{}

	}

	// _ _
	p083e0010:
	{
 		if (skipdoall('p083e0010')) break p083e0010;
 		ACCputo(254);
 		ACCsysmess(137);
 		ACCprocess(64);
 		ACCwrite(539);
 		ACCprocess(71);
 		ACCwrite(540);
 		ACCdone();
		break pro083_restart;
		{}

	}


}
}

function pro084()
{
process_restart=true;
pro084_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p084e0000:
	{
 		if (skipdoall('p084e0000')) break p084e0000;
 		ACCdestroy(37);
 		ACCdestroy(38);
 		ACCdestroy(39);
 		ACCdestroy(38);
 		ACCdestroy(40);
 		ACCdestroy(41);
 		ACCdestroy(42);
 		ACCdestroy(43);
 		ACCdestroy(44);
 		ACCdestroy(45);
 		ACCdestroy(46);
 		ACCdestroy(47);
 		ACCdestroy(48);
		{}

	}

	// _ _
	p084e0001:
	{
 		if (skipdoall('p084e0001')) break p084e0001;
 		ACCcopyff(38,104);
 		ACClet(33,13);
		if (!CNDmove(104)) break p084e0001;
		if (!CNDnotsame(38,104)) break p084e0001;
 		ACCplace(37,33);
		{}

	}

	// _ _
	p084e0002:
	{
 		if (skipdoall('p084e0002')) break p084e0002;
 		ACCcopyff(38,104);
 		ACClet(33,2);
		if (!CNDmove(104)) break p084e0002;
		if (!CNDnotsame(38,104)) break p084e0002;
 		ACCplace(38,33);
		{}

	}

	// _ _
	p084e0003:
	{
 		if (skipdoall('p084e0003')) break p084e0003;
 		ACCcopyff(38,104);
 		ACClet(33,3);
		if (!CNDmove(104)) break p084e0003;
		if (!CNDnotsame(38,104)) break p084e0003;
 		ACCplace(39,33);
		{}

	}

	// _ _
	p084e0004:
	{
 		if (skipdoall('p084e0004')) break p084e0004;
 		ACCcopyff(38,104);
 		ACClet(33,4);
		if (!CNDmove(104)) break p084e0004;
		if (!CNDnotsame(38,104)) break p084e0004;
 		ACCplace(40,33);
		{}

	}

	// _ _
	p084e0005:
	{
 		if (skipdoall('p084e0005')) break p084e0005;
 		ACCcopyff(38,104);
 		ACClet(33,5);
		if (!CNDmove(104)) break p084e0005;
		if (!CNDnotsame(38,104)) break p084e0005;
 		ACCplace(41,33);
		{}

	}

	// _ _
	p084e0006:
	{
 		if (skipdoall('p084e0006')) break p084e0006;
 		ACCcopyff(38,104);
 		ACClet(33,7);
		if (!CNDmove(104)) break p084e0006;
		if (!CNDnotsame(38,104)) break p084e0006;
 		ACCplace(43,33);
		{}

	}

	// _ _
	p084e0007:
	{
 		if (skipdoall('p084e0007')) break p084e0007;
 		ACCcopyff(38,104);
 		ACClet(33,6);
		if (!CNDmove(104)) break p084e0007;
		if (!CNDnotsame(38,104)) break p084e0007;
 		ACCplace(42,33);
		{}

	}

	// _ _
	p084e0008:
	{
 		if (skipdoall('p084e0008')) break p084e0008;
 		ACCcopyff(38,104);
 		ACClet(33,8);
		if (!CNDmove(104)) break p084e0008;
		if (!CNDnotsame(38,104)) break p084e0008;
 		ACCplace(44,33);
		{}

	}

	// _ _
	p084e0009:
	{
 		if (skipdoall('p084e0009')) break p084e0009;
 		ACCcopyff(38,104);
 		ACClet(33,11);
		if (!CNDmove(104)) break p084e0009;
		if (!CNDnotsame(38,104)) break p084e0009;
 		ACCplace(47,33);
		{}

	}

	// _ _
	p084e0010:
	{
 		if (skipdoall('p084e0010')) break p084e0010;
 		ACCcopyff(38,104);
 		ACClet(33,12);
		if (!CNDmove(104)) break p084e0010;
		if (!CNDnotsame(38,104)) break p084e0010;
 		ACCplace(48,33);
		{}

	}

	// _ _
	p084e0011:
	{
 		if (skipdoall('p084e0011')) break p084e0011;
 		ACCcopyff(38,104);
 		ACClet(33,9);
		if (!CNDmove(104)) break p084e0011;
		if (!CNDnotsame(38,104)) break p084e0011;
 		ACCplace(45,33);
		{}

	}

	// _ _
	p084e0012:
	{
 		if (skipdoall('p084e0012')) break p084e0012;
 		ACCcopyff(38,104);
 		ACClet(33,10);
		if (!CNDmove(104)) break p084e0012;
		if (!CNDnotsame(38,104)) break p084e0012;
 		ACCplace(46,33);
		{}

	}

	// _ _
	p084e0013:
	{
 		if (skipdoall('p084e0013')) break p084e0013;
		if (!CNDat(8)) break p084e0013;
 		ACCplace(46,33);
 		ACCplace(44,33);
		{}

	}

	// _ _
	p084e0014:
	{
 		if (skipdoall('p084e0014')) break p084e0014;
		if (!CNDat(11)) break p084e0014;
 		ACCplace(45,33);
 		ACCplace(40,33);
		{}

	}

	// _ _
	p084e0015:
	{
 		if (skipdoall('p084e0015')) break p084e0015;
		if (!CNDat(12)) break p084e0015;
		if (!CNDnotzero(85)) break p084e0015;
 		ACCplace(46,33);
 		ACCplace(47,33);
		{}

	}

	// _ _
	p084e0016:
	{
 		if (skipdoall('p084e0016')) break p084e0016;
		if (!CNDat(14)) break p084e0016;
		if (!CNDnotzero(102)) break p084e0016;
 		ACCplace(40,33);
		{}

	}

	// _ _
	p084e0017:
	{
 		if (skipdoall('p084e0017')) break p084e0017;
		if (!CNDat(14)) break p084e0017;
		if (!CNDworn(6)) break p084e0017;
 		ACCplace(38,33);
		{}

	}

	// _ _
	p084e0018:
	{
 		if (skipdoall('p084e0018')) break p084e0018;
		if (!CNDat(18)) break p084e0018;
 		ACCplace(37,33);
		{}

	}

	// _ _
	p084e0019:
	{
 		if (skipdoall('p084e0019')) break p084e0019;
		if (!CNDat(19)) break p084e0019;
 		ACCplace(39,33);
		{}

	}

	// _ _
	p084e0020:
	{
 		if (skipdoall('p084e0020')) break p084e0020;
		if (!CNDat(26)) break p084e0020;
 		ACCplace(46,33);
 		ACCplace(48,33);
		{}

	}

	// _ _
	p084e0021:
	{
 		if (skipdoall('p084e0021')) break p084e0021;
		if (!CNDat(27)) break p084e0021;
		if (!CNDnotzero(26)) break p084e0021;
 		ACCplace(38,33);
		{}

	}

	// _ _
	p084e0022:
	{
 		if (skipdoall('p084e0022')) break p084e0022;
		if (!CNDat(28)) break p084e0022;
 		ACCplace(45,33);
		{}

	}

	// _ _
	p084e0023:
	{
 		if (skipdoall('p084e0023')) break p084e0023;
 		ACCwrite(541);
 		ACClistat(33);
 		ACCdone();
		break pro084_restart;
		{}

	}


}
}

function pro085()
{
process_restart=true;
pro085_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p085e0000:
	{
 		if (skipdoall('p085e0000')) break p085e0000;
		if (!CNDnotzero(100)) break p085e0000;
 		ACCwrite(542);
 		ACCnewline();
 		ACCdone();
		break pro085_restart;
		{}

	}

	// _ _
	p085e0001:
	{
 		if (skipdoall('p085e0001')) break p085e0001;
 		ACCwrite(543);
		{}

	}

	// _ _
	p085e0002:
	{
 		if (skipdoall('p085e0002')) break p085e0002;
		if (!CNDnoteq(33,24)) break p085e0002;
 		ACCwrite(544);
		{}

	}

	// _ _
	p085e0003:
	{
 		if (skipdoall('p085e0003')) break p085e0003;
		if (!CNDeq(33,24)) break p085e0003;
 		ACCwrite(545);
		{}

	}

	// _ _
	p085e0004:
	{
 		if (skipdoall('p085e0004')) break p085e0004;
 		ACCwrite(546);
 		ACCnewline();
 		ACCset(100);
 		ACCplace(24,25);
 		ACCgoto(25);
		{}

	}

	// _ _
	p085e0005:
	{
 		if (skipdoall('p085e0005')) break p085e0005;
		if (!CNDcarried(12)) break p085e0005;
 		ACCwrite(547);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro085_restart;
		{}

	}

	// _ _
	p085e0006:
	{
 		if (skipdoall('p085e0006')) break p085e0006;
		if (!CNDisat(11,25)) break p085e0006;
 		ACCplus(30,3);
 		ACCwrite(548);
 		ACCnewline();
 		ACCdesc();
		break pro085_restart;
		{}

	}

	// _ _
	p085e0007:
	{
 		if (skipdoall('p085e0007')) break p085e0007;
 		ACCwrite(549);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro085_restart;
		{}

	}


}
}

function pro086()
{
process_restart=true;
pro086_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p086e0000:
	{
 		if (skipdoall('p086e0000')) break p086e0000;
		if (!CNDzero(93)) break p086e0000;
 		ACCset(93);
 		ACCplus(30,2);
		{}

	}

	// _ _
	p086e0001:
	{
 		if (skipdoall('p086e0001')) break p086e0001;
		if (!CNDeq(65,3)) break p086e0001;
 		ACCwrite(550);
 		ACCnewline();
 		ACCgoto(19);
 		ACCdesc();
		break pro086_restart;
		{}

	}

	// _ _
	p086e0002:
	{
 		if (skipdoall('p086e0002')) break p086e0002;
		if (!CNDeq(65,2)) break p086e0002;
 		ACCwrite(551);
 		ACCnewline();
 		ACCgoto(3);
 		ACCdesc();
		break pro086_restart;
		{}

	}

	// _ _
	p086e0003:
	{
 		if (skipdoall('p086e0003')) break p086e0003;
 		ACCwrite(552);
 		ACCnewline();
 		ACCscore();
 		ACCturns();
 		ACCend();
		break pro086_restart;
		{}

	}


}
}

function pro087()
{
process_restart=true;
pro087_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p087e0000:
	{
 		if (skipdoall('p087e0000')) break p087e0000;
 		ACCprocess(38);
		{}

	}

	// _ _
	p087e0001:
	{
 		if (skipdoall('p087e0001')) break p087e0001;
		if (!CNDeq(21,1)) break p087e0001;
 		ACCwrite(553);
 		ACCdone();
		break pro087_restart;
		{}

	}

	// _ _
	p087e0002:
	{
 		if (skipdoall('p087e0002')) break p087e0002;
		if (!CNDeq(21,2)) break p087e0002;
 		ACCwrite(554);
 		ACCdone();
		break pro087_restart;
		{}

	}

	// _ _
	p087e0003:
	{
 		if (skipdoall('p087e0003')) break p087e0003;
		if (!CNDeq(21,3)) break p087e0003;
 		ACCwrite(555);
 		ACCdone();
		break pro087_restart;
		{}

	}

	// _ _
	p087e0004:
	{
 		if (skipdoall('p087e0004')) break p087e0004;
		if (!CNDeq(21,4)) break p087e0004;
 		ACCwrite(556);
 		ACCdone();
		break pro087_restart;
		{}

	}


}
}

function pro088()
{
process_restart=true;
pro088_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p088e0000:
	{
 		if (skipdoall('p088e0000')) break p088e0000;
 		ACCprocess(38);
		{}

	}

	// _ _
	p088e0001:
	{
 		if (skipdoall('p088e0001')) break p088e0001;
		if (!CNDgt(21,2)) break p088e0001;
 		ACCwrite(557);
 		ACCdone();
		break pro088_restart;
		{}

	}


}
}

function pro089()
{
process_restart=true;
pro089_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p089e0000:
	{
 		if (skipdoall('p089e0000')) break p089e0000;
		if (!CNDisat(11,25)) break p089e0000;
		if (!CNDat(25)) break p089e0000;
 		ACCwrite(558);
 		ACCnewline();
 		ACCplace(11,26);
		{}

	}

	// _ _
	p089e0001:
	{
 		if (skipdoall('p089e0001')) break p089e0001;
		if (!CNDisnotat(11,25)) break p089e0001;
		if (!CNDat(25)) break p089e0001;
 		ACCwrite(559);
 		ACCnewline();
		{}

	}

	// _ _
	p089e0002:
	{
 		if (skipdoall('p089e0002')) break p089e0002;
		entry_for_doall = 'p089e0003';
		process_in_doall = 89;
 		ACCdoall(25);
		break pro089_restart;
		{}

	}

	// _ _
	p089e0003:
	{
 		if (skipdoall('p089e0003')) break p089e0003;
 		ACCwhato();
		if (!CNDnoteq(51,11)) break p089e0003;
 		ACCputo(26);
		if (!CNDat(25)) break p089e0003;
 		ACCwrite(560);
 		ACCprocess(64);
 		ACCwrite(561);
		{}

	}

	// _ _
	p089e0004:
	{
 		if (skipdoall('p089e0004')) break p089e0004;
 		ACCdone();
		break pro089_restart;
		{}

	}


}
}

function pro090()
{
process_restart=true;
pro090_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p090e0000:
	{
 		if (skipdoall('p090e0000')) break p090e0000;
		if (!CNDat(24)) break p090e0000;
 		ACCwrite(562);
 		ACCnewline();
		{}

	}

	// _ _
	p090e0001:
	{
 		if (skipdoall('p090e0001')) break p090e0001;
		if (!CNDat(25)) break p090e0001;
 		ACCwrite(563);
 		ACCnewline();
		{}

	}

	// _ _
	p090e0002:
	{
 		if (skipdoall('p090e0002')) break p090e0002;
 		ACCprocess(89);
		{}

	}

	// _ _
	p090e0003:
	{
 		if (skipdoall('p090e0003')) break p090e0003;
		if (!CNDat(25)) break p090e0003;
 		ACCwrite(564);
 		ACCnewline();
 		ACCgoto(26);
 		ACCdesc();
		break pro090_restart;
		{}

	}


}
}

last_process = 90;
// This file is (C) Carlos Sanchez 2014, released under the MIT license

// This function is called first by the start() function that runs when the game starts for the first time
var h_init = function()
{
}


// This function is called last by the start() function that runs when the game starts for the first time
var h_post =  function()
{
}

// This function is called when the engine tries to write any text
var h_writeText =  function (text)
{
	return text;
}

//This function is called every time the user types any order
var h_playerOrder = function(player_order)
{
	return player_order;
}

// This function is called every time a location is described, just after the location text is written
var h_description_init =  function ()
{
}

// This function is called every time a location is described, just after the process 1 is executed
var h_description_post = function()
{
}


// this function is called when the savegame object has been created, in order to be able to add more custom properties
var h_saveGame = function(savegame_object)
{
	return savegame_object;
}


// this function is called after the restore game function has restored the standard information in savegame, in order to restore any additional data included in a patched (by h_saveGame) savegame.
var h_restoreGame = function(savegame_object)
{
}

// this funcion is called before writing a message about player order beeing impossible to understand
var h_invalidOrder = function(player_order)
{
}

// this function is called when a sequence tag is found giving a chance for any hook library to provide a response
// tagparams receives the params inside the tag as an array  {XXXX|nn|mm|yy} => ['XXXX', 'nn', 'mm', 'yy']
var h_sequencetag = function (tagparams)
{
	return '';
}

// this function is called from certain points in the response or process tables via the HOOK condact. Depending on the string received it can do something or not.
// it's designed to allow direct javascript code to take control in the start database just installing a plugin library (avoiding the wirter need to enter code to activate the library)
var h_code = function(str)
{
	return false;
}


// this function is called from the keydown evente handler used by block and other functions to emulate a pause or waiting for a keypress. It is designed to allow plugin condacts or
// libraries to attend those key presses and react accordingly. In case a hook function decides that the standard keydown functions should not be processed, the hook function should return false.
// Also, any h_keydown replacement should probably do the same.
var h_keydown = function (event)
{
	return true;
}


// this function is called every time a process is called,  either by the internall loop of by the PROCESS condact, just before running it.
var h_preProcess = function(procno)
{

}

// this function is called every time a process is called just after the process exits (no matter which DONE status it has), either by the internall loop of by the PROCESS condact
var h_postProcess= function (procno)
{

}// This file is (C) Carlos Sanchez 2014, and is released under the MIT license
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ACCdesc()
{
	describe_location_flag = true;
	ACCbreak(); // Cancel doall loop
}


function ACCdone()
{
	done_flag = true;
}

function CNDat(locno)
{
  return (loc_here()==locno);
}

function CNDnotat(locno)
{
	 return (loc_here()!=locno);
}


function CNDatgt(locno)
{
	 return (loc_here()>locno);
}


function CNDatlt(locno)
{
	 return (loc_here()<locno);
}

function CNDpresent(objno)
{
	var loc = getObjectLocation(objno);
	if (loc == loc_here()) return true;
	if (loc == LOCATION_WORN) return true;
	if (loc == LOCATION_CARRIED) return true;
	if ( (!bittest(getFlag(FLAG_PARSER_SETTINGS),7)) && (objectIsContainer(loc) || objectIsSupporter(loc))  &&  (loc<=last_object_number)  && (CNDpresent(loc)) )  // Extended context and object in another object that is present
	{
		if (objectIsSupporter(loc)) return true;  // On supporter
		if ( objectIsContainer(loc) && objectIsAttr(loc, ATTR_OPENABLE) && objectIsAttr(loc, ATTR_OPEN)) return true; // In a openable & open container
		if ( objectIsContainer(loc) && (!objectIsAttr(loc, ATTR_OPENABLE)) ) return true; // In a not openable container
	}
	return false;
}

function CNDabsent(objno)
{
	return !CNDpresent(objno);
}

function CNDworn(objno)
{
	return (getObjectLocation(objno) == LOCATION_WORN);
}

function CNDnotworn(objno)
{
	return !CNDworn(objno);
}

function CNDcarried(objno)
{
	return (getObjectLocation(objno) == LOCATION_CARRIED);	
}

function CNDnotcarr(objno)
{
	return !CNDcarried(objno);
}


function CNDchance(percent)
{
	 var val = Math.floor((Math.random()*101));
	 return (val<=percent);
}

function CNDzero(flagno)
{
	return (getFlag(flagno) == 0);
}

function CNDnotzero(flagno)
{
	 return !CNDzero(flagno)
}


function CNDeq(flagno, value)
{
	return (getFlag(flagno) == value);
}

function CNDnoteq(flagno,value)
{
	return !CNDeq(flagno, value);
}

function CNDgt(flagno, value)
{
	return (getFlag(flagno) > value);
}

function CNDlt(flagno, value)
{
	return (getFlag(flagno) < value);
}


function CNDadject1(wordno)
{
	return (getFlag(FLAG_ADJECT1) == wordno);
}

function CNDadverb(wordno)
{
	return (getFlag(FLAG_ADVERB) == wordno);
}


function CNDtimeout()
{
	 return bittest(getFlag(FLAG_TIMEOUT_SETTINGS),7);
}


function CNDisat(objno, locno)
{
	return (getObjectLocation(objno) == locno);

}


function CNDisnotat(objno, locno)
{
	return !CNDisat(objno, locno);
}



function CNDprep(wordno)
{
	return (getFlag(FLAG_PREP) == wordno);
}




function CNDnoun2(wordno)
{
	return (getFlag(FLAG_NOUN2) == wordno);
}

function CNDadject2(wordno)
{
	return (getFlag(FLAG_ADJECT2) == wordno);
}

function CNDsame(flagno1,flagno2)
{
	return (getFlag(flagno1) == getFlag(flagno2));
}


function CNDnotsame(flagno1,flagno2)
{
	return (getFlag(flagno1) != getFlag(flagno2));
}

function ACCinven()
{
	var count = 0;
	writeSysMessage(SYSMESS_YOUARECARRYING);
	ACCnewline();
	var listnpcs_with_objects = !bittest(getFlag(FLAG_PARSER_SETTINGS),3);
	var i;
	for (i=0;i<num_objects;i++)
	{
		if ((getObjectLocation(i)) == LOCATION_CARRIED)
		{
			
			if ((listnpcs_with_objects) || (!objectIsNPC(i)))
			{
				writeObject(i);
				if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
				ACCnewline();
				count++;
			}
		}
		if (getObjectLocation(i) == LOCATION_WORN)
		{
			if (listnpcs_with_objects || (!objectIsNPC(i)))
			{
				writeObject(i);
				writeSysMessage(SYSMESS_WORN);
				count++;
				ACCnewline();
			}
		}
	}
	if (!count) 
	{
		 writeSysMessage(SYSMESS_CARRYING_NOTHING);
		 ACCnewline();
	}

	if (!listnpcs_with_objects)
	{
		var numNPC = getNPCCountAt(LOCATION_CARRIED);
		if (numNPC)	ACClistnpc(LOCATION_CARRIED);
	}
	done_flag = true;
}

function desc()
{
	describe_location_flag = true;
}


function ACCquit()
{
	inQUIT = true;
	writeSysMessage(SYSMESS_AREYOUSURE);
}


function ACCend()
{
	$('.input').hide();
	inEND = true;
	writeSysMessage(SYSMESS_PLAYAGAIN);
	done_flag = true;
}


function done()
{
	done_flag = true;
}

function ACCok()
{
	writeSysMessage(SYSMESS_OK);
	done_flag = true;
}



function ACCramsave()
{
	ramsave_value = getSaveGameObject();
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	localStorage.setItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME, savegame);
}

function ACCramload()
{
	if (ramsave_value==null) 
	{
		var json_str = localStorage.getItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME);
		if (json_str)
		{
			savegame_object = JSON.parse(json_str.trim());
			restoreSaveGameObject(savegame_object);
			ACCdesc();
			focusInput();
			return;
		}
		else
		{
			writeText (STR_RAMLOAD_ERROR);
			ACCnewline();
			done_flag = true;
			return;
		}
	}
	restoreSaveGameObject(ramsave_value);
	ACCdesc();
}

function ACCsave()
{
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	filename = prompt(getSysMessageText(SYSMESS_SAVEFILE),'');
	if ( filename !== null ) localStorage.setItem('ngpaws_savegame_' + filename.toUpperCase(), savegame);
	ACCok();
}

 
function ACCload() 	
{
	var json_str;
	filename = prompt(getSysMessageText(SYSMESS_LOADFILE),'');
	if ( filename !== null ) json_str = localStorage.getItem('ngpaws_savegame_' + filename.toUpperCase());
	if (json_str)
	{
		savegame_object = JSON.parse(json_str.trim());
		restoreSaveGameObject(savegame_object);
	}
	else
	{
		writeSysMessage(SYSMESS_FILENOTFOUND);
		ACCnewline();
		done_flag = true; return;
	}
	ACCdesc();
	focusInput();
}



function ACCturns()
{
	var turns = getFlag(FLAG_TURNS_HIGH) * 256 +  getFlag(FLAG_TURNS_LOW);
	writeSysMessage(SYSMESS_TURNS_START);
	writeText(turns + '');
	writeSysMessage(SYSMESS_TURNS_CONTINUE);
	if (turns > 1) writeSysMessage(SYSMESS_TURNS_PLURAL);
	writeSysMessage(SYSMESS_TURNS_END);
}

function ACCscore()
{
	var score = getFlag(FLAG_SCORE);
	writeSysMessage(SYSMESS_SCORE_START);
	writeText(score + '');
	writeSysMessage(SYSMESS_SCORE_END);
}


function ACCcls()
{
	clearScreen();
}

function ACCdropall()
{
	// Done in two different loops cause PAW did it like that, just a question of retro compatibility
	var i;
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_CARRIED)setObjectLocation(i, getFlag(FLAG_LOCATION));
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_WORN)setObjectLocation(i, getFlag(FLAG_LOCATION));
}


function ACCautog()
{
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS),7))  // Extended context for objects
	for (var i=0; i<num_objects;i++) // Try to find it in present containers/supporters
	{
		if (CNDpresent(i) && (isAccesibleContainer(i) || objectIsAttr(i, ATTR_SUPPORTER)) )  // If there is another object present that is an accesible container or a supporter
		{
			objno =findMatchingObject(i);
			if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
		}
	}
	success = false;
	writeSysMessage(SYSMESS_CANTSEETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautod()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };  
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautow()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautor()
{
	var objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOURENOTWEARINGTHAT);
	ACCnewtext();
	ACCdone();
}



function ACCpause(value)
{
 if (value == 0) value = 256;
 pauseRemainingTime = Math.floor(value /50 * 1000);	
 inPause = true;
 showAnykeyLayer();
} 

function ACCgoto(locno)
{
 	setFlag(FLAG_LOCATION,locno);
}

function ACCmessage(mesno)
{
	writeMessage(mesno);
	ACCnewline();
}


function ACCremove(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case loc_here():
			writeSysMessage(SYSMESS_YOUARENOTWEARINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case LOCATION_WORN:
			if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
			{
				writeSysMessage(SYSMESS_CANTREMOVE_TOOMANYOBJECTS);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_CARRIED);
			writeSysMessage(SYSMESS_YOUREMOVEOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUARENOTWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}


function trytoGet(objno)  // auxiliaty function for ACCget
{
	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		doall_flag = false;
		return;
	}
	var weight = 0;
	weight += getObjectWeight(objno);
	weight +=  getLocationObjectsWeight(LOCATION_CARRIED);
	weight +=  getLocationObjectsWeight(LOCATION_WORN);
	if (weight > getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}
	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;
}


 function ACCget(objno)
 {
 	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():
			trytoGet(objno);
			break;

		default: 
			if  ((locno<=last_object_number) && (CNDpresent(locno)))    // If it's not here, carried or worn but it present, that means that bit 7 of flag 12 is cleared, thus you can get objects from present containers/supporters
			{
				trytoGet(objno);
			}
			else
			{
				writeSysMessage(SYSMESS_CANTSEETHAT);
				ACCnewtext();
				ACCdone();
				return;
				break;
		    }
	}
 }

function ACCdrop(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			setObjectLocation(objno, loc_here());
			writeSysMessage(SYSMESS_YOUDROPOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}

function ACCwear(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWAERINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			if (!objectIsWearable(objno))
			{
				writeSysMessage(SYSMESS_YOUCANTWEAROBJECT);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_WORN);
			writeSysMessage(SYSMESS_YOUWEAROBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}



function ACCdestroy(objno)
{
	setObjectLocation(objno, LOCATION_NONCREATED);
}


function ACCcreate(objno)
{
	setObjectLocation(objno, loc_here());
}


function ACCswap(objno1,objno2)
{
	var locno1 = getObjectLocation (objno1);
	var locno2 = getObjectLocation (objno2);
	ACCplace (objno1,locno2);
	ACCplace (objno2,locno1);
	setReferredObject(objno2);
}


function ACCplace(objno, locno)
{
	setObjectLocation(objno, locno);
}

function ACCset(flagno)
{
	setFlag(flagno, SET_VALUE);
}

function ACCclear(flagno)
{
	setFlag(flagno,0);
}

function ACCplus(flagno,value)
{
	var newval = getFlag(flagno) + value;
	setFlag(flagno, newval);
}

function ACCminus(flagno,value)
{
    var newval = getFlag(flagno) - value;
    if (newval < 0) newval = 0;
	setFlag(flagno, newval);
}

function ACClet(flagno,value)
{
	setFlag(flagno,value);
}

function ACCnewline()
{
	writeText(STR_NEWLINE);
}

function ACCprint(flagno)
{
	writeText(getFlag(flagno) +'');
}

function ACCsysmess(sysno)
{
	writeSysMessage(sysno);
}

function ACCcopyof(objno,flagno)
{
	setFlag(flagno, getObjectLocation(objno))
}

function ACCcopyoo(objno1, objno2)
{
	setObjectLocation(objno2,getObjectLocation(objno1));
	setReferredObject(objno2);
}

function ACCcopyfo(flagno,objno)
{
	setObjectLocation(objno, getFlag(flagno));
}

function ACCcopyff(flagno1, flagno2)
{
	setFlag(flagno2, getFlag(flagno1));
}

function ACCadd(flagno1, flagno2)
{
	var newval = getFlag(flagno1) + getFlag(flagno2);
	setFlag(flagno2, newval);
}

function ACCsub(flagno1,flagno2)
{
	var newval = getFlag(flagno2) - getFlag(flagno1);
	if (newval < 0) newval = 0;
	setFlag(flagno2, newval);
}


function CNDparse()
{
	return (!getLogicSentence());
}


function ACClistat(locno, container_objno)   // objno is a container/suppoter number, used to list contents of objects
{
  var listingContainer = false;
  if (arguments.length > 1) listingContainer = true;
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);  
  objscount = objscount - concealed_or_scenery_objcount;
  if (!listingContainer) setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
  if (!objscount) return;
  var continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  if (listingContainer) 
  	{
  		writeText(' (');
  		if (objectIsAttr(container_objno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_OVER_YOUCANSEE); else if (objectIsAttr(container_objno, ATTR_CONTAINER)) writeSysMessage(SYSMESS_INSIDE_YOUCANSEE);
  		continouslisting = true;  // listing contents of container always continuous
  	}
  
  if (!listingContainer)
  {
    setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
    if (!continouslisting) ACCnewline();
  }
  var progresscount = 0;
  for (var i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if  ( ((!objectIsNPC(i)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)))  && (!objectIsAttr(i,ATTR_CONCEALED)) && (!objectIsAttr(i,ATTR_SCENERY))   ) // if not an NPC or parser setting say NPCs are considered objects, and object is not concealed nor scenery
  		  { 
  		     writeText(getObjectText(i)); 
  		     if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
  		     progresscount++
  		     if (continouslisting)
  		     {
		  			if (progresscount <= objscount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  					if (progresscount == objscount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  					if (!listingContainer) if (progresscount == objscount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
  if (arguments.length > 1) writeText(')');
}


function ACClistnpc(locno)
{
  var npccount =  getNPCCountAt(locno);
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  if (!npccount) return;
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  writeSysMessage(SYSMESS_NPCLISTSTART);
  if (!continouslisting) ACCnewline();
  if (npccount==1)  writeSysMessage(SYSMESS_NPCLISTCONTINUE); else writeSysMessage(SYSMESS_NPCLISTCONTINUE_PLURAL);
  var progresscount = 0;
  var i;
  for (i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if ( (objectIsNPC(i)) && (!objectIsAttr(i,ATTR_CONCEALED)) ) // only NPCs not concealed
  		  { 
  		     writeText(getObjectText(i)); 
  		     progresscount++
  		     if (continouslisting)
  		     {
		  	 	if (progresscount <= npccount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  			 	if (progresscount == npccount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  			 	if (progresscount == npccount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
}


function ACClistobj()
{
  var locno = loc_here();
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);

  objscount = objscount - concealed_or_scenery_objcount;
  if (objscount)
  {
	  writeSysMessage(SYSMESS_YOUCANSEE);
      ACClistat(loc_here());
  }
}

function ACCprocess(procno)
{
	if (procno > last_process) 
	{
		writeText(STR_WRONG_PROCESS);
		ACCnewtext();
		ACCdone();
	}
	callProcess(procno);
    if (describe_location_flag) done_flag = true;
}

function ACCmes(mesno)
{
	writeMessage(mesno);
}

function ACCmode(mode)
{
	setFlag(FLAG_MODE, mode);
}

function ACCtime(length, settings)
{
	setFlag(FLAG_TIMEOUT_LENGTH, length);
	setFlag(FLAG_TIMEOUT_SETTINGS, settings);
}

function ACCdoall(locno)
{
	doall_flag = true;
	if (locno == LOCATION_HERE) locno = loc_here();
	// Each object will be considered for doall loop if is at locno and it's not the object specified by the NOUN2/ADJECT2 pair and it's not a NPC (or setting to consider NPCs as objects is set)
	setFlag(FLAG_DOALL_LOC, locno);
	var doall_obj;
	doall_loop:
	for (doall_obj=0;(doall_obj<num_objects) && (doall_flag);doall_obj++)  
	{
		if (getObjectLocation(doall_obj) == locno)
			if ((!objectIsNPC(doall_obj)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3))) 
 			 if (!objectIsAttr(doall_obj, ATTR_CONCEALED)) 
 			  if (!objectIsAttr(doall_obj, ATTR_SCENERY)) 
				if (!( (objectsNoun[doall_obj]==getFlag(FLAG_NOUN2))  &&    ((objectsAdjective[doall_obj]==getFlag(FLAG_ADJECT2)) || (objectsAdjective[doall_obj]==EMPTY_WORD)) ) ) // implements "TAKE ALL EXCEPT BIG SWORD"
				{
					setFlag(FLAG_NOUN1, objectsNoun[doall_obj]);
					setFlag(FLAG_ADJECT1, objectsAdjective[doall_obj]);
					setReferredObject(doall_obj);
					callProcess(process_in_doall);
					if (describe_location_flag) 
						{
							doall_flag = false;
							entry_for_doall = '';
							break doall_loop;
						}
				}
	}
	doall_flag = false;
	entry_for_doall = '';
	if (describe_location_flag) descriptionLoop();
}

function ACCprompt(value)  // deprecated
{
	setFlag(FLAG_PROMPT, value);
	setInputPlaceHolder();
}


function ACCweigh(objno, flagno)
{
	var weight = getObjectWeight(objno);
	setFlag(flagno, weight);
}

function ACCputin(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if (getObjectLocation(objno) == LOCATION_WORN)
	{
		writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == LOCATION_CARRIED)
	{
		setObjectLocation(objno, locno);
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUPUTOBJECTON); else writeSysMessage(SYSMESS_YOUPUTOBJECTIN);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		success = true;
		return;
	}

	writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
	ACCnewtext();
	ACCdone();
}


function ACCtakeout(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if ((getObjectLocation(objno) == LOCATION_WORN) || (getObjectLocation(objno) == LOCATION_CARRIED))
	{
		writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectWeight(objno) + getLocationObjectsWeight(LOCATION_WORN) + getLocationObjectsWeight(LOCATION_CARRIED) >  getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{		
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		return;
	}

	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;


}
function ACCnewtext()
{
	player_order_buffer = '';
}

function ACCability(maxObjectsCarried, maxWeightCarried)
{
	setFlag(FLAG_MAXOBJECTS_CARRIED, maxObjectsCarried);
	setFlag(FLAG_MAXWEIGHT_CARRIED, maxWeightCarried);
}

function ACCweight(flagno)
{
	var weight_carried = getLocationObjectsWeight(LOCATION_CARRIED);
	var weight_worn = getLocationObjectsWeight(LOCATION_WORN);
	var total_weight = weight_worn + weight_carried;
	setFlag(flagno, total_weight);
}


function ACCrandom(flagno)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*100)));
}

function ACCwhato()
{
	var whatofound = getReferredObject();
	if (whatofound != EMPTY_OBJECT) setReferredObject(whatofound);
}

function ACCputo(locno)
{
	setObjectLocation(getFlag(FLAG_REFERRED_OBJECT), locno);
}

function ACCnotdone()
{
	done_flag = false;
}

function ACCautop(locno)
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautot(locno)
{

	var objno =findMatchingObject(locno);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };

	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
			writeText(getObjectFixArticles(locno));
			writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION)
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
	
}


function CNDmove(flagno)
{
	var locno = getFlag(flagno);
	var dirno = getFlag(FLAG_VERB);
	var destination = getConnection( locno,  dirno);
	if (destination != -1) 
		{
			 setFlag(flagno, destination);
			 return true;
		}
	return false;
}


function ACCextern(writeno)
{
	eval(writemessages[writeno]);
}


function ACCpicture(picno)
{
	drawPicture(picno);
}



function ACCgraphic(option)
{
	graphicsON = (option==1);  
	if (!graphicsON) hideGraphicsWindow();	
}

function ACCbeep(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'play');
}

function ACCsound(value)
{
	soundsON = (value==1);  
	if (!soundsON) sfxstopall();
}

function CNDozero(objno, attrno)
{
	if (attrno > 63) return false;
	return !objectIsAttr(objno, attrno);

}

function CNDonotzero(objno, attrno)
{
	return objectIsAttr(objno, attrno);
}

function ACCoset(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		attrs = getObjectLowAttributes(objno);
		var attrs = bitset(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitset(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}

function ACCoclear(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitclear(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitclear(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}


function CNDislight()
{
	if (!isDarkHere()) return true;
	return lightObjectsPresent();
}



function CNDisnotlight()
{
	return ! CNDislight();
}

function ACCversion()
{
	writeText(filterText(STR_RUNTIME_VERSION));
}


function ACCwrite(writeno)
{
	writeWriteMessage(writeno);
}

function ACCwriteln(writeno)
{
	writeWriteMessage(writeno);
	ACCnewline();
}

function ACCrestart()
{
  process_restart = true;
}


function ACCtranscript()
{
	$('#transcript_area').html(transcript);
	$('.transcript_layer').show();
	inTranscript = true;
}

function ACCanykey()
{
	writeSysMessage(SYSMESS_PRESSANYKEY);
	inAnykey = true;
}

function ACCgetkey(flagno)
{
	getkey_return_flag = flagno;
	inGetkey = true;
}


//////////////////
//   LEGACY     //
//////////////////

// From PAW PC
function ACCbell()
{
 	// Empty, PAW PC legacy, just does nothing 
}


// From PAW Spectrum
function ACCreset()
{
	// Legacy condact, does nothing now
}


function ACCpaper(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCink(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCborder(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCcharset(value)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCline(lineno)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCinput()
{
	// Legacy condact, does nothing now
}

function ACCsaveat()
{
	// Legacy condact, does nothing now
}

function ACCbackat()
{
	// Legacy condact, does nothing now
}

function ACCprintat()
{
	// Legacy condact, does nothing now
}

function ACCprotect()
{
	// Legacy condact, does nothing now
}

// From Superglus


function ACCdebug()
{
	// Legacy condact, does nothing now		
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS FOR COMPILER //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CNDverb(wordno)
{
	return (getFlag(FLAG_VERB) == wordno);
}


function CNDnoun1(wordno)
{
	return (getFlag(FLAG_NOUN1) == wordno);
}

//   PLUGINS    ;

//CND RNDWRITELN A 14 14 14 0

function ACCrndwriteln(writeno1,writeno2,writeno3)
{
	ACCrndwrite(writeno1,writeno2,writeno3);
	ACCnewline();
}
//CND SYNONYM A 15 13 0 0

function ACCsynonym(wordno1, wordno2)
{
   if (wordno1!=EMPTY_WORD) setFlag(FLAG_VERB, wordno1);
   if (wordno2!=EMPTY_WORD)	setFlag(FLAG_NOUN1, wordno2);
}
//CND RNDWRITE A 14 14 14 0

function ACCrndwrite(writeno1,writeno2,writeno3)
{
	var val = Math.floor((Math.random()*3));
	switch (val)
	{
		case 0 : writeWriteMessage(writeno1);break;
		case 1 : writeWriteMessage(writeno2);break;
		case 2 : writeWriteMessage(writeno3);break;
	}
}
//CND SETWEIGHT A 4 2 0 0

function ACCsetweight(objno, value)
{
   objectsWeight[objno] = value;
}

//CND BSET A 1 2 0 0

function ACCbset(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitset(getFlag(flagno),bitno));
}
//CND ISNOTMOV C 0 0 0 0

function CNDisnotmov()
{
	return !CNDismov();	
}

//CND VOLUME A 2 2 0 0

function ACCvolume(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxvolume(channelno, value);
}

//CND ISVIDEO C 0 0 0 0

function CNDisvideo()
{
	if (typeof videoElement == 'undefined') return false;
	if (!videoLoopCount) return false;
	if (videoElement.paused) return false;
	return true;
}

//CND GE C 1 2 0 0

function CNDge(flagno, valor)
{
	return (getFlag(flagno)>=valor);
}
//CND ISNOTDOALL C 0 0 0 0

function CNDisnotdoall()
{
	return !CNDisdoall();
}

//CND RESUMEVIDEO A 0 0 0 0


function ACCresumevideo()
{
	if (typeof videoElement != 'undefined') 
		if (videoElement.paused)
		  videoElement.play();
}

//CND PAUSEVIDEO A 0 0 0 0


function ACCpausevideo()
{
	if (typeof videoElement != 'undefined') 
		if (!videoElement.ended) 
		if (!videoElement.paused)
		   videoElement.pause();
}

//CND BZERO C 1 2 0 0

function CNDbzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (!bittest(getFlag(flagno), bitno));
}
//CND BNEG A 1 2 0 0

function ACCbneg(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitneg(getFlag(flagno),bitno));
}
//CND ISDOALL C 0 0 0 0

function CNDisdoall()
{
	return doall_flag;	
}

//CND ONEG A 4 2 0 0

function ACConeg(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitneg(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitneg(attrs, attrno);
	setObjectHighAttributes(objno, attrs);
}

//CND ISDONE C 0 0 0 0

function CNDisdone()
{
	return done_flag;	
}

//CND BLOCK A 14 2 2 0

function ACCblock(writeno, picno, procno)
{
   inBlock = true;
   disableInterrupt();
   $('.block_layer').hide();
   var text = getWriteMessageText(writeno);
   $('.block_text').html(text);
   
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var imgsrc = '<img class="block_picture" src="' + filename + '" />';
		$('.block_graphics').html(imgsrc);
	}
    if (procno == 0 ) unblock_process = null; else unblock_process = procno;
    $('.block_layer').show();

}

//CND HELP A 0 0 0 0

function ACChelp()
{
	if (getLang()=='EN') EnglishHelp(); else SpanishHelp();
}	

function EnglishHelp()
{
	writeText('HOW DO I SEND COMMANDS TO THE PC?');
	writeText(STR_NEWLINE);
	writeText('Use simple orders: OPEN DOOR, TAKE KEY, GO UP, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I MOVE IN THE MAP?');
	writeText(STR_NEWLINE);
	writeText('Usually you will have to use compass directions as north (shortcut: "N"), south (S), east (E), west (W) or other directions (up, down, enter, leave, etc.). Some games allow complex order like "go to well". Usually you would be able to know avaliable exits by location description, some games also provide the "EXITS" command.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK MY INVENTORY?');
	writeText(STR_NEWLINE);
	writeText('type INVENTORY (shortcut "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I USE THE OBJECTS?');
	writeText(STR_NEWLINE);
	writeText('Use the proper verb, that is, instead of USE KEY type OPEN.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK SOMETHING CLOSELY?');
	writeText(STR_NEWLINE);
	writeText('Use "examine" verb: EXAMINE DISH. (shortcut: EX)');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SEE AGAIN THE CURRENT LOCATION DSCRIPTION?');
	writeText(STR_NEWLINE);
	writeText('Type LOOK (shortcut "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I TALK TO OTHER CHARACTERS?');
	writeText(STR_NEWLINE);
	writeText('Most common methods are [CHARACTER, SENTENCE] or [SAY CHARACTER "SENTENCE"]. For instance: [JOHN, HELLO] o [SAY JOHN "HELLO"]. Some games also allow just [TALK TO JOHN]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING IN A CONTAINER, HOW CAN I TAKE SOMETHING OUT?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY IN BOX. TAKE KEY OUT OF BOX. INSERT KEY IN BOX. EXTRACT KEY FROM BOX.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING ON SOMETHING ELSE?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY ON TABLE. TAKE KEY FROM TABLE');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SAVE/RESTORE MY GAME?');
	writeText(STR_NEWLINE);
	writeText('Use SAVE/LOAD commands.');
	writeText(STR_NEWLINE + STR_NEWLINE);

}

function SpanishHelp()
{
	writeText('¿CÓMO DOY ORDENES AL PERSONAJE?');
	writeText(STR_NEWLINE);
	writeText('Utiliza órdenes en imperativo o infinitivo: ABRE PUERTA, COGER LLAVE, SUBIR, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO ME MUEVO POR EL JUEGO?');
	writeText(STR_NEWLINE);
	writeText('Por regla general, mediante los puntos cardinales como norte (abreviado "N"), sur (S), este (E), oeste (O) o direcciones espaciales (arriba, abajo, bajar, subir, entrar, salir, etc.). Algunas aventuras permiten también cosas como "ir a pozo". Normalmente podrás saber en qué dirección puedes ir por la descripción del sitio, aunque algunos juegos facilitan el comando "SALIDAS" que te dirá exactamente cuáles hay.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO SABER QUE OBJETOS LLEVO?');
	writeText(STR_NEWLINE);
	writeText('Teclea INVENTARIO (abreviado "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO USO LOS OBJETOS?');
	writeText(STR_NEWLINE);
	writeText('Utiliza el verbo correcto, en lugar de USAR ESCOBA escribe BARRER.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO MIRAR DE CERCA UN OBJETO U OBSERVARLO MÁS DETALLADAMENTE?');
	writeText(STR_NEWLINE);
	writeText('Con el verbo examinar: EXAMINAR PLATO. Generalmente se puede usar la abreviatura "EX": EX PLATO.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO VER DE NUEVO LA DESCRIPCIÓN DEL SITIO DONDE ESTOY?');
	writeText(STR_NEWLINE);
	writeText('Escribe MIRAR (abreviado "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO HABLO CON LOS PERSONAJES?');
	writeText(STR_NEWLINE);
	writeText('Los modos más comunes son [PERSONAJE, FRASE] o [DECIR A PERSONAJE "FRASE"]. Por ejemplo: [LUIS, HOLA] o [DECIR A LUIS "HOLA"]. En algunas aventuras también se puede utilizar el formato [HABLAR A LUIS]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO METO ALGO EN UN CONTENEDOR? ¿CÓMO LO SACO?');
	writeText(STR_NEWLINE);
	writeText('METER LLAVE EN CAJA. SACAR LLAVE DE CAJA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PONGO ALGO SOBRE ALGO? ¿CÓMO LO QUITO?');
	writeText(STR_NEWLINE);
	writeText('PONER LLAVE EN MESA. COGER LLAVE DE MESA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO GRABO Y CARGO LA PARTIDA?');
	writeText(STR_NEWLINE);
	writeText('Usa las órdenes SAVE y LOAD, o GRABAR y CARGAR.');
	writeText(STR_NEWLINE + STR_NEWLINE);
}

//CND BNOTZERO C 1 2 0 0

function CNDbnotzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (bittest(getFlag(flagno), bitno));
}

//CND PLAYVIDEO A 14 2 2 0

var videoLoopCount;
var videoEscapable;
var videoElement;

function ACCplayvideo(strno, loopCount, settings)
{
	videoEscapable = settings & 1; // if bit 0 of settings is 1, video can be interrupted with ESC key
	if (loopCount == 0) loopCount = -1;
	videoLoopCount = loopCount;

	str = '<video id="videoframe" height="100%">';
	str = str + '<source src="dat/' + writemessages[strno] + '.mp4" type="video/mp4" codecs="avc1.4D401E, mp4a.40.2">';
	str = str + '<source src="dat/' + writemessages[strno] + '.webm" type="video/webm" codecs="vp8.0, vorbis">';
	str = str + '<source src="dat/' + writemessages[strno] + '.ogg" type="video/ogg" codecs="theora, vorbis">';
	str = str + '</video>';
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#videoframe').css('height','100%');
	$('#videoframe').css('display','block');
	$('#videoframe').css('margin-left','auto');
	$('#videoframe').css('margin-right','auto');
	$('#graphics').show();
	videoElement = document.getElementById('videoframe');
	videoElement.onended = function() 
	{
    	if (videoLoopCount == -1) videoElement.play();
    	else
    	{
    		videoLoopCount--;
    		if (videoLoopCount) videoElement.play();
    	}
	};
	videoElement.play();

}

// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_video_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#videoframe").length > 0) $("#videoframe").remove();	
	old_video_h_description_init();
}

// Hook into keypress to cancel video playing if ESC is pressed and video is skippable

var old_video_h_keydown =  h_keydown;
h_keydown = function (event)
{
 	if ((event.keyCode == 27) && (typeof videoElement != 'undefined') && (!videoElement.ended) && (videoEscapable)) 
 	{
 		videoElement.pause(); 
 		return false;  // we've finished attending ESC press
 	}
 	else return old_video_h_keydown(event);
}




//CND TEXTPIC A 2 2 0 0

function ACCtextpic(picno, align)
{
	var style = '';
	var post = '';
	var pre = '';
	switch(align)
	{
		case 0: post='<br style="clear:left">';break;
		case 1: style = 'float:left'; break;
		case 2: style = 'float:right'; break;
		case 3: pre='<center>';post='</center><br style="clear:left">';break;
	}
	filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var texto = pre + "<img alt='' class='textpic' style='"+style+"' src='"+filename+"' />" + post;
		writeText(texto);
		$(".text").scrollTop($(".text")[0].scrollHeight);
	}
}
//CND OBJFOUND C 2 9 0 0

function CNDobjfound(attrno, locno)
{

	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return true; }
	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return false;
}

//CND PICTUREAT A 2 2 2 0

/*
In order to determine the actual size of both background image and pictureat image they should be loaded, thus two chained "onload" are needed. That is, 
background image is loaded to determine its size, then pictureat image is loaded to determine its size. Size of currently displayed background image cannot
be used as it may have been already stretched.
*/

function ACCpictureat(x,y,picno)
{
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (!filename) return;

	// Check location has a picture, otherwise exit
	var currentBackgroundScreenImage = $('.location_picture');
	if (!currentBackgroundScreenImage) return;

	// Create a new image with the contents of current background image, to be able to calculate original height of image
	var virtualBackgroundImage = new Image();
	// Pass required data as image properties in order to be avaliable at "onload" event
	virtualBackgroundImage.bg_data=[];
	virtualBackgroundImage.bg_data.filename = filename; 
	virtualBackgroundImage.bg_data.x = x;
	virtualBackgroundImage.bg_data.y = y;
	virtualBackgroundImage.bg_data.picno = picno;
	virtualBackgroundImage.bg_data.currentBackgroundScreenImage = currentBackgroundScreenImage;


	// Event triggered when virtual background image is loaded
	virtualBackgroundImage.onload = function()
		{
			var originalBackgroundImageHeight = this.height;
			var scale = this.bg_data.currentBackgroundScreenImage.height() / originalBackgroundImageHeight;

			// Create a new image with the contents of picture to show with PICTUREAT, to be able to calculate height of image
			var virtualPictureAtImage = new Image();
			// Also pass data from background image as property so they are avaliable in the onload event
			virtualPictureAtImage.pa_data = [];
			virtualPictureAtImage.pa_data.x = this.bg_data.x;
			virtualPictureAtImage.pa_data.y = this.bg_data.y;
			virtualPictureAtImage.pa_data.picno = this.bg_data.picno;
			virtualPictureAtImage.pa_data.filename = this.bg_data.filename;
			virtualPictureAtImage.pa_data.scale = scale;
			virtualPictureAtImage.pa_data.currentBackgroundImageWidth = this.bg_data.currentBackgroundScreenImage.width();
			
			// Event triggered when virtual PCITUREAT image is loaded
			virtualPictureAtImage.onload = function ()
			{
		    		var imageHeight = this.height; 
					var x = Math.floor(this.pa_data.x * this.pa_data.scale);
					var y = Math.floor(this.pa_data.y * this.pa_data.scale);
					var newimageHeight = Math.floor(imageHeight * this.pa_data.scale);
					var actualBackgroundImageX = Math.floor((parseInt($('.graphics').width()) - this.pa_data.currentBackgroundImageWidth)/2);;
					var id = 'pictureat_' + this.pa_data.picno;

					// Add new image, notice we are not using the virtual image, but creating a new one
					$('.graphics').append('<img  alt="" id="'+id+'" style="display:none" />');				
					$('#' + id).css('position','absolute');
					$('#' + id).css('left', actualBackgroundImageX + x  + 'px');
					$('#' + id).css('top',y + 'px');
					$('#' + id).css('z-index','100');
					$('#' + id).attr('src', this.pa_data.filename);
					$('#' + id).css('height',newimageHeight + 'px');
					$('#' + id).show();
			}

			// Assign the virtual pictureat image the destinationsrc to trigger the "onload" event
			virtualPictureAtImage.src = this.bg_data.filename;
			};

	// Assign the virtual background image same src as current background to trigger the "onload" event
	virtualBackgroundImage.src = currentBackgroundScreenImage.attr("src");

}

//CND ISNOTRESP C 0 0 0 0

function CNDisnotresp()
{
	return !in_response;	
}

//CND ISSOUND C 1 0 0 0

function CNDissound(channelno)
{
	if ((channelno <1 ) || (channelno > MAX_CHANNELS)) return false;
    return channelActive(channelno);
}
//CND ZONE C 8 8 0 0

function CNDzone(locno1, locno2)
{

	if (loc_here()<locno1) return false;
	if (loc_here()>locno2) return false;
	return true;
}
//CND FADEOUT A 2 2 0 0

function ACCfadeout(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxfadeout(channelno, value);
}
//CND CLEAREXIT A 2 0 0 0

function ACCclearexit(wordno)
{
	if ((wordno >= NUM_CONNECTION_VERBS) || (wordno< 0 )) return;
	setConnection(loc_here(),wordno, -1);
}
//CND WHATOX2 A 1 0 0 0

function ACCwhatox2(flagno)
{	
	var auxNoun = getFlag(FLAG_NOUN1);
	var auxAdj = getFlag(FLAG_ADJECT1);
	setFlag(FLAG_NOUN1, getFlag(FLAG_NOUN2));
	setFlag(FLAG_ADJECT1, getFlag(FLAG_ADJECT2));
	var whatox2found = getReferredObject();
	setFlag(flagno,whatox2found);
	setFlag(FLAG_NOUN1, auxNoun);
	setFlag(FLAG_ADJECT1, auxAdj);
}
//CND COMMAND A 2 0 0 0

function ACCcommand(value)
{
	if (value) {$('.input').show();$('.input').focus();} else $('.input').hide();
}
//CND TITLE A 14 0 0 0

function ACCtitle(writeno)
{
	document.title = writemessages[writeno];
}
//CND LE C 1 2 0 0

function CNDle(flagno, valor)
{
	return (getFlag(flagno) <= valor);
}
//CND WARNINGS A 2 0 0 0

function ACCwarnings(value)
{
	if (value) showWarnings = true; else showWarnings = false;
}
//CND BCLEAR A 1 2 0 0

function ACCbclear(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitclear(getFlag(flagno), bitno));
}
//CND DIV A 1 2 0 0

function ACCdiv(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) / valor));
}
//CND OBJAT A 9 1 0 0

function ACCobjat(locno, flagno)
{
	setFlag(flagno, getObjectCountAt(locno));
}
//CND SILENCE A 2 0 0 0

function ACCsilence(channelno)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxstop(channelno);
}
//CND SETEXIT A 2 2 0 0

function ACCsetexit(value, locno)
{
	if (value < NUM_CONNECTION_VERBS) setConnection(loc_here(), value, locno);
}
//CND EXITS A 8 5 0 0

function ACCexits(locno,mesno)
{
  writeText(getExitsText(locno,mesno));
}

//CND HOOK A 14 0 0 5

function ACChook(writeno)
{
	h_code(writemessages[writeno]);
}
//CND RANDOMX A 1 2 0 0

function ACCrandomx(flagno, value)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*value)));
}
//CND ISNOTDONE C 0 0 0 0

function CNDisnotdone()
{
	return !CNDisdone();
}

//CND ATGE C 8 0 0 0

function CNDatge(locno)
{
	return (getFlag(FLAG_LOCATION) >= locno);
}

//CND ATLE C 8 0 0 0

function CNDatle(locno)
{
	return (getFlag(FLAG_LOCATION) <= locno);
}

//CND RESP A 0 0 0 0

function ACCresp()
{
	in_response = true;
}	

//CND ISNOTSOUND C 1 0 0 0

function CNDisnotsound(channelno)
{
  if ((channelno <1) || (channelno >MAX_CHANNELS)) return false;
  return !(CNDissound(channelno));
}
//CND ASK W 14 14 1 0

// Global vars for ASK


var inAsk = false;
var ask_responses = null;
var ask_flagno = null;



function ACCask(writeno, writenoOptions, flagno)
{
	inAsk = true;
	writeWriteMessage(writeno);
	ask_responses = getWriteMessageText(writenoOptions);
	ask_flagno = flagno;
}



// hook replacement
var old_ask_h_keydown  = h_keydown;
h_keydown  = function (event)
{
	if (inAsk)
	{
		var keyCodeAsChar = String.fromCharCode(event.keyCode).toLowerCase();
		if (ask_responses.indexOf(keyCodeAsChar)!= -1)
		{
			setFlag(ask_flagno, ask_responses.indexOf(keyCodeAsChar));
			inAsk = false;
			event.preventDefault();
            $('.input').show();
		    $('.input').focus();
		    hideBlock();
			waitKeyCallback();
		};
		return false; // if we are in ASK condact, no keypress should be considered other than ASK response
	} else return old_ask_h_keydown(event);
}

//CND MUL A 1 2 0 0

function ACCmul(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) * valor));
}
//CND NPCAT A 9 1 0 0

function ACCnpcat(locno, flagno)
{
	setFlag(flagno,getNPCCountAt(locno));
}

//CND SOFTBLOCK A 2 0 0 0

function ACCsoftblock(procno)
{
   inBlock = true;
   disableInterrupt();

   $('.block_layer').css('display','none');
   $('.block_text').html('');
   $('.block_graphics').html('');
   $('.block_layer').css('background','transparent');
   if (procno == 0 ) unblock_process = null; else unblock_process = procno;
   $('.block_layer').css('display','block');
}

//CND LISTCONTENTS A 9 0 0 0

function ACClistcontents(locno)
{
   ACClistat(locno, locno)
}
//CND SPACE A 0 0 0 0

function ACCspace()
{
	writeText(' ');
}
//CND ISNOTMUSIC C 0 0 0 0

function CNDisnotmusic()
{
  return !CNDismusic();
}

//CND BREAK A 0 0 0 0

function ACCbreak()
{
	doall_flag = false; 
	entry_for_doall = '';
}
//CND NORESP A 0 0 0 0

function ACCnoresp()
{
	in_response = false;
}	

//CND ISMUSIC C 0 0 0 0

function CNDismusic()
{
	return (CNDissound(0));	
}

//CND ISRESP C 0 0 0 0

function CNDisresp()
{
	return in_response;	
}

//CND MOD A 1 2 0 0

function ACCmod(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) % valor));
}
//CND FADEIN A 2 2 2 0

function ACCfadein(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'fadein');
}
//CND WHATOX A 1 0 0 0

function ACCwhatox(flagno)
{
	var whatoxfound = getReferredObject();
	setFlag(flagno,whatoxfound);
}

//CND GETEXIT A 2 2 0 0

function ACCgetexit(value,flagno)
{
	if (value >= NUM_CONNECTION_VERBS) 
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	var locno = getConnection(loc_here(),value);
	if (locno == -1)
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	setFlag(flagno,locno);
}
//CND LOG A 14 0 0 0

function ACClog(writeno)
{
  console_log(writemessages[writeno]);
}
//CND VOLUMEVIDEO A 2 0 0 0


function ACCvolumevideo(value)
{
	if (typeof videoElement != 'undefined') 
		videoElement.volume = value  / 65535;
}

//CND OBJNOTFOUND C 2 9 0 0

function CNDobjnotfound(attrno, locno)
{
	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return false; }

	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return true;
}
//CND ISMOV C 0 0 0 0

function CNDismov()
{
	if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)==EMPTY_WORD)) return true;

	if ((getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_VERB)==EMPTY_WORD)) return true;

    if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS)) return true;
    
    return false;
}

//CND YOUTUBE A 14 0 0 0

function ACCyoutube(strno)
{

	var str = '<iframe id="youtube" width="560" height="315" src="http://www.youtube.com/embed/' + writemessages[strno] + '?autoplay=1&controls=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>'
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#youtube').css('height','100%');
	$('#youtube').css('display','block');
	$('#youtube').css('margin-left','auto');
	$('#youtube').css('margin-right','auto');
	$('.graphics').show();
}


// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_youtube_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#youtube").length > 0) $("#youtube").remove();	
	old_youtube_h_description_init();
}
//CND LISTSAVEDGAMES A 0 0 0 0

function ACClistsavedgames()
{
  var numberofgames = 0;
  for(var savedgames in localStorage)
  {
    gamePrefix = savedgames.substring(0,16); // takes out ngpaws_savegame_
    if (gamePrefix == "ngpaws_savegame_")
    {
      gameName = savedgames.substring(16);
      writelnText(gameName);
      numberofgames++;
    }
  }
  if (numberofgames == 0) 
  {
     if (getLang()=='EN') writelnText("No saved games found."); else writelnText("No hay ninguna partida guardada.");
  }
}


// This file is (C) Carlos Sanchez 2014, released under the MIT license


// IMPORTANT: Please notice this file must be encoded with the same encoding the index.html file is, so the "normalize" function works properly.
//            As currently the ngpwas compiler generates utf-8, and the index.html is using utf-8 also, this file must be using that encoding.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                         Auxiliary functions                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General functions
String.prototype.rights= function(n){
    if (n <= 0)
       return "";
    else if (n > String(this).length)
       return this;
    else {
       var iLen = String(this).length;
       return String(this).substring(iLen, iLen - n);
    }
}


String.prototype.firstToLower= function()
{
	return  this.charAt(0).toLowerCase() + this.slice(1);	
}


// Returns true if using Internet Explorer 9 or below, where some features are not supported
function isBadIE () {
  var myNav = navigator.userAgent.toLowerCase();
  if (myNav.indexOf('msie') == -1) return false;
  ieversion =  parseInt(myNav.split('msie')[1]);
  return (ieversion<10);
}


function runningLocal()
{
	return (window.location.protocol == 'file:');
}


// Levenshtein function

function getLevenshteinDistance (a, b)
{
  if(a.length == 0) return b.length; 
  if(b.length == 0) return a.length; 
 
  var matrix = [];
 
  // increment along the first column of each row
  var i;
  for(i = 0; i <= b.length; i++){
    matrix[i] = [i];
  }
 
  // increment each column in the first row
  var j;
  for(j = 0; j <= a.length; j++){
    matrix[0][j] = j;
  }
 
  // Fill in the rest of the matrix
  for(i = 1; i <= b.length; i++){
    for(j = 1; j <= a.length; j++){
      if(b.charAt(i-1) == a.charAt(j-1)){
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                                Math.min(matrix[i][j-1] + 1, // insertion
                                         matrix[i-1][j] + 1)); // deletion
      }
    }
  }
 
  return matrix[b.length][a.length];
};

// waitKey helper for all key-wait condacts

function waitKey(callbackFunction)
{
	waitkey_callback_function.push(callbackFunction);
	showAnykeyLayer();
}

function waitKeyCallback()
{
 	var callback = waitkey_callback_function.pop();
	if ( callback ) callback();
	if (describe_location_flag) descriptionLoop();  		
}


// Check DOALL entry

function skipdoall(entry)
{
	return  ((doall_flag==true) && (entry_for_doall!='') && (current_process==process_in_doall) && (entry_for_doall > entry));
}

// Dynamic attribute use functions
function getNextFreeAttribute()
{
	var value = nextFreeAttr;
	nextFreeAttr++;
	return value;
}


// Gender functions

function getSimpleGender(objno)  // Simple, for english
{
 	isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
 	if (isPlural) return "P";
 	isFemale = objectIsAttr(objno, ATTR_FEMALE);
 	if (isFemale) return "F";
 	isMale = objectIsAttr(objno, ATTR_MALE);
 	if (isMale) return "M";
    return "N"; // Neuter
}

function getAdvancedGender(objno)  // Complex, for spanish
{
 	var isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
 	var isFemale = objectIsAttr(objno, ATTR_FEMALE);
 	var isMale = objectIsAttr(objno, ATTR_MALE);

 	if (!isPlural) 
 	{
	 	if (isFemale) return "F";
	 	if (isMale) return "M";
	    return "N"; // Neuter
 	}
 	else
 	{
	 	if (isFemale) return "PF";
	 	if (isMale) return "PM";
	 	return "PN"; // Neuter plural
 	}

}

function getLang()
{
	var value = bittest(getFlag(FLAG_PARSER_SETTINGS),5);
	if (value) return "ES"; else return "EN";
}

function getObjectFixArticles(objno)
{
	var object_text = getObjectText(objno);
	var object_words = object_text.split(' ');
	if (object_words.length == 1) return object_text;
	var candidate = object_words[0];
	object_words.splice(0, 1);
	if (getLang()=='EN')
	{
		if ((candidate!='an') && (candidate!='a') && (candidate!='some')) return object_text;
		return 'the ' + object_words.join(' ');
	}
	else
	{
		if ( (candidate!='un') && (candidate!='una') && (candidate!='unos') && (candidate!='unas') && (candidate!='alguna') && (candidate!='algunos') && (candidate!='algunas') && (candidate!='algun')) return object_text;
		var gender = getAdvancedGender(objno);
		if (gender == 'F') return 'la ' + object_words.join(' ');
		if (gender == 'M') return 'el ' + object_words.join(' ');
		if (gender == 'N') return 'el ' + object_words.join(' ');
		if (gender == 'PF') return 'las ' + object_words.join(' ');
		if (gender == 'PM') return 'los ' + object_words.join(' ');
		if (gender == 'PN') return 'los ' + object_words.join(' ');
	}	


}



// JS level log functions
function console_log(string)
{
	if (typeof console != "undefined") console.log(string);
}


// Resources functions
function getResourceById(resource_type, id)
{
	for (var i=0;i<resources.length;i++)
	 if ((resources[i][0] == resource_type) && (resources[i][1]==id)) return resources[i][2];
	return false; 
}

// Flag read/write functions
function getFlag(flagno)
{
	 return flags[flagno];
}

function setFlag(flagno, value)
{
	 flags[flagno] = value;
}

// Locations functions
function loc_here()  // Returns current location, avoid direct use of flags
{
	 return getFlag(FLAG_LOCATION);
}


// Connections functions

function setConnection(locno1, dirno, locno2)
{
	connections[locno1][dirno] = locno2;
}

function getConnection(locno, dirno)
{
	return connections[locno][dirno];
}

// Objects text functions

function getObjectText(objno)
{
	return filterText(objects[objno]);
}


// Message text functions
function getMessageText(mesno)
{
	return filterText(messages[mesno]);
}

function getSysMessageText(sysno)
{
	return filterText(sysmessages[sysno]);
}

function getWriteMessageText(writeno)
{
	return filterText(writemessages[writeno]);
}

function getExitsText(locno,mesno)
{
  if ( locno === undefined ) return ''; // game hasn't fully initialised yet
  if ((getFlag(FLAG_LIGHT) == 0) || ((getFlag(FLAG_LIGHT) != 0) && lightObjectsPresent()))
  {
  		var exitcount = 0;
  		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1) exitcount++;
      if (exitcount)
      {
    		var message = getMessageText(mesno);
    		var exitcountprogress = 0;
    		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1)
    		{ 
    			exitcountprogress++;
    			message += getMessageText(mesno + 2 + i);
    			if (exitcountprogress == exitcount) message += getSysMessageText(SYSMESS_LISTEND);
    			if (exitcountprogress == exitcount-1) message += getSysMessageText(SYSMESS_LISTLASTSEPARATOR);
    			if (exitcountprogress <= exitcount-2) message += getSysMessageText(SYSMESS_LISTSEPARATOR);
  		  }
  		  return message;
      } else return getMessageText(mesno + 1);
  } else return getMessageText(mesno + 1);
}


// Location text functions
function getLocationText(locno)
{
	return  filterText(locations[locno]);
}



// Output processing functions
function implementTag(tag)
{
	tagparams = tag.split('|');
	for (var tagindex=0;tagindex<tagparams.length-1;tagindex++) tagparams[tagindex] = tagparams[tagindex].trim();
	if (tagparams.length == 0) {writeWarning(STR_INVALID_TAG_SEQUENCE_EMPTY); return ''}

	var resolved_hook_value = h_sequencetag(tagparams);
	if (resolved_hook_value!='') return resolved_hook_value;

	switch(tagparams[0].toUpperCase())
	{
		case 'URL': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					return '<a target="newWindow" href="' + tagparams[1]+ '">' + tagparams[2] + '</a>'; // Note: _blank would get the underscore character replaced by current selected object so I prefer to use a different target name as most browsers will open a new window
					break;
		case 'CLASS': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span class="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'STYLE': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'INK': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'PAPER': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="background-color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'OBJECT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(objects[getFlag(tagparams[1])]) return getObjectFixArticles(getFlag(tagparams[1])); else return '';
					   break;
		case 'WEIGHT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(objectsWeight[getFlag(tagparams[1])]) return objectsWeight[getFlag(tagparams[1])]; else return '';
					   break;
		case 'OLOCATION': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					      if(objectsLocation[getFlag(tagparams[1])]) return objectsLocation[getFlag(tagparams[1])]; else return '';
					      break;
		case 'MESSAGE':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(messages[getFlag(tagparams[1])]) return getMessageText(getFlag(tagparams[1])); else return '';
					   break;
		case 'SYSMESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(sysmessages[getFlag(tagparams[1])]) return getSysMessageText(getFlag(tagparams[1])); else return '';
					   break;
		case 'LOCATION':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(locations[getFlag(tagparams[1])]) return getLocationText(getFlag(tagparams[1])); else return '';
		case 'EXITS':if (tagparams.length != 3 ) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   return getExitsText(/^@\d+/.test(tagparams[1]) ? getFlag(tagparams[1].substr(1)) : tagparams[1],parseInt(tagparams[2],10));
					   break;
		case 'PROCESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   callProcess(tagparams[1]);
					   return "";
					   break;
		case 'ACTION': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   return '<a href="type: ' + tagparams[1] + '" onmouseup="orderEnteredLoop(\'' + tagparams[1]+ '\');return false;">' + tagparams[2] + '</a>';
					   break;
		case 'RESTART': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					    return '<a href="javascript: void(0)" onmouseup="restart()">' + tagparams[1] + '</a>';
					    break;
		case 'EXTERN': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					    return '<a href="javascript: void(0)" onmouseup="' + tagparams[1] + ' ">' + tagparams[2] + '</a>';
					    break;
		case 'TEXTPIC': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						var style = '';
						var post = '';
						var pre = '';
						align = tagparams[2];
						switch(align)
						{
							case 1: style = 'float:left'; break;
							case 2: style = 'float:right'; break;
							case 3: post = '<br />';
							case 4: pre='<center>';post='</center>';break;
						}
						return pre + "<img class='textpic' style='"+style+"' src='"+ RESOURCES_DIR + tagparams[1]+"' />" + post;
					    break;
		case 'HTML': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						return tagparams[1];
					    break;
		case 'FLAG': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						return getFlag(tagparams[1]);
					    break;
		case 'OREF': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
   			        if(objects[getFlag(FLAG_REFERRED_OBJECT)]) return getObjectFixArticles(getFlag(FLAG_REFERRED_OBJECT)); else return '';
					break;
		case 'TT':  
		case 'TOOLTIP':
					if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					var title = $('<span>'+tagparams[1]+'</span>').text().replace(/'/g,"&apos;").replace(/\n/g, "&#10;");
					var text = tagparams[2];
					return "<span title='"+title+"'>"+text+"</span>";
					break;
		case 'OPRO': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};  // returns the pronoun for a given object, used for english start database
					 switch (getSimpleGender(getFlag(FLAG_REFERRED_OBJECT)))
					 {
					 	case 'M' : return "him";
					 	case "F" : return "her";
					 	case "N" : return "it";
					 	case "P" : return "them";  // plural returns them
					 }
					break;

		default : return '[[[' + STR_INVALID_TAG_SEQUENCE_BADTAG + ' : ' + tagparams[0] + ']]]';
	}
}

function processTags(text)
{
	//Apply the {} tags filtering
	var pre, post, innerTag;
	tagfilter:
	while (text.indexOf('{') != -1)
	{
		if (( text.indexOf('}') == -1 ) || ((text.indexOf('}') < text.indexOf('{'))))
		{
			writeWarning(STR_INVALID_TAG_SEQUENCE + text);
			break tagfilter;
		}
		pre = text.substring(0,text.indexOf('{'));
		var openbracketcont = 1;
		pointer = text.indexOf('{') + 1;
		innerTag = ''
		while (openbracketcont>0)
		{
			if (text.charAt(pointer) == '{') openbracketcont++;
			if (text.charAt(pointer) == '}') openbracketcont--;
			if ( text.length <= pointer )
			{
				writeWarning(STR_INVALID_TAG_SEQUENCE + text);
				break tagfilter;
			}
			innerTag = innerTag + text.charAt(pointer);
			pointer++;
		}
		innerTag = innerTag.substring(0,innerTag.length - 1);
		post = text.substring(pointer);
		if (innerTag.indexOf('{') != -1 ) innerTag = processTags(innerTag); 
		innerTag = implementTag(innerTag);
		text = pre + innerTag + post;
	}
	return text;
}

function filterText(text)
{
	// ngPAWS sequences
	text = processTags(text);


	// Superglus sequences (only \n remains)
    text = text.replace(/\n/g, STR_NEWLINE);

	// PAWS sequences (only underscore)
	objno = getFlag(FLAG_REFERRED_OBJECT);
	if ((objno != EMPTY_OBJECT) && (objects[objno]))	text = text.replace(/_/g,objects[objno].firstToLower()); else text = text.replace(/_/g,'');
	text = text.replace(/¬/g,' ');

	return text;
}


// Text Output functions
function writeText(text, skipAutoComplete)
{
	if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
	text = h_writeText(text); // hook
	$('.text').append(text);
	$('.text').scrollTop($('.text')[0].scrollHeight);
	addToTranscript(text);
	if (!skipAutoComplete) addToAutoComplete(text);
	focusInput();
}

function writeWarning(text)
{
	if (showWarnings) writeText(text)
}

function addToTranscript(text)
{
	transcript = transcript + text;		
}

function writelnText(text, skipAutoComplete)
{
	if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
	writeText(text + STR_NEWLINE, skipAutoComplete);
}

function writeMessage(mesno)
{
	if (messages[mesno]!=null) writeText(getMessageText(mesno)); else writeWarning(STR_NEWLINE + STR_WRONG_MESSAGE + ' [' + mesno + ']');
}

function writeSysMessage(sysno)
{
		if (sysmessages[sysno]!=null) writeText(getSysMessageText(sysno)); else writeWarning(STR_NEWLINE + STR_WRONG_SYSMESS + ' [' + sysno + ']');
		$(".text").scrollTop($(".text")[0].scrollHeight);
}

function writeWriteMessage(writeno)
{
		writeText(getWriteMessageText(writeno)); 
}

function writeObject(objno)
{
	writeText(getObjectText(objno));
}

function clearTextWindow()
{
	$('.text').empty();
}


function clearInputWindow()
{
	$('.prompt').val('');
}


function writeLocation(locno)
{
	if (locations[locno]!=null) writeText(getLocationText(locno) + STR_NEWLINE); else writeWarning(STR_NEWLINE + STR_WRONG_LOCATION + ' [' + locno + ']');
}

// Screen control functions

function clearGraphicsWindow()
{
	$('.graphics').empty();	
}


function clearScreen()
{
	clearInputWindow();
	clearTextWindow();
	clearGraphicsWindow();
}

function copyOrderToTextWindow(player_order)
{

	last_player_orders.push(player_order);
	last_player_orders_pointer = 0;
	clearInputWindow();
	writelnText(STR_PROMPT_START + player_order + STR_PROMPT_END, false);
}

function get_prev_player_order()
{
	if (!last_player_orders.length) return '';
	var last = last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];
	if (last_player_orders_pointer < last_player_orders.length - 1) last_player_orders_pointer++;
	return last;
}

function get_next_player_order()
{
	if (!last_player_orders.length || last_player_orders_pointer == 0) return '';
	last_player_orders_pointer--;
	return last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];

}



// Graphics functions


function hideGraphicsWindow()
{
		$('.text').removeClass('half_text');
		$('.text').addClass('all_text');
		$('.graphics').removeClass('half_graphics');
		$('.graphics').addClass('hidden');
		if ($('.location_picture')) $('.location_picture').remove();
}



function drawPicture(picno)  
{
	var pictureDraw = false;
	if (graphicsON) 
	{
		if ((isDarkHere()) && (!lightObjectsPresent())) picno = 0;
		var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
		if (filename)
		{
			$('.graphics').removeClass('hidden');
			$('.graphics').addClass('half_graphics');
			$('.text').removeClass('all_text');
			$('.text').addClass('half_text');
			$('.graphics').html('<img alt="" class="location_picture" src="' +  filename + '" />');
			$('.location_picture').css('height','100%');
			pictureDraw = true;
		}
	}

	if (!pictureDraw) hideGraphicsWindow();
}




function clearPictureAt() // deletes all pictures drawn by "pictureAT" condact
{
	$.each($('.graphics img'), function () {
		if ($(this)[0].className!= 'location_picture') $(this).remove();
	});

}

// Turns functions

function incTurns()
{
	turns = getFlag(FLAG_TURNS_LOW) + 256 * getFlag(FLAG_TURNS_HIGH)  + 1;
	setFlag(FLAG_TURNS_LOW, turns % 256);
	setFlag(FLAG_TURNS_HIGH, Math.floor(turns / 256));
}

// input box functions

function disableInput()
{
	$(".input").prop('disabled', true); 
}

function enableInput()
{
	$(".input").prop('disabled', false); 
}

function focusInput()
{
	$(".prompt").focus();
	timeout_progress = 0;
}

// Object default attributes functions

function objectIsNPC(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_NPC);
}

function objectIsLight(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_LIGHT);
}

function objectIsWearable(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_WEARABLE);
}

function objectIsContainer(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_CONTAINER);
}

function objectIsSupporter(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_SUPPORTER);
}


function objectIsAttr(objno, attrno)
{
	if (attrno > 63) return false;
	var attrs = getObjectLowAttributes(objno);
	if (attrno > 31)
	{
		attrs = getObjectHighAttributes(objno);
		attrno = attrno - 32;
	}
	return bittest(attrs, attrno);
}

function isAccesibleContainer(objno)
{
	if (objectIsSupporter(objno)) return true;   // supporter
	if ( objectIsContainer(objno) && !objectIsAttr(objno, ATTR_OPENABLE) ) return true;  // No openable container
	if ( objectIsContainer(objno) && objectIsAttr(objno, ATTR_OPENABLE) && objectIsAttr(objno, ATTR_OPEN)  )  return true;  // No openable & open container
	return false;
}

//Objects and NPC functions

function findMatchingObject(locno)
{
	for (var i=0;i<num_objects;i++)
		if ((locno==-1) || (getObjectLocation(i) == locno))
		 if (((objectsNoun[i]) == getFlag(FLAG_NOUN1)) && (((objectsAdjective[i]) == EMPTY_WORD) || ((objectsAdjective[i]) == getFlag(FLAG_ADJECT1))))  return i;
	return EMPTY_OBJECT;
}

function getReferredObject()
{
	var objectfound = EMPTY_OBJECT; 
	refobject_search: 
	{
		object_id = findMatchingObject(LOCATION_CARRIED);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(LOCATION_WORN);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(loc_here());
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(-1);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	
	}
	return objectfound;
}


function getObjectLowAttributes(objno)
{
	return objectsAttrLO[objno];
}

function getObjectHighAttributes(objno)
{
	return objectsAttrHI[objno]
}


function setObjectLowAttributes(objno, attrs)
{
	objectsAttrLO[objno] = attrs;
}

function setObjectHighAttributes(objno, attrs)
{
	objectsAttrHI[objno] = attrs;
}


function getObjectLocation(objno)
{
	if (objno > last_object_number) 
		writeWarning(STR_INVALID_OBJECT + ' [' + objno + ']');
	return objectsLocation[objno];
}

function setObjectLocation(objno, locno)
{
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) - 1);
	objectsLocation[objno] = locno;
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) + 1);
}



// Sets all flags associated to  referred object by current LS  
function setReferredObject(objno) 
{
	if (objno == EMPTY_OBJECT)
	{
		setFlag(FLAG_REFERRED_OBJECT, EMPTY_OBJECT);
		setFlag(FLAG_REFERRED_OBJECT_LOCATION, LOCATION_NONCREATED);
		setFlag(FLAG_REFERRED_OBJECT_WEIGHT, 0);
		setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, 0);
		setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, 0);
		return;
	}
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setFlag(FLAG_REFERRED_OBJECT_LOCATION, getObjectLocation(objno));
	setFlag(FLAG_REFERRED_OBJECT_WEIGHT, getObjectWeight(objno));
	setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, getObjectLowAttributes(objno));
	setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, getObjectHighAttributes(objno));

}


function getObjectWeight(objno) 
{
	var weight = objectsWeight[objno];
	if ( ((objectIsContainer(objno)) || (objectIsSupporter(objno))) && (weight!=0)) // Container with zero weigth are magic boxes, anything you put inside weigths zero
  		weight = weight + getLocationObjectsWeight(objno);
	return weight;
}


function getLocationObjectsWeight(locno) 
{
	var weight = 0;
	for (var i=0;i<num_objects;i++)
	{
		if (getObjectLocation(i) == locno) 
		{
			objweight = objectsWeight[i];
			weight += objweight;
			if (objweight > 0)
			{
				if (  (objectIsContainer(i)) || (objectIsSupporter(i)) )
				{	
					weight += getLocationObjectsWeight(i);
				}
			}
		}
	}
	return weight;
}

function getObjectCountAt(locno) 
{
	var count = 0;
	for (i=0;i<num_objects;i++)
	{
		if (getObjectLocation(i) == locno) 
		{
			attr = getObjectLowAttributes(i);
			if (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)) count ++;  // Parser settings say we should include NPCs as objects
			 else if (!objectIsNPC(i)) count++;     // or object is not an NPC
		}
	}
	return count;
}


function getObjectCountAtWithAttr(locno, attrnoArray) 
{
	var count = 0;
	for (var i=0;i<num_objects;i++)
		if (getObjectLocation(i) == locno)  
			for (var j=0;j<attrnoArray.length;j++)
				if (objectIsAttr(i, attrnoArray[j])) count++;
	return count;
}


function getNPCCountAt(locno) 
{
	var count = 0;
	for (i=0;i<num_objects;i++)
		if ((getObjectLocation(i) == locno) &&  (objectIsNPC(i))) count++;
	return count;
}


// Location light function

function lightObjectsAt(locno) 
{
	return getObjectCountAtWithAttr(locno, [ATTR_LIGHT]) > 0;
}


function lightObjectsPresent() 
{
  if (lightObjectsAt(LOCATION_CARRIED)) return true;
  if (lightObjectsAt(LOCATION_WORN)) return true;
  if (lightObjectsAt(loc_here())) return true;
  return false;
}


function isDarkHere()
{
	return (getFlag(FLAG_LIGHT) != 0);
}

// Sound functions


function preloadsfx()
{
	for (var i=0;i<resources.length;i++)
	 	if (resources[i][0] == 'RESOURCE_TYPE_SND') 
	 	{
	 		var fileparts = resources[i][2].split('.');
			var basename = fileparts[0];
			var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] , preload: true} );
	 	}
}

function sfxplay(sfxno, channelno, times, method)
{

	if (!soundsON) return;
	if ((channelno <0) || (channelno >MAX_CHANNELS)) return;
	if (times == 0) times = -1; // more than 4000 million times
	var filename = getResourceById(RESOURCE_TYPE_SND, sfxno);
	if (filename)
	{
		var fileparts = filename.split('.');
		var basename = fileparts[0];
		var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] });
		if (soundChannels[channelno]) soundChannels[channelno].stop();
		soundLoopCount[channelno] = times;
		mySound.bind("ended", function(e) {
			for (sndloop=0;sndloop<MAX_CHANNELS;sndloop++)
				if (soundChannels[sndloop] == this)
				{
					if (soundLoopCount[sndloop]==-1) {this.play(); return }
					soundLoopCount[sndloop]--;
					if (soundLoopCount[sndloop] > 0) {this.play(); return }
					sfxstop(sndloop);
					return;
				}
		});
		soundChannels[channelno] = mySound;
		if (method=='play')	mySound.play(); else mySound.fadeIn(2000);
	}
}

function playLocationMusic(locno)
{
	if (soundsON) 
		{
			sfxstop(0);
			sfxplay(locno, 0, 0, 'play');
		}
}

function musicplay(musicno, times)  
{
	sfxplay(musicno, 0, times);
}

function channelActive(channelno)
{
	if (soundChannels[channelno]) return true; else return false;
}


function sfxstopall() 
{
	for (channelno=0;channelno<MAX_CHANNELS;channelno++) sfxstop(channelno);

}


function sfxstop(channelno)
{
	if (soundChannels[channelno]) 
		{
			soundChannels[channelno].unbind('ended');
			soundChannels[channelno].stop();
			soundChannels[channelno] = null;
		}
}

function sfxvolume(channelno, value)
{
	if (soundChannels[channelno]) soundChannels[channelno].setVolume(Math.floor( value * 100 / 65535)); // Inherited volume condact uses a number among 0 and 65535, buzz library uses 0-100.
}

function isSFXPlaying(channelno)
{
	if (!soundChannels[channelno]) return false;
	return true;
}


function sfxfadeout(channelno, value)
{
	if (!soundChannels[channelno]) return;
	soundChannels[channelno].fadeOut(value, function() { sfxstop(channelno) });
}

// *** Process functions ***

function callProcess(procno)
{
	if (inEND) return;
	current_process = procno;
	var prostr = procno.toString(); 
	while (prostr.length < 3) prostr = "0" + prostr;
	if (procno==0) in_response = true;
	if (doall_flag && in_response) done_flag = false;
	if (!in_response) done_flag = false;
	h_preProcess(procno);
    eval("pro" + prostr + "()");
	h_postProcess(procno);
	if (procno==0) in_response = false;
}

// Bitwise functions

function bittest(value, bitno)
{
	mask = 1 << bitno;
	return ((value & mask) != 0);
}

function bitset(value, bitno)
{

	mask = 1 << bitno;
	return value | mask;
}

function bitclear(value, bitno)
{
	mask = 1 << bitno;
	return value & (~mask);
}


function bitneg(value, bitno) 
{
	mask = 1 << bitno;
	return value ^ mask;

}

// Savegame functions
function getSaveGameObject()
{
	var savegame_object = new Object();
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	savegame_object.flags = flags.slice();
	savegame_object.objectsLocation = objectsLocation.slice();
	savegame_object.objectsWeight = objectsWeight.slice();
	savegame_object.objectsAttrLO = objectsAttrLO.slice();
	savegame_object.objectsAttrHI = objectsAttrHI.slice();
	savegame_object.connections = connections.slice();
	savegame_object.last_player_orders = last_player_orders.slice();
	savegame_object.last_player_orders_pointer = last_player_orders_pointer;
	savegame_object.transcript = transcript;
	savegame_object = h_saveGame(savegame_object);
	return savegame_object;
}

function restoreSaveGameObject(savegame_object)
{
	flags = savegame_object.flags;
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	objectsLocation = savegame_object.objectsLocation.slice();
	objectsWeight = savegame_object.objectsWeight.slice();
	objectsAttrLO = savegame_object.objectsAttrLO.slice();
	objectsAttrHI = savegame_object.objectsAttrHI.slice();
	connections = savegame_object.connections.slice();
	last_player_orders = savegame_object.last_player_orders.slice();
	last_player_orders_pointer = savegame_object.last_player_orders_pointer;
	transcript = savegame_object.transcript;
	h_restoreGame(savegame_object);
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        The parser                                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function loadPronounSufixes()
{

    var swapped;

	for (var j=0;j<vocabulary.length;j++) if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_PRONOUN)
			 pronoun_suffixes.push(vocabulary[j][VOCABULARY_WORD]);
	// Now sort them so the longest are first, so you rather replace SELOS in (COGESELOS=>COGE SELOS == >TAKE THEM) than LOS (COGESELOS==> COGESE LOS ==> TAKExx THEM) that woul not be understood (COGESE is not a verb, COGE is)
    do {
        swapped = false;
        for (var i=0; i < pronoun_suffixes.length-1; i++) 
        {
            if (pronoun_suffixes[i].length < pronoun_suffixes[i+1].length) 
            {
                var temp = pronoun_suffixes[i];
                pronoun_suffixes[i] = pronoun_suffixes[i+1];
                pronoun_suffixes[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}


function findVocabulary(word, forceDisableLevenshtein)  
{
	// Pending: in general this function is not very efficient. A solution where the vocabulary array is sorted by word so the first search can be binary search
	//          and possible typos are precalculated, so the distance is a lookup table instead of a function, would be much more efficient. On the other hand,
	//          the current solution is fast enough with a 1000+ words game that I don't consider improving this function to have high priority now.

	// Search word in vocabulary
	for (var j=0;j<vocabulary.length;j++)
		if (vocabulary[j][VOCABULARY_WORD] == word)
			 return vocabulary[j];

	if (forceDisableLevenshtein) return null;

	if (word.length <=4) return null; // Don't try to fix typo for words with less than 5 length

	if (bittest(getFlag(FLAG_PARSER_SETTINGS), 8)) return null; // If matching is disabled, we won't try to use levhenstein distance

	// Search words in vocabulary with a Levenshtein distance of 1
	var distance2_match = null;
	for (var k=0;k<vocabulary.length;k++)
	{
		if ([WORDTYPE_VERB,WORDTYPE_NOUN,WORDTYPE_ADJECT,WORDTYPE_ADVERB].indexOf(vocabulary[k][VOCABULARY_TYPE])  != -1 )
		{
			var distance = getLevenshteinDistance(vocabulary[k][VOCABULARY_WORD], word);
			if ((!distance2_match) && (distance==2)) distance2_match = vocabulary[k]; // Save first word with distance=2, in case we don't find any word with distance 1
			if (distance <= 1) return vocabulary[k];
		}
	} 

	// If we found any word with distance 2, return it, only if word was at least 7 characters long
	if ((distance2_match) &&  (word.length >6)) return distance2_match;

	// Word not found
	return null;
}

function normalize(player_order)   
// Removes accented characters and makes sure every sentence separator (colon, semicolon, quotes, etc.) has one space before and after. Also, all separators are converted to comma
{
	var originalchars = 'áéíóúäëïöüâêîôûàèìòùÁÉÍÓÚÄËÏÖÜÂÊÎÔÛÀÈÌÒÙ';
	var i;
	var output = '';
	var pos;

	for (i=0;i<player_order.length;i++) 
	{
		pos = originalchars.indexOf(player_order.charAt(i));
		if (pos!=-1) output = output + "aeiou".charAt(pos % 5); else 
		{
			ch = player_order.charAt(i);
				if ((ch=='.') || (ch==',') || (ch==';') || (ch=='"') || (ch=='\'') || (ch=='«') || (ch=='»')) output = output + ' , '; else output = output + player_order.charAt(i);
		}

	}
	return output;
}

function toParserBuffer(player_order)  // Converts a player order in a list of sentences separated by dot.
{
     player_order = normalize(player_order);
     player_order = player_order.toUpperCase();
    
	 var words = player_order.split(' ');
	 for (var q=0;q<words.length;q++)
	 {
	 	words[q] = words[q].trim();
	 	if  (words[q]!=',')
	 	{
	 		words[q] = words[q].trim();
	 		foundWord = findVocabulary(words[q], false);
	 		if (foundWord)
	 		{
	 			if (foundWord[VOCABULARY_TYPE]==WORDTYPE_CONJUNCTION)
	 			{
	 			words[q] = ','; // Replace conjunctions with commas
		 		} 
	 		}
	 	}
	 }

	 var output = '';
	 for (q=0;q<words.length;q++)
	 {
	 	if (words[q] == ',') output = output + ','; else output = output + words[q] + ' ';
	 }
	 output = output.replace(/ ,/g,',');
	 output = output.trim();
	 player_order_buffer = output;
}

function getSentencefromBuffer()
{
	var sentences = player_order_buffer.split(',');
	var result = sentences[0];
	sentences.splice(0,1);
	player_order_buffer = sentences.join();
	return result;
}

function processPronounSufixes(words)  
{
	// This procedure will split pronominal sufixes into separated words, so COGELA will become COGE LA at the end, and work exactly as TAKE IT does.
	// it's only for spanish so if lang is english then it makes no changes
	if (getLang() == 'EN') return words;
	var verbFound = false;
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS),0)) return words;  // If pronoun sufixes inactive, just do nothing
	// First, we clear the word list from any match with pronouns, cause if we already have something that matches pronouns, probably is just concidence, like in COGE LA LLAVE
	var filtered_words = [];
	for (var q=0;q < words.length;q++)
	{
		foundWord = findVocabulary(words[q], false);
		if (foundWord) 
			{
				if (foundWord[VOCABULARY_TYPE] != WORDTYPE_PRONOUN) filtered_words[filtered_words.length] = words[q];
			}
			else filtered_words[filtered_words.length] = words[q];
	}
	words = filtered_words;

	// Now let's start trying to get sufixes
	new_words = [];
	for (var k=0;k < words.length;k++)
	{
		words[k] = words[k].trim();
		foundWord = findVocabulary(words[k], true); // true to disable Levenshtein distance applied
		if (foundWord) if (foundWord[VOCABULARY_TYPE] == WORDTYPE_VERB) verbFound = true;  // If we found a verb, we don't look for pronoun sufixes, as they have to come together with verb
		suffixFound = false;
		pronunsufix_search:
		for (var l=0;(l<pronoun_suffixes.length) && (!suffixFound) && (!verbFound);l++)
		{

			if (pronoun_suffixes[l] == words[k].rights(pronoun_suffixes[l].length))
			{
				var verb_part = words[k].substring(0,words[k].length - pronoun_suffixes[l].length);
				var checkWord = findVocabulary(verb_part, false);
				if ((!checkWord)  || (checkWord[VOCABULARY_TYPE] != WORDTYPE_VERB))  // If the part before the supposed-to-be pronoun sufix is not a verb, then is not a pronoun sufix
				{
					new_words.push(words[k]);	
					continue pronunsufix_search;
				}
				new_words.push(verb_part);  // split the word in two parts: verb + pronoun. Since that very moment it works like in english (COGERLO ==> COGER LO as of TAKE IT)
				new_words.push(pronoun_suffixes[l]);
				suffixFound = true;
				verbFound = true;
			}
		}
		if (!suffixFound) new_words.push(words[k]);
	}
	return new_words;
}

function getLogicSentence()
{
	parser_word_found = false; ;
	aux_verb = -1;
	aux_noun1 = -1;
	aux_adject1 = -1;
	aux_adverb = -1;
	aux_pronoun = -1
	aux_pronoun_adject = -1
	aux_preposition = -1;
	aux_noun2 = -1;
	aux_adject2 = -1;
	initializeLSWords();
	SL_found = false;

	var order = getSentencefromBuffer();
	setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS),1)); // Initialize flag that says an unknown word was found in the sentence


	words = order.split(" ");
	words = processPronounSufixes(words);
	wordsearch_loop:
	for (var i=0;i<words.length;i++)
	{
		original_word = currentword = words[i];
		if (currentword.length>10) currentword = currentword.substring(0,MAX_WORD_LENGHT);
		foundWord = findVocabulary(currentword, false);
		if (foundWord)
		{
			wordtype = foundWord[VOCABULARY_TYPE];
			word_id = foundWord[VOCABULARY_ID];

			switch (wordtype)
			{
				case WORDTYPE_VERB: if (aux_verb == -1)  aux_verb = word_id; 
				        			break;

				case WORDTYPE_NOUN: if (aux_noun1 == -1) aux_noun1 = word_id; else if (aux_noun2 == -1) aux_noun2 = word_id;
									break;

				case WORDTYPE_ADJECT: if (aux_adject1 == -1) aux_adject1 = word_id; else if (aux_adject2 == -1) aux_adject2 = word_id;
									  break;

				case WORDTYPE_ADVERB: if (aux_adverb == -1) aux_adverb = word_id;
				        			  break;

				case WORDTYPE_PRONOUN: if (aux_pronoun == -1) 
											{
												aux_pronoun = word_id;
												if ((previous_noun != EMPTY_WORD) && (aux_noun1 == -1))
												{
													aux_noun1 = previous_noun;
													if (previous_adject != EMPTY_WORD) aux_adject1 = previous_adject;
												}
											}

				        			   break;

				case WORDTYPE_CONJUNCTION: break wordsearch_loop; // conjunction or nexus. Should not appear in this function, just added for security
				
				case WORDTYPE_PREPOSITION: if (aux_preposition == -1) aux_preposition = word_id;
										   if (aux_noun1!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),2));  // Set bit that determines that a preposition word was found after first noun
										   break;
			}

			// Nouns that can be converted to verbs
			if ((aux_noun1!=-1) && (aux_verb==-1) && (aux_noun1 < NUM_CONVERTIBLE_NOUNS))
			{
				aux_verb = aux_noun1;
				aux_noun1 = -1;
			}

			if ((aux_verb==-1) && (aux_noun1!=-1) && (previous_verb!=EMPTY_WORD)) aux_verb = previous_verb;  // Support "TAKE SWORD AND SHIELD" --> "TAKE WORD AND TAKE SHIELD"

			if ((aux_verb!=-1) || (aux_noun1!=-1) || (aux_adject1!=-1 || (aux_preposition!=-1) || (aux_adverb!=-1))) SL_found = true;



		} else if (aux_verb!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),1));  // Set bit that determines that an unknown word was found after the verb
	}

	if (SL_found)
	{
		if (aux_verb != -1) setFlag(FLAG_VERB, aux_verb);
		if (aux_noun1 != -1) setFlag(FLAG_NOUN1, aux_noun1);
		if (aux_adject1 != -1) setFlag(FLAG_ADJECT1, aux_adject1);
		if (aux_adverb != -1) setFlag(FLAG_ADVERB, aux_adverb);
		if (aux_pronoun != -1) 
			{
				setFlag(FLAG_PRONOUN, aux_noun1);
				setFlag(FLAG_PRONOUN_ADJECT, aux_adject1);
			}
			else
			{
				setFlag(FLAG_PRONOUN, EMPTY_WORD);
				setFlag(FLAG_PRONOUN_ADJECT, EMPTY_WORD);
			}
		if (aux_preposition != -1) setFlag(FLAG_PREP, aux_preposition);
		if (aux_noun2 != -1) setFlag(FLAG_NOUN2, aux_noun2);
		if (aux_adject2 != -1) setFlag(FLAG_ADJECT2, aux_adject2);
		setReferredObject(getReferredObject());
		previous_verb = aux_verb;
		if ((aux_noun1!=-1) && (aux_noun1>=NUM_PROPER_NOUNS))
		{
			previous_noun = aux_noun1;
			if (aux_adject1!=-1) previous_adject = aux_adject1;
		}
		
	}
	if ((aux_verb + aux_noun1+ aux_adject1 + aux_adverb + aux_pronoun + aux_preposition + aux_noun2 + aux_adject2) != -8) parser_word_found = true;

	return SL_found;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        Main functions and main loop                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Interrupt functions

function enableInterrupt()
{
	interruptDisabled = false;
}

function disableInterrupt()
{
	interruptDisabled = true;
}

function timer()
{
	// Timeout control
	timeout_progress=  timeout_progress + 1/32;  //timer happens every 40 milliseconds, but timeout counter should only increase every 1.28 seconds (according to PAWS documentation)
	timeout_length = getFlag(FLAG_TIMEOUT_LENGTH);
	if ((timeout_length) && (timeout_progress> timeout_length))  // time for timeout
	{
		timeout_progress = 0;
		if (($('.prompt').val() == '')  || (($('.prompt').val()!='') && (!bittest(getFlag(FLAG_TIMEOUT_SETTINGS),0))) )  // but first check there is no text type, or is allowed to timeout when text typed already
		{
			setFlag(FLAG_TIMEOUT_SETTINGS, bitset(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
			writeSysMessage(SYSMESS_TIMEOUT);	
			callProcess(PROCESS_TURN);
		}
	}	

	// PAUSE condact control
	if (inPause)
	{
		pauseRemainingTime = pauseRemainingTime - 40; // every tick = 40 milliseconds
		if (pauseRemainingTime<=0)
		{
			inPause = false;
			hideAnykeyLayer();
			waitKeyCallback()
		}
	}

	// Interrupt process control
	if (!interruptDisabled)
	if (interruptProcessExists)
	{
		callProcess(interrupt_proc);
		setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
	}

}

// Initialize and finalize functions

function farewell()
{
	writeSysMessage(SYSMESS_FAREWELL);
	ACCnewline();
}


function initializeConnections()
{
  connections = [].concat(connections_start);
}

function initializeObjects()
{
  for (i=0;i<objects.length;i++)
  {
  	objectsAttrLO = [].concat(objectsAttrLO_start);
  	objectsAttrHI = [].concat(objectsAttrHI_start);
  	objectsLocation = [].concat(objectsLocation_start);
  	objectsWeight = [].concat(objectsWeight_start);
  }
}

function  initializeLSWords()
{
  setFlag(FLAG_PREP,EMPTY_WORD);
  setFlag(FLAG_NOUN2,EMPTY_WORD);
  setFlag(FLAG_ADJECT2,EMPTY_WORD);
  setFlag(FLAG_PRONOUN,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_VERB,EMPTY_WORD);
  setFlag(FLAG_NOUN1,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_ADVERB,EMPTY_WORD);
}


function initializeFlags()
{
  flags = [];
  for (var  i=0;i<FLAG_COUNT;i++) flags.push(0);
  setFlag(FLAG_MAXOBJECTS_CARRIED,4);
  setFlag(FLAG_PARSER_SETTINGS,9); // Pronoun sufixes active, DOALL and others ignore NPCs, etc. 00001001
  setFlag(FLAG_MAXWEIGHT_CARRIED,10);
  initializeLSWords();
  setFlag(FLAG_OBJECT_LIST_FORMAT,64); // List objects in a single sentence (comma separated)
  setFlag(FLAG_OBJECTS_CARRIED_COUNT,carried_objects);  // FALTA: el compilador genera esta variable, hay que cambiarlo en el compilador, ERA numero_inicial_de_objetos_llevados
}

function initializeInternalVars()
{
	num_objects = last_object_number + 1;
	transcript = '';
	timeout_progress = 0;
	previous_noun = EMPTY_WORD;
	previous_verb = EMPTY_WORD;
	previous_adject = EMPTY_WORD;
	player_order_buffer = '';
	last_player_orders = [];
	last_player_orders_pointer = 0;
	graphicsON = true; 
	soundsON = true; 
	interruptDisabled = false;
	unblock_process = null;
	done_flag = false;
	describe_location_flag =false;
	in_response = false;
	success = false;
	doall_flag = false;
	entry_for_doall	= '';
}

function initializeSound()
{
	sfxstopall();
}




function initialize()
{
	preloadsfx();
	initializeInternalVars();
	initializeSound();
	initializeFlags();
	initializeObjects();
	initializeConnections();
}



// Main loops

function descriptionLoop()
{
	do
	{
		describe_location_flag = false;
		if (!getFlag(FLAG_MODE)) clearTextWindow();
		if ((isDarkHere()) && (!lightObjectsPresent())) writeSysMessage(SYSMESS_ISDARK); else writeLocation(loc_here()); 
		h_description_init();
		playLocationMusic(loc_here());
		if (loc_here()) drawPicture(loc_here()); else hideGraphicsWindow(); // Don't show picture at location 0
		ACCminus(FLAG_AUTODEC2,1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC3,1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC4,1);
		callProcess(PROCESS_DESCRIPTION);
		h_description_post();
		if (describe_location_flag) continue; // descriptionLoop() again without nesting
		describe_location_flag = false;
		callProcess(PROCESS_TURN);
		if (describe_location_flag) continue; 
		describe_location_flag = false;
		focusInput();
		break; // Dirty trick to make this happen just one, but many times if descriptioLoop() should be repeated
	} while (true);

}

function orderEnteredLoop(player_order)
{
	previous_verb = EMPTY_WORD;
	setFlag(FLAG_TIMEOUT_SETTINGS, bitclear(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
	if (player_order == '') {writeSysMessage(SYSMESS_SORRY); ACCnewline(); return; };	
	player_order = h_playerOrder(player_order); //hook
	copyOrderToTextWindow(player_order);
	toParserBuffer(player_order);
	do 
	{
		describe_location_flag = false;
		ACCminus(FLAG_AUTODEC5,1);
		ACCminus(FLAG_AUTODEC6,1);
		ACCminus(FLAG_AUTODEC7,1);
		ACCminus(FLAG_AUTODEC8,1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC9,1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC10,1);
		
		if (describe_location_flag) 
		{
			descriptionLoop();
			return;
		};

		if (getLogicSentence())
		{
			incTurns();
			done_flag = false;
			callProcess(PROCESS_RESPONSE); // Response table
			if (describe_location_flag) 
			{
				descriptionLoop();
				return;
			};
			if (!done_flag) 
			{
				if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (CNDmove(FLAG_LOCATION)))
				{
					descriptionLoop();
					return;
				} else if (getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) {writeSysMessage(SYSMESS_WRONGDIRECTION);ACCnewline();}	else {writeSysMessage(SYSMESS_CANTDOTHAT);ACCnewline();};

			}
		} else
		{
			h_invalidOrder(player_order);
			if (parser_word_found) {writeSysMessage(SYSMESS_IDONTUNDERSTAND);   ACCnewline() }
			    		      else {writeSysMessage(SYSMESS_NONSENSE_SENTENCE); ACCnewline() };	
		}  
		callProcess(PROCESS_TURN);
	} while (player_order_buffer !='');
	previous_verb = ''; // Can't use previous verb if a new order is typed (we keep previous noun though, it can be used)
	focusInput();
}


function restart()
{
	location.reload();	
}


function hideBlock()
{
	clearInputWindow();
    $('.block_layer').hide('slow');
    enableInterrupt();   	
    $('.input').show();  
    focusInput();
}

function hideAnykeyLayer()
{
	$('.anykey_layer').hide();
    $('.input').show();  
    focusInput();   
}

function showAnykeyLayer()
{
	$('.anykey_layer').show();
    $('.input').hide();  
}

//called when the block layer is closed
function closeBlock()
{
	if (!inBlock) return;
	inBlock = false;
	hideBlock();
    var proToCall = unblock_process;
	unblock_process = null;
	callProcess(proToCall);
	if (describe_location_flag) descriptionLoop();
}

function setInputPlaceHolder()
{
	var prompt_msg = getFlag(FLAG_PROMPT);
	if (!prompt_msg)
	{
		var random = Math.floor((Math.random()*100));
		if (random<30) prompt_msg = SYSMESS_PROMPT0; else
		if ((random>=30) && (random<60)) prompt_msg = SYSMESS_PROMPT1; else
		if ((random>=60) && (random<90)) prompt_msg = SYSMESS_PROMPT2; else
		if (random>=90) prompt_msg = SYSMESS_PROMPT3;
	}
	$('.prompt').attr('placeholder', $('<div>'+getSysMessageText(prompt_msg).replace(/(?:<br>)*$/,'').replace( /<br>/g, ', ' )+'</div>').text());
}


function divTextScrollUp()
{
   	var currentPos = $('.text').scrollTop();
	if (currentPos>=DIV_TEXT_SCROLL_STEP) $('.text').scrollTop(currentPos - DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop(0);
}

function divTextScrollDown()
{
   	var currentPos = $('.text').scrollTop();
   	if (currentPos <= ($('.text')[0].scrollHeight - DIV_TEXT_SCROLL_STEP)) $('.text').scrollTop(currentPos + DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop($('.text')[0].scrollHeight);
}

// Autocomplete functions

function predictiveText(currentText)
{
	if (currentText == '') return currentText;
	var wordToComplete;
	var words = currentText.split(' ');
	if (autocompleteStep!=0) wordToComplete = autocompleteBaseWord; else wordToComplete = words[words.length-1];
	words[words.length-1] = completedWord(wordToComplete);
	return words.join(' ');
}


function initAutoComplete()
{
	for (var j=0;j<vocabulary.length;j++)
		if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_VERB)
			if (vocabulary[j][VOCABULARY_WORD].length >= 3)
				autocomplete.push(vocabulary[j][VOCABULARY_WORD].toLowerCase());
}

function addToAutoComplete(sentence)
{
	var words = sentence.split(' ');
	for (var i=0;i<words.length;i++)
	{
		var finalWord = '';
		for (var j=0;j<words[i].length;j++)
		{
			var c = words[i][j].toLowerCase();
			if ("abcdefghijklmnopqrstuvwxyzáéíóúàèìòùçäëïÖüâêîôû".indexOf(c) != -1) finalWord = finalWord + c;
			else break;
		}
	
		if (finalWord.length>=3) 
		{
			var index = autocomplete.indexOf(finalWord);
			if (index!=-1) autocomplete.splice(index,1);
			autocomplete.push(finalWord);
		}
	}
}

function completedWord(word)
{
	if (word=='') return '';
   autocompleteBaseWord  =word;
   var foundCount = 0;
   for (var i = autocomplete.length-1;i>=0; i--)
   {
   	  if (autocomplete[i].length > word.length) 
   	  	 if (autocomplete[i].indexOf(word)==0) 
   	  	 	{
   	  	 		foundCount++;
   	  	 		if (foundCount>autocompleteStep)
   	  	 		{
   	  	 			autocompleteStep++;
   	  	 			return autocomplete[i];
   	  	 		}
   	  	 	}
   }
   return word;
}


// Exacution starts here, called by the html file on document.ready()
function start()
{
	h_init(); //hook
	$('.graphics').addClass('half_graphics');
	$('.text').addClass('half_text');
	if (isBadIE()) alert(STR_BADIE)
	loadPronounSufixes();	
    setInputPlaceHolder();
    initAutoComplete();

	// Assign keypress action for input box (detect enter key press)
	$('.prompt').keypress(function(e) {  
    	if (e.which == 13) 
    	{ 
    		setInputPlaceHolder();
    		player_order = $('.prompt').val();
    		if (player_order.charAt(0) == '#')
    		{
    			addToTranscript(player_order + STR_NEWLINE);
    			clearInputWindow();
    		} 
    		else
    		if (player_order!='') 
    				orderEnteredLoop(player_order);
    	}
    });

	// Assign arrow up key press to recover last order
    $('.prompt').keyup( function(e) {
    	if (e.which  == 38) $('.prompt').val(get_prev_player_order());
    	if (e.which  == 40) $('.prompt').val(get_next_player_order());
    });


    // Assign tab keydown to complete word
    $('.prompt').keydown( function(e) {
    	if (e.which == 9) 
    		{
    			$('.prompt').val(predictiveText($('.prompt').val()));
    			e.preventDefault();
    		} else 
    		{
		    	autocompleteStep = 0;
    			autocompleteBaseWord = ''; // Any keypress other than tab resets the autocomplete feature
    		}
    });

    //Detect resize to change flag 12
     $(window).resize(function () {
     	setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
     	clearPictureAt();
     	return;
     });


     // assign any click on block layer --> close it
     $(document).click( function(e) {

	// if waiting for END response
	if (inEND)
	{
		restart();
		return;
	}

     	if (inBlock)
     	{
     		closeBlock();
     		e.preventDefault();
     		return;
     	}

     	if (inAnykey)  // return for ANYKEY, accepts mouse click
     	{
     		inAnykey = false;
     		hideAnykeyLayer();
     		waitKeyCallback();
     		e.preventDefault();
     		return;
    	}

     });

     //Make tap act as click
    //document.addEventListener('touchstart', function(e) {$(document).click(); }, false);   
     
     
	$(document).keydown(function(e) {

		if (!h_keydown(e)) return; // hook

		// if waiting for END response
		if (inEND)
		{
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N'; 
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			switch ( e.keyCode )
			{
				case endYESresponseCode:
				case 13: // return
				case 32: // space
					location.reload();
					break;
				case endNOresponseCode:
					inEND = false;
					sfxstopall();
					$('body').hide('slow');
					break;
			}
			return;
		}


		// if waiting for QUIT response
		if (inQUIT)
		{
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N'; 
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			switch ( e.keyCode )
			{
				case endYESresponseCode:
				case 13: // return
				case 32: // space
					inQUIT=false;
					e.preventDefault();
					waitKeyCallback();
					return;
				case endNOresponseCode:
					inQUIT=false;
					waitkey_callback_function.pop();
					hideAnykeyLayer();
					e.preventDefault();
					break;
			}
		}

		// ignore uninteresting keys
		switch ( e.keyCode )
		{
			case 9:  // tab   \ keys used during
			case 13: // enter / keyboard navigation
			case 16: // shift
			case 17: // ctrl
			case 18: // alt
			case 20: // caps lock
			case 91: // left Windows key
			case 92: // left Windows key
			case 93: // left Windows key
			case 225: // right alt
				// do not focus the input - the user was probably doing something else
				// (e.g. alt-tab'ing to another window)
				return;
		}


		if (inGetkey)  // return for getkey
     	{
     		setFlag(getkey_return_flag, e.keyCode);
     		getkey_return_flag = null;
     		inGetkey = false;
     		hideAnykeyLayer();
     		e.preventDefault();
     		waitKeyCallback();
     		return;
      	}

     	// Scroll text window using PgUp/PgDown
        if (e.keyCode==33)  // PgUp
        {
        	divTextScrollUp();
        	e.preventDefault();
        	return;
        }
        if (e.keyCode==34)  // PgDown
        {
        	divTextScrollDown();
        	return;
        }


     	if (inAnykey)  // return for anykey
     	{
     		inAnykey = false;
     		hideAnykeyLayer();
     		e.preventDefault();
     		waitKeyCallback();
     		return;
     	}

		// if keypress and block displayed, close it
     	if (inBlock)
     		{
     			closeBlock();
     			e.preventDefault();
     			return;
     		}


     	// if ESC pressed and transcript layer visible, close it
     	if ((inTranscript) &&  (e.keyCode == 27)) 
     		{
     			$('.transcript_layer').hide();
     			inTranscript = false;
     			e.preventDefault();
     			return;
     		}

	// focus the input if the user is likely to expect it
	// (but not if they're e.g. ctrl+c'ing some text)
	switch ( e.keyCode )
	{
		case 8: // backspace
		case 9: // tab
		case 13: // enter
			break;
		default:
			if ( !e.ctrlKey && !e.altKey ) focusInput();
	}

	});


    $(document).bind('wheel mousewheel',function(e)
    {
  		if((e.originalEvent.wheelDelta||-e.originalEvent.deltaY) > 0) divTextScrollUp(); else divTextScrollDown();
    });


	initialize();
	descriptionLoop();
	focusInput();
	
	h_post();  //hook

    // Start interrupt process
    setInterval( timer, TIMER_MILLISECONDS );

}

$('document').ready(
	function ()
	{
		start();
	}
	);

// VOCABULARY

vocabulary = [];
vocabulary.push([3, "A", 6]);
vocabulary.push([10, "AB", 1]);
vocabulary.push([10, "ABAJO", 1]);
vocabulary.push([60, "ABANDONA", 0]);
vocabulary.push([60, "ABANDONAR", 0]);
vocabulary.push([42, "ABRE", 0]);
vocabulary.push([42, "ABRIR", 0]);
vocabulary.push([50, "AGARRA", 0]);
vocabulary.push([50, "AGARRAR", 0]);
vocabulary.push([3, "AL", 6]);
vocabulary.push([33, "ANUDA", 0]);
vocabulary.push([33, "ANUDAR", 0]);
vocabulary.push([46, "APAGA", 0]);
vocabulary.push([46, "APAGAR", 0]);
vocabulary.push([30, "APRETAR", 0]);
vocabulary.push([30, "APRIETA", 0]);
vocabulary.push([9, "AR", 1]);
vocabulary.push([53, "ARBOL", 1]);
vocabulary.push([53, "ARBOLES", 1]);
vocabulary.push([20, "ARRANCA", 0]);
vocabulary.push([20, "ARRANCAR", 0]);
vocabulary.push([9, "ARRIBA", 1]);
vocabulary.push([21, "ARROJA", 0]);
vocabulary.push([21, "ARROJAR", 0]);
vocabulary.push([33, "ATA", 0]);
vocabulary.push([33, "ATAR", 0]);
vocabulary.push([75, "BABOSA", 1]);
vocabulary.push([10, "BAJA", 0]);
vocabulary.push([10, "BAJAR", 0]);
vocabulary.push([35, "BALANCEA", 0]);
vocabulary.push([35, "BALANCEATE", 0]);
vocabulary.push([84, "BARROTE", 1]);
vocabulary.push([65, "BLOQUE", 1]);
vocabulary.push([61, "BOTA", 1]);
vocabulary.push([61, "BOTAS", 1]);
vocabulary.push([92, "BOTON", 1]);
vocabulary.push([99, "CANALIZADO", 1]);
vocabulary.push([48, "CARGA", 0]);
vocabulary.push([48, "CARGAR", 0]);
vocabulary.push([43, "CERRAR", 0]);
vocabulary.push([43, "CIERRA", 0]);
vocabulary.push([50, "COGE", 0]);
vocabulary.push([50, "COGER", 0]);
vocabulary.push([39, "COLOCA", 0]);
vocabulary.push([39, "COLOCAR", 0]);
vocabulary.push([35, "COLUMPIA", 0]);
vocabulary.push([35, "COLUMPIATE", 0]);
vocabulary.push([88, "COMPARTIME", 1]);
vocabulary.push([2, "CON", 6]);
vocabulary.push([80, "CONTROLES", 1]);
vocabulary.push([28, "CORTA", 0]);
vocabulary.push([28, "CORTAR", 0]);
vocabulary.push([73, "CRIATURA", 1]);
vocabulary.push([23, "CRUZA", 0]);
vocabulary.push([23, "CRUZAR", 0]);
vocabulary.push([96, "CUADRO", 1]);
vocabulary.push([54, "CUERDA", 1]);
vocabulary.push([61, "DA", 0]);
vocabulary.push([61, "DALE", 0]);
vocabulary.push([61, "DAR", 0]);
vocabulary.push([61, "DARLE", 0]);
vocabulary.push([53, "DECIR", 0]);
vocabulary.push([44, "DEJA", 0]);
vocabulary.push([44, "DEJAR", 0]);
vocabulary.push([5, "DEMANO", 2]);
vocabulary.push([5, "DENTRO", 6]);
vocabulary.push([250, "DERRAMA", 0]);
vocabulary.push([250, "DERRAMAR", 0]);
vocabulary.push([34, "DESANUDA", 0]);
vocabulary.push([34, "DESANUDAR", 0]);
vocabulary.push([34, "DESATA", 0]);
vocabulary.push([34, "DESATAR", 0]);
vocabulary.push([36, "DESPERTAR", 0]);
vocabulary.push([36, "DESPIERTA", 0]);
vocabulary.push([53, "DI", 0]);
vocabulary.push([37, "DISPARA", 0]);
vocabulary.push([37, "DISPARAR", 0]);
vocabulary.push([86, "DISPOSITIV", 1]);
vocabulary.push([3, "E", 1]);
vocabulary.push([250, "ECHA", 0]);
vocabulary.push([250, "ECHAR", 0]);
vocabulary.push([29, "EMPUJA", 0]);
vocabulary.push([29, "EMPUJAR", 0]);
vocabulary.push([5, "EN", 6]);
vocabulary.push([45, "ENCENDER", 0]);
vocabulary.push([45, "ENCIEDE", 0]);
vocabulary.push([62, "ENSENA", 0]);
vocabulary.push([62, "ENSENAR", 0]);
vocabulary.push([62, "ENSEÑA", 0]);
vocabulary.push([62, "ENSEÑAR", 0]);
vocabulary.push([2, "ENTONCES", 5]);
vocabulary.push([11, "ENTRA", 0]);
vocabulary.push([32, "ESCALA", 0]);
vocabulary.push([32, "ESCALAR", 0]);
vocabulary.push([71, "ESPEJO", 1]);
vocabulary.push([41, "ESPERA", 0]);
vocabulary.push([74, "ESQUELETO", 1]);
vocabulary.push([3, "ESTATICO", 2]);
vocabulary.push([3, "ESTE", 1]);
vocabulary.push([55, "EX", 0]);
vocabulary.push([55, "EXAMINA", 0]);
vocabulary.push([55, "EXAMINAR", 0]);
vocabulary.push([60, "FIN", 0]);
vocabulary.push([56, "FOSA", 1]);
vocabulary.push([97, "FUSIBLE", 1]);
vocabulary.push([27, "GOLPEA", 0]);
vocabulary.push([27, "GOLPEAR", 0]);
vocabulary.push([47, "GRABA", 0]);
vocabulary.push([47, "GRABAR", 0]);
vocabulary.push([77, "GUANTES", 1]);
vocabulary.push([69, "GUSANO", 1]);
vocabulary.push([53, "HABLA", 0]);
vocabulary.push([65, "HIELO", 1]);
vocabulary.push([14, "I", 1]);
vocabulary.push([94, "IGNICION", 1]);
vocabulary.push([93, "INICIAR", 1]);
vocabulary.push([39, "INSERTA", 0]);
vocabulary.push([39, "INSERTAR", 0]);
vocabulary.push([39, "INTRODUCE", 0]);
vocabulary.push([39, "INTRODUCIR", 0]);
vocabulary.push([14, "INVENTARIO", 1]);
vocabulary.push([57, "LAGO", 1]);
vocabulary.push([21, "LANZA", 0]);
vocabulary.push([21, "LANZAR", 0]);
vocabulary.push([54, "LIANA", 1]);
vocabulary.push([54, "LIANAS", 1]);
vocabulary.push([70, "LIQUIDO", 1]);
vocabulary.push([48, "LOAD", 0]);
vocabulary.push([2, "LUEGO", 5]);
vocabulary.push([54, "M", 0]);
vocabulary.push([38, "MATA", 0]);
vocabulary.push([38, "MATAR", 0]);
vocabulary.push([57, "METE", 0]);
vocabulary.push([57, "METER", 0]);
vocabulary.push([11, "METETE", 0]);
vocabulary.push([54, "MIRA", 0]);
vocabulary.push([54, "MIRAR", 0]);
vocabulary.push([55, "MOLE", 1]);
vocabulary.push([55, "MOLES", 1]);
vocabulary.push([78, "MONITOR", 1]);
vocabulary.push([31, "MONTA", 0]);
vocabulary.push([62, "MOSTRAR", 0]);
vocabulary.push([40, "MOVER", 0]);
vocabulary.push([62, "MUESTRA", 0]);
vocabulary.push([40, "MUEVE", 0]);
vocabulary.push([13, "N", 1]);
vocabulary.push([22, "NADA", 0]);
vocabulary.push([22, "NADAR", 0]);
vocabulary.push([87, "NAVE", 1]);
vocabulary.push([5, "NE", 1]);
vocabulary.push([2, "NEUTRALIZA", 2]);
vocabulary.push([6, "NO", 1]);
vocabulary.push([5, "NORDESTE", 1]);
vocabulary.push([5, "NORESTE", 1]);
vocabulary.push([6, "NOROESTE", 1]);
vocabulary.push([13, "NORTE", 1]);
vocabulary.push([4, "O", 1]);
vocabulary.push([4, "OESTE", 1]);
vocabulary.push([91, "ORIFICIO", 1]);
vocabulary.push([95, "PALANCA", 1]);
vocabulary.push([81, "PANEL", 1]);
vocabulary.push([27, "PARTE", 0]);
vocabulary.push([27, "PARTIR", 0]);
vocabulary.push([67, "PATADA", 1]);
vocabulary.push([26, "PATEA", 0]);
vocabulary.push([26, "PATEAR", 0]);
vocabulary.push([52, "PEDERNAL", 1]);
vocabulary.push([66, "PIE", 1]);
vocabulary.push([55, "PIEDRA", 1]);
vocabulary.push([55, "PIEDRAS", 1]);
vocabulary.push([66, "PIES", 1]);
vocabulary.push([64, "PINTURA", 1]);
vocabulary.push([63, "PISTOLA", 1]);
vocabulary.push([65, "PLACA", 1]);
vocabulary.push([65, "PLACAS", 1]);
vocabulary.push([56, "PON", 0]);
vocabulary.push([56, "PONER", 0]);
vocabulary.push([58, "PONTE", 0]);
vocabulary.push([50, "PRECIPICIO", 1]);
vocabulary.push([53, "PREGUNTA", 0]);
vocabulary.push([30, "PRESIONA", 0]);
vocabulary.push([30, "PRESIONAR", 0]);
vocabulary.push([76, "PUERTA", 1]);
vocabulary.push([30, "PULSA", 0]);
vocabulary.push([30, "PULSAR", 0]);
vocabulary.push([68, "PUNETAZO", 1]);
vocabulary.push([25, "PUNTOS", 0]);
vocabulary.push([25, "PUNTUACION", 0]);
vocabulary.push([68, "PUÑETAZO", 1]);
vocabulary.push([51, "QUITA", 0]);
vocabulary.push([54, "R", 0]);
vocabulary.push([52, "RAMLOAD", 0]);
vocabulary.push([49, "RAMSAVE", 0]);
vocabulary.push([70, "RECIPIENTE", 1]);
vocabulary.push([83, "REJILLA", 1]);
vocabulary.push([57, "REMOLINO", 1]);
vocabulary.push([60, "RETIRARSE", 0]);
vocabulary.push([60, "RETIRARTE", 0]);
vocabulary.push([58, "RIACHUELO", 1]);
vocabulary.push([58, "RIO", 1]);
vocabulary.push([52, "RL", 0]);
vocabulary.push([82, "ROBOT", 1]);
vocabulary.push([27, "ROMPE", 0]);
vocabulary.push([27, "ROMPER", 0]);
vocabulary.push([49, "RS", 0]);
vocabulary.push([2, "S", 1]);
vocabulary.push([51, "SACA", 0]);
vocabulary.push([51, "SACAR", 0]);
vocabulary.push([59, "SACATE", 0]);
vocabulary.push([12, "SAL", 0]);
vocabulary.push([63, "SALIDAS", 0]);
vocabulary.push([12, "SALIR", 0]);
vocabulary.push([24, "SALTA", 0]);
vocabulary.push([24, "SALTAR", 0]);
vocabulary.push([47, "SAVE", 0]);
vocabulary.push([7, "SE", 1]);
vocabulary.push([100, "SI", 1]);
vocabulary.push([72, "SIMBOLO", 1]);
vocabulary.push([8, "SO", 1]);
vocabulary.push([4, "SOBRE", 6]);
vocabulary.push([44, "SOLTAR", 0]);
vocabulary.push([9, "SUBE", 0]);
vocabulary.push([31, "SUBETE", 0]);
vocabulary.push([9, "SUBIR", 0]);
vocabulary.push([85, "SUCCIONADO", 1]);
vocabulary.push([59, "SUELO", 1]);
vocabulary.push([44, "SUELTA", 0]);
vocabulary.push([2, "SUR", 1]);
vocabulary.push([7, "SURESTE", 1]);
vocabulary.push([8, "SUROESTE", 1]);
vocabulary.push([60, "TABLON", 1]);
vocabulary.push([60, "TABLONES", 1]);
vocabulary.push([79, "TARJETA", 1]);
vocabulary.push([89, "TECNICO", 1]);
vocabulary.push([20, "TIRA", 0]);
vocabulary.push([20, "TIRAR", 0]);
vocabulary.push([51, "TODO", 1]);
vocabulary.push([50, "TOMA", 0]);
vocabulary.push([50, "TOMAR", 0]);
vocabulary.push([32, "TREPA", 0]);
vocabulary.push([32, "TREPAR", 0]);
vocabulary.push([90, "UNIFORME", 1]);
vocabulary.push([62, "UTENSILIO", 1]);
vocabulary.push([62, "UTENSILIOS", 1]);
vocabulary.push([98, "VENTANA", 1]);
vocabulary.push([98, "VENTANITA", 1]);
vocabulary.push([4, "VERDE", 2]);
vocabulary.push([250, "VERTER", 0]);
vocabulary.push([250, "VIERTE", 0]);
vocabulary.push([63, "X", 0]);
vocabulary.push([2, "Y", 5]);
vocabulary.push([41, "Z", 0]);



// SYS MESSAGES

total_sysmessages=183;

sysmessages = [];

sysmessages[0] = "ESTA MUY OSCURO PARA PODER VER";
sysmessages[1] = "También puedes ver¬";
sysmessages[2] = "\n";
sysmessages[3] = "\n Haz algo.\n";
sysmessages[4] = "\n ¿Alguna otra idea?\n";
sysmessages[5] = "\n Espero instrucciones.\n";
sysmessages[6] = "No pude entenderte. Usa otras palabras.\n";
sysmessages[7] = "No puedes ir en esa direccion.\n";
sysmessages[8] = "No sé como hacer eso. Intenta describirlo de otra forma.\n";
sysmessages[9] = "Tienes¬";
sysmessages[10] = "levas puest";
sysmessages[11] = "=<NADA>=\n";
sysmessages[12] = "¿Seguro?\n";
sysmessages[13] = "\n    ¿ Quieres jugar de nuevo ?\n";
sysmessages[14] = "Pues adiós…\n";
sysmessages[15] = "VALE.\n";
sysmessages[16] = "\n {CLASS|centrado|-=PULSA UNA TECLA PARA SEGUIR=-}<br>";
sysmessages[17] = "Has realizado¬";
sysmessages[18] = " movimiento";
sysmessages[19] = "s";
sysmessages[20] = ".\n";
sysmessages[21] = "Tu puntuación es de¬";
sysmessages[22] = " sobre un total de 30.\n";
sysmessages[23] = "No lo llevas puesto.\n";
sysmessages[24] = "No puedes dejar¬";
sysmessages[25] = "Ya tienes¬";
sysmessages[26] = "coger¬";
sysmessages[27] = "No puedes llevar más cosas.\n";
sysmessages[28] = "No tienes nada de eso.\n";
sysmessages[29] = "Ya tienes puest";
sysmessages[30] = "S";
sysmessages[31] = "N";
sysmessages[32] = "\n Pulsa una tecla…";
sysmessages[33] = ">";
sysmessages[34] = ".";
sysmessages[35] = "\n Pasa el tiempo…\n";
sysmessages[36] = "Coges¬";
sysmessages[37] = "Te pones¬";
sysmessages[38] = "Te quitas¬";
sysmessages[39] = "";
sysmessages[40] = "¿Estás intentando ponerte¬";
sysmessages[41] = "";
sysmessages[42] = "";
sysmessages[43] = "";
sysmessages[44] = "";
sysmessages[45] = "";
sysmessages[46] = ",¬";
sysmessages[47] = " y¬";
sysmessages[48] = ".\n";
sysmessages[49] = "No tienes¬";
sysmessages[50] = "No¬";
sysmessages[51] = ".\n";
sysmessages[52] = "No hay nada de eso en¬";
sysmessages[53] = "nada.\n";
sysmessages[54] = "Archivo no encontrado.";
sysmessages[55] = "Archivo corrupto.";
sysmessages[56] = "¡Error de lectura! ¡El archivo no se ha grabado!";
sysmessages[57] = "Directorio lleno.";
sysmessages[58] = "Disco lleno.";
sysmessages[59] = "Error en el nombre del archivo.";
sysmessages[60] = "Escribe el nombre del archivo.¬";
sysmessages[61] = "No sé como hacer eso. Intenta describirlo de otra forma.\n";
sysmessages[62] = "No te he entendido. Usa otras palabras.\n";
sysmessages[63] = "Planet of Death\n Versión 1.1 (05-09-09)\n Programa: Mastodon\n Beta testing: Akbarr y Baltasarq\n";
sysmessages[64] = "La salida del riachuelo forma un pequeño remolino no muy lejos de la orilla.\n";
sysmessages[65] = "Alguien desciende por una fosa con una cuerda.\n";
sysmessages[66] = "En algunos sitios tapizan las paredes como planchas de metal, en otros brotan como deformes protuberancias de la fría roca.\n";
sysmessages[67] = "Un gran espejo cuadrado tras un duro cristal, orientado formando un ángulo de 45 grados. Junto a él, hay un símbolo: un cuadrado y una flecha apuntando al interior.\n";
sysmessages[68] = "Un mapa ocupa gran parte de la pantalla. En la parte de arriba ves un complejo de cuevas. La zona inferior representa una zona de hangares o talleres.";
sysmessages[69] = " Un punto rojo parpadea en el lado derecho de los hangares.\n";
sysmessages[70] = "Un poco extrañas, pero está claro que son para ponérselas en los pies. Son bastante grandes y resistentes.\n";
sysmessages[71] = "Un enorme bloque de hielo, frío y deslizante.\n";
sysmessages[72] = "La fosa es profunda y oscura, no se ve el fondo.\n";
sysmessages[73] = "Un gran espejo cuadrado tras un duro cristal, orientado formando un ángulo de 45 grados. Junto a él, hay un extraño símbolo: un cuadrado y una flecha apuntando al interior.\n";
sysmessages[74] = "Es un espejo corriente de unos cincuenta centímetros de longitud.\n";
sysmessages[75] = "Una enorme y repugnante babosa. Al desplazarse deja como un rastro de asquerosa pulpa verde corrosiva.\n";
sysmessages[76] = "De aspecto antropomorfo, aunque no se podría calificar de humana. Aproximadamente 60 centímetros de longitud, piel verde y abotargada. Piernas cortas, brazos largos con uñas puntiagudas y cabeza achatada.\n";
sysmessages[77] = "Bueno, es bastante grande para lo que estás acostumbrado: unos 50 o 60 cm.\n";
sysmessages[78] = "Lleva un uniforme con máscara, al igual que el resto de operarios. Está dormido.\n";
sysmessages[79] = "Duerme el sueño de los justos.\n";
sysmessages[80] = "Junto a ella hay un orificio hexagonal. Está cerrada.\n";
sysmessages[81] = "Se utiliza para atontar o dejar sin sentido a criaturas humanoides.\n";
sysmessages[82] = "A través de la ventana ves algo parecido a unos talleres, con multitud de hombrecillos uniformados trabajando.\n";
sysmessages[83] = "Pequeña pero potente. En el frontal ves el compartimento del propulsor.\n";
sysmessages[84] = "Una pieza fundamental en la propulsión de tu nave.\n";
sysmessages[85] = "Le falta un barrote, suficiente para caer hacia abajo.\n";
sysmessages[86] = "Está lleno de un líquido verde, viscoso y borboteante.\n";
sysmessages[87] = "Tiene varios botones, en forma de flecha direccional: norte, sur, este, oeste, arriba y abajo.\n";
sysmessages[88] = "Es básicamente un cilindro hermético con ruedas.\n";
sysmessages[89] = "Similar a las babosas en forma, que no en tamaño. Es de un color algo más tirando al marrón. No parece muy ágil.\n";
sysmessages[90] = "Bastante grande y pesado.\n";
sysmessages[91] = "\n";
sysmessages[92] = ".\n";
sysmessages[93] = "En su interior hay varios componentes del propulsor,¬";
sysmessages[94] = "incluyendo el canalizador.\n";
sysmessages[95] = "pero falta el canalizador de plasma.\n";
sysmessages[96] = " Además, l";
sysmessages[97] = "Actualmente no tienes nada.";
sysmessages[98] = "Dejas¬";
sysmessages[99] = "No puedes¬";
sysmessages[100] = "No hay nada que puedas¬";
sysmessages[101] = ", está";
sysmessages[102] = " fijad";
sysmessages[103] = " al sitio.\n";
sysmessages[104] = "con ese nombre. Quizás forme parte del escenario y no sea importante.\n";
sysmessages[105] = "Sólo puedes usar 'TODO' con los verbos COGER y DEJAR.\n";
sysmessages[106] = " llevas puest";
sysmessages[107] = "coger¬";
sysmessages[108] = "No observas nada que te llame la atención";
sysmessages[109] = "No le ves nada que te llame la atención¬";
sysmessages[110] = "abrir¬";
sysmessages[111] = "Abres¬";
sysmessages[112] = "cerrar¬";
sysmessages[113] = "Cierras¬";
sysmessages[114] = "Ya está";
sysmessages[115] = " abiert";
sysmessages[116] = " cerrad";
sysmessages[117] = "meter¬";
sysmessages[118] = "sacar¬";
sysmessages[119] = "cosas en¬";
sysmessages[120] = " mientras esté";
sysmessages[121] = "cosas¬";
sysmessages[122] = "No veo qué quieres sacar ni de dónde.\n";
sysmessages[123] = "dar¬";
sysmessages[124] = " a¬";
sysmessages[125] = ", pertenece¬";
sysmessages[126] = " no parece interesar¬";
sysmessages[127] = "No veo a quién quieres darle¬";
sysmessages[128] = "mostrar¬";
sysmessages[129] = "No veo a quién quieres mostrar¬";
sysmessages[130] = ", forma";
sysmessages[131] = " parte¬";
sysmessages[132] = "No veo dónde quieres meter¬";
sysmessages[133] = "No veo de dónde quieres sacar¬";
sysmessages[134] = "No puedes meter cosas en¬";
sysmessages[135] = "Pones¬";
sysmessages[136] = " en¬";
sysmessages[137] = "Sacas¬";
sysmessages[138] = "En¬";
sysmessages[139] = " hay¬";
sysmessages[140] = " no hay nada.\n";
sysmessages[141] = "No hay nada.\n";
sysmessages[142] = "Dudo que obtengas su colaboración.\n";
sysmessages[143] = " no parece tener nada de eso.\n";
sysmessages[144] = "Es propiedad de¬";
sysmessages[145] = "Nunca ha sido tuy";
sysmessages[146] = " está";
sysmessages[147] = ", y en su interior ves¬";
sysmessages[148] = ", pero está vacío.\n";
sysmessages[149] = "_:¬";
sysmessages[150] = "Juego guardado en memoria.";
sysmessages[151] = ".¬";
sysmessages[152] = "o";
sysmessages[153] = "a";
sysmessages[154] = "os";
sysmessages[155] = "as";
sysmessages[156] = "n";
sysmessages[157] = "el";
sysmessages[158] = "l";
sysmessages[159] = "El";
sysmessages[160] = "L";
sysmessages[161] = "un";
sysmessages[162] = "le";
sysmessages[163] = "s";
sysmessages[164] = "¬";
sysmessages[165] = "_";
sysmessages[166] = " _";
sysmessages[167] = "d";
sysmessages[168] = "e¬";
sysmessages[169] = ",¬";
sysmessages[170] = " _?\n";
sysmessages[171] = " en¬";
sysmessages[172] = "_.\n";
sysmessages[173] = " _.\n";
sysmessages[174] = "de¬";
sysmessages[175] = " _,¬";
sysmessages[176] = " _,";
sysmessages[177] = "Salidas visibles:¬";
sysmessages[178] = ".";
sysmessages[179] = "o¬";
sysmessages[180] = "a¬";
sysmessages[181] = "os¬";
sysmessages[182] = "as¬";

// USER MESSAGES

total_messages=254;

messages = [];

messages[0] = "";
messages[1] = "";
messages[2] = "";
messages[3] = "";
messages[4] = "";
messages[5] = "";
messages[6] = "";
messages[7] = "";
messages[8] = "";
messages[9] = "";
messages[10] = "";
messages[11] = "";
messages[12] = "";
messages[13] = "";
messages[14] = "";
messages[15] = "";
messages[16] = "";
messages[17] = "";
messages[18] = "";
messages[19] = "";
messages[20] = "";
messages[21] = "";
messages[22] = "";
messages[23] = "";
messages[24] = "";
messages[25] = "";
messages[26] = "";
messages[27] = "";
messages[28] = "";
messages[29] = "";
messages[30] = "";
messages[31] = "";
messages[32] = "";
messages[33] = "";
messages[34] = "";
messages[35] = "";
messages[36] = "";
messages[37] = "";
messages[38] = "";
messages[39] = "";
messages[40] = "";
messages[41] = "";
messages[42] = "";
messages[43] = "";
messages[44] = "";
messages[45] = "";
messages[46] = "";
messages[47] = "";
messages[48] = "";
messages[49] = "";
messages[50] = "";
messages[51] = "";
messages[52] = "";
messages[53] = "";
messages[54] = "";
messages[55] = "";
messages[56] = "";
messages[57] = "";
messages[58] = "";
messages[59] = "";
messages[60] = "";
messages[61] = "";
messages[62] = "";
messages[63] = "";
messages[64] = "";
messages[65] = "";
messages[66] = "";
messages[67] = "";
messages[68] = "";
messages[69] = "";
messages[70] = "";
messages[71] = "";
messages[72] = "";
messages[73] = "";
messages[74] = "";
messages[75] = "";
messages[76] = "";
messages[77] = "";
messages[78] = "";
messages[79] = "";
messages[80] = "";
messages[81] = "";
messages[82] = "";
messages[83] = "";
messages[84] = "";
messages[85] = "";
messages[86] = "";
messages[87] = "";
messages[88] = "";
messages[89] = "";
messages[90] = "";
messages[91] = "";
messages[92] = "";
messages[93] = "";
messages[94] = "";
messages[95] = "";
messages[96] = "";
messages[97] = "";
messages[98] = "";
messages[99] = "";
messages[100] = "";
messages[101] = "";
messages[102] = "";
messages[103] = "";
messages[104] = "";
messages[105] = "";
messages[106] = "";
messages[107] = "";
messages[108] = "";
messages[109] = "";
messages[110] = "";
messages[111] = "";
messages[112] = "";
messages[113] = "";
messages[114] = "";
messages[115] = "";
messages[116] = "";
messages[117] = "";
messages[118] = "";
messages[119] = "";
messages[120] = "";
messages[121] = "";
messages[122] = "";
messages[123] = "";
messages[124] = "";
messages[125] = "";
messages[126] = "";
messages[127] = "";
messages[128] = "";
messages[129] = "";
messages[130] = "";
messages[131] = "";
messages[132] = "";
messages[133] = "";
messages[134] = "";
messages[135] = "";
messages[136] = "";
messages[137] = "";
messages[138] = "";
messages[139] = "";
messages[140] = "";
messages[141] = "";
messages[142] = "";
messages[143] = "";
messages[144] = "";
messages[145] = "";
messages[146] = "";
messages[147] = "";
messages[148] = "";
messages[149] = "";
messages[150] = "";
messages[151] = "";
messages[152] = "";
messages[153] = "";
messages[154] = "";
messages[155] = "";
messages[156] = "";
messages[157] = "";
messages[158] = "";
messages[159] = "";
messages[160] = "";
messages[161] = "";
messages[162] = "";
messages[163] = "";
messages[164] = "";
messages[165] = "";
messages[166] = "";
messages[167] = "";
messages[168] = "";
messages[169] = "";
messages[170] = "";
messages[171] = "";
messages[172] = "";
messages[173] = "";
messages[174] = "";
messages[175] = "";
messages[176] = "";
messages[177] = "";
messages[178] = "";
messages[179] = "";
messages[180] = "";
messages[181] = "";
messages[182] = "";
messages[183] = "";
messages[184] = "";
messages[185] = "";
messages[186] = "";
messages[187] = "";
messages[188] = "";
messages[189] = "";
messages[190] = "";
messages[191] = "";
messages[192] = "";
messages[193] = "";
messages[194] = "";
messages[195] = "";
messages[196] = "";
messages[197] = "";
messages[198] = "";
messages[199] = "";
messages[200] = "";
messages[201] = "";
messages[202] = "";
messages[203] = "";
messages[204] = "";
messages[205] = "";
messages[206] = "";
messages[207] = "";
messages[208] = "";
messages[209] = "";
messages[210] = "";
messages[211] = "";
messages[212] = "";
messages[213] = "";
messages[214] = "";
messages[215] = "";
messages[216] = "";
messages[217] = "";
messages[218] = "";
messages[219] = "";
messages[220] = "";
messages[221] = "";
messages[222] = "";
messages[223] = "";
messages[224] = "";
messages[225] = "";
messages[226] = "";
messages[227] = "";
messages[228] = "";
messages[229] = "";
messages[230] = "";
messages[231] = "";
messages[232] = "";
messages[233] = "";
messages[234] = "";
messages[235] = "";
messages[236] = "";
messages[237] = "";
messages[238] = "";
messages[239] = "";
messages[240] = "";
messages[241] = "";
messages[242] = "";
messages[243] = "";
messages[244] = "";
messages[245] = "";
messages[246] = "";
messages[247] = "";
messages[248] = "";
messages[249] = "";
messages[250] = "";
messages[251] = "";
messages[252] = "";
messages[253] = "";

// WRITE MESSAGES

total_writemessages=565;

writemessages = [];

writemessages[0] = "Está flotando sobre el robot.";
writemessages[1] = "No te molestes, los dos estáis en una situación comprometida.";
writemessages[2] = "Está flotando sobre el robot.";
writemessages[3] = "¿A cuál te refieres?";
writemessages[4] = "¿A cuál te refieres?";
writemessages[5] = "El agua está helada, y el suelo resbaloso.";
writemessages[6] = "Te adentras unos pasos en el lago, hasta llegar al remolino que forma el riachuelo…";
writemessages[7] = "Te diriges al terraplen, pero al poner pie en él resbalas por la presencia de placas de hielo. El descenso no es menos doloroso al arrastrarte entre trozos afilados de hielo partido.";
writemessages[8] = "¡Plong! Al llegar a la altura de los orificios te topas con un impenetrable campo de fuerza que te impide avanzar.";
writemessages[9] = "No puedes pasar con el tablón, la cavidad es muy estrecha.";
writemessages[10] = "Atraviesas la cavidad, que da un repentino giro hacia el ";
writemessages[11] = "sur.";
writemessages[12] = "El camino es escarpado y sinuoso, apareces mirando hacia el ";
writemessages[13] = "nordeste.";
writemessages[14] = "El riachuelo es demasiado profundo y caudaloso para cruzarlo.";
writemessages[15] = "El camino es escarpado y sinuoso, apareces mirando hacia el ";
writemessages[16] = "este.";
writemessages[17] = "Trepas un rato por una de las lianas, pero al ver que el camino hasta arriba es más largo de lo que parece desistes.";
writemessages[18] = "Trepas un rato por una de las lianas, pero al ver que el camino hasta arriba es más largo de lo que parece desistes.";
writemessages[19] = "El camino es escarpado y sinuoso, apareces mirando hacia el ";
writemessages[20] = "nordeste.";
writemessages[21] = "Deberías atarla a algo consistente antes de escalar.";
writemessages[22] = "El camino es escarpado y sinuoso, apareces mirando hacia el ";
writemessages[23] = "este.";
writemessages[24] = "La fosa es demasiado profunda para tirarte por ella.";
writemessages[25] = "O el tablón o tú, los dos no cabeis por la fosa.";
writemessages[26] = "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo.";
writemessages[27] = "Te sueltas de la liana y caes en ";
writemessages[28] = "Caes rodando por los restos de basura…";
writemessages[29] = "¡La nave está en posición de lanzamiento, no puedes bajar!";
writemessages[30] = "Deberías atarla a algo consistente antes de escalar.";
writemessages[31] = "El agua está helada, y el suelo resbaloso.";
writemessages[32] = "Te adentras unos pasos en el lago, hasta llegar al remolino que forma el riachuelo…";
writemessages[33] = "El mecanismo de absorción se activa y una fuerza irresistible te chupa al interior. Apareces en…";
writemessages[34] = "Está atascado.";
writemessages[35] = "La fosa es demasiado profunda para tirarte por ella.";
writemessages[36] = "O el tablón o tú, los dos no cabeis por la fosa.";
writemessages[37] = "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo.";
writemessages[38] = "Caes rodando por los restos de basura…";
writemessages[39] = "¡La nave está en posición de lanzamiento, no puedes bajar!";
writemessages[40] = "Te topas con la montaña. El camino hacia la meseta está al oeste.";
writemessages[41] = "No puedes pasar con el tablón, la cavidad es muy estrecha.";
writemessages[42] = "Atraviesas la cavidad, que da un repentino giro hacia el ";
writemessages[43] = "oeste.";
writemessages[44] = "Tiras con fuerza de uno de los tablones hasta desprenderlo del suelo.";
writemessages[45] = "No puedes arrancar más.";
writemessages[46] = "Al acercarte al bicho, te salpica con sus pútridos líquidos corrosivos que te penetran y despedazan en cuestión de segundos.";
writemessages[47] = "No está lo suficientemente suelto. Es necesario un método más contundente.";
writemessages[48] = "Ya has tenido una experiencia con ese tema, no necesitas más.";
writemessages[49] = "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor.";
writemessages[50] = "Déjalo que se recupere.";
writemessages[51] = "Tiras de la palanca. ";
writemessages[52] = "Oyes un 'clong' seco dentro del cuadro, y luego silencio. ";
writemessages[53] = "La palanca vuelve lentamente a su posición inicial.";
writemessages[54] = "Tiras de la palanca. ";
writemessages[55] = "La palanca vuelve lentamente a su posición inicial.";
writemessages[56] = "Tiras de la palanca. ";
writemessages[57] = "En algún lugar hacia el este oyes algo parecido a la apertura de una esclusa y el inconfudible sonido de líquido fluyendo. ";
writemessages[58] = "La palanca vuelve lentamente a su posición inicial.";
writemessages[59] = "Sería una pérdida segura.";
writemessages[60] = "Sería una pérdida segura.";
writemessages[61] = "Eso sería garantía de pérdida. La babosa es muy corrosiva.";
writemessages[62] = "No le ves mucho sentido ahora a tirar ";
writemessages[63] = ".\n";
writemessages[64] = "No lo ves como una opción adecuada en este momento.";
writemessages[65] = "Sería una pérdida segura.";
writemessages[66] = "Sería una pérdida segura.";
writemessages[67] = "Eso sería garantía de pérdida. La babosa es muy corrosiva.";
writemessages[68] = "El riachuelo es demasiado profundo y caudaloso para cruzarlo.";
writemessages[69] = "No lo ves como una opción adecuada en este momento.";
writemessages[70] = "El riachuelo es demasiado profundo y caudaloso para cruzarlo.";
writemessages[71] = "El riachuelo es demasiado profundo y caudaloso para cruzarlo.";
writemessages[72] = "Te sueltas de la liana y caes en ";
writemessages[73] = "Saltas en el sitio. Fútil.";
writemessages[74] = "No parece que esa opción violenta sea la solución en este momento.";
writemessages[75] = "Tiras con fuerza de uno de los tablones hasta desprenderlo del suelo.";
writemessages[76] = "No puedes arrancar más.";
writemessages[77] = "¿Así, con las manos?";
writemessages[78] = "Está fuera de tus posibilidades.";
writemessages[79] = "Podrías romper el espejo, y parece un objeto muy valioso aquí.";
writemessages[80] = "Fútil.";
writemessages[81] = "No está lo suficientemente suelto. Es necesario un método más contundente.";
writemessages[82] = "Ya has tenido una experiencia con ese tema, no necesitas más.";
writemessages[83] = "No parece que esa opción violenta sea la solución en este momento.";
writemessages[84] = "Cortas una de las lianas con el pedernal, y cae al suelo formando una útil cuerda.";
writemessages[85] = "No necesitas podar el bosque entero.";
writemessages[86] = "Las lianas son bastante resistentes, necesitas el material adecuado para cortarlas.";
writemessages[87] = "No le ves sentido a cortar ";
writemessages[88] = " en este momento.";
writemessages[89] = "No le ves sentido a cortar eso en este momento.";
writemessages[90] = "Le das un suave empujoncito al bloque de hielo, que se desliza por la pendiente perdiéndose de vista al instante.";
writemessages[91] = "Son enormes bloques, y están incrustados en la roca.";
writemessages[92] = "Ya ocupa todo el cubículo de abajo, no entra más.";
writemessages[93] = "Al poner la mano sobre la criatura, se ";
writemessages[94] = "despierta repentinamente y dando un ágil salto se cuelga de tu hombro. Instantes después notas sus largas manos y afiladas uñas cerrarse en torno a tu cuello. Pierdes la conciencia al cabo de poco rato.";
writemessages[95] = "Al acercarte al bicho, te salpica con sus pútridos líquidos corrosivos que te penetran y despedazan en cuestión de segundos.";
writemessages[96] = "Fútil.";
writemessages[97] = "No está lo suficientemente suelto. Es necesario un método más contundente.";
writemessages[98] = "Ya has tenido una experiencia con ese tema, no necesitas más.";
writemessages[99] = "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor.";
writemessages[100] = "Déjalo que se recupere.";
writemessages[101] = "No cede, sólo permite tirar de ella.";
writemessages[102] = "No le ves sentido a empujar ";
writemessages[103] = ".\n";
writemessages[104] = "No produce ningún efecto.";
writemessages[105] = " Los sistemas de la nave rugen y una cuenta atrás comienza su andadura. 3..2..1…\n\n Abandonas el planeta ante la sorpresa de los alienígenas, que apenas tienen tiempo de activar sus sistemas defensivos para evitar tu huida.\n\n";
writemessages[106] = " De vuelta a la nave nodriza tus compañeros escuchan tu histora con estupor. Te has ganado unas vacaciones, allá en la Tierra.\n\n Pero parece que podrían durar poco. Dadas tus dotes de supervivencia te han propuesto una arriesgada misión en cierto templo Inca…\n\n Pero esa es otra historia.\n\n";
writemessages[107] = "No has iniciado la secuencia de lanzamiento.";
writemessages[108] = "No le ves sentido a empujar ";
writemessages[109] = ".\n";
writemessages[110] = "No produce ningún efecto.";
writemessages[111] = "Subirte a eso no parece muy adecuado en este momento.";
writemessages[112] = "Trepas un rato por una de las lianas, pero al ver que el camino hasta arriba es más largo de lo que parece desistes.";
writemessages[113] = "Trepas un rato por una de las lianas, pero al ver que el camino hasta arriba es más largo de lo que parece desistes.";
writemessages[114] = "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo.";
writemessages[115] = "Deberías atarla a algo consistente antes de escalar.";
writemessages[116] = "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo.";
writemessages[117] = "Deberías atarla a algo consistente antes de escalar.";
writemessages[118] = "La fosa es demasiado profunda para tirarte por ella.";
writemessages[119] = "O el tablón o tú, los dos no cabeis por la fosa.";
writemessages[120] = "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo.";
writemessages[121] = "No le ves sentido a trepar por ";
writemessages[122] = " en este momento.";
writemessages[123] = "No le ves sentido a trepar por ";
writemessages[124] = "nada";
writemessages[125] = " en este momento.";
writemessages[126] = "No le ves sentido a atar ";
writemessages[127] = ".\n";
writemessages[128] = "No le ves sentido a atar ";
writemessages[129] = "eso.";
writemessages[130] = "Desatas la liana.";
writemessages[131] = "No has atado ";
writemessages[132] = " a nada.";
writemessages[133] = "No ves nada atado.";
writemessages[134] = "No le ves un uso práctico a balancear ";
writemessages[135] = " en este momento.";
writemessages[136] = "No le ves sentido a balancear eso en este momento.";
writemessages[137] = "Al poner la mano sobre la criatura, se ";
writemessages[138] = "despierta repentinamente y dando un ágil salto se cuelga de tu hombro. Instantes después notas sus largas manos y afiladas uñas cerrarse en torno a tu cuello. Pierdes la conciencia al cabo de poco rato.";
writemessages[139] = "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor.";
writemessages[140] = "Déjalo que se recupere.";
writemessages[141] = "Despertar ";
writemessages[142] = " es una idea ridícula.";
writemessages[143] = "No es muy coherente despertar a eso.";
writemessages[144] = "Lanzas una ráfaga a la babosa. La babosa se desintegra por toda la estancia enbadurnándote con sus ácidos fluidos.";
writemessages[145] = "Fútil.";
writemessages[146] = "Lanzas una discreta ráfaga neutralizadora al pobre individuo. Ahora dormirá un buen rato.";
writemessages[147] = "Eso sería ensañamiento.";
writemessages[148] = "Está fuera de tus posibilidades.";
writemessages[149] = "Podrías romper el espejo, y parece un objeto muy valioso aquí.";
writemessages[150] = "No veo muy lógico intentar matar ";
writemessages[151] = ".\n";
writemessages[152] = "No es muy adecuado ponerse a matar ahora mismo.";
writemessages[153] = "No encaja.";
writemessages[154] = "Tiras de la palanca. ";
writemessages[155] = "Oyes un 'clong' seco dentro del cuadro, y luego silencio. ";
writemessages[156] = "La palanca vuelve lentamente a su posición inicial.";
writemessages[157] = "Tiras de la palanca. ";
writemessages[158] = "La palanca vuelve lentamente a su posición inicial.";
writemessages[159] = "Tiras de la palanca. ";
writemessages[160] = "En algún lugar hacia el este oyes algo parecido a la apertura de una esclusa y el inconfudible sonido de líquido fluyendo. ";
writemessages[161] = "La palanca vuelve lentamente a su posición inicial.";
writemessages[162] = "No produce ningún efecto.";
writemessages[163] = "Esperas…";
writemessages[164] = "No es necesario, es de apertura automática.";
writemessages[165] = "No es necesario, es de apertura automática.";
writemessages[166] = "No es necesario, es de apertura automática.";
writemessages[167] = "No parece abrirse del modo tradicional.";
writemessages[168] = "Está cerrada.";
writemessages[169] = "_: ";
writemessages[170] = "Te sueltas de la liana y caes en ";
writemessages[171] = "No parece adecuado dejar el recipiente aquí.";
writemessages[172] = "Sueltas ";
writemessages[173] = ", y ves como ";
writemessages[174] = " engulle el remolino.";
writemessages[175] = "Juego guardado en memoria.";
writemessages[176] = "_: ";
writemessages[177] = "Las lianas cuelgan de los árboles, no puedes cogerlas sin más.";
writemessages[178] = "Las lianas cuelgan de los árboles, no puedes cogerlas sin más.";
writemessages[179] = "Desatas la liana.";
writemessages[180] = "Tiras con fuerza de uno de los tablones hasta desprenderlo del suelo.";
writemessages[181] = "No puedes arrancar más.";
writemessages[182] = "La mayoría está en muy malas condiciones y no sabes ni para qué sirven.";
writemessages[183] = "Es demasiado pesado.";
writemessages[184] = "Son enormes bloques, y están incrustados en la roca.";
writemessages[185] = "Está fuera de tus posibilidades.";
writemessages[186] = "Está atascado.";
writemessages[187] = "Coges el espejo alegremente y la criatura cae rodando del otro lado. Se ";
writemessages[188] = "despierta repentinamente y dando un ágil salto se cuelga de tu hombro. Instantes después notas sus largas manos y afiladas uñas cerrarse en torno a tu cuello. Pierdes la conciencia al cabo de poco rato.";
writemessages[189] = "Al poner la mano sobre la criatura, se ";
writemessages[190] = "despierta repentinamente y dando un ágil salto se cuelga de tu hombro. Instantes después notas sus largas manos y afiladas uñas cerrarse en torno a tu cuello. Pierdes la conciencia al cabo de poco rato.";
writemessages[191] = "No vas a tocar eso.";
writemessages[192] = "Al acercarte al bicho, te salpica con sus pútridos líquidos corrosivos que te penetran y despedazan en cuestión de segundos.";
writemessages[193] = "No está lo suficientemente suelto. Es necesario un método más contundente.";
writemessages[194] = "Ya has tenido una experiencia con ese tema, no necesitas más.";
writemessages[195] = "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor.";
writemessages[196] = "Déjalo que se recupere.";
writemessages[197] = "Le quitas el uniforme al técnico, arrojando al pobre diablo en una de las naves aparcadas para que no llame la atención.";
writemessages[198] = "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor.";
writemessages[199] = "Eso no sería una acción muy inteligente, aquí dentro del lago.";
writemessages[200] = "Está atascado.";
writemessages[201] = "No parece viable con este recipiente en tus manos.";
writemessages[202] = "Le quitas el uniforme al técnico, arrojando al pobre diablo en una de las naves aparcadas para que no llame la atención.";
writemessages[203] = "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor.";
writemessages[204] = "La salida del riachuelo forma un pequeño remolino no muy lejos de la orilla.\n";
writemessages[205] = "Su estado de abandono ha dejado varios asideros entre los tablones.";
writemessages[206] = "Su estado de abandono ha dejado varios asideros entre los tablones.";
writemessages[207] = "Alguien desciende por una fosa con una cuerda.\n";
writemessages[208] = "En algunos sitios tapizan las paredes como planchas de metal, en otros brotan como deformes protuberancias de la fría roca.\n";
writemessages[209] = "Está atascado.";
writemessages[210] = "Un gran espejo cuadrado tras un duro cristal, orientado formando un ángulo de 45 grados. Junto a él, hay un símbolo: un cuadrado y una flecha apuntando al interior.\n";
writemessages[211] = "Un mapa ocupa gran parte de la pantalla. En la parte de arriba ves un complejo de cuevas. La zona inferior representa una zona de hangares o talleres.";
writemessages[212] = " Un punto rojo parpadea en el lado derecho de los hangares.\n";
writemessages[213] = "El succionador es una gran cavidad en una de las paredes";
writemessages[214] = ", actualmente bloqueado por un enorme gusano.";
writemessages[215] = " …oh..ooohhh… ";
writemessages[216] = "El mecanismo de absorción se activa y una fuerza irresistible te chupa al interior. Apareces en…";
writemessages[217] = "No encaja.";
writemessages[218] = "No encaja.";
writemessages[219] = "Eso no sería una acción muy inteligente, aquí dentro del lago.";
writemessages[220] = "No parece viable con este recipiente en tus manos.";
writemessages[221] = "No parece que esa opción violenta sea la solución en este momento.";
writemessages[222] = "¿Así, con las manos?";
writemessages[223] = "No parece que esa opción violenta sea la solución en este momento.";
writemessages[224] = "Está durmiendo.";
writemessages[225] = "La babosa hace un gesto de desdén al mostrarle es espejo.";
writemessages[226] = "La babosa no está interesada.";
writemessages[227] = "Está durmiendo.";
writemessages[228] = "La babosa hace un gesto de desdén al mostrarle es espejo.";
writemessages[229] = "La babosa no está interesada.";
writemessages[230] = "Echas un poco del pútrido líquido sobre el mecanismo. El dispositivo se disuelve.";
writemessages[231] = "Ya está bastante retorcido.";
writemessages[232] = "Eso lo desintegraría completamente, y el barrote puede serte útil.";
writemessages[233] = "Eso lo desintegraría completamente, y el barrote puede serte útil.";
writemessages[234] = "No lo ves apropiado en este momento.";
writemessages[235] = "No le ves sentido ahora a derramar ";
writemessages[236] = ".\n";
writemessages[237] = "No le ves mucho sentido ahora a derramar eso.";
writemessages[238] = "<i><b>Planet of Death</b></i>\nVersión 1.1 (05-09-09)\n<b>Programación</b>: Mastodon\n<i><b>Beta testing:</i></b> Akbarr y Baltasarq\n";
writemessages[239] = "\nCaverna verde\n Estás en una estrecha caverna laberíntica, iluminada débilmente por un suave resplandor verde que emana de las paredes y el suelo.";
writemessages[240] = " La salida del ";
writemessages[241] = "este";
writemessages[242] = " tiene un espejo encima con un extraño símbolo.";
writemessages[243] = " La salida del ";
writemessages[244] = "oeste";
writemessages[245] = " tiene un espejo encima con un extraño símbolo.";
writemessages[246] = " La salida del ";
writemessages[247] = "sur";
writemessages[248] = " tiene un espejo encima con un extraño símbolo.";
writemessages[249] = " Hacia el norte, la caverna asciende en una escarpada pendiente, imposible de escalar.";
writemessages[250] = ", salvo ese enorme gusano que reposa atontado bajo tus pies";
writemessages[251] = ".";
writemessages[252] = ". Al fondo de la estancia hay ";
writemessages[253] = "un enorme gusano";
writemessages[254] = " comiendo compulsivamente de un recipiente con un líquido gelatinoso";
writemessages[255] = ". Al fondo de la estancia hay ";
writemessages[256] = "un recipiente con un líquido gelatinoso";
writemessages[257] = ".";
writemessages[258] = "La criatura verde está durmiendo plácidamente, ";
writemessages[259] = "sobre el espejo. ";
writemessages[260] = "roncando como un colegial. ";
writemessages[261] = "La criatura verde está totalmente aterrorizada, sus ojos de par en par. ";
writemessages[262] = " Una liana atada a una de las piedras se descuelga por la fosa.";
writemessages[263] = "\n";
writemessages[264] = "Hay un enorme gusano atascado entre el succionador y la sala de basuras de abajo. ";
writemessages[265] = "El dispositivo está totalmente destruido.";
writemessages[266] = "Aquí puedes ver tu nave. ";
writemessages[267] = "Junto a ella hay un técnico de mantenimiento ";
writemessages[268] = "durmiendo plácidamente. ";
writemessages[269] = "Junto a ella hay un técnico de mantenimiento ";
writemessages[270] = "grogui en el suelo. ";
writemessages[271] = "Al verse libre la criatura huye despavorida entre los restos de basura hasta desaparecer de tu vista. ";
writemessages[272] = "El tablón queda atravesado entre las orillas del riachuelo.";
writemessages[273] = "La babosa se te sube y te rocía con todos sus fluidos putrefactos y corruptores.";
writemessages[274] = "La babosa llega hasta casi tocarte, pero de repente se da la vuelta y se aleja.";
writemessages[275] = "Algunos de los hombrecillos empiezan a extrañarse por tu presencia y te lanzan miradas suspicaces.";
writemessages[276] = "Un grupo de hombrecillos armados te aborda y, despues de mirarte extrañados, y luego mirarse entre ellos extrañados, te llevan a…";
writemessages[277] = "Varios operarios detectan tu presencia, extraña para ellos, y se arremolinan a tu alrededor. Después de un rato en el que no parecen llegar a una conclusión definitiva sobre tu naturaleza, te llevan a…";
writemessages[278] = ".\n";
writemessages[279] = " _.\n";
writemessages[280] = ".\n";
writemessages[281] = "el";
writemessages[282] = "l";
writemessages[283] = "El";
writemessages[284] = "L";
writemessages[285] = "un";
writemessages[286] = "Atas la liana a una de las piedras y la dejas caer por la fosa.";
writemessages[287] = "Ya está atada.";
writemessages[288] = "La fosa no tiene asideros para atar la liana.";
writemessages[289] = "No tienes nada de eso.";
writemessages[290] = "No le ves sentido en este momento a atar la liana a eso.";
writemessages[291] = "Tienes que atar la liana a algo.";
writemessages[292] = "La tarjeta encaja en el orificio, y la puerta se abre con un siseo. La atraviesas y recoges la tarjeta…";
writemessages[293] = "La babosa se dirige hacia la criatura, ";
writemessages[294] = "pero al acercarse al espejo se da la vuelta y continua reptando por la estancia.";
writemessages[295] = " se sube a ella y en cuestión de segundos la desintegra con sus ácidos fluidos. Ahora sólo queda un esqueleto putrefacto y viscoso.";
writemessages[296] = "La babosa intenta reptar hasta la salida, pero al llegar al espejo cambia repentinamente de dirección.";
writemessages[297] = "La babosa se dirige hacia tí.";
writemessages[298] = "La babosa repta hacia la salida del ";
writemessages[299] = "Una babosa llega reptando desde el ";
writemessages[300] = ", pero antes de llegar, algo la hace darse la vuelta.";
writemessages[301] = ".\n";
writemessages[302] = "este";
writemessages[303] = "norte";
writemessages[304] = "sur";
writemessages[305] = "sureste";
writemessages[306] = "noroeste";
writemessages[307] = "oeste";
writemessages[308] = "sur";
writemessages[309] = "norte";
writemessages[310] = "este";
writemessages[311] = "oeste";
writemessages[312] = "El balanceo cesa completamente y quedas suspendido sobre el terraplén.";
writemessages[313] = "El balanceo pierde fuerza y se queda en un suave vaivén sobre el terraplén.";
writemessages[314] = "Te empiezas a columpiar en la cuerda imprimiéndole un suave movimiento de vaivén.";
writemessages[315] = "Te das un nuevo impulso y ahora te balanceas sobre un ángulo más amplio.";
writemessages[316] = "Te impulsas un poco más, aunque la longitud de la liana no permite ampliar el ángulo de balanceo.";
writemessages[317] = ".\n";
writemessages[318] = " _.\n";
writemessages[319] = ".\n";
writemessages[320] = "Coges el recipiente.";
writemessages[321] = "El gusano podría ser un inconveniente.";
writemessages[322] = "Chorrea líquido viscoso y corrosivo, te abrasarías.";
writemessages[323] = "Coges el recipiente.";
writemessages[324] = "No parece viable con este recipiente en tus manos.";
writemessages[325] = ".\n";
writemessages[326] = ".\n";
writemessages[327] = ".\n";
writemessages[328] = ".\n";
writemessages[329] = ".\n";
writemessages[330] = ".\n";
writemessages[331] = " _, ";
writemessages[332] = ".\n";
writemessages[333] = " _,";
writemessages[334] = ".\n";
writemessages[335] = " _";
writemessages[336] = "_.\n";
writemessages[337] = "_.\n";
writemessages[338] = ".\n";
writemessages[339] = ".\n";
writemessages[340] = ", ";
writemessages[341] = ".\n";
writemessages[342] = ".\n";
writemessages[343] = "En su interior hay varios componentes del propulsor, ";
writemessages[344] = "pero falta el canalizador de plasma.\n";
writemessages[345] = ". ";
writemessages[346] = ".\n";
writemessages[347] = " Te encuentras en suspensión";
writemessages[348] = " Estás balanceándote enérgicamente";
writemessages[349] = ", colgando de una liana, ";
writemessages[350] = "y balanceándote suavemente ";
writemessages[351] = "sobre un peligroso terraplén cubierto de afilados fragmentos de hielo incrustado.";
writemessages[352] = "y a tus pies ves una tranquila caverna de color verdusco.";
writemessages[353] = "sobre una fría gruta con grandes bloques de hielo.";
writemessages[354] = "a la altura del peligroso terraplén de afilados trozos de hielo.";
writemessages[355] = "La descarga no produce ningún efecto. No parece el tipo de objetivo adecuado.";
writemessages[356] = "La descarga no produce ningún efecto. No parece el tipo de objetivo adecuado.";
writemessages[357] = "No le ves utilidad a disparar";
writemessages[358] = " con la pistola ";
writemessages[359] = " en este momento.";
writemessages[360] = "No tienes con qué dispararle ";
writemessages[361] = " en este momento.";
writemessages[362] = "No le ves utilidad a disparar";
writemessages[363] = " en este momento.";
writemessages[364] = "No le ves utilidad a disparar";
writemessages[365] = " a eso";
writemessages[366] = " en este momento.";
writemessages[367] = "Disparar ¿Con qué?";
writemessages[368] = "La fosa es profunda y oscura, no se ve el fondo.\n";
writemessages[369] = "Bastante grande y pesado.\n";
writemessages[370] = "Un poco extrañas, pero está claro que son para ponérselas en los pies. Son bastante grandes y resistentes.\n";
writemessages[371] = "Un poco extrañas, pero está claro que son para ponérselas en los pies. Son bastante grandes y resistentes.\n";
writemessages[372] = "Se utiliza para atontar o dejar sin sentido a criaturas humanoides.\n";
writemessages[373] = "Un enorme bloque de hielo, frío y deslizante.\n";
writemessages[374] = "Similar a las babosas en forma, que no en tamaño. Es de un color algo más tirando al marrón. No parece muy ágil.\n";
writemessages[375] = "Está lleno de un líquido verde, viscoso y borboteante.\n";
writemessages[376] = "Un gran espejo cuadrado tras un duro cristal, orientado formando un ángulo de 45 grados. Junto a él, hay un extraño símbolo: un cuadrado y una flecha apuntando al interior.\n";
writemessages[377] = "Es un espejo corriente de unos cincuenta centímetros de longitud.\n";
writemessages[378] = "De aspecto antropomorfo, aunque no se podría calificar de humana. Aproximadamente 60 centímetros de longitud, piel verde y abotargada. Piernas cortas, brazos largos con uñas puntiagudas y cabeza achatada.\n";
writemessages[379] = "Una enorme y repugnante babosa. Al desplazarse deja como un rastro de asquerosa pulpa verde corrosiva.\n";
writemessages[380] = "Junto a ella hay un orificio hexagonal. Está cerrada.\n";
writemessages[381] = "Tiene varios botones, en forma de flecha direccional: norte, sur, este, oeste, arriba y abajo.\n";
writemessages[382] = "Es básicamente un cilindro hermético con ruedas.\n";
writemessages[383] = "Una especie de respiradero para el basurero de abajo, a bastante altura. ";
writemessages[384] = "Está hecha de barrotes muy separados, casi cabes entre ellos. Hay uno un poco suelto.";
writemessages[385] = "Le falta un barrote, suficiente para caer hacia abajo.\n";
writemessages[386] = "En forma de cubo negro, con extraños símbolos y una serie de luces, todas de color rojo.";
writemessages[387] = "Un amasijo de hierros retorcidos.";
writemessages[388] = "Pequeña pero potente. En el frontal ves el compartimento del propulsor.\n";
writemessages[389] = "Lleva un uniforme con máscara, al igual que el resto de operarios. Está dormido.\n";
writemessages[390] = "Duerme el sueño de los justos.\n";
writemessages[391] = "Bueno, es bastante grande para lo que estás acostumbrado: unos 50 o 60 cm.\n";
writemessages[392] = "A través de la ventana ves algo parecido a unos talleres, con multitud de hombrecillos uniformados trabajando.\n";
writemessages[393] = "Una pieza fundamental en la propulsión de tu nave.\n";
writemessages[394] = "Es una variedad de cuarzo que hace chispas cuando se golpea.";
writemessages[395] = "No sabría qué decirte. No le veo nada de especial.";
writemessages[396] = ".\n";
writemessages[397] = " en ";
writemessages[398] = ".\n";
writemessages[399] = "_.\n";
writemessages[400] = "Eso sería ensañamiento.";
writemessages[401] = "Golpeas discretamente al técnico con ";
writemessages[402] = ", y lo dejas fuera de combate durante un buen rato.";
writemessages[403] = "Golpeas discretamente al técnico con ";
writemessages[404] = ", y lo dejas fuera de combate durante un buen rato.";
writemessages[405] = "Golpeas discretamente al técnico con ";
writemessages[406] = ", y lo dejas fuera de combate durante un buen rato.";
writemessages[407] = "No conseguirías mucho.";
writemessages[408] = "No conseguirías mucho.";
writemessages[409] = "¿Así, con las manos?";
writemessages[410] = "'Iniciando comprobación de sistemas… ";
writemessages[411] = "OK'\nLa nave es transportada a través del hangar hacia la gran compuerta. Una vez abierta, la nave es posicionada verticalmente en un pequeño silo, lista para el lanzamiento.";
writemessages[412] = "El lanzamiento se suspende y la nave es devuelta a su posición en el hangar.";
writemessages[413] = "NOK(ERR70F5). Compartimento abierto'";
writemessages[414] = "NOK(ERR14A5). No detectado. Sist: plato de impulso. Componente: canalizador'";
writemessages[415] = "NOK(ERR83E4). Externo. Sistema hidráulico de posicionamiento'";
writemessages[416] = "o ";
writemessages[417] = "a ";
writemessages[418] = "os ";
writemessages[419] = "as ";
writemessages[420] = "L";
writemessages[421] = "o ";
writemessages[422] = "a ";
writemessages[423] = "os ";
writemessages[424] = "as ";
writemessages[425] = " cae";
writemessages[426] = " por la fosa y se pierde en la oscuridad.";
writemessages[427] = "Oyes como el espejo se despedaza contra el fondo de la fosa.";
writemessages[428] = "Las botas son muy pesadas, y quedan varadas en la orilla.";
writemessages[429] = "Al caer al agua, ";
writemessages[430] = " es arrastrad";
writemessages[431] = " por la corriente y se pierde en el lago.";
writemessages[432] = " cae";
writemessages[433] = " al fondo de la cueva.";
writemessages[434] = "En su interior hay varios componentes del propulsor, ";
writemessages[435] = "incluyendo el canalizador.\n";
writemessages[436] = "pero falta el canalizador de plasma.\n";
writemessages[437] = ".";
writemessages[438] = "El compartimento no es para eso.";
writemessages[439] = "El succionador está bloqueado por el gusano.";
writemessages[440] = " desaparece";
writemessages[441] = ", absorvid";
writemessages[442] = " con ímpetu al interior.";
writemessages[443] = "No puedes meter nada en el cuadro eléctrico, está ocupado por ";
writemessages[444] = "No puedes meter eso en el cuadro eléctrico.";
writemessages[445] = ", ";
writemessages[446] = ".\n";
writemessages[447] = ".\n";
writemessages[448] = ".\n";
writemessages[449] = ".\n";
writemessages[450] = ".\n";
writemessages[451] = ".\n";
writemessages[452] = ".\n";
writemessages[453] = "El succionador está atascado por un gran gusano.";
writemessages[454] = "La rejilla está muy alta y el interior es oscuro.";
writemessages[455] = "Con el tablón sería muy aparatoso.";
writemessages[456] = "Te subes al bloque de hielo, y al instante comienza a moverse hacia el terraplén.  Apenas llega al borde, se precipita veloz hacia el abismo, tropezando y saltando al chocar con los trozos de hielo dispersos del suelo. Cuando llegas abajo, el bloque de hielo es historia, pero tú pareces estar (increiblemente) en buenas condiciones.";
writemessages[457] = ".\n";
writemessages[458] = ".\n";
writemessages[459] = ".\n";
writemessages[460] = " _,";
writemessages[461] = ".\n";
writemessages[462] = " _";
writemessages[463] = "_.\n";
writemessages[464] = "_.\n";
writemessages[465] = "Antes tienes que quitarte todo.";
writemessages[466] = ".\n";
writemessages[467] = " ";
writemessages[468] = ".\n";
writemessages[469] = " _?\n";
writemessages[470] = ".\n";
writemessages[471] = "a";
writemessages[472] = "l";
writemessages[473] = " ";
writemessages[474] = " _";
writemessages[475] = " ";
writemessages[476] = "_";
writemessages[477] = " ";
writemessages[478] = "_";
writemessages[479] = " ";
writemessages[480] = "_";
writemessages[481] = "d";
writemessages[482] = "e ";
writemessages[483] = " _";
writemessages[484] = "_";
writemessages[485] = " _";
writemessages[486] = " _";
writemessages[487] = "l";
writemessages[488] = "le";
writemessages[489] = "s";
writemessages[490] = "Antes tienes que quitarte el resto de cosas que llevas puestas.";
writemessages[491] = ".\n";
writemessages[492] = ".\n";
writemessages[493] = ".\n";
writemessages[494] = ".\n";
writemessages[495] = "Una luz roja parpadea acompañada de un sonido grave. Y luego se apaga.";
writemessages[496] = "El robot emite un sonido agudo, y ves flotar a";
writemessages[497] = "l gusano sobre el robot.";
writemessages[498] = "Una luz verde en el panel parpadea durante unos instantes y luego se apaga.";
writemessages[499] = "¡Eso sería muy peligroso, estás en la misma sala que el robot!";
writemessages[500] = "Una luz verde en el panel parpadea durante unos instantes y luego se apaga.";
writemessages[501] = "Una luz roja parpadea acompañada de un sonido grave. Y luego se apaga.";
writemessages[502] = "Una luz roja parpadea acompañada de un sonido grave. Y luego se apaga.";
writemessages[503] = "El robot emite un sonido agudo, y ves flotar a";
writemessages[504] = "l gusano hasta el suelo.";
writemessages[505] = "El robot emite un sonido agudo, y ves flotar a";
writemessages[506] = " la criatura verde hasta el suelo.";
writemessages[507] = "Una luz verde en el panel parpadea durante unos instantes y luego se apaga.";
writemessages[508] = " Desde el sur te llega el sonido del succionador funcionando.";
writemessages[509] = "Una luz verde en el panel parpadea durante unos instantes y luego se apaga.";
writemessages[510] = " Desde el sur te llega el sonido del succionador funcionando.";
writemessages[511] = "Una luz verde en el panel parpadea durante unos instantes y luego se apaga.";
writemessages[512] = "Una luz verde en el panel parpadea durante unos instantes y luego se apaga.";
writemessages[513] = "El robot lleva ";
writemessages[514] = "un gusano enorme";
writemessages[515] = "una criatura verde";
writemessages[516] = " en suspensión sobre su cabeza. ";
writemessages[517] = "Una luz verde en el panel parpadea durante unos instantes y luego se apaga.";
writemessages[518] = "El robot aparece en la estancia.";
writemessages[519] = "El robot desaparece de la sala deslizándose suavemente.";
writemessages[520] = "Una luz roja parpadea acompañada de un sonido grave. Y luego se apaga.";
writemessages[521] = "Hay un bloque en el suelo ¿Cuantos quieres arrancar?";
writemessages[522] = "No sería mala opción con esas botazas, si al menos las tuvieras puestas.";
writemessages[523] = "No sería mala opción con esas botazas, si al menos las tuvieras puestas.";
writemessages[524] = "Das un enorme patadón a una de las protuberancias heladas. La fuerza del golpe lo desprende de su asidero y cae al suelo entre ecos retumbantes.";
writemessages[525] = "Das un enorme patadón a una de las protuberancias heladas. La fuerza del golpe lo desprende de su asidero y cae al suelo entre ecos retumbantes.";
writemessages[526] = "No tienes el calzado adecuado para esa acción tan radical.";
writemessages[527] = "Tomas impulso y obsequias a uno de los bloques de hielo protuberantes con un tremendo golpe de tablón. El hielo se desquebraja y un bloque cae con un estruendo replicado por toda la estancia.";
writemessages[528] = "No tienes nada de eso.";
writemessages[529] = "El sílex no es suficientemente duro para eso.";
writemessages[530] = "No tienes nada de eso.";
writemessages[531] = "No lo veo como una opción para romper el hielo.";
writemessages[532] = ".\n";
writemessages[533] = "de ";
writemessages[534] = ".\n";
writemessages[535] = ".\n";
writemessages[536] = ".\n";
writemessages[537] = "_.\n";
writemessages[538] = ".\n";
writemessages[539] = " ";
writemessages[540] = ".\n";
writemessages[541] = "Salidas visibles: ";
writemessages[542] = "Ya has tenido una experiencia con ese tema, no necesitas más.";
writemessages[543] = "Das un enorme ";
writemessages[544] = "patadón";
writemessages[545] = "salto";
writemessages[546] = " ejerciendo todo tu peso sobre la rejilla. Uno de los barrotes cede al embate y caéis al vacío. ";
writemessages[547] = "Con el recipiente en tus manos, la caida es muy aparatosa y el líquido se desparrama por todas partes. Cuando llegas al fondo no eres más que una pulpa gelatinosa.";
writemessages[548] = "Por suerte, el bicho para tu caida como si de un colchón se tratara.";
writemessages[549] = "La caída es demasiado dura para sobrevivir.";
writemessages[550] = "…";
writemessages[551] = "…";
writemessages[552] = "medio de los trozos de hielo afilados, precipitándote terraplén abajo. Durante el descenso, las heladas cuchillas literalmente te despedazan.\n\n      \*\*\* HAS PERDIDO \*\*\*\n";
writemessages[553] = "o";
writemessages[554] = "a";
writemessages[555] = "os";
writemessages[556] = "as";
writemessages[557] = "n";
writemessages[558] = "El gusano cede bajo tu peso y cae al vacío.";
writemessages[559] = "Caes al vacío.";
writemessages[560] = "Ves caer ";
writemessages[561] = ".\n";
writemessages[562] = "A través de la rejilla oyes el inconfundible rumor de maquinaria funcionando. Luego el silencio.";
writemessages[563] = "De repente, el suelo de la sala comienza a abrirse.";
writemessages[564] = "Se vuelve a cerrar el suelo. Durante un largo rato notas que te transportan en algún tipo de medio de locomoción. Finalmente eres arrojado a…";

// LOCATION MESSAGES

total_location_messages=34;

locations = [];

locations[0] = "{CLASS|centrado|{CLASS|titulo|AVENTURA A: <i>PLANET OF DEATH</i>} \n\n  Adaptación libre por Mastodon (2009) \n A partir del original de Artic Computing de 1981} \n\n En esta aventura te encuentras varado en un misterioso y lejano planeta del que tienes que escapar tras recuperar tu nave espacial, que te ha sido arrebatada e inutilizada.\n Te encontrarás con varios riesgos y peligros en tu aventura, algunos naturales, otros no, todos los cuales debes superar para tener éxito.\n \n ¡Buena suerte, la vas a necesitar!";
locations[1] = "";
locations[2] = "";
locations[3] = "";
locations[4] = "";
locations[5] = "";
locations[6] = "";
locations[7] = "\n Comedero de babosas\n  Esta parte de la caverna verde es más amplia y más alta";
locations[8] = "\n Meseta\n  Estás en una pequeña meseta pedregosa sobre las montañas, al borde de un pronunciado precipicio y que se extiende al este y al oeste. Bajo las montañas ves un frondoso bosque al que se llega descendiendo por un estrecho camino hacia el suroeste.";
locations[9] = "";
locations[10] = "";
locations[11] = "\n Bosque\n  Te encuentras en un poblado bosque con densa vegetación. Algunos árboles dejan caer sus lianas hasta casi rozar el suelo. Un camino pedregoso desciende de forma pronunciada desde el oeste, y continúa hacia el sur entre sombríos parajes boscosos.";
locations[12] = "\n Al borde de una fosa\n  La meseta finaliza bruscamente en una escarpada pendiente imposible de escalar, tachonada de grandes moles de piedra.";
locations[13] = "\n En suspensión";
locations[14] = "\n Junto al lago\n  Un lago de aguas gélidas se extiende a lo que tu vista alcanza en dirección sur. Esta debió ser una zona habitada, a juzgar por la extraña construcción que hay hacia el este y el cobertizo hacia el oeste. El camino al oeste está bloqueado por un riachuelo que desemboca en el lago. El sendero del bosque conduce al norte.";
locations[15] = "\n Remolino en el lago\n  Te encuentras de pie, unos metros dentro del lago junto a un remolino que forma la desembocadura del riachuelo en el lago.";
locations[16] = "\n Construcción alienígena\n  Una extraña construcción de piedras y barro. Tiene forma hexagonal, con altas ventanas circulares y un simple hueco al oeste a modo de entrada. El suelo está formado por grandes tablones de madera encajados, probablemente procedentes del bosque cercano.";
locations[17] = "\n Viejo cobertizo\n  Estás en el interior de un viejo cobertizo, una especie de almacén aparentemente abandonado. Numerosos utensilios, la mayoría desconocidos para tí, adornan sus paredes.";
locations[18] = "\n Cavidad húmeda\n  Una amplia cavidad con paredes de piedra caliza cubiertas de humedad, de cuyo techo cuelga una miríada de estalactitas de caprichosas formas. En una de las paredes observas una pintura.\n  La cavidad se estrecha y continúa hacia el norte en una leve pendiente descendente, y se abre hacia el oeste al exterior.";
locations[19] = "\n Gruta glaciar\n  En este lugar de la cueva la temperatura es realmente baja, los altos muros que te rodean están cubiertos por enormes placas de hielo, y el techo semeja la bóveda de una nívea catedral. La cavidad continúa hacia el sur bajando por un empinado terraplén cubierto de afilados trozos de hielo incrustados en el suelo. Un estrecho pasaje conduce al este.";
locations[20] = "\n Túnel sinuoso\n  Te encuentras en un túnel sinuoso. Al final del túnel, hacia el este, ves una puerta. El túnel continúa hacia el oeste.";
locations[21] = "\n Sala de computadoras\n  Las paredes de esta sala están repletas de monitores y cuadros de mando, teclados y otros dispositivos para tí desconocidos, y probablemente destinados al control electrónico de la base. Un monitor gigante ocupa buena parte de una de las paredes. La salida está al oeste.";
locations[22] = "\n Sala de control\n  Una pequeña sala circular de techo abovedado semejando un panteón, que da paso al complejo de cavernas verdes hacia el este. En el centro de la estancia hay un panel con varios controles. Un estrecho pasillo continua hacia el sur.";
locations[23] = "";
locations[24] = "\n Sala de basuras\n  Estás en un diminuto cuartucho de paredes metálicas utilizado para salida de residuos, como parece deducirse por el succionador de basuras y la rejilla en el suelo, de la que se desprende un fuerte olor. La única salida visible es hacia el norte.¬";
locations[25] = "\n Estrecho cubículo\n  Estás en una pequeña sala de 2 x 2 metros sin ningún tipo de elemento distinguible";
locations[26] = "\n Vertedero\n  Estás en una montaña de basura y residuos al aire libre. La única salida es hacia abajo.";
locations[27] = "\n Pasaje acolchado\n  Estás en un amplio pasillo de paredes acolchadas que discurre de norte a sur. En dirección al sur, hay una hilera de orificios en el suelo, junto a un extraño dispositivo.";
locations[28] = "\n Hangar\n  El corazón del complejo, el lugar común de todo el tráfico de este planeta. Varios aparatos están estacionados o siendo revisados por extraños hombrecillos de aspecto humanoide. Hay una puerta de acero al oeste, una gran compuerta al este, probablemente destinada a la salida de las naves, y un amplio pasillo acolchado que conduce hacia el norte.";
locations[29] = "\n Tu nave\n  Tu nave es un pequeño y potente vehículo de exploración. De todo el complejo cuadro de mandos que tienes ante ti sólo te interesan en este momento dos botones: INICIAR SECUENCIA e IGNICION.";
locations[30] = "\n Sala de mantenimiento\n  Una estrecha estancia, casi un pasillo, con diversa maquinaria de control. El camino al este está bloqueado por una puerta de acero. El pasillo continúa hacia el sur.";
locations[31] = "\n Final del pasillo\n  El pasillo discurre de norte a sur, donde finaliza frente a una puerta. La puerta tiene una pequeña ventana redonda en el centro.";
locations[32] = "\n Talleres\n  Este lugar está repleto de mesas donde bulle la actividad, instrumental científico de diversa naturaleza, maquinaria de todos los tamaños, así como numerosas piezas que supones pertenecen a las naves del hangar. La única salida es atravesar el arco hacia el norte.";
locations[33] = "";

// CONNECTIONS

connections = [];
connections_start = [];

connections[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[1] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[2] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[3] = [ -1, -1, 4, 20, 6, -1, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[4] = [ -1, -1, 27, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, 3, -1, -1 ];
connections[5] = [ -1, -1, -1, -1, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[6] = [ -1, -1, -1, 3, 22, -1, -1, -1, -1, -1, -1, -1, -1, 7, -1, -1 ];
connections[7] = [ -1, -1, 6, -1, -1, -1, -1, 3, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[8] = [ -1, -1, -1, 18, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[9] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[10] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[11] = [ -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[12] = [ -1, -1, -1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[13] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 12, -1, -1, -1, -1, -1, -1 ];
connections[14] = [ -1, -1, -1, 16, -1, -1, -1, -1, -1, -1, -1, -1, -1, 11, -1, -1 ];
connections[15] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 14, -1, -1 ];
connections[16] = [ -1, -1, -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, 14, -1, -1, -1 ];
connections[17] = [ -1, -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, -1, 14, -1, -1, -1 ];
connections[18] = [ -1, -1, -1, -1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[19] = [ -1, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[20] = [ -1, -1, -1, 21, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[21] = [ -1, -1, -1, -1, 20, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[22] = [ -1, -1, 24, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[23] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[24] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 22, -1, -1 ];
connections[25] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[26] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[27] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 4, -1, -1 ];
connections[28] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 27, -1, -1 ];
connections[29] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 28, -1, 28, -1, -1, -1 ];
connections[30] = [ -1, -1, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[31] = [ -1, -1, 32, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 30, -1, -1 ];
connections[32] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 31, -1, -1 ];
connections[33] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];

connections_start[0] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[1] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[2] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[3] = [ -1, -1, 4, 20, 6, -1, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[4] = [ -1, -1, 27, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, 3, -1, -1 ];
connections_start[5] = [ -1, -1, -1, -1, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[6] = [ -1, -1, -1, 3, 22, -1, -1, -1, -1, -1, -1, -1, -1, 7, -1, -1 ];
connections_start[7] = [ -1, -1, 6, -1, -1, -1, -1, 3, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[8] = [ -1, -1, -1, 18, 12, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[9] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[10] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[11] = [ -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[12] = [ -1, -1, -1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[13] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 12, -1, -1, -1, -1, -1, -1 ];
connections_start[14] = [ -1, -1, -1, 16, -1, -1, -1, -1, -1, -1, -1, -1, -1, 11, -1, -1 ];
connections_start[15] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 14, -1, -1 ];
connections_start[16] = [ -1, -1, -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, 14, -1, -1, -1 ];
connections_start[17] = [ -1, -1, -1, 14, -1, -1, -1, -1, -1, -1, -1, -1, 14, -1, -1, -1 ];
connections_start[18] = [ -1, -1, -1, -1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[19] = [ -1, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[20] = [ -1, -1, -1, 21, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[21] = [ -1, -1, -1, -1, 20, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[22] = [ -1, -1, 24, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[23] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[24] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 22, -1, -1 ];
connections_start[25] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[26] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[27] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 4, -1, -1 ];
connections_start[28] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 27, -1, -1 ];
connections_start[29] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 28, -1, 28, -1, -1, -1 ];
connections_start[30] = [ -1, -1, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[31] = [ -1, -1, 32, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 30, -1, -1 ];
connections_start[32] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 31, -1, -1 ];
connections_start[33] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];


resources=[];


 //OBJECTS

objects = [];
objectsAttrLO = [];
objectsAttrHI = [];
objectsLocation = [];
objectsNoun = [];
objectsAdjective = [];
objectsWeight = [];
objectsAttrLO_start = [];
objectsAttrHI_start = [];
objectsLocation_start = [];
objectsWeight_start = [];

objects[0] = "trozo de pedernal";
objectsNoun[0] = 52;
objectsAdjective[0] = 255;
objectsLocation[0] = 8;
objectsLocation_start[0] = 8;
objectsWeight[0] = 1;
objectsWeight_start[0] = 1;
objectsAttrLO[0] = 1;
objectsAttrLO_start[0] = 1;
objectsAttrHI[0] = 0;
objectsAttrHI_start[0] = 0;

objects[1] = "riachuelo";
objectsNoun[1] = 58;
objectsAdjective[1] = 255;
objectsLocation[1] = 14;
objectsLocation_start[1] = 14;
objectsWeight[1] = 1;
objectsWeight_start[1] = 1;
objectsAttrLO[1] = 4;
objectsAttrLO_start[1] = 4;
objectsAttrHI[1] = 0;
objectsAttrHI_start[1] = 0;

objects[2] = "fosa";
objectsNoun[2] = 56;
objectsAdjective[2] = 255;
objectsLocation[2] = 12;
objectsLocation_start[2] = 12;
objectsWeight[2] = 1;
objectsWeight_start[2] = 1;
objectsAttrLO[2] = 4;
objectsAttrLO_start[2] = 4;
objectsAttrHI[2] = 0;
objectsAttrHI_start[2] = 0;

objects[3] = "liana";
objectsNoun[3] = 54;
objectsAdjective[3] = 255;
objectsLocation[3] = 11;
objectsLocation_start[3] = 11;
objectsWeight[3] = 2;
objectsWeight_start[3] = 2;
objectsAttrLO[3] = 0;
objectsAttrLO_start[3] = 0;
objectsAttrHI[3] = 0;
objectsAttrHI_start[3] = 0;

objects[4] = "piedra";
objectsNoun[4] = 55;
objectsAdjective[4] = 255;
objectsLocation[4] = 12;
objectsLocation_start[4] = 12;
objectsWeight[4] = 2;
objectsWeight_start[4] = 2;
objectsAttrLO[4] = 0;
objectsAttrLO_start[4] = 0;
objectsAttrHI[4] = 0;
objectsAttrHI_start[4] = 0;

objects[5] = "tablón";
objectsNoun[5] = 60;
objectsAdjective[5] = 255;
objectsLocation[5] = 252;
objectsLocation_start[5] = 252;
objectsWeight[5] = 1;
objectsWeight_start[5] = 1;
objectsAttrLO[5] = 0;
objectsAttrLO_start[5] = 0;
objectsAttrHI[5] = 0;
objectsAttrHI_start[5] = 0;

objects[6] = "botas";
objectsNoun[6] = 61;
objectsAdjective[6] = 255;
objectsLocation[6] = 16;
objectsLocation_start[6] = 16;
objectsWeight[6] = 4;
objectsWeight_start[6] = 4;
objectsAttrLO[6] = 2;
objectsAttrLO_start[6] = 2;
objectsAttrHI[6] = 0;
objectsAttrHI_start[6] = 0;

objects[7] = "pistola neutralizante";
objectsNoun[7] = 63;
objectsAdjective[7] = 2;
objectsLocation[7] = 17;
objectsLocation_start[7] = 17;
objectsWeight[7] = 2;
objectsWeight_start[7] = 2;
objectsAttrLO[7] = 0;
objectsAttrLO_start[7] = 0;
objectsAttrHI[7] = 0;
objectsAttrHI_start[7] = 0;

objects[8] = "bloque de hielo";
objectsNoun[8] = 65;
objectsAdjective[8] = 255;
objectsLocation[8] = 252;
objectsLocation_start[8] = 252;
objectsWeight[8] = 1;
objectsWeight_start[8] = 1;
objectsAttrLO[8] = 0;
objectsAttrLO_start[8] = 0;
objectsAttrHI[8] = 0;
objectsAttrHI_start[8] = 0;

objects[9] = "cuadro electrico";
objectsNoun[9] = 96;
objectsAdjective[9] = 255;
objectsLocation[9] = 30;
objectsLocation_start[9] = 30;
objectsWeight[9] = 1;
objectsWeight_start[9] = 1;
objectsAttrLO[9] = 4;
objectsAttrLO_start[9] = 4;
objectsAttrHI[9] = 0;
objectsAttrHI_start[9] = 0;

objects[10] = "compartimento del propulsor";
objectsNoun[10] = 88;
objectsAdjective[10] = 255;
objectsLocation[10] = 28;
objectsLocation_start[10] = 28;
objectsWeight[10] = 1;
objectsWeight_start[10] = 1;
objectsAttrLO[10] = 4;
objectsAttrLO_start[10] = 4;
objectsAttrHI[10] = 0;
objectsAttrHI_start[10] = 0;

objects[11] = "gusano enorme";
objectsNoun[11] = 69;
objectsAdjective[11] = 255;
objectsLocation[11] = 7;
objectsLocation_start[11] = 7;
objectsWeight[11] = 1;
objectsWeight_start[11] = 1;
objectsAttrLO[11] = 0;
objectsAttrLO_start[11] = 0;
objectsAttrHI[11] = 0;
objectsAttrHI_start[11] = 0;

objects[12] = "recipiente";
objectsNoun[12] = 70;
objectsAdjective[12] = 255;
objectsLocation[12] = 7;
objectsLocation_start[12] = 7;
objectsWeight[12] = 1;
objectsWeight_start[12] = 1;
objectsAttrLO[12] = 0;
objectsAttrLO_start[12] = 0;
objectsAttrHI[12] = 0;
objectsAttrHI_start[12] = 0;

objects[13] = "espejo";
objectsNoun[13] = 71;
objectsAdjective[13] = 3;
objectsLocation[13] = 252;
objectsLocation_start[13] = 252;
objectsWeight[13] = 1;
objectsWeight_start[13] = 1;
objectsAttrLO[13] = 0;
objectsAttrLO_start[13] = 0;
objectsAttrHI[13] = 0;
objectsAttrHI_start[13] = 0;

objects[14] = "criatura verde";
objectsNoun[14] = 73;
objectsAdjective[14] = 4;
objectsLocation[14] = 5;
objectsLocation_start[14] = 5;
objectsWeight[14] = 2;
objectsWeight_start[14] = 2;
objectsAttrLO[14] = 0;
objectsAttrLO_start[14] = 0;
objectsAttrHI[14] = 0;
objectsAttrHI_start[14] = 0;

objects[15] = "esqueleto pulposo";
objectsNoun[15] = 74;
objectsAdjective[15] = 255;
objectsLocation[15] = 252;
objectsLocation_start[15] = 252;
objectsWeight[15] = 1;
objectsWeight_start[15] = 1;
objectsAttrLO[15] = 0;
objectsAttrLO_start[15] = 0;
objectsAttrHI[15] = 0;
objectsAttrHI_start[15] = 0;

objects[16] = "espejo";
objectsNoun[16] = 71;
objectsAdjective[16] = 5;
objectsLocation[16] = 5;
objectsLocation_start[16] = 5;
objectsWeight[16] = 1;
objectsWeight_start[16] = 1;
objectsAttrLO[16] = 0;
objectsAttrLO_start[16] = 0;
objectsAttrHI[16] = 0;
objectsAttrHI_start[16] = 0;

objects[17] = "babosa verde";
objectsNoun[17] = 75;
objectsAdjective[17] = 255;
objectsLocation[17] = 6;
objectsLocation_start[17] = 6;
objectsWeight[17] = 2;
objectsWeight_start[17] = 2;
objectsAttrLO[17] = 0;
objectsAttrLO_start[17] = 0;
objectsAttrHI[17] = 0;
objectsAttrHI_start[17] = 0;

objects[18] = "babosa verde";
objectsNoun[18] = 75;
objectsAdjective[18] = 255;
objectsLocation[18] = 6;
objectsLocation_start[18] = 6;
objectsWeight[18] = 2;
objectsWeight_start[18] = 2;
objectsAttrLO[18] = 0;
objectsAttrLO_start[18] = 0;
objectsAttrHI[18] = 0;
objectsAttrHI_start[18] = 0;

objects[19] = "guantes";
objectsNoun[19] = 77;
objectsAdjective[19] = 255;
objectsLocation[19] = 20;
objectsLocation_start[19] = 20;
objectsWeight[19] = 3;
objectsWeight_start[19] = 3;
objectsAttrLO[19] = 2;
objectsAttrLO_start[19] = 2;
objectsAttrHI[19] = 0;
objectsAttrHI_start[19] = 0;

objects[20] = "tarjeta hexagonal";
objectsNoun[20] = 79;
objectsAdjective[20] = 255;
objectsLocation[20] = 21;
objectsLocation_start[20] = 21;
objectsWeight[20] = 2;
objectsWeight_start[20] = 2;
objectsAttrLO[20] = 0;
objectsAttrLO_start[20] = 0;
objectsAttrHI[20] = 0;
objectsAttrHI_start[20] = 0;

objects[21] = "panel de control";
objectsNoun[21] = 81;
objectsAdjective[21] = 255;
objectsLocation[21] = 22;
objectsLocation_start[21] = 22;
objectsWeight[21] = 1;
objectsWeight_start[21] = 1;
objectsAttrLO[21] = 0;
objectsAttrLO_start[21] = 0;
objectsAttrHI[21] = 0;
objectsAttrHI_start[21] = 0;

objects[22] = "robot";
objectsNoun[22] = 82;
objectsAdjective[22] = 255;
objectsLocation[22] = 22;
objectsLocation_start[22] = 22;
objectsWeight[22] = 1;
objectsWeight_start[22] = 1;
objectsAttrLO[22] = 0;
objectsAttrLO_start[22] = 0;
objectsAttrHI[22] = 0;
objectsAttrHI_start[22] = 0;

objects[23] = "rejilla";
objectsNoun[23] = 83;
objectsAdjective[23] = 255;
objectsLocation[23] = 24;
objectsLocation_start[23] = 24;
objectsWeight[23] = 2;
objectsWeight_start[23] = 2;
objectsAttrLO[23] = 0;
objectsAttrLO_start[23] = 0;
objectsAttrHI[23] = 0;
objectsAttrHI_start[23] = 0;

objects[24] = "barrote de hierro";
objectsNoun[24] = 84;
objectsAdjective[24] = 255;
objectsLocation[24] = 252;
objectsLocation_start[24] = 252;
objectsWeight[24] = 1;
objectsWeight_start[24] = 1;
objectsAttrLO[24] = 0;
objectsAttrLO_start[24] = 0;
objectsAttrHI[24] = 0;
objectsAttrHI_start[24] = 0;

objects[25] = "succionador";
objectsNoun[25] = 85;
objectsAdjective[25] = 255;
objectsLocation[25] = 24;
objectsLocation_start[25] = 24;
objectsWeight[25] = 1;
objectsWeight_start[25] = 1;
objectsAttrLO[25] = 0;
objectsAttrLO_start[25] = 0;
objectsAttrHI[25] = 0;
objectsAttrHI_start[25] = 0;

objects[26] = "dispositivo";
objectsNoun[26] = 86;
objectsAdjective[26] = 255;
objectsLocation[26] = 27;
objectsLocation_start[26] = 27;
objectsWeight[26] = 1;
objectsWeight_start[26] = 1;
objectsAttrLO[26] = 0;
objectsAttrLO_start[26] = 0;
objectsAttrHI[26] = 0;
objectsAttrHI_start[26] = 0;

objects[27] = "nave";
objectsNoun[27] = 87;
objectsAdjective[27] = 255;
objectsLocation[27] = 28;
objectsLocation_start[27] = 28;
objectsWeight[27] = 2;
objectsWeight_start[27] = 2;
objectsAttrLO[27] = 0;
objectsAttrLO_start[27] = 0;
objectsAttrHI[27] = 0;
objectsAttrHI_start[27] = 0;

objects[28] = "tecnico de mantenimiento";
objectsNoun[28] = 89;
objectsAdjective[28] = 255;
objectsLocation[28] = 28;
objectsLocation_start[28] = 28;
objectsWeight[28] = 1;
objectsWeight_start[28] = 1;
objectsAttrLO[28] = 0;
objectsAttrLO_start[28] = 0;
objectsAttrHI[28] = 0;
objectsAttrHI_start[28] = 0;

objects[29] = "uniforme de mantenimiento";
objectsNoun[29] = 90;
objectsAdjective[29] = 255;
objectsLocation[29] = 252;
objectsLocation_start[29] = 252;
objectsWeight[29] = 1;
objectsWeight_start[29] = 1;
objectsAttrLO[29] = 2;
objectsAttrLO_start[29] = 2;
objectsAttrHI[29] = 0;
objectsAttrHI_start[29] = 0;

objects[30] = "puerta de acero";
objectsNoun[30] = 76;
objectsAdjective[30] = 255;
objectsLocation[30] = 28;
objectsLocation_start[30] = 28;
objectsWeight[30] = 2;
objectsWeight_start[30] = 2;
objectsAttrLO[30] = 0;
objectsAttrLO_start[30] = 0;
objectsAttrHI[30] = 0;
objectsAttrHI_start[30] = 0;

objects[31] = "botón de iniciar secuencia";
objectsNoun[31] = 93;
objectsAdjective[31] = 255;
objectsLocation[31] = 29;
objectsLocation_start[31] = 29;
objectsWeight[31] = 1;
objectsWeight_start[31] = 1;
objectsAttrLO[31] = 0;
objectsAttrLO_start[31] = 0;
objectsAttrHI[31] = 0;
objectsAttrHI_start[31] = 0;

objects[32] = "botón de ignición";
objectsNoun[32] = 94;
objectsAdjective[32] = 255;
objectsLocation[32] = 29;
objectsLocation_start[32] = 29;
objectsWeight[32] = 1;
objectsWeight_start[32] = 1;
objectsAttrLO[32] = 0;
objectsAttrLO_start[32] = 0;
objectsAttrHI[32] = 0;
objectsAttrHI_start[32] = 0;

objects[33] = "palanca";
objectsNoun[33] = 95;
objectsAdjective[33] = 255;
objectsLocation[33] = 30;
objectsLocation_start[33] = 30;
objectsWeight[33] = 2;
objectsWeight_start[33] = 2;
objectsAttrLO[33] = 0;
objectsAttrLO_start[33] = 0;
objectsAttrHI[33] = 0;
objectsAttrHI_start[33] = 0;

objects[34] = "fusible fundido";
objectsNoun[34] = 97;
objectsAdjective[34] = 255;
objectsLocation[34] = 9;
objectsLocation_start[34] = 9;
objectsWeight[34] = 1;
objectsWeight_start[34] = 1;
objectsAttrLO[34] = 0;
objectsAttrLO_start[34] = 0;
objectsAttrHI[34] = 0;
objectsAttrHI_start[34] = 0;

objects[35] = "ventana";
objectsNoun[35] = 98;
objectsAdjective[35] = 255;
objectsLocation[35] = 31;
objectsLocation_start[35] = 31;
objectsWeight[35] = 2;
objectsWeight_start[35] = 2;
objectsAttrLO[35] = 0;
objectsAttrLO_start[35] = 0;
objectsAttrHI[35] = 0;
objectsAttrHI_start[35] = 0;

objects[36] = "canalizador de plasma";
objectsNoun[36] = 99;
objectsAdjective[36] = 255;
objectsLocation[36] = 32;
objectsLocation_start[36] = 32;
objectsWeight[36] = 1;
objectsWeight_start[36] = 1;
objectsAttrLO[36] = 0;
objectsAttrLO_start[36] = 0;
objectsAttrHI[36] = 0;
objectsAttrHI_start[36] = 0;

objects[37] = "al norte";
objectsNoun[37] = 255;
objectsAdjective[37] = 255;
objectsLocation[37] = 252;
objectsLocation_start[37] = 252;
objectsWeight[37] = 1;
objectsWeight_start[37] = 1;
objectsAttrLO[37] = 0;
objectsAttrLO_start[37] = 0;
objectsAttrHI[37] = 0;
objectsAttrHI_start[37] = 0;

objects[38] = "al sur";
objectsNoun[38] = 255;
objectsAdjective[38] = 255;
objectsLocation[38] = 252;
objectsLocation_start[38] = 252;
objectsWeight[38] = 1;
objectsWeight_start[38] = 1;
objectsAttrLO[38] = 0;
objectsAttrLO_start[38] = 0;
objectsAttrHI[38] = 0;
objectsAttrHI_start[38] = 0;

objects[39] = "al este";
objectsNoun[39] = 255;
objectsAdjective[39] = 255;
objectsLocation[39] = 252;
objectsLocation_start[39] = 252;
objectsWeight[39] = 1;
objectsWeight_start[39] = 1;
objectsAttrLO[39] = 0;
objectsAttrLO_start[39] = 0;
objectsAttrHI[39] = 0;
objectsAttrHI_start[39] = 0;

objects[40] = "al oeste";
objectsNoun[40] = 255;
objectsAdjective[40] = 255;
objectsLocation[40] = 252;
objectsLocation_start[40] = 252;
objectsWeight[40] = 1;
objectsWeight_start[40] = 1;
objectsAttrLO[40] = 0;
objectsAttrLO_start[40] = 0;
objectsAttrHI[40] = 0;
objectsAttrHI_start[40] = 0;

objects[41] = "al nordeste";
objectsNoun[41] = 255;
objectsAdjective[41] = 255;
objectsLocation[41] = 252;
objectsLocation_start[41] = 252;
objectsWeight[41] = 1;
objectsWeight_start[41] = 1;
objectsAttrLO[41] = 0;
objectsAttrLO_start[41] = 0;
objectsAttrHI[41] = 0;
objectsAttrHI_start[41] = 0;

objects[42] = "al noroeste";
objectsNoun[42] = 255;
objectsAdjective[42] = 255;
objectsLocation[42] = 252;
objectsLocation_start[42] = 252;
objectsWeight[42] = 1;
objectsWeight_start[42] = 1;
objectsAttrLO[42] = 0;
objectsAttrLO_start[42] = 0;
objectsAttrHI[42] = 0;
objectsAttrHI_start[42] = 0;

objects[43] = "al sureste";
objectsNoun[43] = 255;
objectsAdjective[43] = 255;
objectsLocation[43] = 252;
objectsLocation_start[43] = 252;
objectsWeight[43] = 1;
objectsWeight_start[43] = 1;
objectsAttrLO[43] = 0;
objectsAttrLO_start[43] = 0;
objectsAttrHI[43] = 0;
objectsAttrHI_start[43] = 0;

objects[44] = "al suroeste";
objectsNoun[44] = 255;
objectsAdjective[44] = 255;
objectsLocation[44] = 252;
objectsLocation_start[44] = 252;
objectsWeight[44] = 1;
objectsWeight_start[44] = 1;
objectsAttrLO[44] = 0;
objectsAttrLO_start[44] = 0;
objectsAttrHI[44] = 0;
objectsAttrHI_start[44] = 0;

objects[45] = "subir";
objectsNoun[45] = 255;
objectsAdjective[45] = 255;
objectsLocation[45] = 252;
objectsLocation_start[45] = 252;
objectsWeight[45] = 1;
objectsWeight_start[45] = 1;
objectsAttrLO[45] = 0;
objectsAttrLO_start[45] = 0;
objectsAttrHI[45] = 0;
objectsAttrHI_start[45] = 0;

objects[46] = "bajar";
objectsNoun[46] = 255;
objectsAdjective[46] = 255;
objectsLocation[46] = 252;
objectsLocation_start[46] = 252;
objectsWeight[46] = 1;
objectsWeight_start[46] = 1;
objectsAttrLO[46] = 0;
objectsAttrLO_start[46] = 0;
objectsAttrHI[46] = 0;
objectsAttrHI_start[46] = 0;

objects[47] = "entrar";
objectsNoun[47] = 255;
objectsAdjective[47] = 255;
objectsLocation[47] = 252;
objectsLocation_start[47] = 252;
objectsWeight[47] = 1;
objectsWeight_start[47] = 1;
objectsAttrLO[47] = 0;
objectsAttrLO_start[47] = 0;
objectsAttrHI[47] = 0;
objectsAttrHI_start[47] = 0;

objects[48] = "salir";
objectsNoun[48] = 255;
objectsAdjective[48] = 255;
objectsLocation[48] = 252;
objectsLocation_start[48] = 252;
objectsWeight[48] = 1;
objectsWeight_start[48] = 1;
objectsAttrLO[48] = 0;
objectsAttrLO_start[48] = 0;
objectsAttrHI[48] = 0;
objectsAttrHI_start[48] = 0;

last_object_number =  48; 
carried_objects = 0;
total_object_messages=49;

