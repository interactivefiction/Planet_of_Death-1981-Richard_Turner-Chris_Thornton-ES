
;#define flg flag_BASURAENBASURERO 5
;#define flg flag_CONTADORPILLADO 6
;#define flg flag_BLOQUEHIELOPUNTUADO 11
;#define flg flag_LOCA_BABOSA 12
;#define flg flag_LOCBABOSA1 13
;#define flg flag_LOCBABOSA2 14
;#define flg flag_MONTADOHIELOPUNTUADO 15
;#define flg flag_PROP_ABIERTO 16
;#define flg flag_PROP_ABRIBLE 17
;#define flg flag_PROP_BABOSA 18
;#define flg flag_PROP_CONTENEDOR 19
;#define flg flag_PROP_ESTATICO 20
;#define flg flag_PROP_GENNUM 21
;#define flg flag_PROP_INCONTABLE 22
;#define flg flag_PROP_NOLISTAR 23
;#define flg flag_ADJTMP 24
;#define flg flag_BABOSAAMENAZA 25
;#define flg flag_CAMPOFUERZADESTRUIDO 26
;#define flg flag_COMPARTIMENTOABIERTO 27
;#define flg flag_COMPLETARTEXTOBABOSA 28
;#define flg flag_PUNTUACION 30
;#define flg flag_COMPUERTANAVEABIERTA 60
;#define flg flag_CONTADORBALANCEO 61
;#define flg flag_CUADROELECTRICOABIERTO 62
;#define flg flag_DESTINO 63
;#define flg flag_ESPEJOMOVIDO 64
;#define flg flag_ESTADOSUSPENSION 65
;#define flg flag_ESTADOTECNICO 66
;#define flg flag_FLAGPRM1 67
;#define flg flag_FLAGPRM2 68
;#define flg flag_FLAGRET 69
;#define flg flag_FLAGRET2 70
;#define flg flag_FLAGRET3 71
;#define flg flag_GUARDAADJ 72
;#define flg flag_GUARDAADJEXA 73
;#define flg flag_GUARDAFIRSTADJ 74
;#define flg flag_GUARDAFIRSTNOUN 75
;#define flg flag_GUARDALOCACTUAL 76
;#define flg flag_GUARDANOUN 77
;#define flg flag_GUARDANOUNEXA 78
;#define flg flag_GUARDAPRESENTCONTAINER 79
;#define flg flag_GUARDAPRESENTLOC 80
;#define flg flag_GUARDAPRESENTOBJ 81
;#define flg flag_GUARDAPRESENTWEARABLE 82
;#define flg flag_GUARDAPRESENTWEIGHT 83
;#define flg flag_LANZAMIENTOINICIADO 84
;#define flg flag_LIANAATADA 85
;#define flg flag_LIANACORTADA 86
;#define flg flag_LOCCRIATURAVERDE 87
;#define flg flag_NOMTMP 88
;#define flg flag_NUMBABOSA 89
;#define flg flag_ORIGEN 90
;#define flg flag_ORIGENROBOT 91
;#define flg flag_POSICIONROBOT 92
;#define flg flag_PUNTUADAFOSA 93
;#define flg flag_PUNTUADOCOMPARTIMENTO 94
;#define flg flag_PUNTUADOESPEJOCOGIDO 95
;#define flg flag_PUNTUADOLANZAMIENTOINICIADO 96
;#define flg flag_PUNTUADORECIPIENTE 97
;#define flg flag_PUNTUADOTARJETA 98
;#define flg flag_RANDOMBABOSA 99
;#define flg flag_REJILLASINBARROTE 100
;#define flg flag_TABLONCOGIDO 101
;#define flg flag_TABLONENRIACHUELO 102
;#define flg flag_TANQUEHIDRAULICOLLENADO 103
;#define flg flag_TMPFLAG1 104
;#define flg flag_TMPFLAG2 105
;#define flg flag_VISITADOCOBERTIZO 106


;#define const proc_ABRIRSUB 3
;#define const proc_ABRIRTEXT 4
;#define const proc_ACCIONROBOT 5
;#define const proc_ARTDET 6
;#define const proc_ARTDETMAYUSC 7
;#define const proc_ARTUNDET 8
;#define const proc_ATALIANAPROC 9
;#define const proc_ATRAVIESAPUERTAACERO 10
;#define const proc_BABOSAACRIATURA 11
;#define const proc_BABOSADAEMON 12
;#define const proc_BABOSAVASTR 13
;#define const proc_BALANCEARDAEMON 14
;#define const proc_BALANCEARPROC 15
;#define const proc_BUSCAREN 16
;#define const proc_BUSCARENLOOP 17
;#define const proc_CERRARSUB 18
;#define const proc_CERRARTEXT 19
;#define const proc_CODESECOND 20
;#define const proc_COGERRECIPIENTEGUSANO 21
;#define const proc_COGERSUB 22
;#define const proc_CONTARAT 23
;#define const proc_CONTARATINC 24
;#define const proc_CONTARATINCREAL 25
;#define const proc_CONTARATREAL 26
;#define const proc_CRIATURAVERDELOC 27
;#define const proc_DARSUB 28
;#define const proc_DEJARSUB 29
;#define const proc_DESCPROPIEDAD 30
;#define const proc_DESCRIBESUSPENDIDO 31
;#define const proc_DISPARARSUB 32
;#define const proc_ESCONTAINERSECOND 33
;#define const proc_ESTACERRADO 34
;#define const proc_EXAMINAPRESENT 35
;#define const proc_EXAMINARSUB 36
;#define const proc_EXTBABOSA 37
;#define const proc_EXTPROPIEDAD 38
;#define const proc_GENNUM 39
;#define const proc_GOLPEATECNICO 40
;#define const proc_GUARDAFIRST 41
;#define const proc_HAYBABOSA 42
;#define const proc_HAYBABOSALOOP 43
;#define const proc_INICIARLANZAMIENTO 44
;#define const proc_INITBABOSAS 45
;#define const proc_INVENTARIO 46
;#define const proc_LIMPIARFOSA 47
;#define const proc_LIMPIARRIACHUELO 48
;#define const proc_LIMPIARSUSPENDIDO 49
;#define const proc_LISTAT 50
;#define const proc_LISTATFIRSTNOUN 51
;#define const proc_LISTATPRINT 52
;#define const proc_LISTOBJ 53
;#define const proc_LOCSECOND 54
;#define const proc_METERSUB 55
;#define const proc_MIRARENSUB 56
;#define const proc_MONTAHIELOPROC 57
;#define const proc_MOSTRARSUB 58
;#define const proc_MUEVEBABOSA 59
;#define const proc_PONERSUB 60
;#define const proc_PRESENT 61
;#define const proc_PRESENTSECOND 62
;#define const proc_PRINTFIRSTAL 63
;#define const proc_PRINTFIRSTARTDET 64
;#define const proc_PRINTFIRSTARTDETMAYUSC 65
;#define const proc_PRINTFIRSTARTUNDET 66
;#define const proc_PRINTFIRSTDEL 67
;#define const proc_PRINTSECOND 68
;#define const proc_PRINTSECONDARTDET 69
;#define const proc_PRINTSECONDARTUNDET 70
;#define const proc_PRINTSECONDDEL 71
;#define const proc_PROACUSATIVO 72
;#define const proc_PRODATIVO 73
;#define const proc_PUNTUAHIELO 74
;#define const proc_PUNTUARRECIPIENTE 75
;#define const proc_QUITARSUB 76
;#define const proc_RESTAURAFIRST 77
;#define const proc_ROBOTCOGER 78
;#define const proc_ROBOTDEJAR 79
;#define const proc_ROBOTDESCRIBIR 80
;#define const proc_ROBOTMOVER 81
;#define const proc_ROMPEHIELO 82
;#define const proc_SACARSUB 83
;#define const proc_SALIDAS 84
;#define const proc_SALTAREJILLA 85
;#define const proc_SUELTALIANAPROC 86
;#define const proc_TERMGENERO 87
;#define const proc_TERMGENEROVERBO 88
;#define const proc_VACIARBASUREROLOOP 89
;#define const proc_VACIARBASUREROPROC 90

/CTL

/VOC
NEUTRALIZA	2	ADJECTIVE
CON	2	PREPOSITION
S	2	NOUN
SUR	2	NOUN
Y	2	CONJUNCTION
ENTONCES	2	CONJUNCTION
LUEGO	2	CONJUNCTION
ESTATICO	3	ADJECTIVE
A	3	PREPOSITION
AL	3	PREPOSITION
E	3	NOUN
ESTE	3	NOUN
VERDE	4	ADJECTIVE
SOBRE	4	PREPOSITION
O	4	NOUN
OESTE	4	NOUN
DEMANO	5	ADJECTIVE
NE	5	NOUN
NORESTE	5	NOUN
NORDESTE	5	NOUN
EN	5	PREPOSITION
DENTRO	5	PREPOSITION
NO	6	NOUN
NOROESTE	6	NOUN
SE	7	NOUN
SURESTE	7	NOUN
SO	8	NOUN
SUROESTE	8	NOUN
SUBE	9	VERB
SUBIR	9	VERB
ARRIBA	9	NOUN
AR	9	NOUN
BAJA	10	VERB
BAJAR	10	VERB
ABAJO	10	NOUN
AB	10	NOUN
ENTRA	11	VERB
METETE	11	VERB
SALIR	12	VERB
SAL	12	VERB
N	13	NOUN
NORTE	13	NOUN
I	14	NOUN
INVENTARIO	14	NOUN
TIRA	20	VERB
TIRAR	20	VERB
ARRANCA	20	VERB
ARRANCAR	20	VERB
ARROJA	21	VERB
LANZA	21	VERB
ARROJAR	21	VERB
LANZAR	21	VERB
NADA	22	VERB
NADAR	22	VERB
CRUZA	23	VERB
CRUZAR	23	VERB
SALTA	24	VERB
SALTAR	24	VERB
PUNTOS	25	VERB
PUNTUACION	25	VERB
PATEA	26	VERB
PATEAR	26	VERB
ROMPE	27	VERB
PARTE	27	VERB
PARTIR	27	VERB
GOLPEA	27	VERB
GOLPEAR	27	VERB
ROMPER	27	VERB
CORTA	28	VERB
CORTAR	28	VERB
EMPUJA	29	VERB
EMPUJAR	29	VERB
PULSA	30	VERB
PRESIONA	30	VERB
APRIETA	30	VERB
APRETAR	30	VERB
PULSAR	30	VERB
PRESIONAR	30	VERB
MONTA	31	VERB
SUBETE	31	VERB
TREPA	32	VERB
ESCALA	32	VERB
TREPAR	32	VERB
ESCALAR	32	VERB
ATA	33	VERB
ATAR	33	VERB
ANUDA	33	VERB
ANUDAR	33	VERB
DESATA	34	VERB
DESANUDA	34	VERB
DESATAR	34	VERB
DESANUDAR	34	VERB
BALANCEA	35	VERB
COLUMPIA	35	VERB
BALANCEATE	35	VERB
COLUMPIATE	35	VERB
DESPIERTA	36	VERB
DESPERTAR	36	VERB
DISPARA	37	VERB
DISPARAR	37	VERB
MATA	38	VERB
MATAR	38	VERB
INSERTA	39	VERB
INTRODUCE	39	VERB
COLOCA	39	VERB
INSERTAR	39	VERB
INTRODUCIR	39	VERB
COLOCAR	39	VERB
MUEVE	40	VERB
MOVER	40	VERB
Z	41	VERB
ESPERA	41	VERB
ABRE	42	VERB
ABRIR	42	VERB
CIERRA	43	VERB
CERRAR	43	VERB
DEJA	44	VERB
DEJAR	44	VERB
SUELTA	44	VERB
SOLTAR	44	VERB
ENCIEDE	45	VERB
ENCENDER	45	VERB
APAGA	46	VERB
APAGAR	46	VERB
SAVE	47	VERB
GRABA	47	VERB
GRABAR	47	VERB
LOAD	48	VERB
CARGA	48	VERB
CARGAR	48	VERB
RS	49	VERB
RAMSAVE	49	VERB
PRECIPICIO	50	NOUN
COGE	50	VERB
COGER	50	VERB
AGARRA	50	VERB
TOMA	50	VERB
TOMAR	50	VERB
AGARRAR	50	VERB
TODO	51	NOUN
QUITA	51	VERB
SACA	51	VERB
SACAR	51	VERB
PEDERNAL	52	NOUN
RL	52	VERB
RAMLOAD	52	VERB
ARBOL	53	NOUN
ARBOLES	53	NOUN
DI	53	VERB
DECIR	53	VERB
HABLA	53	VERB
PREGUNTA	53	VERB
LIANA	54	NOUN
CUERDA	54	NOUN
LIANAS	54	NOUN
M	54	VERB
R	54	VERB
MIRAR	54	VERB
MIRA	54	VERB
MOLE	55	NOUN
MOLES	55	NOUN
PIEDRA	55	NOUN
PIEDRAS	55	NOUN
EX	55	VERB
EXAMINA	55	VERB
EXAMINAR	55	VERB
FOSA	56	NOUN
PON	56	VERB
PONER	56	VERB
LAGO	57	NOUN
REMOLINO	57	NOUN
METE	57	VERB
METER	57	VERB
RIO	58	NOUN
RIACHUELO	58	NOUN
PONTE	58	VERB
SUELO	59	NOUN
SACATE	59	VERB
TABLON	60	NOUN
TABLONES	60	NOUN
FIN	60	VERB
RETIRARTE	60	VERB
ABANDONA	60	VERB
RETIRARSE	60	VERB
ABANDONAR	60	VERB
BOTAS	61	NOUN
BOTA	61	NOUN
DA	61	VERB
DALE	61	VERB
DAR	61	VERB
DARLE	61	VERB
UTENSILIO	62	NOUN
UTENSILIOS	62	NOUN
MUESTRA	62	VERB
MOSTRAR	62	VERB
ENSENA	62	VERB
ENSEÑA	62	VERB
ENSENAR	62	VERB
ENSEÑAR	62	VERB
PISTOLA	63	NOUN
X	63	VERB
SALIDAS	63	VERB
PINTURA	64	NOUN
BLOQUE	65	NOUN
PLACA	65	NOUN
HIELO	65	NOUN
PLACAS	65	NOUN
PIE	66	NOUN
PIES	66	NOUN
PATADA	67	NOUN
PUÑETAZO	68	NOUN
PUNETAZO	68	NOUN
GUSANO	69	NOUN
RECIPIENTE	70	NOUN
LIQUIDO	70	NOUN
ESPEJO	71	NOUN
SIMBOLO	72	NOUN
CRIATURA	73	NOUN
ESQUELETO	74	NOUN
BABOSA	75	NOUN
PUERTA	76	NOUN
GUANTES	77	NOUN
MONITOR	78	NOUN
TARJETA	79	NOUN
CONTROLES	80	NOUN
PANEL	81	NOUN
ROBOT	82	NOUN
REJILLA	83	NOUN
BARROTE	84	NOUN
SUCCIONADO	85	NOUN
DISPOSITIV	86	NOUN
NAVE	87	NOUN
COMPARTIME	88	NOUN
TECNICO	89	NOUN
UNIFORME	90	NOUN
ORIFICIO	91	NOUN
BOTON	92	NOUN
INICIAR	93	NOUN
IGNICION	94	NOUN
PALANCA	95	NOUN
CUADRO	96	NOUN
FUSIBLE	97	NOUN
VENTANA	98	NOUN
VENTANITA	98	NOUN
CANALIZADO	99	NOUN
SI	100	NOUN
DERRAMA	250	VERB
ECHA	250	VERB
ECHAR	250	VERB
VIERTE	250	VERB
VERTER	250	VERB
DERRAMAR	250	VERB
/STX	; SYSMESSAGES
/0
ESTA MUY OSCURO PARA PODER VER
/1
También puedes ver¬
/2
\n
/3
\n
Haz algo.\n
/4
\n
¿Alguna otra idea?\n
/5
\n
Espero instrucciones.\n
/6
No pude entenderte. Usa otras palabras.\n
/7
No puedes ir en esa direccion.\n
/8
No sé como hacer eso. Intenta describirlo de otra forma.\n
/9
Tienes¬
/10
levas puest
/11
=<NADA>=\n
/12
¿Seguro?\n
/13
\n
   ¿ Quieres jugar de nuevo ?\n
/14
Pues adiós…\n
/15
VALE.\n
/16
\n
{CLASS|centrado|-=PULSA UNA TECLA PARA SEGUIR=-}<br>
/17
Has realizado¬
/18
 movimiento
/19
s
/20
.\n
/21
Tu puntuación es de¬
/22
 sobre un total de 30.\n
/23
No lo llevas puesto.\n
/24
No puedes dejar¬
/25
Ya tienes¬
/26
coger¬
/27
No puedes llevar más cosas.\n
/28
No tienes nada de eso.\n
/29
Ya tienes puest
/30
S
/31
N
/32
\n
Pulsa una tecla…
/33
>
/34
.
/35
\n
Pasa el tiempo…\n
/36
Coges¬
/37
Te pones¬
/38
Te quitas¬
/39

/40
¿Estás intentando ponerte¬
/41

/42

/43

/44

/45

/46
,¬
/47
 y¬
/48
.\n
/49
No tienes¬
/50
No¬
/51
.\n
/52
No hay nada de eso en¬
/53
nada.\n
/54
Archivo no encontrado.
/55
Archivo corrupto.
/56
¡Error de lectura! ¡El archivo no se ha grabado!
/57
Directorio lleno.
/58
Disco lleno.
/59
Error en el nombre del archivo.
/60
Escribe el nombre del archivo.¬
/61
No sé como hacer eso. Intenta describirlo de otra forma.\n
/62
No te he entendido. Usa otras palabras.\n
/63
Planet of Death\n
Versión 1.1 (05-09-09)\n
Programa: Mastodon\n
Beta testing: Akbarr y Baltasarq\n
/64
La salida del riachuelo forma un pequeño remolino no muy lejos de la orilla.\n
/65
Alguien desciende por una fosa con una cuerda.\n
/66
En algunos sitios tapizan las paredes como planchas de metal, en otros brotan
como deformes protuberancias de la fría roca.\n
/67
Un gran espejo cuadrado tras un duro cristal, orientado formando un ángulo
de 45 grados. Junto a él, hay un símbolo: un cuadrado y una flecha apuntando
al interior.\n
/68
Un mapa ocupa gran parte de la pantalla. En la parte de arriba ves un complejo
de cuevas. La zona inferior representa una zona de hangares o talleres.
/69
 Un punto rojo parpadea en el lado derecho de los hangares.\n
/70
Un poco extrañas, pero está claro que son para ponérselas en los pies. Son
bastante grandes y resistentes.\n
/71
Un enorme bloque de hielo, frío y deslizante.\n
/72
La fosa es profunda y oscura, no se ve el fondo.\n
/73
Un gran espejo cuadrado tras un duro cristal, orientado formando un ángulo
de 45 grados. Junto a él, hay un extraño símbolo: un cuadrado y una flecha
apuntando al interior.\n
/74
Es un espejo corriente de unos cincuenta centímetros de longitud.\n
/75
Una enorme y repugnante babosa. Al desplazarse deja como un rastro de asquerosa
pulpa verde corrosiva.\n
/76
De aspecto antropomorfo, aunque no se podría calificar de humana. Aproximadamente
60 centímetros de longitud, piel verde y abotargada. Piernas cortas, brazos
largos con uñas puntiagudas y cabeza achatada.\n
/77
Bueno, es bastante grande para lo que estás acostumbrado: unos 50 o 60 cm.\n
/78
Lleva un uniforme con máscara, al igual que el resto de operarios. Está
dormido.\n
/79
Duerme el sueño de los justos.\n
/80
Junto a ella hay un orificio hexagonal. Está cerrada.\n
/81
Se utiliza para atontar o dejar sin sentido a criaturas humanoides.\n
/82
A través de la ventana ves algo parecido a unos talleres, con multitud de
hombrecillos uniformados trabajando.\n
/83
Pequeña pero potente. En el frontal ves el compartimento del propulsor.\n
/84
Una pieza fundamental en la propulsión de tu nave.\n
/85
Le falta un barrote, suficiente para caer hacia abajo.\n
/86
Está lleno de un líquido verde, viscoso y borboteante.\n
/87
Tiene varios botones, en forma de flecha direccional: norte, sur, este,
oeste, arriba y abajo.\n
/88
Es básicamente un cilindro hermético con ruedas.\n
/89
Similar a las babosas en forma, que no en tamaño. Es de un color algo más
tirando al marrón. No parece muy ágil.\n
/90
Bastante grande y pesado.\n
/91
\n
/92
.\n
/93
En su interior hay varios componentes del propulsor,¬
/94
incluyendo el canalizador.\n
/95
pero falta el canalizador de plasma.\n
/96
 Además, l
/97
Actualmente no tienes nada.
/98
Dejas¬
/99
No puedes¬
/100
No hay nada que puedas¬
/101
, está
/102
 fijad
/103
 al sitio.\n
/104
con ese nombre. Quizás forme parte del escenario y no sea importante.\n
/105
Sólo puedes usar 'TODO' con los verbos COGER y DEJAR.\n
/106
 llevas puest
/107
coger¬
/108
No observas nada que te llame la atención
/109
No le ves nada que te llame la atención¬
/110
abrir¬
/111
Abres¬
/112
cerrar¬
/113
Cierras¬
/114
Ya está
/115
 abiert
/116
 cerrad
/117
meter¬
/118
sacar¬
/119
cosas en¬
/120
 mientras esté
/121
cosas¬
/122
No veo qué quieres sacar ni de dónde.\n
/123
dar¬
/124
 a¬
/125
, pertenece¬
/126
 no parece interesar¬
/127
No veo a quién quieres darle¬
/128
mostrar¬
/129
No veo a quién quieres mostrar¬
/130
, forma
/131
 parte¬
/132
No veo dónde quieres meter¬
/133
No veo de dónde quieres sacar¬
/134
No puedes meter cosas en¬
/135
Pones¬
/136
 en¬
/137
Sacas¬
/138
En¬
/139
 hay¬
/140
 no hay nada.\n
/141
No hay nada.\n
/142
Dudo que obtengas su colaboración.\n
/143
 no parece tener nada de eso.\n
/144
Es propiedad de¬
/145
Nunca ha sido tuy
/146
 está
/147
, y en su interior ves¬
/148
, pero está vacío.\n
/149
_:¬
/150
Juego guardado en memoria.
/151
.¬
/152
o
/153
a
/154
os
/155
as
/156
n
/157
el
/158
l
/159
El
/160
L
/161
un
/162
le
/163
s
/164
¬
/165
_
/166
 _
/167
d
/168
e¬
/169
,¬
/170
 _?\n
/171
 en¬
/172
_.\n
/173
 _.\n
/174
de¬
/175
 _,¬
/176
 _,
/177
Salidas visibles:¬
/178
.
/179
o¬
/180
a¬
/181
os¬
/182
as¬
/MTX
/0



/1



/2



/3



/4



/5



/6



/7



/8



/9



/10



/11



/12



/13



/14



/15



/16



/17



/18



/19



/20



/21



/22



/23



/24



/25



/26



/27



/28



/29



/30



/31



/32



/33



/34



/35



/36



/37



/38



/39



/40



/41



/42



/43



/44



/45



/46



/47



/48



/49



/50



/51



/52



/53



/54



/55



/56



/57



/58



/59



/60



/61



/62



/63



/64



/65



/66



/67



/68



/69



/70



/71



/72



/73



/74



/75



/76



/77



/78



/79



/80



/81



/82



/83



/84



/85



/86



/87



/88



/89



/90



/91



/92



/93



/94



/95



/96



/97



/98



/99



/100



/101



/102



/103



/104



/105



/106



/107



/108



/109



/110



/111



/112



/113



/114



/115



/116



/117



/118



/119



/120



/121



/122



/123



/124



/125



/126



/127



/128



/129



/130



/131



/132



/133



/134



/135



/136



/137



/138



/139



/140



/141



/142



/143



/144



/145



/146



/147



/148



/149



/150



/151



/152



/153



/154



/155



/156



/157



/158



/159



/160



/161



/162



/163



/164



/165



/166



/167



/168



/169



/170



/171



/172



/173



/174



/175



/176



/177



/178



/179



/180



/181



/182



/183



/184



/185



/186



/187



/188



/189



/190



/191



/192



/193



/194



/195



/196



/197



/198



/199



/200



/201



/202



/203



/204



/205



/206



/207



/208



/209



/210



/211



/212



/213



/214



/215



/216



/217



/218



/219



/220



/221



/222



/223



/224



/225



/226



/227



/228



/229



/230



/231



/232



/233



/234



/235



/236



/237



/238



/239



/240



/241



/242



/243



/244



/245



/246



/247



/248



/249



/250



/251



/252



/253



/OTX
/0 
trozo de pedernal
/1 
riachuelo
/2 
fosa
/3 
liana
/4 
piedra
/5 
tablón
/6 
botas
/7 
pistola neutralizante
/8 
bloque de hielo
/9 
cuadro electrico
/10 
compartimento del propulsor
/11 
gusano enorme
/12 
recipiente
/13 
espejo
/14 
criatura verde
/15 
esqueleto pulposo
/16 
espejo
/17 
babosa verde
/18 
babosa verde
/19 
guantes
/20 
tarjeta hexagonal
/21 
panel de control
/22 
robot
/23 
rejilla
/24 
barrote de hierro
/25 
succionador
/26 
dispositivo
/27 
nave
/28 
tecnico de mantenimiento
/29 
uniforme de mantenimiento
/30 
puerta de acero
/31 
botón de iniciar secuencia
/32 
botón de ignición
/33 
palanca
/34 
fusible fundido
/35 
ventana
/36 
canalizador de plasma
/37 
al norte
/38 
al sur
/39 
al este
/40 
al oeste
/41 
al nordeste
/42 
al noroeste
/43 
al sureste
/44 
al suroeste
/45 
subir
/46 
bajar
/47 
entrar
/48 
salir
/LTX
/0 
{CLASS|centrado|{CLASS|titulo|AVENTURA A: <i>PLANET OF DEATH</i>}
\n\n
 Adaptación libre por Mastodon (2009)
\n
A partir del original de Artic Computing de 1981}
\n\n
En esta aventura te encuentras varado en un misterioso y lejano planeta del que tienes que escapar tras recuperar tu nave espacial, que te ha sido arrebatada e inutilizada.\n
Te encontrarás con varios riesgos y peligros en tu aventura, algunos naturales, otros no, todos los cuales debes superar para tener éxito.\n
\n
¡Buena suerte, la vas a necesitar!
/1 

/2 

/3 

/4 

/5 

/6 

/7 
\n
Comedero de babosas\n
 Esta parte de la caverna verde es más amplia y más alta
/8 
\n
Meseta\n
 Estás en una pequeña meseta pedregosa sobre las montañas, al borde de un
pronunciado precipicio y que se extiende al este y al oeste. Bajo las
montañas ves un frondoso bosque al que se llega descendiendo por un estrecho
camino hacia el suroeste.
/9 

/10 

/11 
\n
Bosque\n
 Te encuentras en un poblado bosque con densa vegetación. Algunos árboles
dejan caer sus lianas hasta casi rozar el suelo. Un camino pedregoso desciende
de forma pronunciada desde el oeste, y continúa hacia el sur entre sombríos
parajes boscosos.
/12 
\n
Al borde de una fosa\n
 La meseta finaliza bruscamente en una escarpada pendiente imposible de
escalar, tachonada de grandes moles de piedra.
/13 
\n
En suspensión
/14 
\n
Junto al lago\n
 Un lago de aguas gélidas se extiende a lo que tu vista alcanza en dirección
sur. Esta debió ser una zona habitada, a juzgar por la extraña construcción
que hay hacia el este y el cobertizo hacia el oeste. El camino al oeste
está bloqueado por un riachuelo que desemboca en el lago. El sendero del
bosque conduce al norte.
/15 
\n
Remolino en el lago\n
 Te encuentras de pie, unos metros dentro del lago junto a un remolino que
forma la desembocadura del riachuelo en el lago.
/16 
\n
Construcción alienígena\n
 Una extraña construcción de piedras y barro. Tiene forma hexagonal, con
altas ventanas circulares y un simple hueco al oeste a modo de entrada.
El suelo está formado por grandes tablones de madera encajados, probablemente
procedentes del bosque cercano.
/17 
\n
Viejo cobertizo\n
 Estás en el interior de un viejo cobertizo, una especie de almacén aparentemente
abandonado. Numerosos utensilios, la mayoría desconocidos para tí, adornan
sus paredes.
/18 
\n
Cavidad húmeda\n
 Una amplia cavidad con paredes de piedra caliza cubiertas de humedad, de
cuyo techo cuelga una miríada de estalactitas de caprichosas formas. En
una de las paredes observas una pintura.\n
 La cavidad se estrecha y continúa hacia el norte en una leve pendiente
descendente, y se abre hacia el oeste al exterior.
/19 
\n
Gruta glaciar\n
 En este lugar de la cueva la temperatura es realmente baja, los altos muros
que te rodean están cubiertos por enormes placas de hielo, y el techo semeja
la bóveda de una nívea catedral. La cavidad continúa hacia el sur bajando
por un empinado terraplén cubierto de afilados trozos de hielo incrustados
en el suelo. Un estrecho pasaje conduce al este.
/20 
\n
Túnel sinuoso\n
 Te encuentras en un túnel sinuoso. Al final del túnel, hacia el este, ves
una puerta. El túnel continúa hacia el oeste.
/21 
\n
Sala de computadoras\n
 Las paredes de esta sala están repletas de monitores y cuadros de mando,
teclados y otros dispositivos para tí desconocidos, y probablemente destinados
al control electrónico de la base. Un monitor gigante ocupa buena parte
de una de las paredes. La salida está al oeste.
/22 
\n
Sala de control\n
 Una pequeña sala circular de techo abovedado semejando un panteón, que
da paso al complejo de cavernas verdes hacia el este. En el centro de la
estancia hay un panel con varios controles. Un estrecho pasillo continua
hacia el sur.
/23 

/24 
\n
Sala de basuras\n
 Estás en un diminuto cuartucho de paredes metálicas utilizado para salida
de residuos, como parece deducirse por el succionador de basuras y la rejilla
en el suelo, de la que se desprende un fuerte olor. La única salida visible
es hacia el norte.¬
/25 
\n
Estrecho cubículo\n
 Estás en una pequeña sala de 2 x 2 metros sin ningún tipo de elemento distinguible
/26 
\n
Vertedero\n
 Estás en una montaña de basura y residuos al aire libre. La única salida
es hacia abajo.
/27 
\n
Pasaje acolchado\n
 Estás en un amplio pasillo de paredes acolchadas que discurre de norte
a sur. En dirección al sur, hay una hilera de orificios en el suelo, junto
a un extraño dispositivo.
/28 
\n
Hangar\n
 El corazón del complejo, el lugar común de todo el tráfico de este planeta.
Varios aparatos están estacionados o siendo revisados por extraños hombrecillos
de aspecto humanoide. Hay una puerta de acero al oeste, una gran compuerta
al este, probablemente destinada a la salida de las naves, y un amplio pasillo
acolchado que conduce hacia el norte.
/29 
\n
Tu nave\n
 Tu nave es un pequeño y potente vehículo de exploración. De todo el complejo
cuadro de mandos que tienes ante ti sólo te interesan en este momento dos
botones: INICIAR SECUENCIA e IGNICION.
/30 
\n
Sala de mantenimiento\n
 Una estrecha estancia, casi un pasillo, con diversa maquinaria de control.
El camino al este está bloqueado por una puerta de acero. El pasillo continúa
hacia el sur.
/31 
\n
Final del pasillo\n
 El pasillo discurre de norte a sur, donde finaliza frente a una puerta.
La puerta tiene una pequeña ventana redonda en el centro.
/32 
\n
Talleres\n
 Este lugar está repleto de mesas donde bulle la actividad, instrumental
científico de diversa naturaleza, maquinaria de todos los tamaños, así como
numerosas piezas que supones pertenecen a las naves del hangar. La única
salida es atravesar el arco hacia el norte.
/33 

/CON
/0 ; sust(loc_INICIAL//0)
/1 ; sust(loc_LOCRIACHUELO//1)
/2 ; sust(loc_OBJFOSALOC//2)
/3 ; sust(loc_CAVERNAVERDE//3)
	O	6 ; sust(loc_CAVERNAVERDE4/6)
	NO	7 ; sust(loc_CAVERNAVERDE5/7)
	S	4 ; sust(loc_CAVERNAVERDE2/4)
	E	20 ; sust(loc_TUNELSINUOSO/20)
/4 ; sust(loc_CAVERNAVERDE2//4)
	N	3 ; sust(loc_CAVERNAVERDE/3)
	E	5 ; sust(loc_CAVERNAVERDE3/5)
	S	27 ; sust(loc_PASAJEACOLCHADO/27)
/5 ; sust(loc_CAVERNAVERDE3//5)
	O	4 ; sust(loc_CAVERNAVERDE2/4)
/6 ; sust(loc_CAVERNAVERDE4//6)
	E	3 ; sust(loc_CAVERNAVERDE/3)
	N	7 ; sust(loc_CAVERNAVERDE5/7)
	O	22 ; sust(loc_SALADECONTROL/22)
/7 ; sust(loc_CAVERNAVERDE5//7)
	S	6 ; sust(loc_CAVERNAVERDE4/6)
	SE	3 ; sust(loc_CAVERNAVERDE/3)
/8 ; sust(loc_MESETA//8)
	E	18 ; sust(loc_CAVIDADHUMEDA/18)
	O	12 ; sust(loc_FOSA/12)
/9 ; sust(loc_LOCCUADROELECTRICO//9)
/10 ; sust(loc_LOCCOMPARTIMENTO//10)
/11 ; sust(loc_BOSQUEDENSO//11)
	S	14 ; sust(loc_JUNTOLAGO/14)
/12 ; sust(loc_FOSA//12)
	E	8 ; sust(loc_MESETA/8)
/13 ; sust(loc_LOCSUSPENDIDO//13)
	SUBE	12 ; sust(loc_FOSA/12)
/14 ; sust(loc_JUNTOLAGO//14)
	N	11 ; sust(loc_BOSQUEDENSO/11)
	E	16 ; sust(loc_EXTRANACONSTRUCCION/16)
/15 ; sust(loc_LOCREMOLINO//15)
	N	14 ; sust(loc_JUNTOLAGO/14)
/16 ; sust(loc_EXTRANACONSTRUCCION//16)
	O	14 ; sust(loc_JUNTOLAGO/14)
	SALIR	14 ; sust(loc_JUNTOLAGO/14)
/17 ; sust(loc_COBERTIZO//17)
	E	14 ; sust(loc_JUNTOLAGO/14)
	SALIR	14 ; sust(loc_JUNTOLAGO/14)
/18 ; sust(loc_CAVIDADHUMEDA//18)
	O	8 ; sust(loc_MESETA/8)
/19 ; sust(loc_GRUTAGLACIAR//19)
	E	18 ; sust(loc_CAVIDADHUMEDA/18)
/20 ; sust(loc_TUNELSINUOSO//20)
	O	3 ; sust(loc_CAVERNAVERDE/3)
	E	21 ; sust(loc_SALACOMPUTADORA/21)
/21 ; sust(loc_SALACOMPUTADORA//21)
	O	20 ; sust(loc_TUNELSINUOSO/20)
/22 ; sust(loc_SALADECONTROL//22)
	E	6 ; sust(loc_CAVERNAVERDE4/6)
	S	24 ; sust(loc_SALADEBASURAS/24)
/23 ; sust(loc_LOCROBOT//23)
/24 ; sust(loc_SALADEBASURAS//24)
	N	22 ; sust(loc_SALADECONTROL/22)
/25 ; sust(loc_ESTRECHOCUBICULO//25)
/26 ; sust(loc_VERTEDERO//26)
/27 ; sust(loc_PASAJEACOLCHADO//27)
	N	4 ; sust(loc_CAVERNAVERDE2/4)
/28 ; sust(loc_HANGAR//28)
	N	27 ; sust(loc_PASAJEACOLCHADO/27)
/29 ; sust(loc_TUNAVE//29)
	SALIR	28 ; sust(loc_HANGAR/28)
	BAJA	28 ; sust(loc_HANGAR/28)
/30 ; sust(loc_SALADEMANTENIMIENTO//30)
	S	31 ; sust(loc_FINPASILLO/31)
/31 ; sust(loc_FINPASILLO//31)
	S	32 ; sust(loc_TALLERES/32)
	N	30 ; sust(loc_SALADEMANTENIMIENTO/30)
/32 ; sust(loc_TALLERES//32)
	N	31 ; sust(loc_FINPASILLO/31)
/33 ; sust(loc_LOC_COMPAS//33)
/OBJ
/0	8	1	PEDERNAL	_	10000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJPEDERNAL//0) ; sust(loc_MESETA/8)
/1	14	1	RIO	_	00100000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJRIACHUELO//1) ; sust(loc_JUNTOLAGO/14)
/2	12	1	FOSA	_	00100000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJFOSA//2) ; sust(loc_FOSA/12)
/3	11	2	LIANA	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJLIANA//3) ; sust(loc_BOSQUEDENSO/11)
/4	12	2	MOLE	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJPIEDRA//4) ; sust(loc_FOSA/12)
/5	252	1	TABLON	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJTABLON//5)
/6	16	4	BOTAS	_	01000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJBOTAS//6) ; sust(loc_EXTRANACONSTRUCCION/16)
/7	17	2	PISTOLA	NEUTRALIZA	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJPISTOLANEUTRALIZANTE//7) ; sust(loc_COBERTIZO/17)
/8	252	1	BLOQUE	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJBLOQUEHIELO//8)
/9	30	1	CUADRO	_	00100000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJCUADROELECTRICO//9) ; sust(loc_SALADEMANTENIMIENTO/30)
/10	28	1	COMPARTIME	_	00100000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJCOMPARTIMENTO//10) ; sust(loc_HANGAR/28)
/11	7	1	GUSANO	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJGUSANO//11) ; sust(loc_CAVERNAVERDE5/7)
/12	7	1	RECIPIENTE	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJRECIPIENTEGUSANO//12) ; sust(loc_CAVERNAVERDE5/7)
/13	252	1	ESPEJO	ESTATICO	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJESPEJOSALIDA//13)
/14	5	2	CRIATURA	VERDE	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJCRIATURAVERDE//14) ; sust(loc_CAVERNAVERDE3/5)
/15	252	1	ESQUELETO	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJESQUELETOPULPOSO//15)
/16	5	1	ESPEJO	DEMANO	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJESPEJO//16) ; sust(loc_CAVERNAVERDE3/5)
/17	6	2	BABOSA	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJBABOSA1//17) ; sust(loc_CAVERNAVERDE4/6)
/18	6	2	BABOSA	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJBABOSA2//18) ; sust(loc_CAVERNAVERDE4/6)
/19	20	3	GUANTES	_	01000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJGUANTES//19) ; sust(loc_TUNELSINUOSO/20)
/20	21	2	TARJETA	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJTARJETA//20) ; sust(loc_SALACOMPUTADORA/21)
/21	22	1	PANEL	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJPANELCONTROL//21) ; sust(loc_SALADECONTROL/22)
/22	22	1	ROBOT	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJROBOT//22) ; sust(loc_SALADECONTROL/22)
/23	24	2	REJILLA	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJREJILLA//23) ; sust(loc_SALADEBASURAS/24)
/24	252	1	BARROTE	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJBARROTEHIERRO//24)
/25	24	1	SUCCIONADO	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJSUCCIONADOR//25) ; sust(loc_SALADEBASURAS/24)
/26	27	1	DISPOSITIV	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJDISPOSITIVO//26) ; sust(loc_PASAJEACOLCHADO/27)
/27	28	2	NAVE	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJNAVE//27) ; sust(loc_HANGAR/28)
/28	28	1	TECNICO	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJTECNICO//28) ; sust(loc_HANGAR/28)
/29	252	1	UNIFORME	_	01000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJUNIFORME//29)
/30	28	2	PUERTA	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJPUERTAACERO//30) ; sust(loc_HANGAR/28)
/31	29	1	INICIAR	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJBOTONINICIARSECUENCIA//31) ; sust(loc_TUNAVE/29)
/32	29	1	IGNICION	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJBOTONIGNICION//32) ; sust(loc_TUNAVE/29)
/33	30	2	PALANCA	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJPALANCALLENADO//33) ; sust(loc_SALADEMANTENIMIENTO/30)
/34	9	1	FUSIBLE	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJFUSIBLEFUNDIDO//34) ; sust(loc_LOCCUADROELECTRICO/9)
/35	31	2	VENTANA	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJVENTANA//35) ; sust(loc_FINPASILLO/31)
/36	32	1	CANALIZADO	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJCANALIZADORPLASMA//36) ; sust(loc_TALLERES/32)
/37	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_NORTE//37)
/38	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_SUR//38)
/39	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_ESTE//39)
/40	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_OESTE//40)
/41	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_NORDESTE//41)
/42	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_NOROESTE//42)
/43	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_SURESTE//43)
/44	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_SUROESTE//44)
/45	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_ARRIBA//45)
/46	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_ABAJO//46)
/47	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_ENTRAR//47)
/48	252	1	_	_	00000000000000000000000000000000 00000000000000000000000000000000 ; sust(obj_OBJ_SALIR//48)

/PRO 0

_	TODO	NOTEQ 33 50 
		NOTEQ 33 44 
		SYSMESS 105 
		DONE

_	GUSANO	PRESENT 22  ; sust(obj_OBJROBOT/22)
		WHATO 
		EQ 54 23 
		WRITE "Está flotando sobre el robot."
		NEWLINE 
		DONE

_	CRIATURA	PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "No te molestes, los dos estáis en una situación comprometida."
		NEWLINE 
		DONE

_	CRIATURA	PRESENT 22  ; sust(obj_OBJROBOT/22)
		WHATO 
		EQ 54 23 
		WRITE "Está flotando sobre el robot."
		NEWLINE 
		DONE

_	CONTROLES	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		LET 34 81

_	BOTON	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		NOTEQ 44 9 
		NOTEQ 44 10 
		NOTEQ 44 13 
		NOTEQ 44 2 
		NOTEQ 44 3 
		NOTEQ 44 4 
		WRITE "¿A cuál te refieres?"
		NEWLINE 
		DONE

_	BOTON	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		COPYFF 44 34 
		COPYFF 45 35

_	BOTON	AT 29  ; sust(loc_TUNAVE/29)
		NOTEQ 44 93 
		NOTEQ 44 94 
		WRITE "¿A cuál te refieres?"
		NEWLINE 
		DONE

_	BOTON	AT 29  ; sust(loc_TUNAVE/29)
		COPYFF 44 34 
		COPYFF 45 35

S	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		NOTWORN 6  ; sust(obj_OBJBOTAS/6)
		WRITE "El agua está helada, y el suelo resbaloso."
		NEWLINE 
		DONE

S	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		WRITE "Te adentras unos pasos en el lago, hasta llegar al remolino que forma el riachuelo…"
		NEWLINE 
		GOTO 15  ; sust(loc_LOCREMOLINO/15)
		DESC

S	_	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		WRITE "Te diriges al terraplen, pero al poner pie en él resbalas por la presencia de placas de hielo. El descenso no es menos doloroso al arrastrarte entre trozos afilados de hielo partido."
		NEWLINE 
		SCORE 
		TURNS 
		END

S	_	AT 27  ; sust(loc_PASAJEACOLCHADO/27)
		ZERO 26  ; sust(flag_CAMPOFUERZADESTRUIDO/26)
		WRITE "¡Plong! Al llegar a la altura de los orificios te topas con un impenetrable campo de fuerza que te impide avanzar."
		NEWLINE 
		DONE

S	_	AT 27  ; sust(loc_PASAJEACOLCHADO/27)
		GOTO 28  ; sust(loc_HANGAR/28)
		DESC

E	_	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		CARRIED 5  ; sust(obj_OBJTABLON/5)
		WRITE "No puedes pasar con el tablón, la cavidad es muy estrecha."
		NEWLINE 
		DONE

E	_	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		WRITE "Atraviesas la cavidad, que da un repentino giro hacia el " 
		WRITE "sur."
		NEWLINE 
		GOTO 18  ; sust(loc_CAVIDADHUMEDA/18)
		DESC

O	_	AT 11  ; sust(loc_BOSQUEDENSO/11)
		WRITE "El camino es escarpado y sinuoso, apareces mirando hacia el " 
		WRITE "nordeste."
		NEWLINE 
		GOTO 8  ; sust(loc_MESETA/8)
		DESC

O	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		ZERO 102  ; sust(flag_TABLONENRIACHUELO/102)
		WRITE "El riachuelo es demasiado profundo y caudaloso para cruzarlo."
		NEWLINE 
		DONE

O	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		GOTO 17  ; sust(loc_COBERTIZO/17)
		DESC

SO	_	AT 8  ; sust(loc_MESETA/8)
		WRITE "El camino es escarpado y sinuoso, apareces mirando hacia el " 
		WRITE "este."
		NEWLINE 
		GOTO 11  ; sust(loc_BOSQUEDENSO/11)
		DESC

SUBE	ARBOL	AT 11  ; sust(loc_BOSQUEDENSO/11)
		WRITE "Trepas un rato por una de las lianas, pero al ver que el camino hasta arriba es más largo de lo que parece desistes."
		NEWLINE 
		DONE

SUBE	LIANA	AT 11  ; sust(loc_BOSQUEDENSO/11)
		WRITE "Trepas un rato por una de las lianas, pero al ver que el camino hasta arriba es más largo de lo que parece desistes."
		NEWLINE 
		DONE

SUBE	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		PRESENT 8  ; sust(obj_OBJBLOQUEHIELO/8)
		PROCESS 57  ; sust(proc_MONTAHIELOPROC/57)
		DONE

SUBE	_	AT 11  ; sust(loc_BOSQUEDENSO/11)
		WRITE "El camino es escarpado y sinuoso, apareces mirando hacia el " 
		WRITE "nordeste."
		NEWLINE 
		GOTO 8  ; sust(loc_MESETA/8)
		DESC

SUBE	_	PRESENT 27  ; sust(obj_OBJNAVE/27)
		GOTO 29  ; sust(loc_TUNAVE/29)
		DESC

BAJA	FOSA	AT 12  ; sust(loc_FOSA/12)
		NOUN2 LIANA 
		CARRIED 3  ; sust(obj_OBJLIANA/3)
		ZERO 85  ; sust(flag_LIANAATADA/85)
		WRITE "Deberías atarla a algo consistente antes de escalar."
		NEWLINE 
		DONE

BAJA	_	AT 8  ; sust(loc_MESETA/8)
		WRITE "El camino es escarpado y sinuoso, apareces mirando hacia el " 
		WRITE "este."
		NEWLINE 
		GOTO 11  ; sust(loc_BOSQUEDENSO/11)
		DESC

BAJA	_	AT 12  ; sust(loc_FOSA/12)
		ZERO 85  ; sust(flag_LIANAATADA/85)
		WRITE "La fosa es demasiado profunda para tirarte por ella."
		NEWLINE 
		DONE

BAJA	_	AT 12  ; sust(loc_FOSA/12)
		CARRIED 5  ; sust(obj_OBJTABLON/5)
		WRITE "O el tablón o tú, los dos no cabeis por la fosa."
		NEWLINE 
		DONE

BAJA	_	AT 12  ; sust(loc_FOSA/12)
		CLEAR 65  ; sust(flag_ESTADOSUSPENSION/65)
		CLEAR 61  ; sust(flag_CONTADORBALANCEO/61)
		WRITE "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo."
		NEWLINE 
		GOTO 13  ; sust(loc_LOCSUSPENDIDO/13)
		DESC

BAJA	_	AT 13  ; sust(loc_LOCSUSPENDIDO/13)
		WRITE "Te sueltas de la liana y caes en " 
		PROCESS 86  ; sust(proc_SUELTALIANAPROC/86)
		DONE

BAJA	_	AT 26  ; sust(loc_VERTEDERO/26)
		WRITE "Caes rodando por los restos de basura…"
		NEWLINE 
		GOTO 12  ; sust(loc_FOSA/12)
		DESC

BAJA	_	NOTZERO 84  ; sust(flag_LANZAMIENTOINICIADO/84)
		WRITE "¡La nave está en posición de lanzamiento, no puedes bajar!"
		NEWLINE 
		DONE

ENTRA	FOSA	AT 12  ; sust(loc_FOSA/12)
		NOUN2 LIANA 
		CARRIED 3  ; sust(obj_OBJLIANA/3)
		ZERO 85  ; sust(flag_LIANAATADA/85)
		WRITE "Deberías atarla a algo consistente antes de escalar."
		NEWLINE 
		DONE

ENTRA	LAGO	AT 14  ; sust(loc_JUNTOLAGO/14)
		NOTWORN 6  ; sust(obj_OBJBOTAS/6)
		WRITE "El agua está helada, y el suelo resbaloso."
		NEWLINE 
		DONE

ENTRA	LAGO	AT 14  ; sust(loc_JUNTOLAGO/14)
		WRITE "Te adentras unos pasos en el lago, hasta llegar al remolino que forma el riachuelo…"
		NEWLINE 
		GOTO 15  ; sust(loc_LOCREMOLINO/15)
		DESC

ENTRA	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		PRESENT 8  ; sust(obj_OBJBLOQUEHIELO/8)
		PROCESS 57  ; sust(proc_MONTAHIELOPROC/57)
		DONE

ENTRA	SUCCIONADO	PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		ISNOTAT 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE "El mecanismo de absorción se activa y una fuerza irresistible te chupa al interior. Apareces en…"
		NEWLINE 
		GOTO 25  ; sust(loc_ESTRECHOCUBICULO/25)
		DESC

ENTRA	SUCCIONADO	PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		WRITE "Está atascado."
		NEWLINE 
		DONE

ENTRA	NAVE	PRESENT 27  ; sust(obj_OBJNAVE/27)
		GOTO 29  ; sust(loc_TUNAVE/29)
		DESC

ENTRA	_	AT 12  ; sust(loc_FOSA/12)
		ZERO 85  ; sust(flag_LIANAATADA/85)
		WRITE "La fosa es demasiado profunda para tirarte por ella."
		NEWLINE 
		DONE

ENTRA	_	AT 12  ; sust(loc_FOSA/12)
		CARRIED 5  ; sust(obj_OBJTABLON/5)
		WRITE "O el tablón o tú, los dos no cabeis por la fosa."
		NEWLINE 
		DONE

ENTRA	_	AT 12  ; sust(loc_FOSA/12)
		CLEAR 65  ; sust(flag_ESTADOSUSPENSION/65)
		CLEAR 61  ; sust(flag_CONTADORBALANCEO/61)
		WRITE "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo."
		NEWLINE 
		GOTO 13  ; sust(loc_LOCSUSPENDIDO/13)
		DESC

SALIR	_	AT 26  ; sust(loc_VERTEDERO/26)
		WRITE "Caes rodando por los restos de basura…"
		NEWLINE 
		GOTO 12  ; sust(loc_FOSA/12)
		DESC

SALIR	_	NOTZERO 84  ; sust(flag_LANZAMIENTOINICIADO/84)
		WRITE "¡La nave está en posición de lanzamiento, no puedes bajar!"
		NEWLINE 
		DONE

N	_	AT 11  ; sust(loc_BOSQUEDENSO/11)
		WRITE "Te topas con la montaña. El camino hacia la meseta está al oeste."
		NEWLINE 
		DONE

N	_	AT 18  ; sust(loc_CAVIDADHUMEDA/18)
		CARRIED 5  ; sust(obj_OBJTABLON/5)
		WRITE "No puedes pasar con el tablón, la cavidad es muy estrecha."
		NEWLINE 
		DONE

N	_	AT 18  ; sust(loc_CAVIDADHUMEDA/18)
		WRITE "Atraviesas la cavidad, que da un repentino giro hacia el " 
		WRITE "oeste."
		NEWLINE 
		GOTO 19  ; sust(loc_GRUTAGLACIAR/19)
		DESC

I	_	PROCESS 46  ; sust(proc_INVENTARIO/46)
		DONE

I	_	SYSMESS 9 
		LISTAT 254 
		SYSMESS 10 
		LISTAT 253 
		DONE

TIRA	TABLON	AT 16  ; sust(loc_EXTRANACONSTRUCCION/16)
		ZERO 101  ; sust(flag_TABLONCOGIDO/101)
		SET 101  ; sust(flag_TABLONCOGIDO/101)
		PLACE 5 254  ; sust(obj_OBJTABLON/5)
		PLUS 30 1  ; sust(flag_PUNTUACION/30)
		WRITE "Tiras con fuerza de uno de los tablones hasta desprenderlo del suelo."
		NEWLINE 
		DONE

TIRA	TABLON	AT 16  ; sust(loc_EXTRANACONSTRUCCION/16)
		ABSENT 5  ; sust(obj_OBJTABLON/5)
		WRITE "No puedes arrancar más."
		NEWLINE 
		DONE

TIRA	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		LET 33 250

TIRA	BABOSA	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "Al acercarte al bicho, te salpica con sus pútridos líquidos corrosivos que te penetran y despedazan en cuestión de segundos."
		NEWLINE 
		SCORE 
		TURNS 
		END

TIRA	BARROTE	AT 24  ; sust(loc_SALADEBASURAS/24)
		ZERO 100  ; sust(flag_REJILLASINBARROTE/100)
		WRITE "No está lo suficientemente suelto. Es necesario un método más contundente."
		NEWLINE 
		DONE

TIRA	BARROTE	ABSENT 24  ; sust(obj_OBJBARROTEHIERRO/24)
		AT 24  ; sust(loc_SALADEBASURAS/24)
		WRITE "Ya has tenido una experiencia con ese tema, no necesitas más."
		NEWLINE 
		DONE

TIRA	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		ZERO 66  ; sust(flag_ESTADOTECNICO/66)
		WRITE "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor."
		NEWLINE 
		SCORE 
		TURNS 
		END

TIRA	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		WRITE "Déjalo que se recupere."
		NEWLINE 
		DONE

TIRA	PALANCA	PRESENT 33  ; sust(obj_OBJPALANCALLENADO/33)
		ISNOTAT 24 9  ; sust(obj_OBJBARROTEHIERRO/24)
		WRITE "Tiras de la palanca. " 
		WRITE "Oyes un 'clong' seco dentro del cuadro, y luego silencio. " 
		WRITE "La palanca vuelve lentamente a su posición inicial."
		NEWLINE 
		DONE

TIRA	PALANCA	PRESENT 33  ; sust(obj_OBJPALANCALLENADO/33)
		NOTZERO 103  ; sust(flag_TANQUEHIDRAULICOLLENADO/103)
		WRITE "Tiras de la palanca. " 
		WRITE "La palanca vuelve lentamente a su posición inicial."
		NEWLINE 
		DONE

TIRA	PALANCA	PRESENT 33  ; sust(obj_OBJPALANCALLENADO/33)
		SET 103  ; sust(flag_TANQUEHIDRAULICOLLENADO/103)
		PLUS 30 3  ; sust(flag_PUNTUACION/30)
		WRITE "Tiras de la palanca. " 
		WRITE "En algún lugar hacia el este oyes algo parecido a la apertura de una esclusa y el inconfudible sonido de líquido fluyendo. " 
		WRITE "La palanca vuelve lentamente a su posición inicial."
		NEWLINE 
		DONE

TIRA	_	AT 8  ; sust(loc_MESETA/8)
		PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		NOUN2 PRECIPICIO 
		WRITE "Sería una pérdida segura."
		NEWLINE 
		DONE

TIRA	_	AT 12  ; sust(loc_FOSA/12)
		NOUN2 FOSA 
		PROCESS 55  ; sust(proc_METERSUB/55)
		DONE

TIRA	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		NOUN2 LAGO 
		WRITE "Sería una pérdida segura."
		NEWLINE 
		DONE

TIRA	_	NOUN2 BABOSA 
		WHATO 
		EQ 54 254 
		LET 44 255 
		COPYFF 38 67  ; sust(flag_FLAGPRM1/67)
		PROCESS 42  ; sust(proc_HAYBABOSA/42)
		LET 44 75 
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "Eso sería garantía de pérdida. La babosa es muy corrosiva."
		NEWLINE 
		DONE

TIRA	_	NOUN2 SUCCIONADO 
		PROCESS 55  ; sust(proc_METERSUB/55)
		DONE

TIRA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		PREP A 
		PROCESS 55  ; sust(proc_METERSUB/55)
		DONE

TIRA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		PREP EN 
		PROCESS 55  ; sust(proc_METERSUB/55)
		DONE

TIRA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No le ves mucho sentido ahora a tirar " 
		PROCESS 67  ; sust(proc_PRINTFIRSTDEL/67)
		WRITE ".\n" 
		DONE

TIRA	_	WRITE "No lo ves como una opción adecuada en este momento."
		NEWLINE 
		DONE

ARROJA	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		LET 33 250

ARROJA	_	AT 8  ; sust(loc_MESETA/8)
		PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		NOUN2 PRECIPICIO 
		WRITE "Sería una pérdida segura."
		NEWLINE 
		DONE

ARROJA	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		NOUN2 LAGO 
		WRITE "Sería una pérdida segura."
		NEWLINE 
		DONE

ARROJA	_	NOUN2 BABOSA 
		WHATO 
		EQ 54 254 
		LET 44 255 
		COPYFF 38 67  ; sust(flag_FLAGPRM1/67)
		PROCESS 42  ; sust(proc_HAYBABOSA/42)
		LET 44 75 
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "Eso sería garantía de pérdida. La babosa es muy corrosiva."
		NEWLINE 
		DONE

ARROJA	_	PROCESS 55  ; sust(proc_METERSUB/55)
		DONE

NADA	RIO	AT 14  ; sust(loc_JUNTOLAGO/14)
		WRITE "El riachuelo es demasiado profundo y caudaloso para cruzarlo."
		NEWLINE 
		DONE

NADA	_	WRITE "No lo ves como una opción adecuada en este momento."
		NEWLINE 
		DONE

CRUZA	RIO	AT 14  ; sust(loc_JUNTOLAGO/14)
		ZERO 102  ; sust(flag_TABLONENRIACHUELO/102)
		WRITE "El riachuelo es demasiado profundo y caudaloso para cruzarlo."
		NEWLINE 
		DONE

CRUZA	RIO	AT 14  ; sust(loc_JUNTOLAGO/14)
		GOTO 17  ; sust(loc_COBERTIZO/17)
		DESC

SALTA	RIO	AT 14  ; sust(loc_JUNTOLAGO/14)
		WRITE "El riachuelo es demasiado profundo y caudaloso para cruzarlo."
		NEWLINE 
		DONE

SALTA	_	AT 13  ; sust(loc_LOCSUSPENDIDO/13)
		WRITE "Te sueltas de la liana y caes en " 
		PROCESS 86  ; sust(proc_SUELTALIANAPROC/86)
		DONE

SALTA	_	AT 24  ; sust(loc_SALADEBASURAS/24)
		PROCESS 85  ; sust(proc_SALTAREJILLA/85)
		DONE

SALTA	_	WRITE "Saltas en el sitio. Fútil."
		NEWLINE 
		DONE

PUNTOS	_	SYSMESS 21 
		PRINT 30  ; sust(flag_PUNTUACION/30)
		SYSMESS 22 
		DONE

PATEA	_	WORN 6  ; sust(obj_OBJBOTAS/6)
		LET 33 27 
		LET 43 2 
		LET 44 61

PATEA	_	NOTWORN 6  ; sust(obj_OBJBOTAS/6)
		LET 33 27 
		LET 43 2 
		LET 44 66

PATEA	_	WRITE "No parece que esa opción violenta sea la solución en este momento."
		NEWLINE 
		DONE

ROMPE	LIANA	AT 11  ; sust(loc_BOSQUEDENSO/11)
		LET 33 28

ROMPE	TABLON	AT 16  ; sust(loc_EXTRANACONSTRUCCION/16)
		ZERO 101  ; sust(flag_TABLONCOGIDO/101)
		SET 101  ; sust(flag_TABLONCOGIDO/101)
		PLACE 5 254  ; sust(obj_OBJTABLON/5)
		PLUS 30 1  ; sust(flag_PUNTUACION/30)
		WRITE "Tiras con fuerza de uno de los tablones hasta desprenderlo del suelo."
		NEWLINE 
		DONE

ROMPE	TABLON	AT 16  ; sust(loc_EXTRANACONSTRUCCION/16)
		ABSENT 5  ; sust(obj_OBJTABLON/5)
		WRITE "No puedes arrancar más."
		NEWLINE 
		DONE

ROMPE	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		PREP CON 
		PROCESS 82  ; sust(proc_ROMPEHIELO/82)
		DONE

ROMPE	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		WRITE "¿Así, con las manos?"
		NEWLINE 
		DONE

ROMPE	GUSANO	PRESENT 11  ; sust(obj_OBJGUSANO/11)
		WRITE "Está fuera de tus posibilidades."
		NEWLINE 
		DONE

ROMPE	CRIATURA	PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		ZERO 64  ; sust(flag_ESPEJOMOVIDO/64)
		WRITE "Podrías romper el espejo, y parece un objeto muy valioso aquí."
		NEWLINE 
		DONE

ROMPE	PUERTA	PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		WRITE "Fútil."
		NEWLINE 
		DONE

ROMPE	REJILLA	AT 24  ; sust(loc_SALADEBASURAS/24)
		NOUN2 PIE 
		PROCESS 85  ; sust(proc_SALTAREJILLA/85)
		DONE

ROMPE	REJILLA	AT 24  ; sust(loc_SALADEBASURAS/24)
		WORN 6  ; sust(obj_OBJBOTAS/6)
		NOUN2 BOTAS 
		PROCESS 85  ; sust(proc_SALTAREJILLA/85)
		DONE

ROMPE	BARROTE	AT 24  ; sust(loc_SALADEBASURAS/24)
		NOUN2 PIE 
		PROCESS 85  ; sust(proc_SALTAREJILLA/85)
		DONE

ROMPE	BARROTE	AT 24  ; sust(loc_SALADEBASURAS/24)
		WORN 6  ; sust(obj_OBJBOTAS/6)
		NOUN2 BOTAS 
		PROCESS 85  ; sust(proc_SALTAREJILLA/85)
		DONE

ROMPE	BARROTE	AT 24  ; sust(loc_SALADEBASURAS/24)
		ZERO 100  ; sust(flag_REJILLASINBARROTE/100)
		WRITE "No está lo suficientemente suelto. Es necesario un método más contundente."
		NEWLINE 
		DONE

ROMPE	BARROTE	ABSENT 24  ; sust(obj_OBJBARROTEHIERRO/24)
		AT 24  ; sust(loc_SALADEBASURAS/24)
		WRITE "Ya has tenido una experiencia con ese tema, no necesitas más."
		NEWLINE 
		DONE

ROMPE	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		PROCESS 40  ; sust(proc_GOLPEATECNICO/40)
		DONE

ROMPE	_	WRITE "No parece que esa opción violenta sea la solución en este momento."
		NEWLINE 
		DONE

CORTA	LIANA	AT 11  ; sust(loc_BOSQUEDENSO/11)
		CARRIED 0  ; sust(obj_OBJPEDERNAL/0)
		ZERO 86  ; sust(flag_LIANACORTADA/86)
		SET 86  ; sust(flag_LIANACORTADA/86)
		PLUS 30 1  ; sust(flag_PUNTUACION/30)
		WRITE "Cortas una de las lianas con el pedernal, y cae al suelo formando una útil cuerda."
		NEWLINE 
		DONE

CORTA	LIANA	AT 11  ; sust(loc_BOSQUEDENSO/11)
		CARRIED 0  ; sust(obj_OBJPEDERNAL/0)
		WRITE "No necesitas podar el bosque entero."
		NEWLINE 
		DONE

CORTA	LIANA	AT 11  ; sust(loc_BOSQUEDENSO/11)
		WRITE "Las lianas son bastante resistentes, necesitas el material adecuado para cortarlas."
		NEWLINE 
		DONE

CORTA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No le ves sentido a cortar " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE " en este momento."
		NEWLINE 
		DONE

CORTA	_	WRITE "No le ves sentido a cortar eso en este momento."
		NEWLINE 
		DONE

EMPUJA	S	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

EMPUJA	E	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

EMPUJA	O	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

EMPUJA	ARRIBA	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

EMPUJA	ABAJO	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

EMPUJA	N	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

EMPUJA	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		PRESENT 8  ; sust(obj_OBJBLOQUEHIELO/8)
		DESTROY 8  ; sust(obj_OBJBLOQUEHIELO/8)
		WRITE "Le das un suave empujoncito al bloque de hielo, que se desliza por la pendiente perdiéndose de vista al instante."
		NEWLINE 
		DONE

EMPUJA	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		WRITE "Son enormes bloques, y están incrustados en la roca."
		NEWLINE 
		DONE

EMPUJA	GUSANO	PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE "Ya ocupa todo el cubículo de abajo, no entra más."
		NEWLINE 
		DONE

EMPUJA	CRIATURA	PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		NOTAT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "Al poner la mano sobre la criatura, se " 
		WRITE "despierta repentinamente y dando un ágil salto se cuelga de tu hombro. Instantes después notas sus largas manos y afiladas uñas cerrarse en torno a tu cuello. Pierdes la conciencia al cabo de poco rato."
		NEWLINE 
		SCORE 
		TURNS 
		END

EMPUJA	BABOSA	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "Al acercarte al bicho, te salpica con sus pútridos líquidos corrosivos que te penetran y despedazan en cuestión de segundos."
		NEWLINE 
		SCORE 
		TURNS 
		END

EMPUJA	PUERTA	PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		WRITE "Fútil."
		NEWLINE 
		DONE

EMPUJA	BARROTE	AT 24  ; sust(loc_SALADEBASURAS/24)
		ZERO 100  ; sust(flag_REJILLASINBARROTE/100)
		WRITE "No está lo suficientemente suelto. Es necesario un método más contundente."
		NEWLINE 
		DONE

EMPUJA	BARROTE	ABSENT 24  ; sust(obj_OBJBARROTEHIERRO/24)
		AT 24  ; sust(loc_SALADEBASURAS/24)
		WRITE "Ya has tenido una experiencia con ese tema, no necesitas más."
		NEWLINE 
		DONE

EMPUJA	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		ZERO 66  ; sust(flag_ESTADOTECNICO/66)
		WRITE "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor."
		NEWLINE 
		SCORE 
		TURNS 
		END

EMPUJA	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		WRITE "Déjalo que se recupere."
		NEWLINE 
		DONE

EMPUJA	INICIAR	AT 29  ; sust(loc_TUNAVE/29)
		LET 33 30

EMPUJA	IGNICION	AT 29  ; sust(loc_TUNAVE/29)
		LET 33 30

EMPUJA	PALANCA	PRESENT 33  ; sust(obj_OBJPALANCALLENADO/33)
		WRITE "No cede, sólo permite tirar de ella."
		NEWLINE 
		DONE

EMPUJA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No le ves sentido a empujar " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		DONE

EMPUJA	_	WRITE "No produce ningún efecto."
		NEWLINE 
		DONE

PULSA	S	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

PULSA	E	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

PULSA	O	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

PULSA	ARRIBA	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

PULSA	ABAJO	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

PULSA	N	PRESENT 21  ; sust(obj_OBJPANELCONTROL/21)
		PROCESS 5  ; sust(proc_ACCIONROBOT/5)
		DONE

PULSA	INICIAR	AT 29  ; sust(loc_TUNAVE/29)
		PROCESS 44  ; sust(proc_INICIARLANZAMIENTO/44)
		DONE

PULSA	IGNICION	AT 29  ; sust(loc_TUNAVE/29)
		NOTZERO 84  ; sust(flag_LANZAMIENTOINICIADO/84)
		CLS 
		WRITE " Los sistemas de la nave rugen y una cuenta atrás comienza su andadura. 3..2..1…\n\n Abandonas el planeta ante la sorpresa de los alienígenas, que apenas tienen tiempo de activar sus sistemas defensivos para evitar tu huida.\n\n"
		NEWLINE 
		ANYKEY 
		CLS 
		WRITE " De vuelta a la nave nodriza tus compañeros escuchan tu histora con estupor. Te has ganado unas vacaciones, allá en la Tierra.\n\n Pero parece que podrían durar poco. Dadas tus dotes de supervivencia te han propuesto una arriesgada misión en cierto templo Inca…\n\n Pero esa es otra historia.\n\n"
		NEWLINE 
		SCORE 
		TURNS 
		END

PULSA	IGNICION	AT 29  ; sust(loc_TUNAVE/29)
		WRITE "No has iniciado la secuencia de lanzamiento."
		NEWLINE 
		DONE

PULSA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No le ves sentido a empujar " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		DONE

PULSA	_	WRITE "No produce ningún efecto."
		NEWLINE 
		DONE

MONTA	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		PRESENT 8  ; sust(obj_OBJBLOQUEHIELO/8)
		PROCESS 57  ; sust(proc_MONTAHIELOPROC/57)
		DONE

MONTA	_	WRITE "Subirte a eso no parece muy adecuado en este momento."
		NEWLINE 
		DONE

TREPA	ARBOL	AT 11  ; sust(loc_BOSQUEDENSO/11)
		WRITE "Trepas un rato por una de las lianas, pero al ver que el camino hasta arriba es más largo de lo que parece desistes."
		NEWLINE 
		DONE

TREPA	LIANA	AT 11  ; sust(loc_BOSQUEDENSO/11)
		WRITE "Trepas un rato por una de las lianas, pero al ver que el camino hasta arriba es más largo de lo que parece desistes."
		NEWLINE 
		DONE

TREPA	LIANA	AT 12  ; sust(loc_FOSA/12)
		NOTZERO 85  ; sust(flag_LIANAATADA/85)
		CLEAR 65  ; sust(flag_ESTADOSUSPENSION/65)
		CLEAR 61  ; sust(flag_CONTADORBALANCEO/61)
		WRITE "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo."
		NEWLINE 
		GOTO 13  ; sust(loc_LOCSUSPENDIDO/13)
		DESC

TREPA	LIANA	AT 12  ; sust(loc_FOSA/12)
		CARRIED 3  ; sust(obj_OBJLIANA/3)
		WRITE "Deberías atarla a algo consistente antes de escalar."
		NEWLINE 
		DONE

TREPA	FOSA	AT 12  ; sust(loc_FOSA/12)
		NOTZERO 85  ; sust(flag_LIANAATADA/85)
		CLEAR 65  ; sust(flag_ESTADOSUSPENSION/65)
		CLEAR 61  ; sust(flag_CONTADORBALANCEO/61)
		WRITE "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo."
		NEWLINE 
		GOTO 13  ; sust(loc_LOCSUSPENDIDO/13)
		DESC

TREPA	FOSA	AT 12  ; sust(loc_FOSA/12)
		NOUN2 LIANA 
		CARRIED 3  ; sust(obj_OBJLIANA/3)
		ZERO 85  ; sust(flag_LIANAATADA/85)
		WRITE "Deberías atarla a algo consistente antes de escalar."
		NEWLINE 
		DONE

TREPA	FOSA	AT 12  ; sust(loc_FOSA/12)
		ZERO 85  ; sust(flag_LIANAATADA/85)
		WRITE "La fosa es demasiado profunda para tirarte por ella."
		NEWLINE 
		DONE

TREPA	FOSA	AT 12  ; sust(loc_FOSA/12)
		CARRIED 5  ; sust(obj_OBJTABLON/5)
		WRITE "O el tablón o tú, los dos no cabeis por la fosa."
		NEWLINE 
		DONE

TREPA	FOSA	AT 12  ; sust(loc_FOSA/12)
		CLEAR 65  ; sust(flag_ESTADOSUSPENSION/65)
		CLEAR 61  ; sust(flag_CONTADORBALANCEO/61)
		WRITE "Desciendes por la liana con cuidado, pero su longitud no te permite llegar al fondo."
		NEWLINE 
		GOTO 13  ; sust(loc_LOCSUSPENDIDO/13)
		DESC

TREPA	_	AT 13  ; sust(loc_LOCSUSPENDIDO/13)
		GOTO 12  ; sust(loc_FOSA/12)
		DESC

TREPA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No le ves sentido a trepar por " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE " en este momento."
		NEWLINE 
		DONE

TREPA	_	WRITE "No le ves sentido a trepar por " 
		WRITE "nada" 
		WRITE " en este momento."
		NEWLINE 
		DONE

ATA	LIANA	AT 12  ; sust(loc_FOSA/12)
		PROCESS 9  ; sust(proc_ATALIANAPROC/9)
		DONE

ATA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No le ves sentido a atar " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		DONE

ATA	_	WRITE "No le ves sentido a atar " 
		WRITE "eso."
		NEWLINE 
		DONE

DESATA	LIANA	AT 12  ; sust(loc_FOSA/12)
		NOTZERO 85  ; sust(flag_LIANAATADA/85)
		CLEAR 85  ; sust(flag_LIANAATADA/85)
		PLACE 3 254  ; sust(obj_OBJLIANA/3)
		WRITE "Desatas la liana."
		NEWLINE 
		DONE

DESATA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No has atado " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE " a nada."
		NEWLINE 
		DONE

DESATA	_	WRITE "No ves nada atado."
		NEWLINE 
		DONE

BALANCEA	LIANA	AT 13  ; sust(loc_LOCSUSPENDIDO/13)
		PROCESS 15  ; sust(proc_BALANCEARPROC/15)
		DONE

BALANCEA	_	AT 13  ; sust(loc_LOCSUSPENDIDO/13)
		EQ 34 255 
		PROCESS 15  ; sust(proc_BALANCEARPROC/15)
		DONE

BALANCEA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No le ves un uso práctico a balancear " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE " en este momento."
		NEWLINE 
		DONE

BALANCEA	_	WRITE "No le ves sentido a balancear eso en este momento."
		NEWLINE 
		DONE

DESPIERTA	CRIATURA	PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		NOTAT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "Al poner la mano sobre la criatura, se " 
		WRITE "despierta repentinamente y dando un ágil salto se cuelga de tu hombro. Instantes después notas sus largas manos y afiladas uñas cerrarse en torno a tu cuello. Pierdes la conciencia al cabo de poco rato."
		NEWLINE 
		SCORE 
		TURNS 
		END

DESPIERTA	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		ZERO 66  ; sust(flag_ESTADOTECNICO/66)
		WRITE "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor."
		NEWLINE 
		SCORE 
		TURNS 
		END

DESPIERTA	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		WRITE "Déjalo que se recupere."
		NEWLINE 
		DONE

DESPIERTA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "Despertar " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE " es una idea ridícula."
		NEWLINE 
		DONE

DESPIERTA	_	WRITE "No es muy coherente despertar a eso."
		NEWLINE 
		DONE

DISPARA	PISTOLA	COPYFF 44 88  ; sust(flag_NOMTMP/88)
		COPYFF 45 24  ; sust(flag_ADJTMP/24)
		COPYFF 34 44 
		COPYFF 35 45 
		COPYFF 88 34  ; sust(flag_NOMTMP/88)
		COPYFF 24 35 ; sust(flag_ADJTMP/24)

DISPARA	BABOSA	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		CARRIED 7  ; sust(obj_OBJPISTOLANEUTRALIZANTE/7)
		WRITE "Lanzas una ráfaga a la babosa. La babosa se desintegra por toda la estancia enbadurnándote con sus ácidos fluidos."
		NEWLINE 
		SCORE 
		TURNS 
		END

DISPARA	PUERTA	PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		WRITE "Fútil."
		NEWLINE 
		DONE

DISPARA	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		CARRIED 7  ; sust(obj_OBJPISTOLANEUTRALIZANTE/7)
		EQ 66 0  ; sust(flag_ESTADOTECNICO/66)
		LET 66 1  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Lanzas una discreta ráfaga neutralizadora al pobre individuo. Ahora dormirá un buen rato."
		NEWLINE 
		DONE

DISPARA	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		EQ 66 1  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Eso sería ensañamiento."
		NEWLINE 
		DONE

DISPARA	_	PROCESS 32  ; sust(proc_DISPARARSUB/32)
		DONE

MATA	GUSANO	PRESENT 11  ; sust(obj_OBJGUSANO/11)
		WRITE "Está fuera de tus posibilidades."
		NEWLINE 
		DONE

MATA	CRIATURA	PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		ZERO 64  ; sust(flag_ESPEJOMOVIDO/64)
		WRITE "Podrías romper el espejo, y parece un objeto muy valioso aquí."
		NEWLINE 
		DONE

MATA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No veo muy lógico intentar matar " 
		PROCESS 63  ; sust(proc_PRINTFIRSTAL/63)
		WRITE ".\n" 
		DONE

MATA	_	WRITE "No es muy adecuado ponerse a matar ahora mismo."
		NEWLINE 
		DONE

INSERTA	TARJETA	PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		CARRIED 20  ; sust(obj_OBJTARJETA/20)
		NOUN2 ORIFICIO 
		PROCESS 10  ; sust(proc_ATRAVIESAPUERTAACERO/10)
		DONE

INSERTA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		NOUN2 ORIFICIO 
		WRITE "No encaja."
		NEWLINE 
		DONE

INSERTA	_	PROCESS 55  ; sust(proc_METERSUB/55)
		DONE

MUEVE	PALANCA	PRESENT 33  ; sust(obj_OBJPALANCALLENADO/33)
		ISNOTAT 24 9  ; sust(obj_OBJBARROTEHIERRO/24)
		WRITE "Tiras de la palanca. " 
		WRITE "Oyes un 'clong' seco dentro del cuadro, y luego silencio. " 
		WRITE "La palanca vuelve lentamente a su posición inicial."
		NEWLINE 
		DONE

MUEVE	PALANCA	PRESENT 33  ; sust(obj_OBJPALANCALLENADO/33)
		NOTZERO 103  ; sust(flag_TANQUEHIDRAULICOLLENADO/103)
		WRITE "Tiras de la palanca. " 
		WRITE "La palanca vuelve lentamente a su posición inicial."
		NEWLINE 
		DONE

MUEVE	PALANCA	PRESENT 33  ; sust(obj_OBJPALANCALLENADO/33)
		SET 103  ; sust(flag_TANQUEHIDRAULICOLLENADO/103)
		PLUS 30 3  ; sust(flag_PUNTUACION/30)
		WRITE "Tiras de la palanca. " 
		WRITE "En algún lugar hacia el este oyes algo parecido a la apertura de una esclusa y el inconfudible sonido de líquido fluyendo. " 
		WRITE "La palanca vuelve lentamente a su posición inicial."
		NEWLINE 
		DONE

MUEVE	_	WRITE "No produce ningún efecto."
		NEWLINE 
		DONE

Z	_	WRITE "Esperas…"
		NEWLINE 
		DONE

ABRE	PUERTA	AT 20  ; sust(loc_TUNELSINUOSO/20)
		WRITE "No es necesario, es de apertura automática."
		NEWLINE 
		DONE

ABRE	PUERTA	AT 21  ; sust(loc_SALACOMPUTADORA/21)
		WRITE "No es necesario, es de apertura automática."
		NEWLINE 
		DONE

ABRE	PUERTA	AT 31  ; sust(loc_FINPASILLO/31)
		WRITE "No es necesario, es de apertura automática."
		NEWLINE 
		DONE

ABRE	PUERTA	PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		WRITE "No parece abrirse del modo tradicional."
		NEWLINE 
		DONE

ABRE	COMPARTIME	PRESENT 10  ; sust(obj_OBJCOMPARTIMENTO/10)
		ZERO 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		SET 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		PROCESS 4  ; sust(proc_ABRIRTEXT/4)
		PROCESS 51  ; sust(proc_LISTATFIRSTNOUN/51)
		DONE

ABRE	CUADRO	PRESENT 9  ; sust(obj_OBJCUADROELECTRICO/9)
		ZERO 62  ; sust(flag_CUADROELECTRICOABIERTO/62)
		SET 62  ; sust(flag_CUADROELECTRICOABIERTO/62)
		PROCESS 4  ; sust(proc_ABRIRTEXT/4)
		PROCESS 51  ; sust(proc_LISTATFIRSTNOUN/51)
		DONE

ABRE	_	PROCESS 3  ; sust(proc_ABRIRSUB/3)
		DONE

CIERRA	PUERTA	PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		WRITE "Está cerrada."
		NEWLINE 
		DONE

CIERRA	COMPARTIME	PRESENT 10  ; sust(obj_OBJCOMPARTIMENTO/10)
		NOTZERO 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		CLEAR 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		PROCESS 19  ; sust(proc_CERRARTEXT/19)
		DONE

CIERRA	CUADRO	PRESENT 9  ; sust(obj_OBJCUADROELECTRICO/9)
		NOTZERO 62  ; sust(flag_CUADROELECTRICOABIERTO/62)
		CLEAR 62  ; sust(flag_CUADROELECTRICOABIERTO/62)
		PROCESS 19  ; sust(proc_CERRARTEXT/19)
		DONE

CIERRA	_	PROCESS 18  ; sust(proc_CERRARSUB/18)
		DONE

DEJA	_	CLEAR 50

DEJA	_	EQ 34 51 
		DOALL 254

DEJA	_	NOTZERO 50 
		WRITE "_: "

DEJA	LIANA	AT 13  ; sust(loc_LOCSUSPENDIDO/13)
		WRITE "Te sueltas de la liana y caes en " 
		PROCESS 86  ; sust(proc_SUELTALIANAPROC/86)
		DONE

DEJA	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		NOTAT 7  ; sust(loc_CAVERNAVERDE5/7)
		WRITE "No parece adecuado dejar el recipiente aquí."
		NEWLINE 
		DONE

DEJA	_	AT 15  ; sust(loc_LOCREMOLINO/15)
		WHATO 
		EQ 54 254 
		PUTO 255 
		WRITE "Sueltas " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ", y ves como " 
		PROCESS 72  ; sust(proc_PROACUSATIVO/72)
		WRITE " engulle el remolino."
		NEWLINE 
		DONE

DEJA	_	NOUN2 _ 
		PROCESS 29  ; sust(proc_DEJARSUB/29)
		DONE

DEJA	_	PROCESS 55  ; sust(proc_METERSUB/55)
		DONE

SAVE	_	SAVE

LOAD	_	LOAD

RS	_	RAMSAVE 
		WRITE "Juego guardado en memoria."

COGE	_	CLEAR 50

COGE	_	EQ 34 51 
		DOALL 255

COGE	_	NOTZERO 50 
		WRITE "_: "

COGE	LIANA	AT 11  ; sust(loc_BOSQUEDENSO/11)
		ZERO 86  ; sust(flag_LIANACORTADA/86)
		WRITE "Las lianas cuelgan de los árboles, no puedes cogerlas sin más."
		NEWLINE 
		DONE

COGE	LIANA	AT 11  ; sust(loc_BOSQUEDENSO/11)
		ABSENT 3  ; sust(obj_OBJLIANA/3)
		WRITE "Las lianas cuelgan de los árboles, no puedes cogerlas sin más."
		NEWLINE 
		DONE

COGE	LIANA	AT 12  ; sust(loc_FOSA/12)
		NOTZERO 85  ; sust(flag_LIANAATADA/85)
		CLEAR 85  ; sust(flag_LIANAATADA/85)
		PLACE 3 254  ; sust(obj_OBJLIANA/3)
		WRITE "Desatas la liana."
		NEWLINE 
		DONE

COGE	TABLON	AT 16  ; sust(loc_EXTRANACONSTRUCCION/16)
		ZERO 101  ; sust(flag_TABLONCOGIDO/101)
		SET 101  ; sust(flag_TABLONCOGIDO/101)
		PLACE 5 254  ; sust(obj_OBJTABLON/5)
		PLUS 30 1  ; sust(flag_PUNTUACION/30)
		WRITE "Tiras con fuerza de uno de los tablones hasta desprenderlo del suelo."
		NEWLINE 
		DONE

COGE	TABLON	AT 16  ; sust(loc_EXTRANACONSTRUCCION/16)
		ABSENT 5  ; sust(obj_OBJTABLON/5)
		WRITE "No puedes arrancar más."
		NEWLINE 
		DONE

COGE	UTENSILIO	AT 17  ; sust(loc_COBERTIZO/17)
		WRITE "La mayoría está en muy malas condiciones y no sabes ni para qué sirven."
		NEWLINE 
		DONE

COGE	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		ISAT 8 19  ; sust(obj_OBJBLOQUEHIELO/8)
		WRITE "Es demasiado pesado."
		NEWLINE 
		DONE

COGE	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		WRITE "Son enormes bloques, y están incrustados en la roca."
		NEWLINE 
		DONE

COGE	GUSANO	PRESENT 11  ; sust(obj_OBJGUSANO/11)
		WRITE "Está fuera de tus posibilidades."
		NEWLINE 
		DONE

COGE	GUSANO	PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE "Está atascado."
		NEWLINE 
		DONE

COGE	RECIPIENTE	NOTCARR 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		PRESENT 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		PROCESS 21  ; sust(proc_COGERRECIPIENTEGUSANO/21)
		DONE

COGE	ESPEJO	ISAT 16 255  ; sust(obj_OBJESPEJO/16)
		LET 35 5

COGE	ESPEJO	AT 5  ; sust(loc_CAVERNAVERDE3/5)
		ZERO 64  ; sust(flag_ESPEJOMOVIDO/64)
		WRITE "Coges el espejo alegremente y la criatura cae rodando del otro lado. Se " 
		WRITE "despierta repentinamente y dando un ágil salto se cuelga de tu hombro. Instantes después notas sus largas manos y afiladas uñas cerrarse en torno a tu cuello. Pierdes la conciencia al cabo de poco rato."
		NEWLINE 
		SCORE 
		TURNS 
		END

COGE	ESPEJO	ZERO 95  ; sust(flag_PUNTUADOESPEJOCOGIDO/95)
		PRESENT 16  ; sust(obj_OBJESPEJO/16)
		SET 95  ; sust(flag_PUNTUADOESPEJOCOGIDO/95)
		PLUS 30 2 ; sust(flag_PUNTUACION/30)

COGE	CRIATURA	PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		NOTAT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "Al poner la mano sobre la criatura, se " 
		WRITE "despierta repentinamente y dando un ágil salto se cuelga de tu hombro. Instantes después notas sus largas manos y afiladas uñas cerrarse en torno a tu cuello. Pierdes la conciencia al cabo de poco rato."
		NEWLINE 
		SCORE 
		TURNS 
		END

COGE	ESQUELETO	PRESENT 15  ; sust(obj_OBJESQUELETOPULPOSO/15)
		WRITE "No vas a tocar eso."
		NEWLINE 
		DONE

COGE	BABOSA	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "Al acercarte al bicho, te salpica con sus pútridos líquidos corrosivos que te penetran y despedazan en cuestión de segundos."
		NEWLINE 
		SCORE 
		TURNS 
		END

COGE	BARROTE	AT 24  ; sust(loc_SALADEBASURAS/24)
		ZERO 100  ; sust(flag_REJILLASINBARROTE/100)
		WRITE "No está lo suficientemente suelto. Es necesario un método más contundente."
		NEWLINE 
		DONE

COGE	BARROTE	ABSENT 24  ; sust(obj_OBJBARROTEHIERRO/24)
		AT 24  ; sust(loc_SALADEBASURAS/24)
		WRITE "Ya has tenido una experiencia con ese tema, no necesitas más."
		NEWLINE 
		DONE

COGE	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		ZERO 66  ; sust(flag_ESTADOTECNICO/66)
		WRITE "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor."
		NEWLINE 
		SCORE 
		TURNS 
		END

COGE	TECNICO	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		WRITE "Déjalo que se recupere."
		NEWLINE 
		DONE

COGE	UNIFORME	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		EQ 66 1  ; sust(flag_ESTADOTECNICO/66)
		PLUS 30 2  ; sust(flag_PUNTUACION/30)
		DESTROY 28  ; sust(obj_OBJTECNICO/28)
		LET 66 2  ; sust(flag_ESTADOTECNICO/66)
		PLACE 29 254  ; sust(obj_OBJUNIFORME/29)
		WRITE "Le quitas el uniforme al técnico, arrojando al pobre diablo en una de las naves aparcadas para que no llame la atención."
		NEWLINE 
		DONE

COGE	UNIFORME	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		WRITE "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor."
		NEWLINE 
		SCORE 
		TURNS 
		END

COGE	_	NOUN2 _ 
		PROCESS 22  ; sust(proc_COGERSUB/22)
		DONE

COGE	_	PROCESS 83  ; sust(proc_SACARSUB/83)
		DONE

QUITA	BOTAS	AT 15  ; sust(loc_LOCREMOLINO/15)
		WRITE "Eso no sería una acción muy inteligente, aquí dentro del lago."
		NEWLINE 
		DONE

QUITA	GUSANO	NOUN2 SUCCIONADO 
		PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE "Está atascado."
		NEWLINE 
		DONE

QUITA	GUANTES	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		WRITE "No parece viable con este recipiente en tus manos."
		NEWLINE 
		DONE

QUITA	UNIFORME	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		EQ 66 1  ; sust(flag_ESTADOTECNICO/66)
		PLUS 30 2  ; sust(flag_PUNTUACION/30)
		DESTROY 28  ; sust(obj_OBJTECNICO/28)
		LET 66 2  ; sust(flag_ESTADOTECNICO/66)
		PLACE 29 254  ; sust(obj_OBJUNIFORME/29)
		WRITE "Le quitas el uniforme al técnico, arrojando al pobre diablo en una de las naves aparcadas para que no llame la atención."
		NEWLINE 
		DONE

QUITA	UNIFORME	PRESENT 28  ; sust(obj_OBJTECNICO/28)
		WRITE "El técnico se despierta y da la alarma. Al instante acude un equipo de hombrecillos armados que te provoca un final rápido, pero no carente de dolor."
		NEWLINE 
		SCORE 
		TURNS 
		END

QUITA	_	AT 15  ; sust(loc_LOCREMOLINO/15)
		NOUN2 LAGO 
		PROCESS 22  ; sust(proc_COGERSUB/22)
		DONE

QUITA	_	NOUN2 _ 
		PROCESS 76  ; sust(proc_QUITARSUB/76)
		DONE

QUITA	_	PROCESS 83  ; sust(proc_SACARSUB/83)
		DONE

RL	_	RAMLOAD 255 
		DESC

M	_	PREP EN 
		PROCESS 56  ; sust(proc_MIRARENSUB/56)
		DONE

M	_	NOTEQ 34 255 
		LET 33 55

M	_	DESC

EX	LAGO	AT 14  ; sust(loc_JUNTOLAGO/14)
		WRITE "La salida del riachuelo forma un pequeño remolino no muy lejos de la orilla.\n" 
		DONE

EX	SUELO	AT 16  ; sust(loc_EXTRANACONSTRUCCION/16)
		WRITE "Su estado de abandono ha dejado varios asideros entre los tablones."
		NEWLINE 
		DONE

EX	TABLON	AT 16  ; sust(loc_EXTRANACONSTRUCCION/16)
		ABSENT 5  ; sust(obj_OBJTABLON/5)
		WRITE "Su estado de abandono ha dejado varios asideros entre los tablones."
		NEWLINE 
		DONE

EX	PINTURA	AT 18  ; sust(loc_CAVIDADHUMEDA/18)
		WRITE "Alguien desciende por una fosa con una cuerda.\n" 
		DONE

EX	BLOQUE	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		ISNOTAT 8 19  ; sust(obj_OBJBLOQUEHIELO/8)
		WRITE "En algunos sitios tapizan las paredes como planchas de metal, en otros brotan como deformes protuberancias de la fría roca.\n" 
		DONE

EX	GUSANO	PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE "Está atascado."
		NEWLINE 
		DONE

EX	SIMBOLO	PRESENT 13  ; sust(obj_OBJESPEJOSALIDA/13)
		WRITE "Un gran espejo cuadrado tras un duro cristal, orientado formando un ángulo de 45 grados. Junto a él, hay un símbolo: un cuadrado y una flecha apuntando al interior.\n" 
		DONE

EX	MONITOR	AT 21  ; sust(loc_SALACOMPUTADORA/21)
		WRITE "Un mapa ocupa gran parte de la pantalla. En la parte de arriba ves un complejo de cuevas. La zona inferior representa una zona de hangares o talleres."

EX	MONITOR	AT 21  ; sust(loc_SALACOMPUTADORA/21)
		ZERO 103  ; sust(flag_TANQUEHIDRAULICOLLENADO/103)
		WRITE " Un punto rojo parpadea en el lado derecho de los hangares.\n" 
		DONE

EX	SUCCIONADO	PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		WRITE "El succionador es una gran cavidad en una de las paredes"

EX	SUCCIONADO	PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE ", actualmente bloqueado por un enorme gusano."
		NEWLINE 
		DONE

EX	SUCCIONADO	PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		WRITE " …oh..ooohhh… " 
		WRITE "El mecanismo de absorción se activa y una fuerza irresistible te chupa al interior. Apareces en…"
		NEWLINE 
		GOTO 25  ; sust(loc_ESTRECHOCUBICULO/25)
		DESC

EX	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		PROCESS 35  ; sust(proc_EXAMINAPRESENT/35)
		ZERO 69  ; sust(flag_FLAGRET/69)
		DONE

EX	_	PROCESS 36  ; sust(proc_EXAMINARSUB/36)
		DONE

PON	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		LET 33 250

PON	TARJETA	PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		CARRIED 20  ; sust(obj_OBJTARJETA/20)
		NOUN2 ORIFICIO 
		PROCESS 10  ; sust(proc_ATRAVIESAPUERTAACERO/10)
		DONE

PON	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		NOUN2 ORIFICIO 
		WRITE "No encaja."
		NEWLINE 
		DONE

PON	_	NOUN2 _ 
		PROCESS 60  ; sust(proc_PONERSUB/60)
		DONE

PON	_	PROCESS 55  ; sust(proc_METERSUB/55)
		DONE

METE	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		LET 33 250

METE	TARJETA	PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		CARRIED 20  ; sust(obj_OBJTARJETA/20)
		NOUN2 ORIFICIO 
		PROCESS 10  ; sust(proc_ATRAVIESAPUERTAACERO/10)
		DONE

METE	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		NOUN2 ORIFICIO 
		WRITE "No encaja."
		NEWLINE 
		DONE

METE	_	PROCESS 55  ; sust(proc_METERSUB/55)
		DONE

PONTE	_	PROCESS 60  ; sust(proc_PONERSUB/60)
		DONE

SACATE	BOTAS	AT 15  ; sust(loc_LOCREMOLINO/15)
		WRITE "Eso no sería una acción muy inteligente, aquí dentro del lago."
		NEWLINE 
		DONE

SACATE	GUANTES	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		WRITE "No parece viable con este recipiente en tus manos."
		NEWLINE 
		DONE

SACATE	_	PROCESS 76  ; sust(proc_QUITARSUB/76)
		DONE

FIN	_	QUIT 
		TURNS 
		SCORE 
		END

DA	PATADA	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		PREP A 
		NOUN2 BLOQUE 
		LET 33 27 
		LET 34 65 
		LET 35 255 
		LET 44 66 
		LET 45 255 
		PROCESS 82  ; sust(proc_ROMPEHIELO/82)
		DONE

DA	PATADA	AT 24  ; sust(loc_SALADEBASURAS/24)
		NOUN2 BARROTE 
		PROCESS 85  ; sust(proc_SALTAREJILLA/85)
		DONE

DA	PATADA	AT 24  ; sust(loc_SALADEBASURAS/24)
		NOUN2 REJILLA 
		PROCESS 85  ; sust(proc_SALTAREJILLA/85)
		DONE

DA	PATADA	WRITE "No parece que esa opción violenta sea la solución en este momento."
		NEWLINE 
		DONE

DA	PUÑETAZO	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		PREP A 
		NOUN2 BLOQUE 
		WRITE "¿Así, con las manos?"
		NEWLINE 
		DONE

DA	PUÑETAZO	WRITE "No parece que esa opción violenta sea la solución en este momento."
		NEWLINE 
		DONE

DA	_	NOUN2 CRIATURA 
		NOTAT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		WRITE "Está durmiendo."
		NEWLINE 
		DONE

DA	_	NOUN2 BABOSA 
		WHATO 
		EQ 54 254 
		EQ 51 16 
		LET 44 255 
		COPYFF 38 67  ; sust(flag_FLAGPRM1/67)
		PROCESS 42  ; sust(proc_HAYBABOSA/42)
		LET 44 75 
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "La babosa hace un gesto de desdén al mostrarle es espejo."
		NEWLINE 
		DONE

DA	_	NOUN2 BABOSA 
		WHATO 
		EQ 54 254 
		LET 44 255 
		COPYFF 38 67  ; sust(flag_FLAGPRM1/67)
		PROCESS 42  ; sust(proc_HAYBABOSA/42)
		LET 44 75 
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "La babosa no está interesada."
		NEWLINE 
		DONE

DA	_	PROCESS 28  ; sust(proc_DARSUB/28)
		DONE

MUESTRA	_	NOUN2 CRIATURA 
		NOTAT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		WRITE "Está durmiendo."
		NEWLINE 
		DONE

MUESTRA	_	NOUN2 BABOSA 
		WHATO 
		EQ 54 254 
		EQ 51 16 
		LET 44 255 
		COPYFF 38 67  ; sust(flag_FLAGPRM1/67)
		PROCESS 42  ; sust(proc_HAYBABOSA/42)
		LET 44 75 
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "La babosa hace un gesto de desdén al mostrarle es espejo."
		NEWLINE 
		DONE

MUESTRA	_	NOUN2 BABOSA 
		WHATO 
		EQ 54 254 
		LET 44 255 
		COPYFF 38 67  ; sust(flag_FLAGPRM1/67)
		PROCESS 42  ; sust(proc_HAYBABOSA/42)
		LET 44 75 
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "La babosa no está interesada."
		NEWLINE 
		DONE

MUESTRA	_	PROCESS 58  ; sust(proc_MOSTRARSUB/58)
		DONE

X	_	PROCESS 84  ; sust(proc_SALIDAS/84)
		DONE

DERRAMA	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		AT 27  ; sust(loc_PASAJEACOLCHADO/27)
		NOUN2 DISPOSITIV 
		ZERO 26  ; sust(flag_CAMPOFUERZADESTRUIDO/26)
		SET 26  ; sust(flag_CAMPOFUERZADESTRUIDO/26)
		PLUS 30 2  ; sust(flag_PUNTUACION/30)
		WRITE "Echas un poco del pútrido líquido sobre el mecanismo. El dispositivo se disuelve."
		NEWLINE 
		DONE

DERRAMA	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		AT 27  ; sust(loc_PASAJEACOLCHADO/27)
		NOUN2 DISPOSITIV 
		WRITE "Ya está bastante retorcido."
		NEWLINE 
		DONE

DERRAMA	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		AT 24  ; sust(loc_SALADEBASURAS/24)
		NOUN2 BARROTE 
		ZERO 100  ; sust(flag_REJILLASINBARROTE/100)
		WRITE "Eso lo desintegraría completamente, y el barrote puede serte útil."
		NEWLINE 
		DONE

DERRAMA	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		AT 24  ; sust(loc_SALADEBASURAS/24)
		NOUN2 REJILLA 
		ZERO 100  ; sust(flag_REJILLASINBARROTE/100)
		WRITE "Eso lo desintegraría completamente, y el barrote puede serte útil."
		NEWLINE 
		DONE

DERRAMA	RECIPIENTE	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		WRITE "No lo ves apropiado en este momento."
		NEWLINE 
		DONE

DERRAMA	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No le ves sentido ahora a derramar " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		DONE

DERRAMA	_	WRITE "No le ves mucho sentido ahora a derramar eso."
		NEWLINE 
		DONE

_	INICIAR	AT 29  ; sust(loc_TUNAVE/29)
		NOTDONE

_	IGNICION	AT 29  ; sust(loc_TUNAVE/29)
		NOTDONE

/PRO 1

_	_	AT 0  ; sust(loc_INICIAL/0)
		MODE 1 
		LET 53 64 
        BSET 12 5
		PROMPT 2 
		ABILITY 255 255 
		PROCESS 45  ; sust(proc_INITBABOSAS/45)
		LET 92 22  ; sust(flag_POSICIONROBOT/92)
		ANYKEY 
		GOTO 8  ; sust(loc_MESETA/8)
		CLS 
		WRITE "<i><b>Planet of Death</b></i>\nVersión 1.1 (05-09-09)\n<b>Programación</b>: Mastodon\n<i><b>Beta testing:</i></b> Akbarr y Baltasarq\n" 
		DESC

_	_	GT 38 2 
		LT 38 7 
		WRITE "\nCaverna verde\n Estás en una estrecha caverna laberíntica, iluminada débilmente por un suave resplandor verde que emana de las paredes y el suelo."

_	_	AT 3  ; sust(loc_CAVERNAVERDE/3)
		PLACE 13 3  ; sust(obj_OBJESPEJOSALIDA/13)
		WRITE " La salida del " 
		WRITE "este" 
		WRITE " tiene un espejo encima con un extraño símbolo."

_	_	AT 6  ; sust(loc_CAVERNAVERDE4/6)
		PLACE 13 6  ; sust(obj_OBJESPEJOSALIDA/13)
		WRITE " La salida del " 
		WRITE "oeste" 
		WRITE " tiene un espejo encima con un extraño símbolo."

_	_	AT 4  ; sust(loc_CAVERNAVERDE2/4)
		PLACE 13 4  ; sust(obj_OBJESPEJOSALIDA/13)
		WRITE " La salida del " 
		WRITE "sur" 
		WRITE " tiene un espejo encima con un extraño símbolo."

_	_	AT 3  ; sust(loc_CAVERNAVERDE/3)
		WRITE " Hacia el norte, la caverna asciende en una escarpada pendiente, imposible de escalar."

_	_	AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		PRESENT 11  ; sust(obj_OBJGUSANO/11)
		WRITE ", salvo ese enorme gusano que reposa atontado bajo tus pies"

_	_	AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "."

_	_	AT 7  ; sust(loc_CAVERNAVERDE5/7)
		ISAT 11 7  ; sust(obj_OBJGUSANO/11)
		WRITE ". Al fondo de la estancia hay " 
		WRITE "un enorme gusano"

_	_	AT 7  ; sust(loc_CAVERNAVERDE5/7)
		ISAT 11 7  ; sust(obj_OBJGUSANO/11)
		ISAT 12 7  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		WRITE " comiendo compulsivamente de un recipiente con un líquido gelatinoso"

_	_	AT 7  ; sust(loc_CAVERNAVERDE5/7)
		ISNOTAT 11 7  ; sust(obj_OBJGUSANO/11)
		ISAT 12 7  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		WRITE ". Al fondo de la estancia hay " 
		WRITE "un recipiente con un líquido gelatinoso"

_	_	AT 7  ; sust(loc_CAVERNAVERDE5/7)
		WRITE "."

_	_	NEWLINE 
		ZERO 0 
		ABSENT 0  ; sust(obj_OBJPEDERNAL/0)
		PROCESS 53 ; sust(proc_LISTOBJ/53)

_	_	PRESENT 0  ; sust(obj_OBJPEDERNAL/0)
		PROCESS 53 ; sust(proc_LISTOBJ/53)

_	_	PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		NOTAT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		NOTAT 26  ; sust(loc_VERTEDERO/26)
		WRITE "La criatura verde está durmiendo plácidamente, " 
		ZERO 64  ; sust(flag_ESPEJOMOVIDO/64)
		WRITE "sobre el espejo. "

_	_	PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		NOTAT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		NOTAT 26  ; sust(loc_VERTEDERO/26)
		NOTZERO 64  ; sust(flag_ESPEJOMOVIDO/64)
		WRITE "roncando como un colegial. "

_	_	PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "La criatura verde está totalmente aterrorizada, sus ojos de par en par. "

_	_	AT 12  ; sust(loc_FOSA/12)
		NOTZERO 85  ; sust(flag_LIANAATADA/85)
		WRITE " Una liana atada a una de las piedras se descuelga por la fosa."
		NEWLINE

_	_	AT 13  ; sust(loc_LOCSUSPENDIDO/13)
		PROCESS 31 ; sust(proc_DESCRIBESUSPENDIDO/31)

_	_	AT 17  ; sust(loc_COBERTIZO/17)
		ZERO 106  ; sust(flag_VISITADOCOBERTIZO/106)
		SET 106  ; sust(flag_VISITADOCOBERTIZO/106)
		PLUS 30 2 ; sust(flag_PUNTUACION/30)

_	_	GT 38 2 
		LT 38 8 
		WRITE "\n" 
		PROCESS 84 ; sust(proc_SALIDAS/84)

_	_	NOTSAME 25 38  ; sust(flag_BABOSAAMENAZA/25)
		CLEAR 25 ; sust(flag_BABOSAAMENAZA/25)

_	_	PRESENT 22  ; sust(obj_OBJROBOT/22)
		PROCESS 80 ; sust(proc_ROBOTDESCRIBIR/80)

_	_	AT 24  ; sust(loc_SALADEBASURAS/24)
		ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE "Hay un enorme gusano atascado entre el succionador y la sala de basuras de abajo. "

_	_	AT 27  ; sust(loc_PASAJEACOLCHADO/27)
		NOTZERO 26  ; sust(flag_CAMPOFUERZADESTRUIDO/26)
		WRITE "El dispositivo está totalmente destruido."

_	_	AT 28  ; sust(loc_HANGAR/28)
		WRITE "Aquí puedes ver tu nave. "

_	_	AT 28  ; sust(loc_HANGAR/28)
		ZERO 66  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Junto a ella hay un técnico de mantenimiento " 
		WRITE "durmiendo plácidamente. "

_	_	AT 28  ; sust(loc_HANGAR/28)
		EQ 66 1  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Junto a ella hay un técnico de mantenimiento " 
		WRITE "grogui en el suelo. "

_	_	NOTZERO 6  ; sust(flag_CONTADORPILLADO/6)
		NOTAT 28  ; sust(loc_HANGAR/28)
		CLEAR 6 ; sust(flag_CONTADORPILLADO/6)

/PRO 2

_	_	ISAT 14 26  ; sust(obj_OBJCRIATURAVERDE/14)
		DESTROY 14  ; sust(obj_OBJCRIATURAVERDE/14)
		AT 26  ; sust(loc_VERTEDERO/26)
		WRITE "Al verse libre la criatura huye despavorida entre los restos de basura hasta desaparecer de tu vista. "

_	_	AT 12  ; sust(loc_FOSA/12)
		LET 67 2  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		PROCESS 47 ; sust(proc_LIMPIARFOSA/47)

_	_	AT 13  ; sust(loc_LOCSUSPENDIDO/13)
		LET 67 13  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		PROCESS 49 ; sust(proc_LIMPIARSUSPENDIDO/49)

_	_	AT 13  ; sust(loc_LOCSUSPENDIDO/13)
		NOTZERO 65  ; sust(flag_ESTADOSUSPENSION/65)
		PROCESS 14  ; sust(proc_BALANCEARDAEMON/14)
		GT 65 1  ; sust(flag_ESTADOSUSPENSION/65)
		PROCESS 31 ; sust(proc_DESCRIBESUSPENDIDO/31)

_	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		NOTZERO 102  ; sust(flag_TABLONENRIACHUELO/102)
		ISNOTAT 5 1  ; sust(obj_OBJTABLON/5)
		CLEAR 102 ; sust(flag_TABLONENRIACHUELO/102)

_	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		ZERO 102  ; sust(flag_TABLONENRIACHUELO/102)
		ISAT 5 1  ; sust(obj_OBJTABLON/5)
		SET 102  ; sust(flag_TABLONENRIACHUELO/102)
		WRITE "El tablón queda atravesado entre las orillas del riachuelo."
		NEWLINE

_	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		LET 67 1  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		PROCESS 48 ; sust(proc_LIMPIARRIACHUELO/48)

_	_	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		ZERO 11  ; sust(flag_BLOQUEHIELOPUNTUADO/11)
		SET 11  ; sust(flag_BLOQUEHIELOPUNTUADO/11)
		PLUS 30 2 ; sust(flag_PUNTUACION/30)

_	_	NOTZERO 25  ; sust(flag_BABOSAAMENAZA/25)
		NOTCARR 16  ; sust(obj_OBJESPEJO/16)
		WRITE "La babosa se te sube y te rocía con todos sus fluidos putrefactos y corruptores."
		NEWLINE 
		SCORE 
		TURNS 
		END

_	_	NOTZERO 25  ; sust(flag_BABOSAAMENAZA/25)
		CLEAR 25  ; sust(flag_BABOSAAMENAZA/25)
		WRITE "La babosa llega hasta casi tocarte, pero de repente se da la vuelta y se aleja."
		NEWLINE

_	_	LET 67 1  ; sust(flag_FLAGPRM1/67)
		PROCESS 12  ; sust(proc_BABOSADAEMON/12)
		LET 67 2  ; sust(flag_FLAGPRM1/67)
		PROCESS 12 ; sust(proc_BABOSADAEMON/12)

_	_	ZERO 5  ; sust(flag_BASURAENBASURERO/5)
		LET 67 25  ; sust(flag_FLAGPRM1/67)
		PROCESS 26  ; sust(proc_CONTARATREAL/26)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		LET 5 3 ; sust(flag_BASURAENBASURERO/5)

_	_	ZERO 5  ; sust(flag_BASURAENBASURERO/5)
		AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		LET 5 3 ; sust(flag_BASURAENBASURERO/5)

_	_	EQ 5 1  ; sust(flag_BASURAENBASURERO/5)
		PROCESS 90 ; sust(proc_VACIARBASUREROPROC/90)

_	_	AT 28  ; sust(loc_HANGAR/28)
		ZERO 6  ; sust(flag_CONTADORPILLADO/6)
		NOTWORN 29  ; sust(obj_OBJUNIFORME/29)
		LET 6 5 ; sust(flag_CONTADORPILLADO/6)

_	_	AT 28  ; sust(loc_HANGAR/28)
		NOTWORN 29  ; sust(obj_OBJUNIFORME/29)
		EQ 6 3  ; sust(flag_CONTADORPILLADO/6)
		WRITE "Algunos de los hombrecillos empiezan a extrañarse por tu presencia y te lanzan miradas suspicaces."
		NEWLINE

_	_	AT 28  ; sust(loc_HANGAR/28)
		NOTWORN 29  ; sust(obj_OBJUNIFORME/29)
		EQ 6 1  ; sust(flag_CONTADORPILLADO/6)
		WRITE "Un grupo de hombrecillos armados te aborda y, despues de mirarte extrañados, y luego mirarse entre ellos extrañados, te llevan a…"
		NEWLINE 
		ISNOTAT 11 25  ; sust(obj_OBJGUSANO/11)
		GOTO 25  ; sust(loc_ESTRECHOCUBICULO/25)
		DESC

_	_	AT 28  ; sust(loc_HANGAR/28)
		NOTWORN 29  ; sust(obj_OBJUNIFORME/29)
		EQ 6 1  ; sust(flag_CONTADORPILLADO/6)
		GOTO 26  ; sust(loc_VERTEDERO/26)
		DESC

_	_	AT 32  ; sust(loc_TALLERES/32)
		NOTWORN 29  ; sust(obj_OBJUNIFORME/29)
		WRITE "Varios operarios detectan tu presencia, extraña para ellos, y se arremolinan a tu alrededor. Después de un rato en el que no parecen llegar a una conclusión definitiva sobre tu naturaleza, te llevan a…"
		NEWLINE 
		ISNOTAT 11 25  ; sust(obj_OBJGUSANO/11)
		GOTO 25  ; sust(loc_ESTRECHOCUBICULO/25)
		DESC

_	_	AT 32  ; sust(loc_TALLERES/32)
		NOTWORN 29  ; sust(obj_OBJUNIFORME/29)
		GOTO 26  ; sust(loc_VERTEDERO/26)
		DESC

/PRO 3 ; sust(proc_ABRIRSUB/3)

_	_	WHATO

_	_	SAME 38 54 
		LT 34 50 
		SYSMESS 142 
		NEWTEXT 
		DONE

_	_	NOTSAME 54 38 
		NOTEQ 54 254 
		NOTEQ 54 253 
		SYSMESS 100 
		SYSMESS 110 
		SYSMESS 104 
		NEWTEXT 
		DONE

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTZERO 16  ; sust(flag_PROP_ABIERTO/16)
		SYSMESS 114 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 115 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE ".\n" 
		DONE

_	_	SYSMESS 99 
		SYSMESS 110 
		PROCESS 6  ; sust(proc_ARTDET/6)
		WRITE " _.\n" 
		NEWTEXT 
		DONE

/PRO 4 ; sust(proc_ABRIRTEXT/4)

_	_	SYSMESS 111 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		DONE

/PRO 5 ; sust(proc_ACCIONROBOT/5)

_	_	EQ 34 9 
		PROCESS 78  ; sust(proc_ROBOTCOGER/78)
		DONE

_	_	EQ 34 10 
		PROCESS 79  ; sust(proc_ROBOTDEJAR/79)
		DONE

_	_	COPYFF 34 33 
		PROCESS 81  ; sust(proc_ROBOTMOVER/81)
		DONE

/PRO 6 ; sust(proc_ARTDET/6)

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		EQ 21 1  ; sust(flag_PROP_GENNUM/21)
		WRITE "el" 
		DONE

_	_	WRITE "l" 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		DONE

/PRO 7 ; sust(proc_ARTDETMAYUSC/7)

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		EQ 21 1  ; sust(flag_PROP_GENNUM/21)
		WRITE "El" 
		DONE

_	_	WRITE "L" 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		DONE

/PRO 8 ; sust(proc_ARTUNDET/8)

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTZERO 22  ; sust(flag_PROP_INCONTABLE/22)
		DONE

_	_	WRITE "un"

_	_	NOTEQ 21 1  ; sust(flag_PROP_GENNUM/21)
		PROCESS 87 ; sust(proc_TERMGENERO/87)

_	_	DONE

/PRO 9 ; sust(proc_ATALIANAPROC/9)

_	_	NOUN2 MOLE 
		CARRIED 3  ; sust(obj_OBJLIANA/3)
		ZERO 85  ; sust(flag_LIANAATADA/85)
		SET 85  ; sust(flag_LIANAATADA/85)
		DESTROY 3  ; sust(obj_OBJLIANA/3)
		WRITE "Atas la liana a una de las piedras y la dejas caer por la fosa."
		NEWLINE 
		DONE

_	_	NOUN2 MOLE 
		NOTZERO 85  ; sust(flag_LIANAATADA/85)
		WRITE "Ya está atada."
		NEWLINE 
		DONE

_	_	NOUN2 FOSA 
		CARRIED 3  ; sust(obj_OBJLIANA/3)
		WRITE "La fosa no tiene asideros para atar la liana."
		NEWLINE 
		DONE

_	_	NOTCARR 3  ; sust(obj_OBJLIANA/3)
		WRITE "No tienes nada de eso."
		NEWLINE 
		DONE

_	_	PREP A 
		WRITE "No le ves sentido en este momento a atar la liana a eso."
		NEWLINE 
		DONE

_	_	WRITE "Tienes que atar la liana a algo."
		NEWLINE 
		DONE

/PRO 10 ; sust(proc_ATRAVIESAPUERTAACERO/10)

_	_	ZERO 98  ; sust(flag_PUNTUADOTARJETA/98)
		SET 98  ; sust(flag_PUNTUADOTARJETA/98)
		PLUS 30 1 ; sust(flag_PUNTUACION/30)

_	_	WRITE "La tarjeta encaja en el orificio, y la puerta se abre con un siseo. La atraviesas y recoges la tarjeta…"
		NEWLINE 
		AT 28  ; sust(loc_HANGAR/28)
		PLACE 30 30  ; sust(obj_OBJPUERTAACERO/30)
		GOTO 30  ; sust(loc_SALADEMANTENIMIENTO/30)
		DESC

_	_	AT 30  ; sust(loc_SALADEMANTENIMIENTO/30)
		PLACE 30 28  ; sust(obj_OBJPUERTAACERO/30)
		GOTO 28  ; sust(loc_HANGAR/28)
		DESC

/PRO 11 ; sust(proc_BABOSAACRIATURA/11)

_	_	SAME 38 90  ; sust(flag_ORIGEN/90)
		WRITE "La babosa se dirige hacia la criatura, "

_	_	ZERO 64  ; sust(flag_ESPEJOMOVIDO/64)
		SAME 38 90  ; sust(flag_ORIGEN/90)
		WRITE "pero al acercarse al espejo se da la vuelta y continua reptando por la estancia."
		NEWLINE 
		DONE

_	_	NOTZERO 64  ; sust(flag_ESPEJOMOVIDO/64)
		SWAP 14 15  ; sust(obj_OBJCRIATURAVERDE/14) ; sust(obj_OBJESQUELETOPULPOSO/15)
		SAME 38 90  ; sust(flag_ORIGEN/90)
		WRITE " se sube a ella y en cuestión de segundos la desintegra con sus ácidos fluidos. Ahora sólo queda un esqueleto putrefacto y viscoso."
		NEWLINE 
		DONE

/PRO 12 ; sust(proc_BABOSADAEMON/12)

_	_	CLEAR 28  ; sust(flag_COMPLETARTEXTOBABOSA/28)
		COPYFF 67 89 ; sust(flag_FLAGPRM1/67) ; sust(flag_NUMBABOSA/89)

_	_	EQ 89 1  ; sust(flag_NUMBABOSA/89)
		LET 51 17

_	_	EQ 89 2  ; sust(flag_NUMBABOSA/89)
		LET 51 18

_	_	PROCESS 37  ; sust(proc_EXTBABOSA/37)
		COPYFF 12 90  ; sust(flag_LOCA_BABOSA/12) ; sust(flag_ORIGEN/90)
		COPYFF 90 67  ; sust(flag_ORIGEN/90) ; sust(flag_FLAGPRM1/67)
		PROCESS 59 ; sust(proc_MUEVEBABOSA/59)

_	_	LT 69 3  ; sust(flag_FLAGRET/69)
		COPYFF 90 63 ; sust(flag_ORIGEN/90) ; sust(flag_DESTINO/63)

_	_	GT 69 2  ; sust(flag_FLAGRET/69)
		COPYFF 69 63 ; sust(flag_FLAGRET/69) ; sust(flag_DESTINO/63)

_	_	ZERO 69  ; sust(flag_FLAGRET/69)
		SAME 38 90  ; sust(flag_ORIGEN/90)
		WRITE "La babosa intenta reptar hasta la salida, pero al llegar al espejo cambia repentinamente de dirección."
		NEWLINE

_	_	EQ 69 1  ; sust(flag_FLAGRET/69)
		PROCESS 11 ; sust(proc_BABOSAACRIATURA/11)

_	_	EQ 69 2  ; sust(flag_FLAGRET/69)
		SAME 38 90  ; sust(flag_ORIGEN/90)
		COPYFF 38 25  ; sust(flag_BABOSAAMENAZA/25)
		WRITE "La babosa se dirige hacia tí."
		NEWLINE

_	_	SAME 38 90  ; sust(flag_ORIGEN/90)
		NOTSAME 90 63  ; sust(flag_ORIGEN/90) ; sust(flag_DESTINO/63)
		WRITE "La babosa repta hacia la salida del " 
		COPYFF 63 67  ; sust(flag_DESTINO/63) ; sust(flag_FLAGPRM1/67)
		PROCESS 13  ; sust(proc_BABOSAVASTR/13)
		SET 28 ; sust(flag_COMPLETARTEXTOBABOSA/28)

_	_	SAME 38 63  ; sust(flag_DESTINO/63)
		NOTSAME 90 63  ; sust(flag_ORIGEN/90) ; sust(flag_DESTINO/63)
		WRITE "Una babosa llega reptando desde el " 
		COPYFF 90 67  ; sust(flag_ORIGEN/90) ; sust(flag_FLAGPRM1/67)
		PROCESS 13  ; sust(proc_BABOSAVASTR/13)
		SET 28 ; sust(flag_COMPLETARTEXTOBABOSA/28)

_	_	NOTSAME 90 63  ; sust(flag_ORIGEN/90) ; sust(flag_DESTINO/63)
		COPYFF 63 67  ; sust(flag_DESTINO/63) ; sust(flag_FLAGPRM1/67)
		PROCESS 42  ; sust(proc_HAYBABOSA/42)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		COPYFF 90 63  ; sust(flag_ORIGEN/90) ; sust(flag_DESTINO/63)
		NOTZERO 28  ; sust(flag_COMPLETARTEXTOBABOSA/28)
		WRITE ", pero antes de llegar, algo la hace darse la vuelta."
		NEWLINE

_	_	NOTSAME 90 63  ; sust(flag_ORIGEN/90) ; sust(flag_DESTINO/63)
		ZERO 69  ; sust(flag_FLAGRET/69)
		NOTZERO 28  ; sust(flag_COMPLETARTEXTOBABOSA/28)
		WRITE ".\n"

_	_	EQ 89 1  ; sust(flag_NUMBABOSA/89)
		COPYFF 63 13  ; sust(flag_DESTINO/63) ; sust(flag_LOCBABOSA1/13)
		COPYFO 63 17 ; sust(flag_DESTINO/63) ; sust(obj_OBJBABOSA1/17)

_	_	EQ 89 2  ; sust(flag_NUMBABOSA/89)
		COPYFF 63 14  ; sust(flag_DESTINO/63) ; sust(flag_LOCBABOSA2/14)
		COPYFO 63 18 ; sust(flag_DESTINO/63) ; sust(obj_OBJBABOSA2/18)

/PRO 13 ; sust(proc_BABOSAVASTR/13)

_	_	AT 6  ; sust(loc_CAVERNAVERDE4/6)
		EQ 67 3  ; sust(flag_FLAGPRM1/67)
		WRITE "este"

_	_	AT 6  ; sust(loc_CAVERNAVERDE4/6)
		EQ 67 7  ; sust(flag_FLAGPRM1/67)
		WRITE "norte"

_	_	AT 7  ; sust(loc_CAVERNAVERDE5/7)
		EQ 67 6  ; sust(flag_FLAGPRM1/67)
		WRITE "sur"

_	_	AT 7  ; sust(loc_CAVERNAVERDE5/7)
		EQ 67 3  ; sust(flag_FLAGPRM1/67)
		WRITE "sureste"

_	_	AT 3  ; sust(loc_CAVERNAVERDE/3)
		EQ 67 7  ; sust(flag_FLAGPRM1/67)
		WRITE "noroeste"

_	_	AT 3  ; sust(loc_CAVERNAVERDE/3)
		EQ 67 6  ; sust(flag_FLAGPRM1/67)
		WRITE "oeste"

_	_	AT 3  ; sust(loc_CAVERNAVERDE/3)
		EQ 67 4  ; sust(flag_FLAGPRM1/67)
		WRITE "sur"

_	_	AT 4  ; sust(loc_CAVERNAVERDE2/4)
		EQ 67 3  ; sust(flag_FLAGPRM1/67)
		WRITE "norte"

_	_	AT 4  ; sust(loc_CAVERNAVERDE2/4)
		EQ 67 5  ; sust(flag_FLAGPRM1/67)
		WRITE "este"

_	_	AT 5  ; sust(loc_CAVERNAVERDE3/5)
		EQ 67 4  ; sust(flag_FLAGPRM1/67)
		WRITE "oeste"

/PRO 14 ; sust(proc_BALANCEARDAEMON/14)

_	_	MINUS 61 1 ; sust(flag_CONTADORBALANCEO/61)

_	_	ZERO 61  ; sust(flag_CONTADORBALANCEO/61)
		CLEAR 65  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "El balanceo cesa completamente y quedas suspendido sobre el terraplén."
		NEWLINE 
		DONE

_	_	EQ 61 2  ; sust(flag_CONTADORBALANCEO/61)
		LET 65 1  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "El balanceo pierde fuerza y se queda en un suave vaivén sobre el terraplén."
		NEWLINE 
		DONE

_	_	EQ 65 2  ; sust(flag_ESTADOSUSPENSION/65)
		LET 65 4  ; sust(flag_ESTADOSUSPENSION/65)
		DONE

_	_	EQ 65 4  ; sust(flag_ESTADOSUSPENSION/65)
		LET 65 3  ; sust(flag_ESTADOSUSPENSION/65)
		DONE

_	_	EQ 65 3  ; sust(flag_ESTADOSUSPENSION/65)
		LET 65 5  ; sust(flag_ESTADOSUSPENSION/65)
		DONE

_	_	EQ 65 5  ; sust(flag_ESTADOSUSPENSION/65)
		LET 65 2 ; sust(flag_ESTADOSUSPENSION/65)

/PRO 15 ; sust(proc_BALANCEARPROC/15)

_	_	ZERO 65  ; sust(flag_ESTADOSUSPENSION/65)
		LET 65 1  ; sust(flag_ESTADOSUSPENSION/65)
		LET 61 2  ; sust(flag_CONTADORBALANCEO/61)
		WRITE "Te empiezas a columpiar en la cuerda imprimiéndole un suave movimiento de vaivén."
		NEWLINE 
		DONE

_	_	EQ 65 1  ; sust(flag_ESTADOSUSPENSION/65)
		LET 61 10  ; sust(flag_CONTADORBALANCEO/61)
		LET 65 4  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "Te das un nuevo impulso y ahora te balanceas sobre un ángulo más amplio."
		NEWLINE 
		DONE

_	_	GT 65 1  ; sust(flag_ESTADOSUSPENSION/65)
		LET 61 10  ; sust(flag_CONTADORBALANCEO/61)
		WRITE "Te impulsas un poco más, aunque la longitud de la liana no permite ampliar el ángulo de balanceo."
		NEWLINE 
		DONE

/PRO 16 ; sust(proc_BUSCAREN/16)

_	_	PROCESS 26  ; sust(proc_CONTARATREAL/26)
		ZERO 69  ; sust(flag_FLAGRET/69)
		DONE

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		NOTEQ 67 253  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 255  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 252  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 254  ; sust(flag_FLAGPRM1/67)
		COPYFF 67 38 ; sust(flag_FLAGPRM1/67)

_	_	CLEAR 69  ; sust(flag_FLAGRET/69)
		PROCESS 17  ; sust(proc_BUSCARENLOOP/17)
		COPYFF 104 38  ; sust(flag_TMPFLAG1/104)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 17 ; sust(proc_BUSCARENLOOP/17)

_	_	NOTEQ 67 253  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 252  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 254  ; sust(flag_FLAGPRM1/67)
		DOALL 255

_	_	EQ 67 253  ; sust(flag_FLAGPRM1/67)
		DOALL 253

_	_	EQ 67 252  ; sust(flag_FLAGPRM1/67)
		DOALL 252

_	_	EQ 67 254  ; sust(flag_FLAGPRM1/67)
		DOALL 254

_	_	ZERO 69  ; sust(flag_FLAGRET/69)
		PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTZERO 19  ; sust(flag_PROP_CONTENEDOR/19)
		NOTZERO 16  ; sust(flag_PROP_ABIERTO/16)
		SAME 68 51  ; sust(flag_FLAGPRM2/68)
		COPYFF 68 69  ; sust(flag_FLAGPRM2/68) ; sust(flag_FLAGRET/69)
		COPYFF 34 70  ; sust(flag_FLAGRET2/70)
		COPYFF 35 71  ; sust(flag_FLAGRET3/71)
		DONE

/PRO 18 ; sust(proc_CERRARSUB/18)

_	_	WHATO

_	_	SAME 38 54 
		LT 34 50 
		SYSMESS 142 
		NEWTEXT 
		DONE

_	_	NOTSAME 54 38 
		NOTEQ 54 254 
		NOTEQ 54 253 
		SYSMESS 100 
		SYSMESS 112 
		SYSMESS 104 
		NEWTEXT 
		DONE

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTZERO 17  ; sust(flag_PROP_ABRIBLE/17)
		ZERO 16  ; sust(flag_PROP_ABIERTO/16)
		SYSMESS 114 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 116 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE ".\n" 
		DONE

_	_	SYSMESS 99 
		SYSMESS 112 
		PROCESS 6  ; sust(proc_ARTDET/6)
		WRITE " _.\n" 
		NEWTEXT 
		DONE

/PRO 19 ; sust(proc_CERRARTEXT/19)

_	_	SYSMESS 113 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		DONE

/PRO 20 ; sust(proc_CODESECOND/20)

_	_	EQ 44 255 
		LET 69 255  ; sust(flag_FLAGRET/69)
		DONE

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		COPYFF 51 69  ; sust(flag_FLAGRET/69)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 21 ; sust(proc_COGERRECIPIENTEGUSANO/21)

_	_	WORN 19  ; sust(obj_OBJGUANTES/19)
		PRESENT 11  ; sust(obj_OBJGUSANO/11)
		CARRIED 16  ; sust(obj_OBJESPEJO/16)
		WRITE "Coges el recipiente."
		NEWLINE 
		PLACE 12 254  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		PROCESS 75  ; sust(proc_PUNTUARRECIPIENTE/75)
		DONE

_	_	PRESENT 11  ; sust(obj_OBJGUSANO/11)
		NOTCARR 16  ; sust(obj_OBJESPEJO/16)
		WRITE "El gusano podría ser un inconveniente."
		NEWLINE 
		DONE

_	_	NOTWORN 19  ; sust(obj_OBJGUANTES/19)
		WRITE "Chorrea líquido viscoso y corrosivo, te abrasarías."
		NEWLINE 
		DONE

_	_	WRITE "Coges el recipiente."
		NEWLINE 
		PLACE 12 254  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		PROCESS 75  ; sust(proc_PUNTUARRECIPIENTE/75)
		DONE

/PRO 22 ; sust(proc_COGERSUB/22)

_	_	WHATO

_	_	NOTEQ 51 12 
		CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		WRITE "No parece viable con este recipiente en tus manos."
		NEWLINE 
		DONE

_	_	SAME 38 54 
		LT 34 50 
		SYSMESS 142 
		DONE

_	_	EQ 54 254 
		SYSMESS 25 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	EQ 54 253 
		SYSMESS 25 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	SAME 54 38 
		PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTZERO 20  ; sust(flag_PROP_ESTATICO/20)
		SYSMESS 99 
		SYSMESS 107 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		SYSMESS 101 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 102 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		SYSMESS 103 
		NEWTEXT 
		DONE

_	_	SAME 54 38 
		SYSMESS 36 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		PUTO 254 
		DONE

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		COPYFF 70 44  ; sust(flag_FLAGRET2/70)
		COPYFF 71 45  ; sust(flag_FLAGRET3/71)
		PROCESS 83  ; sust(proc_SACARSUB/83)
		DONE

_	_	SYSMESS 100 
		SYSMESS 26 
		SYSMESS 104 
		NEWTEXT 
		DONE

/PRO 23 ; sust(proc_CONTARAT/23)

_	_	CLEAR 70  ; sust(flag_FLAGRET2/70)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		EQ 67 254  ; sust(flag_FLAGPRM1/67)
		LET 70 1  ; sust(flag_FLAGRET2/70)
		COPYFF 1 69  ; sust(flag_FLAGRET/69)
		DONE

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		CLEAR 69  ; sust(flag_FLAGRET/69)
		NOTEQ 67 253  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 255  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 252  ; sust(flag_FLAGPRM1/67)
		COPYFF 67 38 ; sust(flag_FLAGPRM1/67)

_	_	PROCESS 24  ; sust(proc_CONTARATINC/24)
		COPYFF 104 38  ; sust(flag_TMPFLAG1/104)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 24 ; sust(proc_CONTARATINC/24)

_	_	NOTEQ 67 253  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 252  ; sust(flag_FLAGPRM1/67)
		DOALL 255

_	_	EQ 67 253  ; sust(flag_FLAGPRM1/67)
		DOALL 253

_	_	EQ 67 252  ; sust(flag_FLAGPRM1/67)
		DOALL 252

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		ZERO 23  ; sust(flag_PROP_NOLISTAR/23)
		PLUS 69 1 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 23  ; sust(flag_PROP_NOLISTAR/23)
		EQ 50 254 
		PLUS 69 1 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 23  ; sust(flag_PROP_NOLISTAR/23)
		EQ 50 253 
		PLUS 69 1 ; sust(flag_FLAGRET/69)

_	_	ZERO 70  ; sust(flag_FLAGRET2/70)
		COPYFF 21 70 ; sust(flag_PROP_GENNUM/21) ; sust(flag_FLAGRET2/70)

/PRO 25 ; sust(proc_CONTARATINCREAL/25)

_	_	NOTEQ 67 253  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 252  ; sust(flag_FLAGPRM1/67)
		DOALL 255

_	_	EQ 67 253  ; sust(flag_FLAGPRM1/67)
		DOALL 253

_	_	EQ 67 252  ; sust(flag_FLAGPRM1/67)
		DOALL 252

_	_	PLUS 69 1  ; sust(flag_FLAGRET/69)
		ZERO 70  ; sust(flag_FLAGRET2/70)
		COPYFF 21 70 ; sust(flag_PROP_GENNUM/21) ; sust(flag_FLAGRET2/70)

/PRO 26 ; sust(proc_CONTARATREAL/26)

_	_	CLEAR 70  ; sust(flag_FLAGRET2/70)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		EQ 67 254  ; sust(flag_FLAGPRM1/67)
		LET 70 1  ; sust(flag_FLAGRET2/70)
		COPYFF 1 69  ; sust(flag_FLAGRET/69)
		DONE

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		CLEAR 69  ; sust(flag_FLAGRET/69)
		NOTEQ 67 253  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 255  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 252  ; sust(flag_FLAGPRM1/67)
		COPYFF 67 38 ; sust(flag_FLAGPRM1/67)

_	_	PROCESS 25  ; sust(proc_CONTARATINCREAL/25)
		COPYFF 104 38  ; sust(flag_TMPFLAG1/104)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 27 ; sust(proc_CRIATURAVERDELOC/27)

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		LET 34 73 
		LET 35 4 
		WHATO 
		COPYFF 54 87  ; sust(flag_LOCCRIATURAVERDE/87)
		PROCESS 77 ; sust(proc_RESTAURAFIRST/77)

/PRO 28 ; sust(proc_DARSUB/28)

_	_	WHATO

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		ZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 28 
		NEWTEXT 
		DONE

_	_	PROCESS 20  ; sust(proc_CODESECOND/20)
		EQ 69 255  ; sust(flag_FLAGRET/69)
		SYSMESS 127 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		COPYFF 54 104  ; sust(flag_TMPFLAG1/104)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		NOTSAME 104 38  ; sust(flag_TMPFLAG1/104)
		NOTEQ 104 254  ; sust(flag_TMPFLAG1/104)
		NOTEQ 104 253  ; sust(flag_TMPFLAG1/104)
		SYSMESS 127 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	GT 44 49 
		SYSMESS 99 
		SYSMESS 123 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		SYSMESS 124 
		PROCESS 70  ; sust(proc_PRINTSECONDARTUNDET/70)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	EQ 54 253 
		SYSMESS 99 
		SYSMESS 123 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		WRITE " _, " 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		PROCESS 72  ; sust(proc_PROACUSATIVO/72)
		SYSMESS 106 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTEQ 69 254  ; sust(flag_FLAGRET/69)
		GT 70 49  ; sust(flag_FLAGRET2/70)
		SYSMESS 99 
		SYSMESS 123 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		WRITE " _," 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		SYSMESS 146 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 136 
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 70 34  ; sust(flag_FLAGRET2/70)
		COPYFF 71 35  ; sust(flag_FLAGRET3/71)
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTEQ 69 254  ; sust(flag_FLAGRET/69)
		SYSMESS 99 
		SYSMESS 123 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		WRITE " _" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		SYSMESS 125 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 70 34  ; sust(flag_FLAGRET2/70)
		COPYFF 71 35  ; sust(flag_FLAGRET3/71)
		WHATO 
		WRITE "_.\n" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		NEWTEXT 
		DONE

_	_	PROCESS 65  ; sust(proc_PRINTFIRSTARTDETMAYUSC/65)
		SYSMESS 126 
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		WRITE "_.\n" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		NEWTEXT 
		DONE

/PRO 29 ; sust(proc_DEJARSUB/29)

_	_	WHATO

_	_	SAME 38 54 
		LT 34 50 
		SYSMESS 145 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE ".\n" 
		DONE

_	_	EQ 54 254 
		SYSMESS 98 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		PUTO 255 
		DONE

_	_	EQ 54 253 
		SYSMESS 24 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ", " 
		PROCESS 72  ; sust(proc_PROACUSATIVO/72)
		SYSMESS 106 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	SAME 54 38 
		SYSMESS 49 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	SYSMESS 28 
		NEWTEXT 
		DONE

/PRO 30 ; sust(proc_DESCPROPIEDAD/30)

_	_	EQ 51 10 
		NOTZERO 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		ISNOTAT 36 10  ; sust(obj_OBJCANALIZADORPLASMA/36)
		WRITE "En su interior hay varios componentes del propulsor, " 
		WRITE "pero falta el canalizador de plasma.\n" 
		DONE

_	_	PROCESS 38 ; sust(proc_EXTPROPIEDAD/38)

_	_	NOTZERO 17  ; sust(flag_PROP_ABRIBLE/17)
		NOTZERO 16  ; sust(flag_PROP_ABIERTO/16)
		PROCESS 65  ; sust(proc_PRINTFIRSTARTDETMAYUSC/65)
		SYSMESS 146 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 115 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		NOTZERO 19  ; sust(flag_PROP_CONTENEDOR/19)
		COPYFF 51 67  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		ZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 148 
		DONE

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTZERO 17  ; sust(flag_PROP_ABRIBLE/17)
		ZERO 16  ; sust(flag_PROP_ABIERTO/16)
		PROCESS 65  ; sust(proc_PRINTFIRSTARTDETMAYUSC/65)
		SYSMESS 146 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 116 
		PROCESS 87 ; sust(proc_TERMGENERO/87)

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTZERO 17  ; sust(flag_PROP_ABRIBLE/17)
		NOTZERO 16  ; sust(flag_PROP_ABIERTO/16)
		NOTZERO 19  ; sust(flag_PROP_CONTENEDOR/19)
		WRITE ". " 
		PROCESS 51  ; sust(proc_LISTATFIRSTNOUN/51)
		NEWLINE 
		DONE

_	_	ZERO 17  ; sust(flag_PROP_ABRIBLE/17)
		NOTZERO 19  ; sust(flag_PROP_CONTENEDOR/19)
		PROCESS 51  ; sust(proc_LISTATFIRSTNOUN/51)
		DONE

_	_	WRITE ".\n"

/PRO 31 ; sust(proc_DESCRIBESUSPENDIDO/31)

_	_	LT 65 2  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE " Te encuentras en suspensión"

_	_	GT 65 1  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE " Estás balanceándote enérgicamente"

_	_	WRITE ", colgando de una liana, "

_	_	EQ 65 1  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "y balanceándote suavemente "

_	_	LT 65 2  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "sobre un peligroso terraplén cubierto de afilados fragmentos de hielo incrustado."
		NEWLINE

_	_	EQ 65 2  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "y a tus pies ves una tranquila caverna de color verdusco."
		NEWLINE

_	_	EQ 65 3  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "sobre una fría gruta con grandes bloques de hielo."
		NEWLINE

_	_	GT 65 3  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "a la altura del peligroso terraplén de afilados trozos de hielo."
		NEWLINE

/PRO 32 ; sust(proc_DISPARARSUB/32)

_	_	EQ 34 73 
		PRESENT 14  ; sust(obj_OBJCRIATURAVERDE/14)
		CARRIED 7  ; sust(obj_OBJPISTOLANEUTRALIZANTE/7)
		WRITE "La descarga no produce ningún efecto. No parece el tipo de objetivo adecuado."
		NEWLINE 
		DONE

_	_	EQ 34 69 
		PRESENT 11  ; sust(obj_OBJGUSANO/11)
		CARRIED 7  ; sust(obj_OBJPISTOLANEUTRALIZANTE/7)
		WRITE "La descarga no produce ningún efecto. No parece el tipo de objetivo adecuado."
		NEWLINE 
		DONE

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		CARRIED 7  ; sust(obj_OBJPISTOLANEUTRALIZANTE/7)
		WRITE "No le ves utilidad a disparar" 
		WRITE " con la pistola " 
		PROCESS 63  ; sust(proc_PRINTFIRSTAL/63)
		WRITE " en este momento."
		NEWLINE 
		DONE

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No tienes con qué dispararle " 
		PROCESS 63  ; sust(proc_PRINTFIRSTAL/63)
		WRITE " en este momento."
		NEWLINE 
		DONE

_	_	CARRIED 7  ; sust(obj_OBJPISTOLANEUTRALIZANTE/7)
		EQ 34 255 
		WRITE "No le ves utilidad a disparar" 
		WRITE " en este momento."
		NEWLINE 
		DONE

_	_	CARRIED 7  ; sust(obj_OBJPISTOLANEUTRALIZANTE/7)
		WRITE "No le ves utilidad a disparar" 
		WRITE " a eso" 
		WRITE " en este momento."
		NEWLINE 
		DONE

_	_	WRITE "Disparar ¿Con qué?"
		NEWLINE 
		DONE

/PRO 33 ; sust(proc_ESCONTAINERSECOND/33)

_	_	CLEAR 69  ; sust(flag_FLAGRET/69)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		BNOTZERO 56 2 
		SET 69 ; sust(flag_FLAGRET/69)

_	_	PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 34 ; sust(proc_ESTACERRADO/34)

/PRO 35 ; sust(proc_EXAMINAPRESENT/35)

_	_	CLEAR 69  ; sust(flag_FLAGRET/69)
		COPYFF 34 78  ; sust(flag_GUARDANOUNEXA/78)
		COPYFF 35 73  ; sust(flag_GUARDAADJEXA/73)
		PARSE 
		COPYFF 78 34  ; sust(flag_GUARDANOUNEXA/78)
		COPYFF 73 35 ; sust(flag_GUARDAADJEXA/73)

_	FOSA	WRITE "La fosa es profunda y oscura, no se ve el fondo.\n" 
		DONE

_	TABLON	WRITE "Bastante grande y pesado.\n" 
		DONE

_	BOTAS	WRITE "Un poco extrañas, pero está claro que son para ponérselas en los pies. Son bastante grandes y resistentes.\n" 
		DONE

_	BOTAS	WRITE "Un poco extrañas, pero está claro que son para ponérselas en los pies. Son bastante grandes y resistentes.\n" 
		DONE

_	PISTOLA	WRITE "Se utiliza para atontar o dejar sin sentido a criaturas humanoides.\n" 
		DONE

_	BLOQUE	WRITE "Un enorme bloque de hielo, frío y deslizante.\n" 
		DONE

_	GUSANO	WRITE "Similar a las babosas en forma, que no en tamaño. Es de un color algo más tirando al marrón. No parece muy ágil.\n" 
		DONE

_	RECIPIENTE	WRITE "Está lleno de un líquido verde, viscoso y borboteante.\n" 
		DONE

_	ESPEJO	ABSENT 16  ; sust(obj_OBJESPEJO/16)
		WRITE "Un gran espejo cuadrado tras un duro cristal, orientado formando un ángulo de 45 grados. Junto a él, hay un extraño símbolo: un cuadrado y una flecha apuntando al interior.\n" 
		DONE

_	ESPEJO	WRITE "Es un espejo corriente de unos cincuenta centímetros de longitud.\n" 
		DONE

_	CRIATURA	WRITE "De aspecto antropomorfo, aunque no se podría calificar de humana. Aproximadamente 60 centímetros de longitud, piel verde y abotargada. Piernas cortas, brazos largos con uñas puntiagudas y cabeza achatada.\n" 
		DONE

_	BABOSA	WRITE "Una enorme y repugnante babosa. Al desplazarse deja como un rastro de asquerosa pulpa verde corrosiva.\n" 
		DONE

_	PUERTA	PRESENT 30  ; sust(obj_OBJPUERTAACERO/30)
		WRITE "Junto a ella hay un orificio hexagonal. Está cerrada.\n" 
		DONE

_	PANEL	WRITE "Tiene varios botones, en forma de flecha direccional: norte, sur, este, oeste, arriba y abajo.\n" 
		DONE

_	ROBOT	WRITE "Es básicamente un cilindro hermético con ruedas.\n" 
		PROCESS 80  ; sust(proc_ROBOTDESCRIBIR/80)
		CLEAR 69  ; sust(flag_FLAGRET/69)
		DONE

_	REJILLA	WRITE "Una especie de respiradero para el basurero de abajo, a bastante altura. " 
		ZERO 100  ; sust(flag_REJILLASINBARROTE/100)
		WRITE "Está hecha de barrotes muy separados, casi cabes entre ellos. Hay uno un poco suelto."
		NEWLINE 
		DONE

_	REJILLA	WRITE "Le falta un barrote, suficiente para caer hacia abajo.\n" 
		DONE

_	DISPOSITIV	ZERO 26  ; sust(flag_CAMPOFUERZADESTRUIDO/26)
		WRITE "En forma de cubo negro, con extraños símbolos y una serie de luces, todas de color rojo."
		NEWLINE 
		DONE

_	DISPOSITIV	WRITE "Un amasijo de hierros retorcidos."
		NEWLINE 
		DONE

_	NAVE	WRITE "Pequeña pero potente. En el frontal ves el compartimento del propulsor.\n" 
		LET 34 88 
		LET 35 255

_	TECNICO	ZERO 66  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Lleva un uniforme con máscara, al igual que el resto de operarios. Está dormido.\n" 
		DONE

_	TECNICO	EQ 66 1  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Duerme el sueño de los justos.\n" 
		DONE

_	FUSIBLE	WRITE "Bueno, es bastante grande para lo que estás acostumbrado: unos 50 o 60 cm.\n" 
		DONE

_	VENTANA	WRITE "A través de la ventana ves algo parecido a unos talleres, con multitud de hombrecillos uniformados trabajando.\n" 
		DONE

_	CANALIZADO	WRITE "Una pieza fundamental en la propulsión de tu nave.\n" 
		DONE

_   PEDERNAL    WRITELN "Es una variedad de cuarzo que hace chispas cuando se golpea."
		DONE


_   _   WRITELN "No sabría qué decirte. No le veo nada de especial."

_	_	SET 69 ; sust(flag_FLAGRET/69)

/PRO 36 ; sust(proc_EXAMINARSUB/36)

_	_	WHATO

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		ZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 108 
		WRITE ".\n" 
		DONE

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		ZERO 17  ; sust(flag_PROP_ABRIBLE/17)
		ZERO 19  ; sust(flag_PROP_CONTENEDOR/19)
		GT 34 49 
		SYSMESS 108 
		WRITE " en " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		DONE

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		LT 34 50 
		SYSMESS 109 
		WRITE "_.\n" 
		DONE

_	_	PROCESS 30  ; sust(proc_DESCPROPIEDAD/30)
		DONE

/PRO 37 ; sust(proc_EXTBABOSA/37)

_	_	CLEAR 18  ; sust(flag_PROP_BABOSA/18)
		CLEAR 12 ; sust(flag_LOCA_BABOSA/12)

_	_	EQ 51 17 
		SET 18  ; sust(flag_PROP_BABOSA/18)
		COPYFF 13 12 ; sust(flag_LOCBABOSA1/13) ; sust(flag_LOCA_BABOSA/12)

_	_	EQ 51 18 
		SET 18  ; sust(flag_PROP_BABOSA/18)
		COPYFF 14 12 ; sust(flag_LOCBABOSA2/14) ; sust(flag_LOCA_BABOSA/12)

/PRO 38 ; sust(proc_EXTPROPIEDAD/38)

_	_	CLEAR 23  ; sust(flag_PROP_NOLISTAR/23)
		CLEAR 20  ; sust(flag_PROP_ESTATICO/20)
		CLEAR 17  ; sust(flag_PROP_ABRIBLE/17)
		CLEAR 16  ; sust(flag_PROP_ABIERTO/16)
		CLEAR 19  ; sust(flag_PROP_CONTENEDOR/19)
		CLEAR 22  ; sust(flag_PROP_INCONTABLE/22)
		LET 21 5  ; sust(flag_PROP_GENNUM/21)
		WHATO

_	_	EQ 51 3 
		ZERO 86  ; sust(flag_LIANACORTADA/86)
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 4 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 2 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		LET 21 2 ; sust(flag_PROP_GENNUM/21)

_	_	EQ 51 1 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23  ; sust(flag_PROP_NOLISTAR/23)
		LET 21 1 ; sust(flag_PROP_GENNUM/21)

_	_	EQ 51 11 
		SET 20 ; sust(flag_PROP_ESTATICO/20)

_	_	EQ 51 11 
		ISAT 11 7  ; sust(obj_OBJGUSANO/11)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 11 
		ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 12 
		SET 23  ; sust(flag_PROP_NOLISTAR/23)
		SET 20 ; sust(flag_PROP_ESTATICO/20)

_	_	EQ 51 13 
		SET 23  ; sust(flag_PROP_NOLISTAR/23)
		SET 20 ; sust(flag_PROP_ESTATICO/20)

_	_	EQ 51 21 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 22 
		SET 20 ; sust(flag_PROP_ESTATICO/20)

_	_	EQ 51 23 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 25 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 26 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 27 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 10 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23  ; sust(flag_PROP_NOLISTAR/23)
		SET 17  ; sust(flag_PROP_ABRIBLE/17)
		LET 21 1  ; sust(flag_PROP_GENNUM/21)
		NOTZERO 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		SET 16 ; sust(flag_PROP_ABIERTO/16)

_	_	EQ 51 28 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 30 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 31 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 32 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 51 33 
		SET 20 ; sust(flag_PROP_ESTATICO/20)

_	_	EQ 51 9 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 17  ; sust(flag_PROP_ABRIBLE/17)
		LET 21 1  ; sust(flag_PROP_GENNUM/21)
		NOTZERO 62  ; sust(flag_CUADROELECTRICOABIERTO/62)
		SET 16 ; sust(flag_PROP_ABIERTO/16)

_	_	EQ 51 35 
		SET 20  ; sust(flag_PROP_ESTATICO/20)
		SET 23 ; sust(flag_PROP_NOLISTAR/23)

_	_	EQ 21 5  ; sust(flag_PROP_GENNUM/21)
		COPYFF 55 21 ; sust(flag_PROP_GENNUM/21)

_	_	BNOTZERO 56 2 
		SET 19  ; sust(flag_PROP_CONTENEDOR/19)
		ZERO 17  ; sust(flag_PROP_ABRIBLE/17)
		SET 16 ; sust(flag_PROP_ABIERTO/16)

_	_	DONE

/PRO 39 ; sust(proc_GENNUM/39)

/PRO 40 ; sust(proc_GOLPEATECNICO/40)

_	_	EQ 66 1  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Eso sería ensañamiento."
		NEWLINE 
		DONE

_	_	PREP CON 
		PROCESS 62  ; sust(proc_PRESENTSECOND/62)
		NOTEQ 69 254  ; sust(flag_FLAGRET/69)
		NOTEQ 69 253  ; sust(flag_FLAGRET/69)
		SYSMESS 28 
		DONE

_	_	NOUN2 PEDERNAL 
		LET 66 1  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Golpeas discretamente al técnico con " 
		PROCESS 69  ; sust(proc_PRINTSECONDARTDET/69)
		WRITE ", y lo dejas fuera de combate durante un buen rato."
		NEWLINE 
		DONE

_	_	NOUN2 BOTAS 
		LET 66 1  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Golpeas discretamente al técnico con " 
		PROCESS 69  ; sust(proc_PRINTSECONDARTDET/69)
		WRITE ", y lo dejas fuera de combate durante un buen rato."
		NEWLINE 
		DONE

_	_	NOUN2 BARROTE 
		LET 66 1  ; sust(flag_ESTADOTECNICO/66)
		WRITE "Golpeas discretamente al técnico con " 
		PROCESS 69  ; sust(proc_PRINTSECONDARTDET/69)
		WRITE ", y lo dejas fuera de combate durante un buen rato."
		NEWLINE 
		DONE

_	_	EQ 69 254  ; sust(flag_FLAGRET/69)
		WRITE "No conseguirías mucho."
		NEWLINE 
		DONE

_	_	EQ 69 253  ; sust(flag_FLAGRET/69)
		WRITE "No conseguirías mucho."
		NEWLINE 
		DONE

_	_	WRITE "¿Así, con las manos?"
		NEWLINE 
		DONE

/PRO 41 ; sust(proc_GUARDAFIRST/41)

_	_	COPYFF 34 75  ; sust(flag_GUARDAFIRSTNOUN/75)
		COPYFF 35 74  ; sust(flag_GUARDAFIRSTADJ/74)
		COPYFF 51 81  ; sust(flag_GUARDAPRESENTOBJ/81)
		COPYFF 54 80  ; sust(flag_GUARDAPRESENTLOC/80)
		COPYFF 55 83  ; sust(flag_GUARDAPRESENTWEIGHT/83)
		COPYFF 56 79  ; sust(flag_GUARDAPRESENTCONTAINER/79)
		COPYFF 57 82  ; sust(flag_GUARDAPRESENTWEARABLE/82)
		DONE

/PRO 42 ; sust(proc_HAYBABOSA/42)

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		CLEAR 69  ; sust(flag_FLAGRET/69)
		COPYFF 38 76  ; sust(flag_GUARDALOCACTUAL/76)
		COPYFF 67 38 ; sust(flag_FLAGPRM1/67)

_	_	PROCESS 43 ; sust(proc_HAYBABOSALOOP/43)

_	_	COPYFF 76 38  ; sust(flag_GUARDALOCACTUAL/76)
		PROCESS 77 ; sust(proc_RESTAURAFIRST/77)

/PRO 43 ; sust(proc_HAYBABOSALOOP/43)

_	_	DOALL 255

_	_	WHATO 
		PROCESS 37  ; sust(proc_EXTBABOSA/37)
		NOTZERO 18  ; sust(flag_PROP_BABOSA/18)
		SET 69 ; sust(flag_FLAGRET/69)

/PRO 44 ; sust(proc_INICIARLANZAMIENTO/44)

_	_	ZERO 84  ; sust(flag_LANZAMIENTOINICIADO/84)
		WRITE "'Iniciando comprobación de sistemas… " 
		PAUSE 40

_	_	ISAT 36 10  ; sust(obj_OBJCANALIZADORPLASMA/36)
		ZERO 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		NOTZERO 103  ; sust(flag_TANQUEHIDRAULICOLLENADO/103)
		ZERO 84  ; sust(flag_LANZAMIENTOINICIADO/84)
		ZERO 96  ; sust(flag_PUNTUADOLANZAMIENTOINICIADO/96)
		SET 96  ; sust(flag_PUNTUADOLANZAMIENTOINICIADO/96)
		PLUS 30 5 ; sust(flag_PUNTUACION/30)

_	_	ISAT 36 10  ; sust(obj_OBJCANALIZADORPLASMA/36)
		ZERO 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		NOTZERO 103  ; sust(flag_TANQUEHIDRAULICOLLENADO/103)
		ZERO 84  ; sust(flag_LANZAMIENTOINICIADO/84)
		SET 84  ; sust(flag_LANZAMIENTOINICIADO/84)
		WRITE "OK'\nLa nave es transportada a través del hangar hacia la gran compuerta. Una vez abierta, la nave es posicionada verticalmente en un pequeño silo, lista para el lanzamiento."
		NEWLINE 
		DONE

_	_	NOTZERO 84  ; sust(flag_LANZAMIENTOINICIADO/84)
		CLEAR 84  ; sust(flag_LANZAMIENTOINICIADO/84)
		WRITE "El lanzamiento se suspende y la nave es devuelta a su posición en el hangar."
		NEWLINE 
		DONE

_	_	NOTZERO 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		WRITE "NOK(ERR70F5). Compartimento abierto'"
		NEWLINE 
		DONE

_	_	ISNOTAT 36 10  ; sust(obj_OBJCANALIZADORPLASMA/36)
		WRITE "NOK(ERR14A5). No detectado. Sist: plato de impulso. Componente: canalizador'"
		NEWLINE 
		DONE

_	_	ZERO 103  ; sust(flag_TANQUEHIDRAULICOLLENADO/103)
		WRITE "NOK(ERR83E4). Externo. Sistema hidráulico de posicionamiento'"
		NEWLINE 
		DONE

/PRO 45 ; sust(proc_INITBABOSAS/45)

_	_	LET 13 6  ; sust(flag_LOCBABOSA1/13)
		PLACE 17 6  ; sust(obj_OBJBABOSA1/17)
		LET 14 5  ; sust(flag_LOCBABOSA2/14)
		PLACE 18 5 ; sust(obj_OBJBABOSA2/18)

/PRO 46 ; sust(proc_INVENTARIO/46)

_	_	NOTZERO 1 
		SYSMESS 9 
		LET 67 254  ; sust(flag_FLAGPRM1/67)
		PROCESS 50  ; sust(proc_LISTAT/50)
		LET 67 253  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 96 
		SYSMESS 10

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		EQ 70 1  ; sust(flag_FLAGRET2/70)
		WRITE "o "

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		EQ 70 2  ; sust(flag_FLAGRET2/70)
		WRITE "a "

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		EQ 70 3  ; sust(flag_FLAGRET2/70)
		WRITE "os "

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		EQ 70 4  ; sust(flag_FLAGRET2/70)
		WRITE "as "

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		PROCESS 50  ; sust(proc_LISTAT/50)
		NEWLINE 
		DONE

_	_	LET 67 253  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "L" 
		SYSMESS 10

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		EQ 70 1  ; sust(flag_FLAGRET2/70)
		WRITE "o "

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		EQ 70 2  ; sust(flag_FLAGRET2/70)
		WRITE "a "

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		EQ 70 3  ; sust(flag_FLAGRET2/70)
		WRITE "os "

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		EQ 70 4  ; sust(flag_FLAGRET2/70)
		WRITE "as "

_	_	NOTZERO 69  ; sust(flag_FLAGRET/69)
		PROCESS 50  ; sust(proc_LISTAT/50)
		NEWLINE 
		DONE

_	_	ZERO 1 
		LET 67 253  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		ZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 97 
		NEWLINE 
		DONE

_	_	ZERO 69  ; sust(flag_FLAGRET/69)
		NEWLINE

/PRO 47 ; sust(proc_LIMPIARFOSA/47)

_	_	DOALL 2

_	_	WHATO

_	_	EQ 34 60 
		PUTO 19

_	_	NOTEQ 34 71 
		NOTEQ 34 60 
		PUTO 3

_	_	PROCESS 65  ; sust(proc_PRINTFIRSTARTDETMAYUSC/65)
		WRITE " cae" 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		WRITE " por la fosa y se pierde en la oscuridad."
		NEWLINE

_	_	EQ 34 71 
		DESTROY 16  ; sust(obj_OBJESPEJO/16)
		WRITE "Oyes como el espejo se despedaza contra el fondo de la fosa."
		NEWLINE

_	_	DONE

/PRO 48 ; sust(proc_LIMPIARRIACHUELO/48)

_	_	DOALL 1

_	_	EQ 34 61 
		WRITE "Las botas son muy pesadas, y quedan varadas en la orilla."
		NEWLINE 
		PUTO 14 
		DONE

_	_	NOTEQ 34 60 
		WHATO 
		PUTO 15 
		WRITE "Al caer al agua, " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE " es arrastrad" 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE " por la corriente y se pierde en el lago."
		NEWLINE 
		DONE

/PRO 49 ; sust(proc_LIMPIARSUSPENDIDO/49)

_	_	DOALL 13

_	_	WHATO

_	_	NOTEQ 65 3  ; sust(flag_ESTADOSUSPENSION/65)
		PUTO 3

_	_	EQ 65 3  ; sust(flag_ESTADOSUSPENSION/65)
		PUTO 19

_	_	PROCESS 65  ; sust(proc_PRINTFIRSTARTDETMAYUSC/65)
		WRITE " cae" 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		WRITE " al fondo de la cueva."
		NEWLINE 
		DONE

/PRO 50 ; sust(proc_LISTAT/50)

_	_	PROCESS 23  ; sust(proc_CONTARAT/23)
		ZERO 69  ; sust(flag_FLAGRET/69)
		DONE

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		NOTEQ 67 253  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 255  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 252  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 254  ; sust(flag_FLAGPRM1/67)
		COPYFF 67 38 ; sust(flag_FLAGPRM1/67)

_	_	PROCESS 52  ; sust(proc_LISTATPRINT/52)
		COPYFF 104 38  ; sust(flag_TMPFLAG1/104)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 51 ; sust(proc_LISTATFIRSTNOUN/51)

_	_	EQ 51 10 
		WRITE "En su interior hay varios componentes del propulsor, " 
		ISAT 36 10  ; sust(obj_OBJCANALIZADORPLASMA/36)
		WRITE "incluyendo el canalizador.\n" 
		DONE

_	_	EQ 51 10 
		WRITE "pero falta el canalizador de plasma.\n" 
		DONE

_	_	WHATO 
		SYSMESS 138 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		COPYFF 51 67  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 139 
		PROCESS 50  ; sust(proc_LISTAT/50)
		DONE

_	_	SYSMESS 140 
		DONE

/PRO 52 ; sust(proc_LISTATPRINT/52)

_	_	LET 105 1 ; sust(flag_TMPFLAG2/105)

_	_	NOTEQ 67 253  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 252  ; sust(flag_FLAGPRM1/67)
		NOTEQ 67 254  ; sust(flag_FLAGPRM1/67)
		DOALL 255

_	_	EQ 67 253  ; sust(flag_FLAGPRM1/67)
		DOALL 253

_	_	EQ 67 252  ; sust(flag_FLAGPRM1/67)
		DOALL 252

_	_	EQ 67 254  ; sust(flag_FLAGPRM1/67)
		DOALL 254

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTZERO 23  ; sust(flag_PROP_NOLISTAR/23)
		NOTEQ 50 254 
		NOTEQ 50 253 
		DONE

_	_	GT 105 1  ; sust(flag_TMPFLAG2/105)
		NOTSAME 105 69  ; sust(flag_TMPFLAG2/105) ; sust(flag_FLAGRET/69)
		SYSMESS 46

_	_	GT 105 1  ; sust(flag_TMPFLAG2/105)
		SAME 105 69  ; sust(flag_TMPFLAG2/105) ; sust(flag_FLAGRET/69)
		SYSMESS 47

_	_	PROCESS 66  ; sust(proc_PRINTFIRSTARTUNDET/66)
		SAME 105 69  ; sust(flag_TMPFLAG2/105) ; sust(flag_FLAGRET/69)
		WRITE "."

_	_	PLUS 105 1  ; sust(flag_TMPFLAG2/105)
		DONE

/PRO 53 ; sust(proc_LISTOBJ/53)

_	_	LET 67 255  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		ZERO 69  ; sust(flag_FLAGRET/69)
		DONE

_	_	SYSMESS 1 
		LET 67 255  ; sust(flag_FLAGPRM1/67)
		PROCESS 50  ; sust(proc_LISTAT/50)
		NEWLINE 
		DONE

/PRO 54 ; sust(proc_LOCSECOND/54)

_	_	EQ 44 255 
		LET 69 252  ; sust(flag_FLAGRET/69)
		DONE

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		COPYFF 54 69  ; sust(flag_FLAGRET/69)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 55 ; sust(proc_METERSUB/55)

_	_	NOUN2 COMPARTIME 
		NOTZERO 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		PRESENT 10  ; sust(obj_OBJCOMPARTIMENTO/10)
		NOTEQ 34 99 
		WRITE "El compartimento no es para eso."
		NEWLINE 
		DONE

_	_	NOUN2 COMPARTIME 
		NOTZERO 27  ; sust(flag_COMPARTIMENTOABIERTO/27)
		PRESENT 10  ; sust(obj_OBJCOMPARTIMENTO/10)
		ZERO 94  ; sust(flag_PUNTUADOCOMPARTIMENTO/94)
		SET 94  ; sust(flag_PUNTUADOCOMPARTIMENTO/94)
		PLUS 30 1 ; sust(flag_PUNTUACION/30)

_	_	WHATO

_	_	EQ 54 254 
		NOUN2 SUCCIONADO 
		PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE "El succionador está bloqueado por el gusano."
		NEWLINE 
		DONE

_	_	EQ 54 254 
		NOUN2 SUCCIONADO 
		PRESENT 25  ; sust(obj_OBJSUCCIONADOR/25)
		PUTO 25 
		PROCESS 65  ; sust(proc_PRINTFIRSTARTDETMAYUSC/65)
		WRITE " desaparece" 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		WRITE ", absorvid" 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE " con ímpetu al interior."
		NEWLINE 
		DONE

_	_	NOUN2 CUADRO 
		PRESENT 9  ; sust(obj_OBJCUADROELECTRICO/9)
		EQ 54 254 
		NOTZERO 62  ; sust(flag_CUADROELECTRICOABIERTO/62)
		LET 67 9  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "No puedes meter nada en el cuadro eléctrico, está ocupado por " 
		PROCESS 50  ; sust(proc_LISTAT/50)
		NEWLINE 
		DONE

_	_	NOUN2 CUADRO 
		PRESENT 9  ; sust(obj_OBJCUADROELECTRICO/9)
		EQ 54 254 
		NOTZERO 62  ; sust(flag_CUADROELECTRICOABIERTO/62)
		NOTEQ 51 34 
		NOTEQ 51 24 
		WRITE "No puedes meter eso en el cuadro eléctrico."
		NEWLINE 
		DONE

_	_	NOTSAME 38 54 
		NOTEQ 54 254 
		NOTEQ 54 253 
		SYSMESS 100 
		SYSMESS 117 
		SYSMESS 104 
		NEWTEXT 
		DONE

_	_	EQ 54 253 
		SYSMESS 99 
		SYSMESS 117 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ", " 
		PROCESS 72  ; sust(proc_PROACUSATIVO/72)
		SYSMESS 106 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	SAME 54 38 
		LT 34 50 
		SYSMESS 142 
		NEWTEXT 
		DONE

_	_	SAME 54 38 
		SYSMESS 49 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	NOUN2 _ 
		EQ 54 254 
		SYSMESS 132 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 54  ; sust(proc_LOCSECOND/54)
		NOTSAME 38 69  ; sust(flag_FLAGRET/69)
		NOTEQ 69 254  ; sust(flag_FLAGRET/69)
		NOTEQ 69 253  ; sust(flag_FLAGRET/69)
		SYSMESS 132 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 33  ; sust(proc_ESCONTAINERSECOND/33)
		LT 44 50 
		SYSMESS 142 
		NEWTEXT 
		DONE

_	_	PROCESS 33  ; sust(proc_ESCONTAINERSECOND/33)
		ZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 134 
		PROCESS 70  ; sust(proc_PRINTSECONDARTUNDET/70)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		NOTZERO 17  ; sust(flag_PROP_ABRIBLE/17)
		ZERO 16  ; sust(flag_PROP_ABIERTO/16)
		SYSMESS 99 
		SYSMESS 117 
		SYSMESS 119 
		PROCESS 69  ; sust(proc_PRINTSECONDARTDET/69)
		SYSMESS 120 
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 116 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		WRITE ".\n" 
		DONE

_	_	PROCESS 20  ; sust(proc_CODESECOND/20)
		COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		COPYFF 69 38  ; sust(flag_FLAGRET/69)
		AUTOD 
		COPYFF 104 38  ; sust(flag_TMPFLAG1/104)
		SYSMESS 135 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		SYSMESS 136 
		PROCESS 69  ; sust(proc_PRINTSECONDARTDET/69)
		WRITE ".\n" 
		DONE

/PRO 56 ; sust(proc_MIRARENSUB/56)

_	_	WHATO

_	_	AT 24  ; sust(loc_SALADEBASURAS/24)
		EQ 34 85 
		ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE "El succionador está atascado por un gran gusano."
		NEWLINE 
		DONE

_	_	AT 24  ; sust(loc_SALADEBASURAS/24)
		EQ 34 83 
		WRITE "La rejilla está muy alta y el interior es oscuro."
		NEWLINE 
		DONE

_	_	BNOTZERO 56 2 
		PROCESS 30  ; sust(proc_DESCPROPIEDAD/30)
		DONE

_	_	NOTEQ 51 255 
		SYSMESS 138 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		SYSMESS 140 
		DONE

_	_	SYSMESS 141 
		DONE

/PRO 57 ; sust(proc_MONTAHIELOPROC/57)

_	_	CARRIED 5  ; sust(obj_OBJTABLON/5)
		WRITE "Con el tablón sería muy aparatoso."
		NEWLINE 
		DONE

_	_	DESTROY 8  ; sust(obj_OBJBLOQUEHIELO/8)
		PROCESS 74  ; sust(proc_PUNTUAHIELO/74)
		WRITE "Te subes al bloque de hielo, y al instante comienza a moverse hacia el terraplén.  Apenas llega al borde, se precipita veloz hacia el abismo, tropezando y saltando al chocar con los trozos de hielo dispersos del suelo. Cuando llegas abajo, el bloque de hielo es historia, pero tú pareces estar (increiblemente) en buenas condiciones."
		NEWLINE 
		GOTO 3  ; sust(loc_CAVERNAVERDE/3)
		DESC

/PRO 58 ; sust(proc_MOSTRARSUB/58)

_	_	WHATO

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		ZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 28 
		NEWTEXT 
		DONE

_	_	PROCESS 20  ; sust(proc_CODESECOND/20)
		EQ 69 255  ; sust(flag_FLAGRET/69)
		SYSMESS 129 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		COPYFF 54 104  ; sust(flag_TMPFLAG1/104)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		NOTSAME 104 38  ; sust(flag_TMPFLAG1/104)
		NOTEQ 104 254  ; sust(flag_TMPFLAG1/104)
		NOTEQ 104 253  ; sust(flag_TMPFLAG1/104)
		SYSMESS 129 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	GT 44 49 
		SYSMESS 99 
		SYSMESS 128 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		SYSMESS 124 
		PROCESS 70  ; sust(proc_PRINTSECONDARTUNDET/70)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTEQ 69 254  ; sust(flag_FLAGRET/69)
		NOTEQ 69 253  ; sust(flag_FLAGRET/69)
		GT 70 49  ; sust(flag_FLAGRET2/70)
		SYSMESS 99 
		SYSMESS 128 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		WRITE " _," 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		SYSMESS 146 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 136 
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 70 34  ; sust(flag_FLAGRET2/70)
		COPYFF 71 35  ; sust(flag_FLAGRET3/71)
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		NOTEQ 69 254  ; sust(flag_FLAGRET/69)
		NOTEQ 69 253  ; sust(flag_FLAGRET/69)
		SYSMESS 99 
		SYSMESS 128 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		WRITE " _" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		SYSMESS 125 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 70 34  ; sust(flag_FLAGRET2/70)
		COPYFF 71 35  ; sust(flag_FLAGRET3/71)
		WHATO 
		WRITE "_.\n" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		NEWTEXT 
		DONE

_	_	PROCESS 65  ; sust(proc_PRINTFIRSTARTDETMAYUSC/65)
		SYSMESS 126 
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		WRITE "_.\n" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		NEWTEXT 
		DONE

/PRO 59 ; sust(proc_MUEVEBABOSA/59)

_	_	COPYFF 67 69 ; sust(flag_FLAGPRM1/67) ; sust(flag_FLAGRET/69)

_	_	CHANCE 30 
		DONE

_	_	PROCESS 27  ; sust(proc_CRIATURAVERDELOC/27)
		SAME 67 87  ; sust(flag_FLAGPRM1/67) ; sust(flag_LOCCRIATURAVERDE/87)
		CHANCE 50 
		LET 69 1  ; sust(flag_FLAGRET/69)
		DONE

_	_	SAME 67 38  ; sust(flag_FLAGPRM1/67)
		CHANCE 50 
		LET 69 2  ; sust(flag_FLAGRET/69)
		DONE

_	_	RANDOM 99 ; sust(flag_RANDOMBABOSA/99)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 3  ; sust(flag_FLAGPRM1/67)
		LT 99 26  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 7 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 3  ; sust(flag_FLAGPRM1/67)
		GT 99 25  ; sust(flag_RANDOMBABOSA/99)
		LT 99 51  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 6 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 3  ; sust(flag_FLAGPRM1/67)
		GT 99 50  ; sust(flag_RANDOMBABOSA/99)
		LT 99 76  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 4 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 3  ; sust(flag_FLAGPRM1/67)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 69 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 7  ; sust(flag_FLAGPRM1/67)
		LT 99 51  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 3 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 7  ; sust(flag_FLAGPRM1/67)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 6 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 6  ; sust(flag_FLAGPRM1/67)
		LT 99 34  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 7 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 6  ; sust(flag_FLAGPRM1/67)
		GT 99 66  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 3 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 6  ; sust(flag_FLAGPRM1/67)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 69 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 4  ; sust(flag_FLAGPRM1/67)
		LT 99 34  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 3 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 4  ; sust(flag_FLAGPRM1/67)
		GT 99 66  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 5 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		EQ 67 4  ; sust(flag_FLAGPRM1/67)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 69 ; sust(flag_FLAGRET/69)

_	_	NOTZERO 99  ; sust(flag_RANDOMBABOSA/99)
		CLEAR 99  ; sust(flag_RANDOMBABOSA/99)
		LET 69 4 ; sust(flag_FLAGRET/69)

/PRO 60 ; sust(proc_PONERSUB/60)

_	_	WHATO

_	_	EQ 51 29 
		CARRIED 29  ; sust(obj_OBJUNIFORME/29)
		LET 67 253  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "Antes tienes que quitarte todo."
		NEWLINE 
		DONE

_	_	SAME 38 54 
		LT 34 50 
		SYSMESS 142 
		DONE

_	_	EQ 54 254 
		BNOTZERO 56 1 
		SYSMESS 37 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		PUTO 253 
		DONE

_	_	EQ 54 253 
		SYSMESS 29 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE " " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	EQ 54 254 
		SYSMESS 40 
		PROCESS 8  ; sust(proc_ARTUNDET/8)
		WRITE " _?\n" 
		NEWTEXT 
		DONE

_	_	SAME 54 38 
		SYSMESS 49 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	SYSMESS 28 
		NEWTEXT 
		DONE

/PRO 61 ; sust(proc_PRESENT/61)

_	_	CLEAR 69  ; sust(flag_FLAGRET/69)
		WHATO 
		SAME 54 38 
		COPYFF 38 69  ; sust(flag_FLAGRET/69)
		DONE

_	_	EQ 54 254 
		COPYFF 54 69  ; sust(flag_FLAGRET/69)
		DONE

_	_	EQ 54 253 
		COPYFF 54 69  ; sust(flag_FLAGRET/69)
		DONE

_	_	COPYFF 54 68  ; sust(flag_FLAGPRM2/68)
		LET 67 255  ; sust(flag_FLAGPRM1/67)
		PROCESS 16  ; sust(proc_BUSCAREN/16)
		ZERO 69  ; sust(flag_FLAGRET/69)
		LET 67 254  ; sust(flag_FLAGPRM1/67)
		PROCESS 16  ; sust(proc_BUSCAREN/16)
		ZERO 69  ; sust(flag_FLAGRET/69)
		LET 67 253  ; sust(flag_FLAGPRM1/67)
		PROCESS 16 ; sust(proc_BUSCAREN/16)

/PRO 62 ; sust(proc_PRESENTSECOND/62)

_	_	COPYFF 34 77  ; sust(flag_GUARDANOUN/77)
		COPYFF 35 72  ; sust(flag_GUARDAADJ/72)
		COPYFF 44 34 
		COPYFF 45 35 
		PROCESS 61  ; sust(proc_PRESENT/61)
		COPYFF 77 34  ; sust(flag_GUARDANOUN/77)
		COPYFF 72 35  ; sust(flag_GUARDAADJ/72)
		WHATO

/PRO 63 ; sust(proc_PRINTFIRSTAL/63)

_	_	WHATO 
		WRITE "a" 
		PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		EQ 21 1  ; sust(flag_PROP_GENNUM/21)
		WRITE "l"

_	_	NOTEQ 21 1  ; sust(flag_PROP_GENNUM/21)
		WRITE " " 
		PROCESS 6 ; sust(proc_ARTDET/6)

_	_	WRITE " _" 
		DONE

/PRO 64 ; sust(proc_PRINTFIRSTARTDET/64)

_	_	GT 34 49 
		PROCESS 6  ; sust(proc_ARTDET/6)
		WRITE " "

_	_	WRITE "_" 
		DONE

/PRO 65 ; sust(proc_PRINTFIRSTARTDETMAYUSC/65)

_	_	GT 34 49 
		PROCESS 7  ; sust(proc_ARTDETMAYUSC/7)
		WRITE " "

_	_	WRITE "_" 
		DONE

/PRO 66 ; sust(proc_PRINTFIRSTARTUNDET/66)

_	_	GT 34 49 
		PROCESS 8  ; sust(proc_ARTUNDET/8)
		PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		ZERO 22  ; sust(flag_PROP_INCONTABLE/22)
		WRITE " "

_	_	WRITE "_" 
		DONE

/PRO 67 ; sust(proc_PRINTFIRSTDEL/67)

_	_	WHATO 
		WRITE "d" 
		PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTEQ 21 1  ; sust(flag_PROP_GENNUM/21)
		WRITE "e "

_	_	PROCESS 6  ; sust(proc_ARTDET/6)
		WRITE " _" 
		DONE

/PRO 68 ; sust(proc_PRINTSECOND/68)

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		WRITE "_" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 69 ; sust(proc_PRINTSECONDARTDET/69)

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		PROCESS 6  ; sust(proc_ARTDET/6)
		WRITE " _" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 70 ; sust(proc_PRINTSECONDARTUNDET/70)

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		PROCESS 8  ; sust(proc_ARTUNDET/8)
		WRITE " _" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		DONE

/PRO 71 ; sust(proc_PRINTSECONDDEL/71)

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		PROCESS 67 ; sust(proc_PRINTFIRSTDEL/67)

_	_	PROCESS 77 ; sust(proc_RESTAURAFIRST/77)

/PRO 72 ; sust(proc_PROACUSATIVO/72)

_	_	WRITE "l" 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		DONE

/PRO 73 ; sust(proc_PRODATIVO/73)

_	_	WRITE "le" 
		PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		GT 21 2  ; sust(flag_PROP_GENNUM/21)
		WRITE "s" 
		DONE

_	_	DONE

/PRO 74 ; sust(proc_PUNTUAHIELO/74)

_	_	ZERO 15  ; sust(flag_MONTADOHIELOPUNTUADO/15)
		SET 15  ; sust(flag_MONTADOHIELOPUNTUADO/15)
		PLUS 30 1 ; sust(flag_PUNTUACION/30)

/PRO 75 ; sust(proc_PUNTUARRECIPIENTE/75)

_	_	ZERO 97  ; sust(flag_PUNTUADORECIPIENTE/97)
		SET 97  ; sust(flag_PUNTUADORECIPIENTE/97)
		PLUS 30 2 ; sust(flag_PUNTUACION/30)

/PRO 76 ; sust(proc_QUITARSUB/76)

_	_	WHATO

_	_	EQ 51 29 
		WORN 29  ; sust(obj_OBJUNIFORME/29)
		LET 67 253  ; sust(flag_FLAGPRM1/67)
		PROCESS 23  ; sust(proc_CONTARAT/23)
		GT 69 1  ; sust(flag_FLAGRET/69)
		WRITE "Antes tienes que quitarte el resto de cosas que llevas puestas."
		NEWLINE 
		DONE

_	_	SAME 38 54 
		LT 34 50 
		SYSMESS 145 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE ".\n" 
		DONE

_	_	EQ 54 253 
		SYSMESS 38 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		PUTO 254 
		DONE

_	_	EQ 54 254 
		SYSMESS 50 
		PROCESS 72  ; sust(proc_PROACUSATIVO/72)
		SYSMESS 106 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	SAME 54 38 
		SYSMESS 49 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	SYSMESS 28 
		NEWTEXT 
		DONE

/PRO 77 ; sust(proc_RESTAURAFIRST/77)

_	_	COPYFF 75 34  ; sust(flag_GUARDAFIRSTNOUN/75)
		COPYFF 74 35  ; sust(flag_GUARDAFIRSTADJ/74)
		COPYFF 81 51  ; sust(flag_GUARDAPRESENTOBJ/81)
		COPYFF 80 54  ; sust(flag_GUARDAPRESENTLOC/80)
		COPYFF 83 55  ; sust(flag_GUARDAPRESENTWEIGHT/83)
		COPYFF 79 56  ; sust(flag_GUARDAPRESENTCONTAINER/79)
		COPYFF 82 57  ; sust(flag_GUARDAPRESENTWEARABLE/82)
		DONE

/PRO 78 ; sust(proc_ROBOTCOGER/78)

_	_	LET 67 23  ; sust(flag_FLAGPRM1/67)
		PROCESS 26  ; sust(proc_CONTARATREAL/26)
		NOTZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "Una luz roja parpadea acompañada de un sonido grave. Y luego se apaga."
		NEWLINE 
		DONE

_	_	PRESENT 22  ; sust(obj_OBJROBOT/22)
		PRESENT 11  ; sust(obj_OBJGUSANO/11)
		WRITE "El robot emite un sonido agudo, y ves flotar a" 
		WRITE "l gusano sobre el robot."
		NEWLINE

_	_	LET 34 69 
		LET 35 255 
		WHATO 
		SAME 92 54  ; sust(flag_POSICIONROBOT/92)
		PLACE 11 23  ; sust(obj_OBJGUSANO/11)
		WRITE "Una luz verde en el panel parpadea durante unos instantes y luego se apaga."
		NEWLINE 
		DONE

_	_	SAME 92 38  ; sust(flag_POSICIONROBOT/92)
		WRITE "¡Eso sería muy peligroso, estás en la misma sala que el robot!"
		NEWLINE 
		DONE

_	_	LET 34 73 
		LET 35 255 
		WHATO 
		SAME 92 54  ; sust(flag_POSICIONROBOT/92)
		PLACE 14 23  ; sust(obj_OBJCRIATURAVERDE/14)
		WRITE "Una luz verde en el panel parpadea durante unos instantes y luego se apaga."
		NEWLINE 
		SET 64  ; sust(flag_ESPEJOMOVIDO/64)
		DONE

_	_	WRITE "Una luz roja parpadea acompañada de un sonido grave. Y luego se apaga."
		NEWLINE

/PRO 79 ; sust(proc_ROBOTDEJAR/79)

_	_	LET 67 23  ; sust(flag_FLAGPRM1/67)
		PROCESS 26  ; sust(proc_CONTARATREAL/26)
		ZERO 69  ; sust(flag_FLAGRET/69)
		WRITE "Una luz roja parpadea acompañada de un sonido grave. Y luego se apaga."
		NEWLINE 
		DONE

_	_	PRESENT 22  ; sust(obj_OBJROBOT/22)
		ISAT 11 23  ; sust(obj_OBJGUSANO/11)
		WRITE "El robot emite un sonido agudo, y ves flotar a" 
		WRITE "l gusano hasta el suelo."
		NEWLINE

_	_	PRESENT 22  ; sust(obj_OBJROBOT/22)
		ISAT 14 23  ; sust(obj_OBJCRIATURAVERDE/14)
		WRITE "El robot emite un sonido agudo, y ves flotar a" 
		WRITE " la criatura verde hasta el suelo."
		NEWLINE

_	_	ISAT 11 23  ; sust(obj_OBJGUSANO/11)
		EQ 92 24  ; sust(flag_POSICIONROBOT/92)
		PLACE 11 25  ; sust(obj_OBJGUSANO/11)
		WRITE "Una luz verde en el panel parpadea durante unos instantes y luego se apaga." 
		WRITE " Desde el sur te llega el sonido del succionador funcionando."
		NEWLINE 
		DONE

_	_	ISAT 14 23  ; sust(obj_OBJCRIATURAVERDE/14)
		EQ 92 24  ; sust(flag_POSICIONROBOT/92)
		ISNOTAT 11 25  ; sust(obj_OBJGUSANO/11)
		PLACE 14 25  ; sust(obj_OBJCRIATURAVERDE/14)
		WRITE "Una luz verde en el panel parpadea durante unos instantes y luego se apaga." 
		WRITE " Desde el sur te llega el sonido del succionador funcionando."
		NEWLINE 
		DONE

_	_	ISAT 11 23  ; sust(obj_OBJGUSANO/11)
		COPYFO 92 11  ; sust(flag_POSICIONROBOT/92) ; sust(obj_OBJGUSANO/11)
		WRITE "Una luz verde en el panel parpadea durante unos instantes y luego se apaga."
		NEWLINE 
		DONE

_	_	ISAT 14 23  ; sust(obj_OBJCRIATURAVERDE/14)
		COPYFO 92 14  ; sust(flag_POSICIONROBOT/92) ; sust(obj_OBJCRIATURAVERDE/14)
		WRITE "Una luz verde en el panel parpadea durante unos instantes y luego se apaga."
		NEWLINE 
		DONE

/PRO 80 ; sust(proc_ROBOTDESCRIBIR/80)

_	_	LET 67 23  ; sust(flag_FLAGPRM1/67)
		PROCESS 26  ; sust(proc_CONTARATREAL/26)
		ZERO 69  ; sust(flag_FLAGRET/69)
		DONE

_	_	WRITE "El robot lleva "

_	_	ISAT 11 23  ; sust(obj_OBJGUSANO/11)
		WRITE "un gusano enorme"

_	_	ISAT 14 23  ; sust(obj_OBJCRIATURAVERDE/14)
		WRITE "una criatura verde"

_	_	WRITE " en suspensión sobre su cabeza. " 
		DONE

/PRO 81 ; sust(proc_ROBOTMOVER/81)

_	_	COPYFF 92 91  ; sust(flag_POSICIONROBOT/92) ; sust(flag_ORIGENROBOT/91)
		MOVE 92 ; sust(flag_POSICIONROBOT/92)

_	_	EQ 92 20  ; sust(flag_POSICIONROBOT/92)
		COPYFF 91 92 ; sust(flag_ORIGENROBOT/91) ; sust(flag_POSICIONROBOT/92)

_	_	EQ 92 27  ; sust(flag_POSICIONROBOT/92)
		COPYFF 91 92 ; sust(flag_ORIGENROBOT/91) ; sust(flag_POSICIONROBOT/92)

_	_	NOTSAME 91 92  ; sust(flag_ORIGENROBOT/91) ; sust(flag_POSICIONROBOT/92)
		COPYFO 92 22  ; sust(flag_POSICIONROBOT/92) ; sust(obj_OBJROBOT/22)
		WRITE "Una luz verde en el panel parpadea durante unos instantes y luego se apaga."
		NEWLINE

_	_	NOTSAME 91 92  ; sust(flag_ORIGENROBOT/91) ; sust(flag_POSICIONROBOT/92)
		PRESENT 22  ; sust(obj_OBJROBOT/22)
		WRITE "El robot aparece en la estancia."
		NEWLINE

_	_	NOTSAME 91 92  ; sust(flag_ORIGENROBOT/91) ; sust(flag_POSICIONROBOT/92)
		SAME 91 38  ; sust(flag_ORIGENROBOT/91)
		WRITE "El robot desaparece de la sala deslizándose suavemente."
		NEWLINE

_	_	SAME 91 92  ; sust(flag_ORIGENROBOT/91) ; sust(flag_POSICIONROBOT/92)
		WRITE "Una luz roja parpadea acompañada de un sonido grave. Y luego se apaga."
		NEWLINE

/PRO 82 ; sust(proc_ROMPEHIELO/82)

_	_	PRESENT 8  ; sust(obj_OBJBLOQUEHIELO/8)
		WRITE "Hay un bloque en el suelo ¿Cuantos quieres arrancar?"
		NEWLINE 
		DONE

_	_	NOUN2 BOTAS 
		PRESENT 6  ; sust(obj_OBJBOTAS/6)
		NOTWORN 6  ; sust(obj_OBJBOTAS/6)
		WRITE "No sería mala opción con esas botazas, si al menos las tuvieras puestas."
		NEWLINE 
		DONE

_	_	NOUN2 PIE 
		PRESENT 6  ; sust(obj_OBJBOTAS/6)
		NOTWORN 6  ; sust(obj_OBJBOTAS/6)
		WRITE "No sería mala opción con esas botazas, si al menos las tuvieras puestas."
		NEWLINE 
		DONE

_	_	NOUN2 BOTAS 
		WORN 6  ; sust(obj_OBJBOTAS/6)
		PLACE 8 19  ; sust(obj_OBJBLOQUEHIELO/8)
		WRITE "Das un enorme patadón a una de las protuberancias heladas. La fuerza del golpe lo desprende de su asidero y cae al suelo entre ecos retumbantes."
		NEWLINE 
		DONE

_	_	NOUN2 PIE 
		WORN 6  ; sust(obj_OBJBOTAS/6)
		PLACE 8 19  ; sust(obj_OBJBLOQUEHIELO/8)
		WRITE "Das un enorme patadón a una de las protuberancias heladas. La fuerza del golpe lo desprende de su asidero y cae al suelo entre ecos retumbantes."
		NEWLINE 
		DONE

_	_	NOUN2 PIE 
		WRITE "No tienes el calzado adecuado para esa acción tan radical."
		NEWLINE 
		DONE

_	_	NOUN2 TABLON 
		CARRIED 5  ; sust(obj_OBJTABLON/5)
		PLACE 8 19  ; sust(obj_OBJBLOQUEHIELO/8)
		WRITE "Tomas impulso y obsequias a uno de los bloques de hielo protuberantes con un tremendo golpe de tablón. El hielo se desquebraja y un bloque cae con un estruendo replicado por toda la estancia."
		NEWLINE 
		DONE

_	_	NOUN2 TABLON 
		WRITE "No tienes nada de eso."
		NEWLINE 
		DONE

_	_	NOUN2 PEDERNAL 
		CARRIED 0  ; sust(obj_OBJPEDERNAL/0)
		WRITE "El sílex no es suficientemente duro para eso."
		NEWLINE 
		DONE

_	_	NOUN2 PEDERNAL 
		WRITE "No tienes nada de eso."
		NEWLINE 
		DONE

_	_	WRITE "No lo veo como una opción para romper el hielo."
		NEWLINE 
		DONE

/PRO 83 ; sust(proc_SACARSUB/83)

_	_	WHATO

_	_	PROCESS 61  ; sust(proc_PRESENT/61)
		ZERO 69  ; sust(flag_FLAGRET/69)
		PROCESS 62  ; sust(proc_PRESENTSECOND/62)
		ZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 122 
		NEWTEXT 
		DONE

_	_	PROCESS 54  ; sust(proc_LOCSECOND/54)
		NOTSAME 38 69  ; sust(flag_FLAGRET/69)
		NOTEQ 69 254  ; sust(flag_FLAGRET/69)
		NOTEQ 69 253  ; sust(flag_FLAGRET/69)
		SYSMESS 133 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 33  ; sust(proc_ESCONTAINERSECOND/33)
		ZERO 69  ; sust(flag_FLAGRET/69)
		LT 44 50 
		PROCESS 68  ; sust(proc_PRINTSECOND/68)
		SYSMESS 143 
		NEWTEXT 
		DONE

_	_	ZERO 69  ; sust(flag_FLAGRET/69)
		SYSMESS 99 
		SYSMESS 118 
		SYSMESS 121 
		WRITE "de " 
		PROCESS 70  ; sust(proc_PRINTSECONDARTUNDET/70)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		NOTZERO 17  ; sust(flag_PROP_ABRIBLE/17)
		ZERO 16  ; sust(flag_PROP_ABIERTO/16)
		SYSMESS 99 
		SYSMESS 118 
		SYSMESS 121 
		PROCESS 71  ; sust(proc_PRINTSECONDDEL/71)
		SYSMESS 120 
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 116 
		PROCESS 87  ; sust(proc_TERMGENERO/87)
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		WRITE ".\n" 
		DONE

_	_	PROCESS 20  ; sust(proc_CODESECOND/20)
		NOTSAME 54 69  ; sust(flag_FLAGRET/69)
		LT 44 50 
		PROCESS 68  ; sust(proc_PRINTSECOND/68)
		SYSMESS 143 
		NEWTEXT 
		DONE

_	_	NOTSAME 54 69  ; sust(flag_FLAGRET/69)
		SYSMESS 52 
		PROCESS 69  ; sust(proc_PRINTSECONDARTDET/69)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	LT 44 50 
		SYSMESS 99 
		SYSMESS 107 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		SYSMESS 125 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		PROCESS 41  ; sust(proc_GUARDAFIRST/41)
		COPYFF 44 34 
		COPYFF 45 35 
		WHATO 
		WRITE "_.\n" 
		PROCESS 77  ; sust(proc_RESTAURAFIRST/77)
		NEWTEXT 
		DONE

_	_	PROCESS 38  ; sust(proc_EXTPROPIEDAD/38)
		NOTZERO 20  ; sust(flag_PROP_ESTATICO/20)
		SYSMESS 99 
		SYSMESS 118 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		SYSMESS 130 
		PROCESS 88  ; sust(proc_TERMGENEROVERBO/88)
		SYSMESS 131 
		PROCESS 71  ; sust(proc_PRINTSECONDDEL/71)
		WRITE ".\n" 
		NEWTEXT 
		DONE

_	_	PUTO 254 
		SYSMESS 137 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE " " 
		PROCESS 71  ; sust(proc_PRINTSECONDDEL/71)
		WRITE ".\n" 
		DONE

/PRO 84 ; sust(proc_SALIDAS/84)

_	_	DESTROY 37  ; sust(obj_OBJ_NORTE/37)
		DESTROY 38  ; sust(obj_OBJ_SUR/38)
		DESTROY 39  ; sust(obj_OBJ_ESTE/39)
		DESTROY 38  ; sust(obj_OBJ_SUR/38)
		DESTROY 40  ; sust(obj_OBJ_OESTE/40)
		DESTROY 41  ; sust(obj_OBJ_NORDESTE/41)
		DESTROY 42  ; sust(obj_OBJ_NOROESTE/42)
		DESTROY 43  ; sust(obj_OBJ_SURESTE/43)
		DESTROY 44  ; sust(obj_OBJ_SUROESTE/44)
		DESTROY 45  ; sust(obj_OBJ_ARRIBA/45)
		DESTROY 46  ; sust(obj_OBJ_ABAJO/46)
		DESTROY 47  ; sust(obj_OBJ_ENTRAR/47)
		DESTROY 48 ; sust(obj_OBJ_SALIR/48)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 13 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 37 33 ; sust(obj_OBJ_NORTE/37)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 2 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 38 33 ; sust(obj_OBJ_SUR/38)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 3 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 39 33 ; sust(obj_OBJ_ESTE/39)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 4 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 40 33 ; sust(obj_OBJ_OESTE/40)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 5 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 41 33 ; sust(obj_OBJ_NORDESTE/41)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 7 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 43 33 ; sust(obj_OBJ_SURESTE/43)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 6 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 42 33 ; sust(obj_OBJ_NOROESTE/42)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 8 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 44 33 ; sust(obj_OBJ_SUROESTE/44)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 11 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 47 33 ; sust(obj_OBJ_ENTRAR/47)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 12 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 48 33 ; sust(obj_OBJ_SALIR/48)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 9 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 45 33 ; sust(obj_OBJ_ARRIBA/45)

_	_	COPYFF 38 104  ; sust(flag_TMPFLAG1/104)
		LET 33 10 
		MOVE 104  ; sust(flag_TMPFLAG1/104)
		NOTSAME 38 104  ; sust(flag_TMPFLAG1/104)
		PLACE 46 33 ; sust(obj_OBJ_ABAJO/46)

_	_	AT 8  ; sust(loc_MESETA/8)
		PLACE 46 33  ; sust(obj_OBJ_ABAJO/46)
		PLACE 44 33 ; sust(obj_OBJ_SUROESTE/44)

_	_	AT 11  ; sust(loc_BOSQUEDENSO/11)
		PLACE 45 33  ; sust(obj_OBJ_ARRIBA/45)
		PLACE 40 33 ; sust(obj_OBJ_OESTE/40)

_	_	AT 12  ; sust(loc_FOSA/12)
		NOTZERO 85  ; sust(flag_LIANAATADA/85)
		PLACE 46 33  ; sust(obj_OBJ_ABAJO/46)
		PLACE 47 33 ; sust(obj_OBJ_ENTRAR/47)

_	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		NOTZERO 102  ; sust(flag_TABLONENRIACHUELO/102)
		PLACE 40 33 ; sust(obj_OBJ_OESTE/40)

_	_	AT 14  ; sust(loc_JUNTOLAGO/14)
		WORN 6  ; sust(obj_OBJBOTAS/6)
		PLACE 38 33 ; sust(obj_OBJ_SUR/38)

_	_	AT 18  ; sust(loc_CAVIDADHUMEDA/18)
		PLACE 37 33 ; sust(obj_OBJ_NORTE/37)

_	_	AT 19  ; sust(loc_GRUTAGLACIAR/19)
		PLACE 39 33 ; sust(obj_OBJ_ESTE/39)

_	_	AT 26  ; sust(loc_VERTEDERO/26)
		PLACE 46 33  ; sust(obj_OBJ_ABAJO/46)
		PLACE 48 33 ; sust(obj_OBJ_SALIR/48)

_	_	AT 27  ; sust(loc_PASAJEACOLCHADO/27)
		NOTZERO 26  ; sust(flag_CAMPOFUERZADESTRUIDO/26)
		PLACE 38 33 ; sust(obj_OBJ_SUR/38)

_	_	AT 28  ; sust(loc_HANGAR/28)
		PLACE 45 33 ; sust(obj_OBJ_ARRIBA/45)

_	_	WRITE "Salidas visibles: " 
		LISTAT 33 
		DONE

/PRO 85 ; sust(proc_SALTAREJILLA/85)

_	_	NOTZERO 100  ; sust(flag_REJILLASINBARROTE/100)
		WRITE "Ya has tenido una experiencia con ese tema, no necesitas más."
		NEWLINE 
		DONE

_	_	WRITE "Das un enorme "

_	_	NOTEQ 33 24 
		WRITE "patadón"

_	_	EQ 33 24 
		WRITE "salto"

_	_	WRITE " ejerciendo todo tu peso sobre la rejilla. Uno de los barrotes cede al embate y caéis al vacío. "
		NEWLINE 
		SET 100  ; sust(flag_REJILLASINBARROTE/100)
		PLACE 24 25  ; sust(obj_OBJBARROTEHIERRO/24)
		GOTO 25 ; sust(loc_ESTRECHOCUBICULO/25)

_	_	CARRIED 12  ; sust(obj_OBJRECIPIENTEGUSANO/12)
		WRITE "Con el recipiente en tus manos, la caida es muy aparatosa y el líquido se desparrama por todas partes. Cuando llegas al fondo no eres más que una pulpa gelatinosa."
		NEWLINE 
		SCORE 
		TURNS 
		END

_	_	ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		PLUS 30 3  ; sust(flag_PUNTUACION/30)
		WRITE "Por suerte, el bicho para tu caida como si de un colchón se tratara."
		NEWLINE 
		DESC

_	_	WRITE "La caída es demasiado dura para sobrevivir."
		NEWLINE 
		SCORE 
		TURNS 
		END

/PRO 86 ; sust(proc_SUELTALIANAPROC/86)

_	_	ZERO 93  ; sust(flag_PUNTUADAFOSA/93)
		SET 93  ; sust(flag_PUNTUADAFOSA/93)
		PLUS 30 2 ; sust(flag_PUNTUACION/30)

_	_	EQ 65 3  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "…"
		NEWLINE 
		GOTO 19  ; sust(loc_GRUTAGLACIAR/19)
		DESC

_	_	EQ 65 2  ; sust(flag_ESTADOSUSPENSION/65)
		WRITE "…"
		NEWLINE 
		GOTO 3  ; sust(loc_CAVERNAVERDE/3)
		DESC

_	_	WRITE "medio de los trozos de hielo afilados, precipitándote terraplén abajo. Durante el descenso, las heladas cuchillas literalmente te despedazan.\n\n      \*\*\* HAS PERDIDO \*\*\*\n"
		NEWLINE 
		SCORE 
		TURNS 
		END

/PRO 87 ; sust(proc_TERMGENERO/87)

_	_	PROCESS 38 ; sust(proc_EXTPROPIEDAD/38)

_	_	EQ 21 1  ; sust(flag_PROP_GENNUM/21)
		WRITE "o" 
		DONE

_	_	EQ 21 2  ; sust(flag_PROP_GENNUM/21)
		WRITE "a" 
		DONE

_	_	EQ 21 3  ; sust(flag_PROP_GENNUM/21)
		WRITE "os" 
		DONE

_	_	EQ 21 4  ; sust(flag_PROP_GENNUM/21)
		WRITE "as" 
		DONE

/PRO 88 ; sust(proc_TERMGENEROVERBO/88)

_	_	PROCESS 38 ; sust(proc_EXTPROPIEDAD/38)

_	_	GT 21 2  ; sust(flag_PROP_GENNUM/21)
		WRITE "n" 
		DONE

/PRO 89 ; sust(proc_VACIARBASUREROLOOP/89)

_	_	ISAT 11 25  ; sust(obj_OBJGUSANO/11)
		AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "El gusano cede bajo tu peso y cae al vacío."
		NEWLINE 
		PLACE 11 26 ; sust(obj_OBJGUSANO/11)

_	_	ISNOTAT 11 25  ; sust(obj_OBJGUSANO/11)
		AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "Caes al vacío."
		NEWLINE

_	_	DOALL 25

_	_	WHATO 
		NOTEQ 51 11 
		PUTO 26 
		AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "Ves caer " 
		PROCESS 64  ; sust(proc_PRINTFIRSTARTDET/64)
		WRITE ".\n"

_	_	DONE

/PRO 90 ; sust(proc_VACIARBASUREROPROC/90)

_	_	AT 24  ; sust(loc_SALADEBASURAS/24)
		WRITE "A través de la rejilla oyes el inconfundible rumor de maquinaria funcionando. Luego el silencio."
		NEWLINE

_	_	AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "De repente, el suelo de la sala comienza a abrirse."
		NEWLINE

_	_	PROCESS 89 ; sust(proc_VACIARBASUREROLOOP/89)

_	_	AT 25  ; sust(loc_ESTRECHOCUBICULO/25)
		WRITE "Se vuelve a cerrar el suelo. Durante un largo rato notas que te transportan en algún tipo de medio de locomoción. Finalmente eres arrojado a…"
		NEWLINE 
		GOTO 26  ; sust(loc_VERTEDERO/26)
		DESC
